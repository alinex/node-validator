# Alinex Validator

![validator icon](https://assets.gitlab-static.net/uploads/-/system/project/avatar/11623256/validator-icon.png){: .right .icon}

It is the **most powerful** and complete **data validation** library based on a schema definition which can validate complex data structures in depth and also **sanitize and transform** them. To complete it you can also **load** such **data from different sources** like files or web services in nearly **every data format**. Using the included CLI process you may also check, preprocess and transform data structures before they are used in any program.

Basically the alinex-validator is an extension to the [alinex-datastore](https://alinex.gitlab.io/node-datastore) with addition of checking and optimizing the value structures. As such it nearly has the same API with some additional methods.

Big Features:

-   read, write and transform data structures
-   different protocols, compression and formats are supported
-   with simple and complex data types in schema
-   supporting references in checks
-   import and export of schema definitions
-   multilingual and detailed descriptions or errors

This allows to:

-   read configuration data and check that they are valid
-   give the user a data structure help text
-   call REST services and check for valid result
-   check any external data to prevent data injection

In contrast to [JSON Schema](https://json-schema.org/understanding-json-schema/reference/index.html) and [hapi joi](https://hapi.dev/family/joi/?v=16.1.7) a lot more possibilities are build into it.

## Modules

For such a big set of possibilities a lot of modules are needed. But keep cool, this library will install them all but only load the ones really used.

## Chapters

Read all about the [alinex-validator](https://alinex.gitlab.io/node-validator) in the chapters (top navigation menu):

-   [Usage](usage) - How to Guide
-   [Schema](schema) - Detailed API for defining validation schemas
-   [Architecture](dev) - Internal development basics if you want to extend this library

## Support

I don't give any paid support but you may create [GitLab Issues](https://gitlab.com/alinex/node-validator/-/issues):

-   Bug Reports or Feature: Please make sure to give as much information as possible. And explain how the system should behave in your opinion.
-   Code Fixing or Extending: If you help to develop this package I am thankful. Develop in a fork and make a merge request after done. I will have a look at it.
-   Should anybody be willing to join the core team feel free to ask, too.
-   Translations: If you are willing to translate into other languages we will find the easiest way for you to do the translation and include it. Also see [i18n documentation](dev/i18n.md).
-   Any other comment or discussion as far as it is on this package is also kindly accepted.

Please use for all of them the [GitLab Issues](https://gitlab.com/alinex/node-validator/-/issues) which only requires you to register with a free account.

{!docs/assets/stats-pdf-license.txt!}

{!docs/assets/abbreviations.txt!}
