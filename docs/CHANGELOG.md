# Last Changes

## Version 3.9.0 (21.05.2021)

-   upgrade to ES2019, Node >= 12

## Version 3.8.0 (26.02.2021)

-   allow to use it directly on already defined DataStore
-   package updates

-   Fix 3.8.1 (08.03.2021) - support i18n in error messages
-   Fix 3.8.2 (18.03.2021) - fixed binary number conversions and denyUndefined, removeUndefined with wildcard item will be ignored
-   Fix 3.8.3 (15.04.2021) - support homedir ~ in file validation

## Version 3.7.4 (04.12.2020)

-   fix description of empty object items
-   fix object size validation on undefined value
-   fix german translation
-   update some dependent packages

-   Update 3.7.5 (03.01.2021) - update packages and doc theme

## Version 3.7.1 (21.09.2020)

-   use exclusive instance of i18next to prevent problem with other modules using i18next
-   optimize descriptive text
-   NumberSchema: convert between decimalBin, decimalDec
-   NumberSchema: fixed validation of true which should not validated to 1

-   Update 3.7.2 (23.10.2020) - update packages and doc structure
-   Update 3.7.3 (29.10.2020) - add schema definition for DataSource

## Version 3.7.0 (13.09.2020)

Breaking change: An additional `lang` parameter is now needed in all methods which load data or describe the schema.

-   replace y18n with i18next
-   title/detail as function called using lang as parameter
-   lang parameter in calls
-   boolean: added value option
-   number: sanitize percent values

## Version 3.6.0 (04.02.2020)

Breaking changes:

-   array.items deprecated, -> use item['*'], item['3'], item['2-'], item['2-5'] = <schema>
-   object.keys deprecated, -> use item['*], item['xxx'], item['/a-z/'] = <schema>
-   internally use new item methods

Extensions:

-   boolean with allow/disallow

Fixes:

-   Bugfixes 3.6.1 (06.02.2020) - fixes in object mandatory with item and display of source in error message
-   Hotfix 3.6.2 (15.02.2020) - update package management + package patches + object.mandatory with regexp
-   Bugfix 3.6.3 (23.04.2020) - schema import to also support array.filter with schema
-   Hotfix 3.6.7 (10.07.2020) - fix array min/max to work also with 0 values
-   Bugfix 3.6.8 (13.07.2020) - object.mandatory now works with default settings in item
-   Bugfix 3.6.10 (12.08.2020) - fix multipleResolves in object.mandatory check
-   Bugfix 3.6.11 (18.08.2020) - more possibilities to get error message
-   Hotfix 3.6.12 (22.08.2020) - respect object item.* setting
-   Hotfix 3.6.15 (25.08.2020) - bug in array if using split and item schemas

## Version 3.5.0 (10.11.2019)

- list format: allow masked dot `\.` and bracket notation `[name]`
- import tree/list: allow, disallow without array possible
- array: allow, disallow with schema definitions

Bugfix 3.5.1 (13.11.2019) - fix simplification to work on deep schema

## Version 3.4.0 (11.09.2019)

Breaking changes:

- `importTree()` and `importList()` are both made asynchronous
- instead of '[]' as import wildcard index or key '\*' is used, now

Extensions:

- new schema type: `regexp`
- added validator schema for tree format definition

Bugfix 3.4.1 (16.09.2019) - Updated datastore

Bugfix 3.4.2 (01.10.2019) - Updated datastore

Bugfix 3.4.3 (08.10.2019) - added support for sub description (starting at specified path)

## Version 3.3.0 (28.08.2019)

Breaking changes in the schema builder:

THe methods `schemaFromTree()` and `schemaFromList()` are renamed to `importTree()` and `importList()` to get a convenient naming for the new export/import methods.
Also the key `TYPE` within the schema builder is renamed to `SCHEMA_TYPE` to don't collide with any data structure.

Extensions:

- updated DataStore
- added CLI corresponding to DataStore
- added `file` schema handler
- added `logic` schema handler
- added `range` format in `DateSchema` which returns seconds from now
- add predefined schema combinations as [meta schema](schema/meta.md)
- tree format and list format can now be exported, too
- allow recursion using `set()` after schema is initialized

New shortcut methods:

- `Validator.load()` to easily initialize and load data
- `Validator.url()` to easily initialize and load single sources in one step
- `Validator.data()` to initialize with preset data structure
- `Validator.datastore()` to initialize and load using preset DataStore

Bug Fixes:

- Parsing in `Date` was not using current timezone as default.
- Parameter in `schema.describe(depth)` was not respected.

## Version 3.2.0 (9.07.2019)

Extensions:

- added IPSchema, now also with country validation
- added DomainSchema, with country validation, too
- `schemaFromList` and `schemaFromTree` methods now also support ArraySchema in path
- added EmailSchema, with whitelist and graylist support
- added URLSchema

Hotfix 3.2.1 (17.07.2019)

A bug in blacklist loading broke whole package build.

## Version 3.1.0 (12.06.2019)

Breaking changes:

- `mandatoryKeys`, `forbiddenKeys` renamed to `mandatory`, `forbidden` in ObjectSchema
- moved schema classes to `@alinex/validator/schema` to include separately as builder

Extensions:

- `mandatory` now allows `true` value in ObjectSchema
- new DateTime Schema with parsing human text, relative times and local support
- allow ranges in `allow` and `disallow` of NumberSchema
- added PortSchema with support for named ports
- added simplified `schemaFromList` and `schemaFromTree` methods in schema builder

## Version 3.0.0 (2.06.2019)

This is a complete rework, as such not all changing steps are listed here, but the biggest differences.
If you want to upgrade you have to completely reintegrate and rewrite all Schema definitions.

- Complete rewrite using **TypeScript** this gives the user support in IDE while writing schema definitions.
- Starting at first with only the **basic schema types** but more will come soon. The definition is given in the constructor now instead by using multiple methods and the possibilities are also slightly changed.
- **References** are completely rewritten by using the DataStore and it's syntax, which is completely changed to the old one. But reference are supported only on some really needed positions at first.
- Added **i18n support** in describe and error messages. At first english and german are supported.

It is not complete but will also be used in production by myself.

## Version 2.1.0 (28.10.2016)

- Extended options

## Version 2.0.0 (27.09.2016)

- Rewrite using Flow for type checking
- Extended email validation support
- Adding reference support
- Asynchronous validation

## version 1.0.0 (16.06.2015)

- Original validator written in CoffeeScript

{!docs/assets/abbreviations.txt!}
