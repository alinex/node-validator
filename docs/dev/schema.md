# Schema

The schema definition is done using TypeScript or JavaScript by implementing a schema instance which may consists of different sub schema instances. These different schema classes not only hold the definition but also the rules to validate or describe the structure.

!!! info

    There are also [meta schemas](meta.md) which are another model building on these.
    Often you need them and no new schema bacause the most can be done with the current classes.

Possible extensions may be:

-   new schema classes (seldom)
-   new options for existing schema types
-   allowance of references within an option

## Inheritance

Beside the main schema classes some specific ones are build as subset of them with a simpler option set and some predefinitions. Or which extend the simple type in some way.

Then it is a good way to extend the schema class to make the changes.

But also consider using sub-checks.

## Sub checks

It is also possible to call another validation on parts of the data.
This makes it possible to to also combine multiple simple types to a more complex type. But if there is no other functional checking beside combining them, better use [meta schemas](meta.md).

To use sub checks start with:

1.  init a specific schema instance
2.  create a new Workbench for it
3.  call `schema.validate(workbench)`
4.  use the value or error from the workbench

## Data References

If data references are allowed the export method has to be changed to make this work:

```ts
export(): any {
    const def: any = super.export()
    if (def.SCHEMA_META) return def.SCHEMA_META;
    def.default = this.exportReference(def.default);
    return def;
}
```

## Schemas in Options

Schema classes which allow sub schema in their options need special attention on defining. They need a specific export method and as such a definition may contain circular references it is important to detect these on import and export.

First they need to have unique instance string names which are made using a counter and setting them in the constructor:

```ts
let ID_COUNT = 0;

export class ObjectSchema extends base.BaseSchema {
    _refID: string;
    _refNum = 0;

    constructor(opt: Options = {}) {
        super(opt);
        this._refID = `Object_${++ID_COUNT}`;
        ...
    }
}
```

!!! info

    You need to change the Title in the `this._refID` setting to the class name.

And they need a specific export method (here the example from the object class):

```ts
/**
 * Export schema as tree data structure.
 */
export(parent: any = {}): any {
    const def: any = super.export()
    if (def.SCHEMA_META) return def.SCHEMA_META;
    // check for circular references
    if (parent[this._refID]) {
        parent[this._refID]._refNum++;
        return {
            SCHEMA_TYPE: this.constructor.name.replace(/Schema$/, ''),
            SCHEMA_REF: this._refID
        };
    }
    const map: any = { ...parent };
    map[this._refID] = this;
    ...
    // exact export
    if (def.keys)
        def.keys.forEach((key: any) => {
            key.schema = key.schema.export(map);
        });
    if (this._refNum) def.SCHEMA_ID = this._refID;
    return def;
}
```

And at last the `describe()` method needs to respect the depth to restrict output and prevent endless loop in circular references.
Therefore the Describer method needs the depth parameter to pass it further on to the next describe call.

```ts
private keysDescriber(depth: number) {
    const opt = <Options>this.opt;
    const msg = ''
    ...
    msg += def.schema.describe(depth).replace(/\n/g, '\n  ');
    ...
    return msg;
}
```

{!docs/assets/abbreviations.txt!}
