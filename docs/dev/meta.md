# Meta Schema

A meta schema is a function used as builder. They are defined in the meta module as different functions like:

```ts
export function timeout(opt?: {
    default?: string;
    unit?: 's' | 'min' | 'h' | 'd' | 'week' | 'month' | 'year';
}): Schema {
    if (!opt) opt = {};
    const dateOpt: any = {
        parse: true,
        format: 'range'
    };
    if (opt.default) dateOpt.default = opt.default;
    const numberSchema = new NumberSchema({
        unit: { from: 's', to: opt.unit || 's' },
        min: 0,
        round: { precision: 0 }
    });
    return new LogicSchema({
        SCHEMA_META: { SCHEMA_TYPE: 'timeout', ...opt },
        detail: 'A timeout value which will define a time range as seconds or in natural language.',
        // combined using AND
        if: new DateTimeSchema(dateOpt),
        check: [new DateTimeSchema(dateOpt), numberSchema],
        else: numberSchema
    });
}
```

The `SCHEMA_META` value will store the original definition to be used on export. Also the `detail` property is often defined to give it a more specific as the base classes alone will create.

{!docs/assets/abbreviations.txt!}
