# Workbench

Internal a work structure is used to validate and work with the data.

-   schema
    -   options loaded into schema (how to overwrite or add)
-   original
    -   type - merge/reference
    -   source - uri
    -   sourcePath - which part to use (merge)
    -   destinationPath - where to put it (merge)
    -   data - structure
    -   importTime - last import date
-   update
    -   data - structure
        -   parent
        -   reference - to original
            -   original ref
            -   path
            -   data ref
-   active
    -   updateTime - set if done
    -   data - from update
    -   listener

Methods will get this to work at a specific position:

-   context

{!docs/assets/abbreviations.txt!}
