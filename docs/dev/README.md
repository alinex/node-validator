title: Overview

# Internal Development

!!! warning

    The following information is only needed to develop within this library.
    If you only want to use this module it is not neccessary to know this.

The development is based on the [alinex core rules](https://alinex.gitlab.io/node-core/standard/).

## Architecture

The main class is used to load, sanitize, validate and store the data structure.

Basic Idea:

-   schema classes define the transformations and resulting structure
-   meta schemas are functions using the schema classes to make higher level definitions possible
-   DataStore is used to load and hold the data and context
-   each schema can describe itself human readable and multi lingual
-   human readable and detailed error descriptions on mismatch with schema

## Extending

Mostly this will be one of:

-   [schema classes](schema.md)
-   [meta schema](meta.md)

{!docs/assets/abbreviations.txt!}
