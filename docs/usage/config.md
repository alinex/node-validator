# Config Parser

Another use case is to use the validator to extract app configuration settings in an easy to write file.

You have the following possibilities:

1. load values at startup using validator
2. create preparsed config before app start

In both cases you have the ability to use any config file type your user is familiar with. Good choices are:

-   INI - because it is the simplest one
-   YAML - easier for complex and deep structures

But any other can be used also.

## Loading Config

In this case you will load the configuration settings within your code using the validator as they are needed. Parsing, checking and transformation is done and all necessary modules are loaded.

```ts
import { Validator } from '@alinex/validator';
import schema from './schema'; // like defined above

const val = new Validator(schema);
val.load({ source: 'config.json' })
    .then(() => {
        // do something with the result
        console.log(val.get('server.port'));
    })
    .catch(e => {
        console.error(e);
    });
```

You may also support multiple formats by checking which one is found first:

```ts
import * as fs from 'fs';
import { promisify } from 'util';
import async from '@alinex/async';
import { Validator } from '@alinex/validator';
import schema from './schema'; // like defined above

// find the first existing file
const files = ['config.json', 'config.yaml', 'config.ini']
const exists = await async.filter(files, promisify(fs.exists))
if (!exists.length) throw new Error('No valid configutation file found!');

const val = new Validator(schema);
val.load({ source: exists[0] })
// val.load(
//     { source: '/etc/myapp/base.json' },
//     { source: '/etc/myapp/base.yaml', alternate: ['/etc/myapp/base.json'] },
//     { source: '/etc/myapp/base.ini', alternate: ['/etc/myapp/base.json','/etc/myapp/base.yaml'] },
//     { source: '/etc/myapp/email.yaml', base: 'email', depend: ['/etc/myapp/base.json','/etc/myapp/base.yaml'] },
//     { source: '~/.myapp/base.yaml', object: 'replace' }
//     { source: '~/.myapp/email.yaml', base: 'email', depend: '~/.myapp/base.yaml' },
// )
    .then(() => {
        // do something with the result
        console.log(val.get('server.port'));
    })
    .catch(e => {
        console.error(e);
    });
```

## Prebuild Config

This is an option which can be used everywhere and also in other languages because it will run independently to the real system before you start it.

1. call the validator which will read, check and transform the configuration from the source destination to the programs location:

    ```bash
    validator -i <source-file> -s <schema> -o <program-config>
    ```

2. Now you can run your program which will load the `<program-config>` file.

{!docs/assets/abbreviations.txt!}
