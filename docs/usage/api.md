title: API

# API Usage

## Initialization

If you want to load into the Validator it has to be initialized. Therefore an URL and maybe some options defining the concrete handling are needed.
But you can also set a list of data sources which will be merged together.

!!! definition

    ```ts
    type Schema =
        | AnySchema
        | BooleanSchema
        | StringSchema
        | NumberSchema
        | DateTimeSchema
        | ArraySchema
        | ObjectSchema
        | PortSchema
        | IPSchema
        | DomainSchema
        | EmailSchema
        | URLSchema;

    interface DataSource {
        // URL to load this structure
        source?: string;
        options?: Options;
        // alteratively directly give data
        data?: any;
        // set base path in final structure
        base?: string;
        // relations
        depend?: string[];
        alternate?: string[];
        // default combine methods
        object?:  'combine' | 'replace' | 'missing' | 'alternate'; // default: 'combine'
        array?: 'concat' | 'replace' | 'overwrite' | 'combine'; // default: 'concat'
        // overwrite per position
        pathObject?: {[key: string]: 'combine' | 'replace' | 'missing' | 'alternate' }
        pathArray?: {[key: string]: 'concat' | 'replace' | 'overwrite' | 'combine' }
        // general settings
        clone?: boolean;
        allowFlags?: boolean; // default: true
        map?: boolean; // default: false
    }

    interface Options {
        // security
        privateKey?: Buffer | string;
        passphrase?: string;
        // compression
        compression?: 'gzip' | 'brotli' | 'bzip2' | 'lzma' | 'tar' | 'tgz' |
                'tbz2' | 'tlz' | 'zip';
        compressionLevel?: number;
        // formatting
        format?: 'bson' | 'coffee' | 'cson' | 'csv' | 'ini' | 'js' | 'json' |
                'msgpack' | 'properties' | 'toml' | 'xml' | 'yaml';
        records?: boolean;
        module?: boolean;
        rootName?: string;
    }
    ```

    This can be used to initialize a new Validator:

    ```ts
    // initialize new store
    Validator.constructor(schema: Schema, ...input?: DataSource[]): Validator
    ```

Additionally four static methods allow to initialize and load a Validator which afterwards may directly be red.

!!! definition

    Create a new data store and load it directly:

    ```ts
    Validator.load(schema: Schema, lang?: string, ...input: DataSource[]): Promise<Validator>
    ```

    From single source this can be called with:

    - `source` URL to load from
    - `options` options for loading

    Or with a predefined data structure.

    ```ts
    Validator.url(schema: Schema, lang: string, source: string, options?: Options): Promise<Validator>
    ```

    Or with a predefined [DataStore](https://alinex.gitlab.io/node-datastore):

    ```ts
    Validator.validate(schema: Schema, lang: string | undefined, input: DataStore): Promise<Validator>
    ```

    And to setup with predefined data:

    ```ts
    Validator.data(schema: Schema, lang: string, source: any): Promise<Validator>
    ```

    And at last a ready set DataStore can also be used:

    ```ts
    Validator.datastore(schema: Schema, lang?: string, source: DataSource): Promise<Validator>
    ```

The first two attributes from DataSource are used for loading, the others are described in more detail in the [merge](https://alinex.gitlab.io/node-data/merge/) function.

You may also set or change the sources on each `load()` or `save()` method.

### Single source

The simplest use case is to use a source defined by an URI string with possible options to specify how to read them:

```ts
const val = new Validator(schema, { source: 'file:config.yml' });
const val = new Validator(schema, { source: 'http://my-site/config', options: { format: 'json' } });
```

All option settings are defined below.

For single sources you can also initialize and load a Validator in one step:

```ts
const val = Validator.url(schema, 'en', 'file:config.yml');
```

### Combine multiple sources

It is also possible to load the data structure from multiple sources. This allows to separate big configuration in multiple files or allow for base settings which can be overwritten with specific ones at another location.

Each DataStore can either be used as multi source loader or with single source load and save.

```ts
const val = new Validator(
    { source: '/etc/myapp/base.json' },
    { source: '/etc/myapp/base.yaml', alternate: ['/etc/myapp/base.json'] },
    { source: '/etc/myapp/base.ini', alternate: ['/etc/myapp/base.json','/etc/myapp/base.yaml'] },
    { source: '/etc/myapp/email.yaml', base: 'email', depend: ['/etc/myapp/base.json','/etc/myapp/base.yaml'] },
    { source: '~/.myapp/base.yaml', object: 'replace' }
    { source: '~/.myapp/email.yaml', base: 'email', depend: '~/.myapp/base.yaml' },
);
```

This will load `/etc/myapp/base.json` (2) or if not found the yaml (3) or ini (4) format of it. The `/etc/myapp/email.yaml` (5) is only loaded if one of the base configs in the same directory is loaded.
If also a local config like `~/.myapp/base.yaml` (7) is found it will replace the global config and also has an dependent `email.yml` (8).

### Preset Data Structure

If you want to mock it and don't load anything but directly create a DataStore with a result data structure this is possible using:

```ts
const val = new Validator(schema);
val.load('en', { name: 'preset data structure' });
```

The same is possible in one call using:

```ts
const val = Validator.data(schema, 'en', [1, 2, 3]);
```

But you can also initialize and load it:

```ts
const val = new Validator(schema, { data: { name: 'preset data structure' } });
await val.load(); // using default langauge
```

You can also add a source URL to one of this examples later and store it, if you want.

But really useful is this in combination with a list of DataSources. This allows to use default values or overwrite specific elements with code calculated values.

```ts
const val = new Validator(
    schema,
    { source: 'file:config.yml' },
    { data: { vehicle: 'car', model: 'BMW' } }
);
```

If you try to load a Validator, which has no real sources defined, it will throw an error.

## Options

This can be done directly as parameters to the `load()` or `save()` methods or directly as described here. The following options may also be specified.

-   `privateKey: Buffer | string` - used for SFTP protocol
-   `passphrase: string` - used for SFTP protocol
-   `compression: string` - compression method to be used (default is detected by file extension)
-   `compressionLevel: number` - gzip compression level 0 (no compression) to 9 (best compression, default)
-   `format: string` - set a specific format handler to be used (default is detected by file extension)
-   `module: boolean` - use module format in storing as JavaScript, so you can load it using normal `require` or `import`
-   `rootName: string` - define the root element name in formatting as XML
-   `records: boolean` - flag to read the CSV or table data always as with records (only needed with less than 3 columns)

To set the options on the DataStore directly use:

=== "TypeScript"

    ```ts
    import Validator from '@alinex/validator';

    async function config() {
        const schema = await import('my-schema.js');
        const val = new Validator(schema, {
            source: 'file:/etc/my-app.json',
            options: { format: 'json' }
        });
        await val.load();
        // go on using the results in val.get()
    }
    ```

=== "JavaScript"

    ```js
    const Validator = require('@alinex/validator').default;

    async function config() {
        const schema = await import('my-schema.js');
        const val = new Validator(schema, {
            source: 'file:/etc/my-app.json',
            options: { format: 'json' }
        });
        await val.load();
        // go on using the results in val.get()
    }
    ```

## Load

Load a data structure from the defined source URL.

!!! definition

    ```ts
    <Validator>.load(lang?: string, ...input: DataSource[]): Promise<any>
    ```

As described above you may load a data source if it is already defined using:

```js
const val = new Validator(schema, { source: 'file:/etc/my-app.json' });
await val.load();
console.log(val.get());
```

If no DataSource was given on initialization or if you want to change it, this may be given as second argument, too.

Calling load without parameters will only load the Validator if not already done. So you may call it also if you don't know if it is already loaded and directly use the returning data structure. To load again use the `reload()` method below.

But you can also set them as needed in the call to `load`:

```js
const val = new Validator();
await val.load(schema, undefined, { source: 'http://my-server.de/config', options: { format: 'json' } });
console.log(val.get());
```

The above example needs to define the format in the options, because it is not set as file extension in the URL.

!!! attention

    **It is not possible to use the same schema for multiple validations at the same time!**
    That's because the to be validated data is running through the schema classes while processing.

    So if it may occur you have to clone the schema for each run.

    ```js
    import { clone } from '@alinex/data'

    const val = new Validator();
    await val.load(clone(schema), undefined, { source: 'http://my-server.de/config', options: { format: 'json' } });
    console.log(val.get());
    ```

## Validate

If you already have a defined or loaded [DataStore](https://alinex.gitlab.io/node-datastore) you can directly do the validation without the previous load call. The DataStore only needs to be defined, if it isn't loaded this will be done, too.

!!! definition

    ```ts
    <Validator>.validate(lang?: string, input?: DataStore): Promise<any>
    ```

If no DataStore was given it will use the one already set using some initialization methods and simply to the validation again. It will neither reload but only validate.

## Save

Store the current data structure to the defined source URL.

!!! definition

    ```ts
    <Validator>.save(output?: DataSource): Promise<boolean>
    ```

If you want to transfer to another format or you changed something you may save the data structure again. If it should go in another URI you may give it directly here or set it before.

```js
await val.save();
console.log(val.get());
```

Like for loading you can also give a source and options here.

## Reload

Reload the current data structure if it has been changed on disk.

!!! definition

    ```ts
    <Validator>.reload(time?: number, lang?: string): Promise<boolean>
    ```

If you want to reload the Validator later, you can easily call the `val.reload()` method. This will reload if the file was changed (returning true) or not.
If a `time` (number of seconds) setting is given, the reload will not take place within this range.

There are no parameters here, because it is meant to update only. But you can also call `load()` a second time to load it again - but this will always load, also if the source wasn't changed.

## Error

If something went wrong while loading, the error can be displayed differently, displayed on a simple example:

-   `error.message` - is the problem in short

        Ein numerischer Wert wird benötigt.

-   `error.toString(lang)` - will also show there the error occured

        Error at name: Ein numerischer Wert wird benötigt.

-   `util.inspect(error)` - adds the value, which was checked

        Error at name: Ein numerischer Wert wird benötigt.
        Found value: 'Local User'

-   `error.describe(lang)` - will show the description of the schema, which can additionally to one of the above used

        Defined as name
            The detail description.
            Ein nummerischer Wert.

## Access

As already said the `get()` and `has` methods are the same as in the [DataStore](https://alinex.gitlab.io/node-datastore/api/access/) using the [filtering API](https://alinex.gitlab.io/node-data/filter/) for its argument.

!!! definition

    ```ts
    <Validator>.has(filter?: string): boolean
    <Validator>.get(filter?: string): any
    ```

Some example uses are:

```ts
val.get('contact.name');
val.get('staff.[].name.$trim');
```

{!docs/assets/abbreviations.txt!}
