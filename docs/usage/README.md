title: Overview

# Module Usage

As the Alinex Validator is an extension to the [DataStore](https://alinex.gitlab.io/node-datastore) its API is similar but with addition of checking and optimizing the value structures.

At first the module should be included and loaded.

```bash
npm install @alinex/validator --save
```

## Create Schema

A schema is needed, which defines the correct structure of the data object. Therefore you need to define the Validator based on such a schema definition.

=== "TypeScript"

    ```ts
    import * as Builder from '@alinex/validator/lib/schema';

    export default const schema = Builder.schemaFromList({
        'status.message': { TYPE: 'String', allow: ['OK'] }
    });
    ```

=== "Common JS"

    ```js
    const Builder = require('@alinex/validator/lib/schema');

    const schema = Builder.schemaFromList({
        'status.message': { TYPE: 'String', allow: ['OK'] }
    });

    module.exports = {
        default: schema
    };
    ```

## Validation

With such a schema the validation can be run on a data set from any location:

=== "TypeScript"

    ```ts
    import { Validator } from '@alinex/validator';
    import schema from './schema'; // like defined above

    const val = new Validator(schema);
    val.load({ source: 'data.json' })
        .then(() => {
            // do something with the result
            console.log(val.get('server.port'));
        })
        .catch(e => {
            console.error(e);
        });
    ```

The validation will take place within the `load()` method call.

As the Validator is using the [DataStore](https://alinex.gitlab.io/node-datastore/api/access) and all of the access methods are possible here, too.

{!docs/assets/abbreviations.txt!}
