# Schema

The schema, which is needed to initialize the Validator can be gathered in multiple ways:

1. Directly define it (optimal using TypeScript) using the classes or Builder.
2. Using export/import with the tree format.
3. Also the additional and more simple list format can be used for import/export,

For all these have a look at the examples below.

## Schema Builder

To create a schema definition the load the complete schema classes as Builder like below. But you may also load only the needed schema types.

```ts
import * as Builder from '@alinex/validator/lib/schema';

const schema = new Builder.ObjectSchema({
    item: {
        'status':
            schema: new Builder.ObjectSchema({
                item: {
                    'message': new Builder.StringSchema({ allow: ['OK'] })
                },
                mandatory: true
            })
    },
    mandatory: true
});
```

The real schema definition is build by combining different Schema Types together. The ObjectSchema and ArraySchema are the main classes to build structures.

The above is a first and simple example. The schema types can be much more complex. Then it is preferable to combine it using different variables. This also allows to reuse objects multiple times and so reduces memory usage.

```ts
import * as Builder from '@alinex/validator/lib/schema';

const type_boolean = new Builder.BooleanSchema({ tolerant: true });
const type_string = new Builder.StringSchema();
const type_integer_pos = new Builder.NumberSchema({ integer: { unsigned: true }, sanitize: true });
const type_bytes = new Builder.NumberSchema({
    unit: { from: 'B', to: 'B' },
    integer: { unsigned: true },
    sanitize: true
});

const schema = new Builder.ObjectSchema({
    item: {
        title: type_string,
        name: type_string,
        address: type_string,
        age: type_integer_pos,
        disabled: type_boolean,
        mboxSize: type_bytes,
        spamFilter: type_boolean
    },
    mandatory: true
});
```

!!! Tip

    To have a smaller memory print you may also directly load only the needed schema classes directly using:

    ```ts
    import { StringSchema } from '@alinex/validator/lib/type/StringSchema';
    import Meta from '@alinex/validator/lib/Meta';
    ```

    This should be used if only a few schema types are needed.

See the [schema](../schema) documentation for a complete list of definitions for each schema type.

### Circular Reference

Sometimes you will have a recursion in your definition if a schema may come in any depth. This is not possible in the constructor, so you have to use the set method to change the options after creating an empty schema instance:

```ts
const all = new Builder.LogicSchema(); // will be set later because of recursion
const string = new Builder.StringSchema();
const object = new Builder.ObjectSchema({ item: { '*': all } });
// now set the recursion to object which already using all
def_all.set({
    operator: 'or',
    check: [string, object]
});
```

## Tree Format

This format is used to export and import the schema definition into a data structure.

To make export or import from a data structure (without instances):

-   It uses the special `SCHEMA_TYPE` property instead of making a class instances.
-   But a mixed use with class instances are also possible.
-   Simple objects which are used as parent to the defined schema types will be interpreted as objects with all keys mandatory.
-   Objects with only numerical keys are treated as array schema definition.
-   Objects with a key named '\*' will be a definition for all elements in an array.

Take a look at the below examples and everything will be clear. You may also use the below examples to directly write your definitions. But keep in mind that the direct format is the fastest to create the schema.

The value for the `SCHEMA_TYPE` can be defined in any case and consist of the first part from the schema class, without the 'Schema' at the end. By default real schema classes are starting with upper case letter, meta classes with lower case.

Another specific value will be `SCHEMA_ID` and `SCHEMA_REF` which are used for circular references. The id will be set in the upper element which can be used again in a sub element by setting only the `SCHEMA_TYPE` and `SCHEMA_REF`. Id and ref values have to match.

Simplifications:

-   allow/disallow can also be given as direct value instead of array if only one element is needed

### Import Tree

At first have a look at how to use a tree format definition:

```ts
import { importTree } from '@alinex/validator/lib/schema';

const definition = {
    status: {
        message: {
            SCHEMA_TYPE: 'String',
            allow: ['OK', 'true']
        }
    }
};
const schema = async importTree(definition);
```

As mixed structure with instances this may look like:

```ts
import { importTree, StringSchema } as Schema from '@alinex/validator/lib/schema';

const definition = {
    status: {
        message: new StringSchema({ allow: ['OK', 'true'] })
    }
};
const schema = async importTree(definition);
```

### Export Tree

The export will always use the full tree format without any instances.

```ts
import { exportTree } from '@alinex/validator/schema';

const schema = ...

// using the schema class
const definition = exportTree(schema);

// alternative direct call
const definition = schema.export();
```

### Tree Examples

For all examples the code shows the direct definition compared to the tree format.

A simple BooleanSchema:

```ts
const base = new Schema.BooleanSchema();
const definition = { SCHEMA_TYPE: 'Boolean' };
```

A StringSchema with an option:

```ts
const schema = new Schema.StringSchema({ trim: true });
const definition = { SCHEMA_TYPE: 'String', trim: true };
```

Simplified allow/disallow option:

```ts
const schema = new Schema.StringSchema({ allow: ['a'] });
const definition = { SCHEMA_TYPE: 'String', allow: 'a' };
```

ObjectSchema with two keys set:

```ts
const schema = new Schema.ObjectSchema({
    item: {
        flag: new Schema.BooleanSchema(),
        '/a*/': new Schema.StringSchema()
    }
});
const definition = {
    item: {
        flag: { SCHEMA_TYPE: 'Boolean' },
        '/a*/': { SCHEMA_TYPE: 'String' }
    },
    SCHEMA_TYPE: 'Object'
};
```

ArraySchema using StringSchema for all items:

```ts
const schema = new Schema.ArraySchema({
    item: { '*': new Schema.StringSchema() }
});
const definition = { '*': { SCHEMA_TYPE: 'String' } };
```

Using meta schema classes:

```ts
const schema = Schema.Meta.timeout({ default: '1200' });
const definition = { SCHEMA_TYPE: 'timeout', default: '1200' };
```

ObjectSchema which can be shortened:

```ts
const schema = new Schema.ObjectSchema({
    item: { message: new Schema.StringSchema() },
    mandatory: true
});
const definition = { message: { SCHEMA_TYPE: 'String' } };
```

And the same ObjectSchema if it is one level deeper:

```ts
const schema = new Schema.ObjectSchema({
    item: {
        status: new Schema.ObjectSchema({
            item: { message: new Schema.StringSchema() },
            mandatory: true
        })
    },
    mandatory: true
});
const definition = { status: { message: { SCHEMA_TYPE: 'String' } } };
```

And at last an example using ArraySchema simplification:

```ts
const schema = new Schema.ArraySchema({
    item: { '0': new Schema.StringSchema() }
});
const definition = { 0: { SCHEMA_TYPE: 'String' } };
```

## List Format

And to make it another step simpler as the tree format use the list. Here multiple objects and arrays without specific definition are combined together as path names.

### Import List

It works like the tree format, but needs one step more at import and export.

```ts
import { importList } from '@alinex/validator/schema';

const definition = {
    'status.message': {
        SCHEMA_TYPE: 'StringSchema',
        allow: ['OK']
    }
};
const schema = async importList(definition);
```

And if you want, you can use a mixed definition with instances here, too.

```ts
import { importList, StringSchema } from '@alinex/validator/schema';

const definition = {
    'status.message': new StringSchema({ allow: ['OK'] })
};
const schema = async importList(definition);
```

And at last an example with circular references, where a logic may contain an object which will have the same logic again:

```ts
import { importList } from '@alinex/validator/schema';

const definition = {
    SCHEMA_TYPE: 'Logic',
    SCHEMA_ID: 'Logic_1',
    operator: 'or',
    check: [
        { SCHEMA_TYPE: 'String' },
        {
            SCHEMA_TYPE: 'Object',
            item: { '*': { SCHEMA_TYPE: 'Logic', SCHEMA_REF: 'Logic_1' } }
        }
    ]
};
```

### Export List

The list format can also be used for a loss free export/import:

```ts
import { exportList } from '@alinex/validator/schema';

const schema = ...
const definition = exportList(schema);
```

### List Examples

THis shows how the list format can make things more simple:

```ts
// tree format
const definition = {
    status: {
        message: { SCHEMA_TYPE: 'String' },
        code: { SCHEMA_TYPE: 'Number' }
    }
};
// list format
const definition = {
    'status.message': { SCHEMA_TYPE: 'String' },
    'status.code': { SCHEMA_TYPE: 'Number' }
};
```

## Human Description

Each schema can describe itself with a readable text at any schema element using the `describe()` method.

```ts
const schema = new ObjectSchema({
    item: { one: new StringSchema({ title: 'string' }) }
keep});
schema.describe();
```

The Result will be (i18n support):

    Ein Datenobjekt mit Wertepaaren. Die Elemente haben die folgenden Formate:
    -   'one': string:
        Ein Text Element.

By default the description will go 5 level deep into the structure, but you may set an other `depth` and also a `start` position as dotted path.

```ts
schema.describe(2, 'server.ssl');
```

This will describe from `server.ssl` two levels deep.

{!docs/assets/abbreviations.txt!}
