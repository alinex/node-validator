title: Examples

# Schema Definition Examples

The following examples should help you make your own schema definitions. All are shown in the different schema types, possible.

!!! Note

    The additional Formats [JSON Schema](https://json-schema.org/understanding-json-schema/reference/index.html) and [hapi joi](https://hapi.dev/family/joi/?v=16.1.7) are not 100% compatible but the core is identical so that most cases can be defined in all of them.

## Simple

### Anything

!!! Example

    === "Schema Builder"

        ```ts
        new AnySchema();
        ```

    === "Tree/List Format"

        ```js
        {
            SCHEMA_TYPE: 'Any';
        }
        ```

    === "JSON Schema"

        ```json
        { "$schema": "http://json-schema.org/schema#", "$id": "AnySchema" }
        // short syntax
        {}
        // alternative in draft 6
        true;
        ```

    === "hapi joi"

        ```js
        Joi.any();
        ```

This should allow the following data structures:

```ts
1;
'test';
```

### Specific types

!!! Example

    === "Schema Builder"

        ```ts
        new StringSchema();
        ```

    === "Tree/List Format"

        ```js
        {
            SCHEMA_TYPE: 'String';
        }
        ```

    === "JSON Schema"

        ```json
        {
            "$schema": "http://json-schema.org/schema#",
            "$id": "StringSchema",
            "type": "string"
        }
        ```

    === "hapi joi"

        ```js
        Joi.string().allow('');
        ```

This should allow the following data structures:

```ts
'';
'test';
```

{!docs/assets/abbreviations.txt!}
