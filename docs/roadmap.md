# Roadmap

**What's coming next?**

That's a big question on a freelancer project. The roadmap may change any time or progress may be stuck. But hopefully I will come further on in any way.

- [X] ~~*file ~/ replaced by local path*~~ [2021-04-15]

## Not Planned

Validator Extensions:

-   IP: range=true - allow also range definition as CIDR

Updates:

- chrono v2 if more languages are supported (currently only ja/en) with timezone problems.

Export/import:

-   JSON Schema export/import: https://json-schema.org/understanding-json-schema/structuring.html
    -   schema.exportJSON() exportTree to JSON
    -   schema.importJson() JSON to importTree
-   Hapi/Joy export/import: https://github.com/hapijs/joi#readme
-   export schema to config file: writeSchema() using exportList()
-   load config file as schema: loadSchema() autodetect format and use importList()

More references:

-   number: min, max, greater, less with reference
-   date: min, max, greater, less with reference
-   file reference test
-   data:inline with wait till validated
-   data:inline with relative path
-   port: free=true to check that port is not used on localhost

{!docs/assets/abbreviations.txt!}
