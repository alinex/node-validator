title: IP

# IPSchema

The value has to be an IP address.

The following options are possible:

-   Meta Data: [title](README.md#title), [detail](README.md#detail)
-   Sanitizing: [default](README.md#default), [map](ip.md#map)
-   Validating: [version](ip.md#version), [lookup](ip.md#lookup), [allow](ip.md#allow), [disallow](ip.md#disallow), [allowCountry](ip.md#allowcountry), [disallowCountry](ip.md#disallowcountry)
-   Sanitizing: [format](ip.md#format)
-   Revert: [raw](README.md#raw)

## Specific Options

### lookup

If this flag is set names can be given, too. They will be translated into an IP address using DNS lookup.

```js
const schema = new IPSchema({
    lookup: true
});
// 'localhost' => '127.0.0.1'
```

### version

Set a specific type of IP which is necessary:

-   4 allows only IPv4 addresses
-   6 allows only IPv6 addresses

```js
const schema = new IPSchema({
    version: 4
});
// '192.168.1.15' => OK
// 'FE80:0000:0000:0000:0202:B3FF:FE1E:8329' => fail
```

### map

If the wrong type of IP address is given. An IPv4 address can be mapped into an IPv6 address but the other way works only on these and not on all addresses.

```js
const schema = new IPSchema({
    version: 6,
    map: true
});
// '127.0.0.1' => '::ffff:7f00:1'
```

### allow

In principal this is identical to the `any` type but the matching is a bit complexer.

You can set a list of concrete addresses or IP ranges in the form of CIDR (the IP address and the significant bits behind e.g. ‘127.0.0.1/8’) or by the following named ranges:

-   unspecified
-   broadcast
-   multicast
-   linklocal
-   loopback
-   private
-   reserved
-   uniquelocal
-   ipv4mapped
-   rfc6145
-   rfc6052
-   6to4
-   teredo
-   special => all of the named ranges above

```js
const schema = new IPSchema({ allow: ['127.0.0.1'] });
// '127.0.0.1' => OK
// '192.168.10.1' => fail
const schema = new IPSchema({ allow: ['192.168.0.0/16'] });
// '127.0.0.1' => fail
// '192.168.10.1' => OK
const schema = new IPSchema({ allow: ['private'] });
// '127.0.0.1' => OK
// '192.168.10.1' => OK
```

Concrete addresses, ranges and named special ranges may also be mixed.
You may also use [references](reference.md) to get the allowed values.

```js
const schema = new IPSchema({
    allow: new Reference({ 'data:context', 'ips'})
});
```

And it can also be mixed together with concrete values:

```js
const schema = new AnySchema({
    allow: [
        'private',
        new Reference({ 'data:context', 'ips'})
    ]
});
```

### disallow

The `disallow` is like a blacklist defining all values or patterns that will fail.

```js
const schema = new IPSchema({ disallow: ['127.0.0.1'] });
// '127.0.0.1' => fail
// '192.168.10.1' => OK
const schema = new IPSchema({ disallow: ['192.168.0.0/16'] });
// '127.0.0.1' => OK
// '192.168.10.1' => fail
```

If `allow` and also `disallow` is set, the more specific (range with the least variable bits) has priority.

### allowCountry

It is also possible to restrict the IP to specific countries. This only works, if the country of the IP could be detected. All other IPs like private ones are always allowed.

To specify the allowed countries use the upper case two character ISO codes in a list.

```js
const schema = new IPSchema({ allowCountry: ['DE'] });
// '92.211.90.122' (DE) => OK
// '192.211.90.122' (US) => fail
// '127.0.0.1' => OK
```

The country codes can also gathered using a reference:

```js
const schema = new IPSchema({
    allowCountry: new Reference({ 'data:context', 'africa.countries'})
});
```

And it can also be mixed together with concrete values:

```js
const schema = new AnySchema({
    allowCountry: [
        'DE',
        new Reference({ 'data:context', 'africa.countries'})
    ]
});
```

### disallowCountry

The `disallowCountry` is like a blacklist defining all country codes which will fail.

```js
const schema = new IPSchema({ disallowCountry: ['DE'] });
// '92.211.90.122' (DE) => fail
// '192.211.90.122' (US) => OK
// '127.0.0.1' => OK
```

If `allowCountry` and also `disallowCountry` the disallow will have precedence.

### format

The IP address will be given in a short form by default but you may also use:

-   short - ffff:: (default)
-   long - ffff:0:0:0:0:0:0:0
-   array - [65535, 0, 0, 0, 0, 0, 0, 0]
-   bytes - [255, 255, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

```js
const schema = new IPSchema({
    format: 'long'
});
```

{!docs/assets/abbreviations.txt!}
