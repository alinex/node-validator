title: String

# StringSchema

This type defines a textual string value with lots of customization possibilities.

The following options are possible:

-   Meta Data: [title](README.md#title), [detail](README.md#detail)
-   Sanitizing: [default](README.md#default), [makeString](string.md#makestring), [trim](string.md#trim), [replace](string.md#replace), [upperCase](string.md#uppercase), [lowerCase](string.md#lowercase)
-   Possibly Sanitizing: [stripDisallowed](string.md#stripdisallowed) with [hex](string.md#hex) [alphaNum](string.md#alphanum) [noHTML](string.md#), [pad](string.md#pad) with [min](string.md#min), [truncate](string.md#truncate) with [max](string.md#max)
-   Validating: [allow](string.md#allow), [disallow](string.md#disallow)
-   Revert: [raw](README.md#raw)

## Specific Options

### makeString

If the source element is not already a string, it will be automatically converted to one using the `toString()` method.

```js
const schema = new StringSchema({
    makeString: true
});
// 5 => '5'
```

### trim

Whitespace will be trimmed off on one or both sides of the string, but not within.

```js
const schema = new StringSchema({
    trim: true
});
// '  my  string  ' => 'my  string'
```

This was on both side but it can also be done only on one side:

```js
const schema = new StringSchema({
    trim: 'left'
});
// '  my  string  ' => 'my  string  '
```

```js
const schema = new StringSchema({
    trim: 'right'
});
// '  my  string  ' => '  my  string'
```

### replace

This is a list of replacements like:

```js
const schema = new StringSchema({
    replace: [{ match: ' single sign on ', replace: ' SSO ', hint: 'use shortcut' }]
});
// 'use single sign on' => 'use SSO'
```

Each replacement rule consists at least of an `match` and `replace` value, while the `hint` is optional. The match can also be a `RegExp` or a `string`.

If the `match` is given as string, it will only replace the first occurrence. To substitute multiple use a regular expression with the `/g` modifier at the end.

### hex

This option will disallow different characters or parts.

Hexadecimal characters only (a-f, A-F and 0-9) are allowed:

```js
const schema = new StringSchema({
    hex: true
});
// 'ad5cff' => OK
// 'abcdefgh' => fail
```

### alphaNum

Only alpha numerical characters (a-z, A-Z, 0-9 and \_) are allowed:

```js
const schema = new StringSchema({
    alphaNum: true
});
// 'abcdefgh' => OK
// 'abc def' => fail
```

In combination with `stripDisallowed` these characters will automatically be removed.

### noHTML

HTML Tags within the string are not allowed. If a list of strings are given, this tag names are the only ones which are allowed.

```js
const schema = new StringSchema({
    noHTML: true
});
// 'This is <b>bold</b>' => fail
```

```js
const schema = new StringSchema({
    noHTML: ['b']
});
// 'This is <b>bold</b>' => OK
```

In combination with `stripDisallowed` the disallowed tags will automatically be removed.

### stripDisallowed

If this option is set the definitions above from `hex`, `alphaum` and `noHTML` will no longer fail but quietly remove the disallowed characters or parts.

```js
const schema = new StringSchema({
    hex: true,
    stripDisallowed: true
});
// 'ad5cff' => 'ad5cff'
// 'abcdefgh' => 'abcdef'
```

```js
const schema = new StringSchema({
    noHTML: ['b'],
    stripDisallowed: true
});
// '<p>This is <b>bold</b></p>' => 'This is <b>bold</b>'
```

### upperCase

The text may be changed as a complete element to upper or lower case of `all` text or `first` character.

-   `upperCase: true`
-   `upperCase: "first"`

See below for examples.

### lowerCase

The text may be changed as a complete element to upper or lower case of `all` text or `first` character.

-   `lowerCase: true`
-   `lowerCase: "first"`

See the following examples:

```js
const schema = new StringSchema({ lowerCase: true });
// 'eXaMpLe' => 'example'
const schema = new StringSchema({ upperCase: true });
// 'eXaMpLe' => 'EXAMPLE'
const schema = new StringSchema({ upperCase: 'first' });
// 'eXaMpLe' => 'EXaMpLe'
const schema = new StringSchema({ lowerCase: true, upperCase: 'first' });
// 'eXaMpLe' => 'Example'
```

### min

Define the minimum length of the string. It is not allowed to be shorter than `min`.

```js
const schema = new StringSchema({ min: 3 });
// '12' => fail
```

### max

Define the maximum length of the string. It is not allowed to be longer than `max`.

```js
const schema = new StringSchema({ max: 5 });
// '123456' => fail
```

Use both settings together to make a range or a specific exact length.

### pad

Instead of failing a too short text, it can be automatically lengthened. Therefore you have to set the side on which the padding occurs and the characters used to pad. The minimal length is used from `min` setting.

```js
const schema = new StringSchema({ min: 8, pad: { side: 'left', characters: ' ' } });
// '1234' => '    1234'
```

It is possible to give more characters to be used, if to less

-   the right one will be repeated (with side left)
-   the left one will be repeated (with side right)
-   for both sides, the first half is used on left side the second half of characters for right side.

### truncate

Also if the text is to long, it can be truncated, with a possible appendix. The maximum length is used from `max` setting.

```js
const schema = new StringSchema({ max: 5, truncate: true });
// '1234567' => '12345'
const schema = new StringSchema({ max: 5, truncate: '...' });
// '1234567' => '12...'
```

### allow

This gives a list of values or patterns which can be set. All values which are not matched will fail here.

```js
const schema = new StringSchema({ allow: ['car', 'bike'] });
// 'car' => OK
// 'boat' => fail
const schema = new StringSchema({ allow: [/\d./] });
// '3. point' => OK
// 'a. something' => fail
```

Values and patterns may also be mixed.
You may also use [references](reference.md) to get the allowed values.

```js
const schema = new StringSchema({
    allow: new Reference({ 'data:context', 'cities.europe'})
});
```

And it can also be mixed together with concrete values:

```js
const schema = new AnySchema({
    allow: [
        'New York',
        new Reference({ 'data:context', 'cities.europe'})
    ]
});
```

### disallow

The `disallow` is like a blacklist defining all values or patterns that will fail.

```js
const schema = new StringSchema({ disallow: ['car', 'bike'] });
// 'car' => fail
// 'boat' => OK
const schema = new StringSchema({ disallow: [/\d/] });
// 'three cars' => OK
// '3 cars' => fail
```

If `allow` and also `disallow` is set, the `disallow` has priority and values which are in both are disallowed. Reference are also possible like shown in [allow](#allow).

{!docs/assets/abbreviations.txt!}
