title: URL

# URLSchema

An URL representing an internet address.

The following options are possible:

-   Meta Data: [title](README.md#title), [detail](README.md#detail)
-   Sanitizing: [default](README.md#default), [resolve](url.md#resolve)
-   Validating: [allow](string.md#allow), [disallow](string.md#disallow), [allowIP](ip.md#allow), [disallowIP](ip.md#disallow), [allowCountry](ip.md#allowcountry), [disallowCountry](ip.md#disallowcountry), [exists](url.md#exists)
-   Sanitizing: [format](url.md#format)
-   Revert: [raw](README.md#raw)

## Specific Options

### resolve

If relative URLs are given, they will be resolved.

```js
const schema = new URLSchema({ resolve: 'http://alinex.de' });
// '/test' => 'http://alinex.de/test'
```

### exists

The URL has to exist and be accessible.

```js
const schema = new URLSchema({ exists: true });
// 'http://alinex.de' => OK
// 'http://alinex.de/not-existing' => fail
```

### format

Format to be returned:

-   `href` - the URL as a string like used in links (default)
-   `object` - an object with the parts of the URL

```js
const schema = new URLSchema({ format: 'object' });
// 'http.//alinex.de' =>
//      href: 'http://alinex.de/',
//      origin: 'http://alinex.de',
//      protocol: 'http:',
//      username: '',
//      password: '',
//      host: 'alinex.de',
//      hostname: 'alinex.de',
//      port: '',
//      pathname: '/',
//      search: '',
//      hash: ''
```

{!docs/assets/abbreviations.txt!}
