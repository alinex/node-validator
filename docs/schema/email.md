title: Email

# EmailSchema

An email address:

-   given as simple email like `info@alinex.de` as defined by RFC 5322
-   or using the commonly used display name format like `Alexander Schilling <info@alinex.de>`

The following options are possible:

-   Meta Data: [title](README.md#title), [detail](README.md#detail)
-   Sanitizing: [default](README.md#default), [normalize](email.md#normalize), [makeString](string.md#makestring)
-   Validating: [allow](string.md#allow), [disallow](string.md#disallow), [registered](email.md#registered), [blacklist](email.md#blacklist), [graylist](email.md#graylist), [connect](email.md#connect)
-   Sanitizing: [format](email.md#format)
-   Revert: [raw](README.md#raw)

## Specific Options

### normalize

If this flag is set the email address is normalized to it's real form. This allows to find duplicates.
The following rules are made depending on the email provider:

-   lowercase the domain part
-   lowercase the local part (wrong per RFC but no big mail provider works case-sensitive)
-   remove dots
-   remove tags staring with `+` or `-`
-   use username from subdomain `<tag>@<user>.fastmail.com`

The concrete rules used, are based on the mail domain or if detectable the mail provider.

```js
const schema = new EmailSchema({ normalize: true });
// 'Alexander.REINER.Schilling+INFO@googlemail.com' => 'alexanderreinerschilling@gmail.com'
```

If the local part is changed and no name is set the original local part will be used as name.

!!! warning

    If you find additional or wrong normalization rules, send me a message to update it.

### registered

If this flag is set the email domain needs an public MX record in the nameserver.

```js
const schema = new EmailSchema({ registered: true });
// 'alexander.reiner.schilling@googlemail.com' => OK
// 'info@my-non-existent-mail-domain.com' => fail
```

### blacklist

If this flag is set the email server is checked against blacklists. This will not check the email domain but the server names behind this email domain.

```js
const schema = new EmailSchema({ blacklist: true });
```

!!! warning

    Bacause every mailserver name behind your domain is checked against multiple lists this is a time consuming task which may take up to 10 seconds.

### graylist

If this flag is set the email server is checked against graylists. This will not check the email domain but the server names behind this email domain.

```js
const schema = new EmailSchema({ graylist: true });
```

!!! warning

    Bacause every mailserver name behind your domain is checked against multiple lists this is a time consuming task which may take up to 5 seconds.

### connect

This will make a connection to the server, checking if it is working. It should be possible to connect and send a `HELO` command.

```js
const schema = new EmailSchema({ connect: true });
```

!!! warning

    If the mailserver is not accessible from here, also only for a short time it will revoke the validation. To make it a bit more solid it will try 5 times with a sleep of a half second.

### format

The schema allows to return emails in three different formats:

-   `object` - will return an object with
    -   name - display name if given, else null
    -   address - the real email address
    -   local - the local part of the email address
    -   domain - the domain part of the email address
-   `displayName` - if a name is set it will return the display format
-   `address` - (default) only the real email address is returned

```js
const schema = new EmailSchema();
// 'Alexander Schilling <info@alinex.de>' => 'info@alinex.de'
```

```js
const schema = new EmailSchema({ fotmat: 'object' });
// 'Alexander Schilling <info@alinex.de>' => {
//   name: 'Alexander Schilling',
//   address: 'info@alinex.de',
//   local: 'info',
//   domain: 'alinex.de'
// }
```
