# References

References may be used at different positions to not specify a value directly but use the one set on any other position in the data structure or in any other data structure.

Such references are added using:

```ts
let ref = new Reference(source: string, filter: string);
```

It uses the [DataStore](https://alinex.gitlab.io/node-datastore) to access the data which enables all the different protocols and formats to be used.

Some special DataStore URLs only possible here are:

-   'data:inline' - to reference another position within the same data structure
-   'data:context' - to reference some data in `Validator.context`
-   'file:///....' - other files
-   ... or any other store like web service...

## Specify Context

The Validator has a `context` property which can be set to any value. This is only intended to be used as reference to predefined data.

{!docs/assets/abbreviations.txt!}
