title: Object

# ObjectSchema

Create a schema that matches any data object which contains key/value pairs.
The values may be of any other type.

The following options are possible:

-   Meta Data: [title](README.md#title), [detail](README.md#detail)
-   Sanitizing: [default](README.md#default), [flatten](object.md#flatten), [rearrange](object.md#rearrange), [unflatten](object.md#unflatten)
-   Sub schema: [item](object.md#item)
-   Sanitizing: [removeUndefined](object.md#removeundefined)
-   Validating: [denyUndefined](object.md#denyundefined), [mandatory](object.md#mandatory), [combination](object.md#combination), [forbidden](object.md#forbidden), [min](object.md#min), [max](object.md#max)
-   Revert: [raw](README.md#raw)

Allow and disallow can be also used on the items, by specifying it within the items schema setting.

## Specific Options

### flatten

Flattens the object - it'll return an object one level deep, regardless of how nested the original object was a given separator or '.' will be used to join the keys together:

```js
const schema = new ObjectSchema({ flatten: true });
// { address: { city: 'Stuttgart', street: 'Königsstraße' } }
// -> { 'address.city': 'Stuttgart', 'address.street': 'Königsstraße' }
```

A specific separator may also be set:

```js
const schema = new ObjectSchema({ flatten: '::' });
// { address: { city: 'Stuttgart', street: 'Königsstraße' } }
// -> { 'address::city': 'Stuttgart', 'address::street': 'Königsstraße' }
```

### rearrange

This settings allows to set multiple rules to move or copy attributes within the object. If the given `key` is not defined, the rule will be skipped.

```js
const schema = new ObjectSchema({
    rearrange: [{ key: 'capital', moveTo: 'city', overwrite: true }]
});
```

If `overwrite` is not set, the rule will change nothing if the destination already is defined.

### unflatten

The opposite of flatten, which use a given separator or '.' to split up the key names and be converted into a deep structure.

```js
const schema = new ObjectSchema({ unflatten: true });
// { 'address.city': 'Stuttgart', 'address.street': 'Königsstraße' }
// -> { address: { city: 'Stuttgart', street: 'Königsstraße' } }
```

A specific separator may also be set:

```js
const schema = new ObjectSchema({ unflatten: '::' });
// { 'address::city': 'Stuttgart', 'address::street': 'Königsstraße' }
// -> { address: { city: 'Stuttgart', street: 'Königsstraße' } }
```

### item

Specify the schema for one or multiple keys of the object structure.
The key can be described as `*` as default for any unspecified ones, a regexp pattern like `/^flag-/` or a direct key.

```js
const schema = new ObjectSchema({
    item: {
        number: new AnySchema(),
        '/^flag-/': new BooleanSchema({ tolerant: true }),
        '*': new StringSchema()
    }
});
```

The example above shows all three types of definition:

-   direct key name definition
-   pattern definition
-   default schema, as fallback

If no check for a key is given and also no default is defined it will be kept unchecked.

### removeUndefined

If set, this option will remove all keys from the object which are not specifically defined with a schema in the keys option. A default Schema is not needed here and everything which normally goes into the default schema will be removed instead.

```js
const schema = new ObjectSchema({
    item: {
        street: new StringSchema(),
        number: new AnySchema()
    },
    removeUndefined: true
});
let data = {
    city: 'Stuttgart',
    street: 'Königsstraße',
    number: 6
};
// -> city will be removed
```

### denyUndefined

This is like removeUndefined but will fail if an undefined key is found. A default Schema is not needed here and everything which normally goes into the default schema will fail.

```js
const schema = new ObjectSchema({
    item: {
        street: new StringSchema(),
        number: new AnySchema()
    },
    denyUndefined: true
});
let data = {
    city: 'Stuttgart',
    street: 'Königsstraße',
    number: 6
};
// -> fail because of city
```

### mandatory

This contains a list of strings or patterns which all have to be contained as keys in the object. In case of patterns at least one element needs to be there.

```js
const schema = new ObjectSchema({
    mandatory: ['city', 'street', /^flag-/]
});
let data = {
    city: 'Stuttgart',
    street: 'Königsstraße',
    number: 6
};
// -> fail because of flag-... is missing
```

As simplification you can also set `mandatory` to `true` which is the same as listing all keys defined. Also you may use a schema which work also for undefined values using a `default` option, which will only be triggered if it is also mandatory.

```js
const schema = new ObjectSchema({
    item: {
        city: new StringSchema(),
        street: new StringSchema(),
        country: new StringSchema({default: 'Germany'})
    },
    mandatory: true
});
let data = {
    city: 'Stuttgart',
    street: 'Königsstraße'
};
// -> ok, both are set and the third will be set to default
```

### forbidden

This contains a list of strings or patterns which all are not allowed as attributes in this object. It will fail if such attribute is set.

```js
const schema = new ObjectSchema({
    forbidden: ['number']
});
let data = {
    city: 'Stuttgart',
    street: 'Königsstraße',
    number: 6
};
// -> fail because of number attribute
```

### combination

This is a logical rule engine, which will check the defined attributes:

```js
const schema = new ObjectSchema({
    combination: [
        { and: ['color', 'shape'] },
        { nand: ['street', 'postbox'] },
        { or: ['length', 'weight', 'volume'] },
        { xor: ['wheels', 'rotor'] },
        { key: 'type', with: ['category'] },
        { key: 'type', without: ['city'] }
    ]
});
```

All the possible rules are shown above:

-   and - All of the attributes in `and` need to be set.
-   nand - Some attributes of `nand` are allowed, but not all.
-   or - At least one attribute of `or` is needed.
-   xor - Only one of the attribute of `xor` is needed but not more.
-   with - If `key` is present the attributes `with` are also needed.
-   without - If `key` is present the attributes `without` are not allowed.

### min

Specifies the minimum number of elements in the object which are allowed.

```js
const schema = new ArraySchema({
    min: 3
});
```

### max

Specifies the maximum number of elements in the object which are allowed.

```js
const schema = new ArraySchema({
    max: 3
});
```

If both, `min` and `max` is set you have a range and if both are the same, this exact number of elements has to be present.

{!docs/assets/abbreviations.txt!}
