# Meta Schema

This are no new schema types but handy calls to create specific validations with preset of the base schema types. They all are functions with maybe parameters for more specification which will return ready to use schema.

## timeout

For timeouts you may often want to be able to define it in seconds or natural language:

```ts
const schema = Meta.timeout();
// '2 hours 10 seconds' => 7210
// 7210 => 7210
```

Other units are also possible like: min, d, week, month, year. If one of these are used the values will be rounded.

```ts
const schema = Meta.timeout({
    unit: 'min'
});
// '2 hours 10 seconds' => 120
// 7210 => 120
```

You may also give a default if not defined like:

```ts
const schema = Meta.timeout({
    default: '1 minute',
    unit: 'min'
});
// undefined => 1
```

{!docs/assets/abbreviations.txt!}
