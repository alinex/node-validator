title: Domain

# DomainSchema

An internet domain name:

- the ASCII notation is mot allowed longer than 253 characters (international domains are converted)
- only contain ASCII characters, digits and hyphen '-'
- each part (separated ny dot) has to be smaller than 64 characters
- hyphens are not allowed at start or end of part
- the top level domain has tp consist of only ASCII characters (at least two)

The following options are possible:

-   Meta Data: [title](README.md#title), [detail](README.md#detail)
-   Sanitizing: [default](README.md#default), [sanitize](domain.md#sanitize)
-   Validating: [min](domain.md#min), [max](domain.md#max), [allow](domain.md#allow), [disallow](domain.md#disallow), [allowCountry](ip.md#allowcountry), [disallowCountry](ip.md#disallowcountry), [registered](ip.md#registered)
-   Sanitizing: [punycode](domain.md#punycode), [resolve](domain.md#resolve)
-   Revert: [raw](README.md#raw)

## Specific Options

### sanitize

If this flag is set names can be given, too. They will be translated into an IP address using DNS lookup.

```js
const schema = new DomainSchema({
    sanitize: true
});
// 'http://alinex.de/test' => 'alinex.de'
// 'info@alinex.de' => 'alinex.de'
```

### allow

This gives a list of names which can be used. All values which are not matched will fail here.

```js
const schema = new DomainSchema({ allow: ['de', 'com'] })
// 'alinex.de' => OK
// 'demo.alinex.org' => fail
```

The precedence is (later, more detailed is higher):

| Type        | Example                   |
| ----------- | ------------------------- |
| IANA        | public TLD<br>private TLD |
| TLD         | de                        |
| 2. level    | alinex.de                 |
| 3.level ... | demo.alinex.de            |

You may also use [references](reference.md) to get the values.

### disallow

The `disallow` is like a blacklist defining all names that will fail.

```js
const schema = new DomainSchema({ disallow: ['net', 'private TLD'] })
```

If `allow` and also `disallow` is set, the more detailed match (see precedence above) will be used. Reference are also possible like in [allow](#allow).

### min

The minimum depth of domain level is set. All values has to have at least this depth. A domain name with one dot within is a 2nd level domain.

```js
const schema = new DomainSchema({ min:2 })
// 'alinex.de' => OK
// 'de' => fail
```

### max

As with `min` a maximum can be defined or then both are equal an exact level is required.

```js
const schema = new DomainSchema({ max:2 })
// 'alinex.de' => OK
// 'demo.alinex.de' => fail
```

### registered

You can check that the domain name is registered in the DNS.

```js
const schema = new DomainSchema({ registered: true })
// 'alinex.de' => OK
// 'alinex.athome' => fail
```

It is also possible to allow only names with specific entries if type is given. The type may be a list of:

| Type    | Record Description             |
| ------- | ------------------------------ |
| `A`     | IPv4 addresses                 |
| `AAAA`  | IPv6 addresses                 |
| `ANY`   | any records                    |
| `CNAME` | canonical name records         |
| `MX`    | mail exchange records          |
| `NAPTR` | name authority pointer records |
| `NS`    | name server records            |
| `PTR`   | pointer records                |
| `SOA`   | start of authority records     |
| `SRV`   | service records                |
| `TXT`   | text records                   |

```js
const schema = new DomainSchema({ registered: ['CNAME'] })
// 'smtp.gmail.com' => OK
// 'alinex.de' => fail
```

### punycode

The punycode is an ASCII presentation of international domain names. This is the form which is used internally in the DNS while the browser accepts and displays the unicode representation of it. Set this flag to return the ASCII representation.

```js
const schema = new DomainSchema({ punycode: true })
// 'alinex.de' => 'alinex.de'
// 'käse.de' => 'xn--kse-qla.de'
```

### resolve

If this flag is set the domain will be resolved to one of it's IP which is given back.

```js
const schema = new DomainSchema({ resolve: true })
// 'alinex.de' = '185.26.156.22'
```

{!docs/assets/abbreviations.txt!}
