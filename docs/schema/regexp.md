title: RegExp

# RegExpSchema

A regular expression.

The following options are possible:

-   Meta Data: [title](README.md#title), [detail](README.md#detail)
-   Sanitizing: [default](README.md#default)
-   Validation: [min](regexp.md#min), [max](regexp.md#max)
-   Revert: [raw](README.md#raw)

## Specific Options

You may define the number of matched groups within the regular expression.

### min

Minimum number of captured groups in pattern:

```js
const schema = new RegExpSchema({ min: 2 });
```

### max

Maximum number of captured groups in pattern:

```js
const schema = new RegExpSchema({ max: 2 });
```

{!docs/assets/abbreviations.txt!}
