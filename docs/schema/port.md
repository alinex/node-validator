title: Port

# PortSchema

This type defines a port number used to connect to server daemons.

The following options are possible:

-   Meta Data: [title](README.md#title), [detail](README.md#detail)
-   Sanitizing: [default](README.md#default)
-   Validating: [min](number.md#min), [max](number.md#max), [greater](number.md#greater), [less](number.md#less), [allow](port.md#allow), [disallow](port.md#disallow)
-   Sanitizing: [makeString](port.md#makestring)
-   Revert: [raw](README.md#raw)

## Specific Options

Already without an option port names as values will be resolved to it's default port:

```js
const schema = new PortSchema();
// 'ftp' => 21
```

### allow

This works exactly like in [NumberSchema](number.md#allow) but has the following predefined ranges:

| Name       | Range          |
| ---------- | -------------- |
| system     | [0, 1023]      |
| registered | [1024, 49151]  |
| dynamic    | [49152, 65535] |

```js
const schema = new PortSchema({
    allow: ['registered', 'dynamic']
});
// 80 => fail
// 8080 => OK
```

### disallow

That's the negation of allow and also works like in [NumberSchema](number.md#disallow) with the above defined ranges:

```js
const schema = new PortSchema({
    disallow: ['system']
});
// 80 => fail
// 8080 => OK
```

### makeString

If a default port is used, it will be returned with it's standard protocol.

```js
const schema = new PortSchema({
    makeString: true
});
// 80 => 'http'
```

{!docs/assets/abbreviations.txt!}
