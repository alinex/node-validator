title: Array

# ArraySchema

It have to be a list of elements those specific format may be specified as sub schema.

The following options are possible:

-   Meta Data: [title](README.md#title), [detail](README.md#detail)
-   Sanitizing: [default](README.md#default), [split](array.md#split), [makeArray](array.md#makearray), [removeDuplicate](array.md#removeduplicate), [filter](array.md#filter)
-   Ordering: [shuffle](array.md#shuffle), [sort](array.md#sort), [reverse](array.md#reverse)
-   Sub schema: [item](array.md#item)
-   Validating: [unique](array.md#unique), [min](array.md#min), [max](array.md#max), [allow](array.md#allow), [disallow](array.md#disallow)
-   Revert: [raw](README.md#raw)

Allow and disallow can be also used on the items, by specifying it within the items schema setting.

## Specific Options

### split

This option allows to give the list as a string with separator. It will be split up to get the list values.

```js
const schema = new ArraySchema({ split: ':' });
// '1:2:3' => ['1', '2', '3']
```

And regular expressions are also possible:

```js
const schema = new ArraySchema({ split: /\s*,\s*/ });
// '1, 2   ,   3' => ['1', '2', '3']
```

### makeArray

If the source element is not already an array, it will be automatically put as single element into an array.

```js
const schema = new ArraySchema({
    makeArray: true
});
// '123' => ['123']
```

### removeDuplicate

Remove duplicate elements to get only unique values:

```js
const schema = new ArraySchema({
    removeDuplicate: true
});
// [1,2,2,4,2,3] => [1,2,4,3]
```

### filter

Filter the list of elements to remove all empty elements or all that don't match the given schema
definition.

Remove only empty elements:

```js
const schema = new ArraySchema({
    filter: true
});
```

Or keep only elements with specific sub schema:

```js
const schema = new ArraySchema({
    filter: new StringSchema()
});
```

### shuffle

All list items will be shuffled to get a random order:

```js
const schema = new ArraySchema({
    shuffle: true
});
// [1,2,3] => [2,3,1]
```

### sort

Sort a list alphabetically or numeric by it's contents:

```js
const schema = new ArraySchema({
    sort: true // same as 'alpha'
});
// [3,2,11] => [11,2,3]
```

This was the alphabetic search, numerical will be:

```js
const schema = new ArraySchema({
    sort: 'num'
});
// [3,2,11] => [2,3,11]
```

### reverse

Additionally to `sort` or separately, the order of the list items will be reversed:

```js
const schema = new ArraySchema({
    sort: true,
    reverse: true
});
```

### item

Specify the schema for one or multiple items of the array list.
Given in a list the `index` number as key and a `schema` definition as value is needed.
You may use `*` to match all other not directly defined values or give a range like: `2-6`, `2-` or `-1` to specify the last element.

The following definition allows a list containing multiple string elements, but the first has to be a number:

```js
const schema = new ArraySchema({
    item: {
        '*': new StringSchema(),
        `0`: new NumberSchema()
    }
});
```

Use negative index numbers to find the element counting from the end. If multiple definitions match the order is:

1. use the concrete index number
2. use the negative number (from the end)
3. use the first matching range
4. use the default '\*' schema

### unique

If called without sanitize it will alert if duplicate values are contained.

```js
const schema = new ArraySchema({
    unique: true
});
```

### min

Specifies the minimum number of items in the array which are allowed.

```js
const schema = new ArraySchema({
    min: 3
});
```

### max

Specifies the maximum number of items in the array which are allowed.

```js
const schema = new ArraySchema({
    max: 3
});
```

If both, `min` and `max` is set you have a range and if both are the same, this exact number of elements has to be present.

### allow

This gives a list of schemas which are valid for the items. Each item has to match at least one of this schema or the validation will fail.

In contrast to the use of items with a default schema using Logic operator this won't change the values and is more readable in some cases.

```js
const schema = new ArraySchema({ allow: [new NumberSchema()] });
// [1] => OK
// ['1'] => fail
```

### disallow

The `disallow` is like a blacklist defining all schemas that will fail.

```js
const schema = new ArraySchema({ disallow: [new NumberSchema()] });
// ['1'] => OK
// [1] => fail
```

If `allow` and also `disallow` is set, the `disallow` has priority and values which are in both are disallowed.

{!docs/assets/abbreviations.txt!}
