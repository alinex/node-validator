title: Any

# AnySchema

This type is an all rounder allowing basically any data type.

The following options are possible:

-   Meta Data: [title](README.md#title), [detail](README.md#detail)
-   Sanitizing: [default](README.md#default)
-   Validating: [allow](any.md#allow), [disallow](any.md#disallow)
-   Revert: [raw](README.md#raw)

## Specific Options

### allow

This gives a list of values which can be set. All values which are not matched will fail here.

```js
const schema = new AnySchema({ allow: ['car', 'bike'] })
// 'car' => OK
// 'boat' => fail
```

You may also use [references](reference.md) to get the values.

```js
const schema = new AnySchema({
    allow: new Reference({ 'data:context', 'cities.europe'})
});
```

And it can also be mixed together with concrete values:

```js
const schema = new AnySchema({
    allow: [
        'New York',
        new Reference({ 'data:context', 'cities.europe'})
    ]
});
```

### disallow

The `disallow` is like a blacklist defining all values or patterns that will fail.

```js
const schema = new AnySchema({ disallow: ['car', 'bike'] })
// 'car' => fail
// 'boat' => OK
```

If `allow` and also `disallow` is set, the `disallow` has priority and values which are in both are disallowed. Reference are also possible like shown in [allow](#allow).

{!docs/assets/abbreviations.txt!}
