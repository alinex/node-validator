title: Overview

# Schema

The schema defines the data structure and is used to validate and sanitize it. The complete schema is a combination of different type schemas combined together. See under [usage](../usage/schema.md) on how to call this classes.

![Schema Overview](schema-types.svg)

The above graph shows all the possible types with all their specific options and inheritance.
Each of them is defined in the following pages. But you may also use the additional [meta types](meta.md) which are predefined selections and combinations of these.

As far as possible the schema will be checked against validity with TypeScript definitions. Some invalid setup may not occur and misconfiguration like `new NumberSchema({ min: 6, max: 3})` will be checked and directly throw an Error because it can neither come to a valid state.

The examples below and on the following pages are simplified and only show the definition of the schema, not how to use it. Therefore have a look at the [usage page](../usage).

## Common Options

All types have the following common options to be used:

-   Meta Data: [title](README.md#title), [detail](README.md#detail)
-   Sanitizing: [default](README.md#default)
-   Revert: [raw](README.md#raw)

### title

This is a short title text for the data element, which is optional. It is used in description and error messages.

To make the title multilingual you can give a translate function like done in the following example using [i18next](../dev/i18n.md):

```ts
import { T } from './i18n'

new StringSchema({
    title: (lang) => T(lang)('string.title'),
})
```

### detail

A text describing the content's subject. It is used in description and error messages and should be at least a complete sentence.

Multilingual text is possible here in the same way as for the title.

```js
const schema = new StringSchema({
    title: 'Address',
    detail: 'The home address to deliver packages to.'
});
```

### default

If no value specified directly a default value can be used instead.

```js
const schema = new NumberSchema({
    default: 999
});
```

This can also be a referenced value:

```js
const schema = new StringSchema({
    default: new Reference({'data:context', 'path'})
});
```

### raw

If this option is set after validating and sanitizing the original value (like from the source) will be returned. This may be used in combination with other options which change the value but only to validate them.

```js
const schema = new StringSchema({
    trim: true,
    max: 3,
    raw: true
});
// ' EUR ' => OK
```

In the above example the text string is trimmed and the resulting length is checked. Here the string is three characters long, so valid but because of the `raw` option the original value will be return unchanged.

{!docs/assets/abbreviations.txt!}
