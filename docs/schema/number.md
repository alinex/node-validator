title: Number

# NumberSchema

This type defines a textual string value with lots of customization possibilities.

The following options are possible:

-   Meta Data: [title](README.md#title), [detail](README.md#detail)
-   Sanitizing: [default](README.md#default), [sanitize](number.md#sanitize), [unit](number.md#unit)
-   Validating: [integer](number.md#integer), [min](number.md#min), [max](number.md#max), [greater](number.md#greater), [less](number.md#less), [multipleOf](number.md#multipleof), [allow](number.md#allow), [disallow](number.md#disallow), [ranges](number.md#ranges)
-   Sanitizing: [format](number.md#format)
-   Revert: [raw](README.md#raw)

## Specific Options

### sanitize

If this flag is set the first numerical value from the given text will be used. That allows any non numerical characters before or after the value to be ignored.

```js
const schema = new NumberSchema({
    sanitize: true
});
// 'the 5 rooms' => 5
```

In combination with `integer` it will automatically round any float value.

Also percent values will be recognized with the sanitize option and converted to their pure numerical value:

```js
const schema = new NumberSchema({
    sanitize: true
});
// '5%' => 0.05
```

### unit

This specifies the unit of the stored numerical value and also allows to convert values in each compatible unit which will automatically be recognized.

To use it an object with `from` and `to` setting are needed. The `from` value is used, if no unit is given in the value itself like a default unit and then it is converted to `to`. Both can also be the same, if no conversion is needed except other units are given.

```js
const schema = new NumberSchema({
    unit: { from: 'mm', to: 'cm' }
});
// 194 => 19.4
```

The following units are possible:

-   Length: mm, cm, m, km, in, yd, ft-us, ft, fathom, mi, nMi
-   Area: mm2, cm2, m2, ha, km2, in2, ft2, ac, mi2
-   Mass: mcg, mg, g, kg, oz, lb, mt, t
-   Volume: mm3, cm3, ml, l, kl, m3, km3, tsp, Tbs, in3, fl-oz, cup, pnt, qt, gal, ft3, yd3
-   Volume Flow Rate: mm3/s, cm3/s, ml/s, cl/s, dl/s, l/s, l/min, l/h, kl/s, kl/min, kl/h, m3/s, m3/min, m3/h, km3/s, tsp/s, Tbs/s, in3/s, in3/min, in3/h, fl-oz/s, fl-oz/min, fl-oz/h, cup/s, pnt/s, pnt/min, pnt/h, qt/s, gal/s, gal/min, gal/h, ft3/s, ft3/min, ft3/h, yd3/s, yd3/min, yd3/h'
-   Temperature: C, F, K, R
-   Time: ns, mu, ms, s, min, h, d, week, month, year
-   Frequency: Hz, mHz, kHz, MHz, GHz, THz, rpm, deg/s, rad/s
-   Speed: m/s, km/h, m/h, knot, ft/s
-   Pace: s/m, min/km, s/ft, min/km
-   Pressure: Pa, hPa, kPa, MPa, bar, torr, psi, ksi
-   Digital (Binary): ib, Kib, Mib, Gib, Tib, iB, KiB, MiB, GiB, TiB
-   Digital (Decimal): b, Kb, Mb, Gb, Tb, B, KB, MB, GB, TB
-   Illuminance: lx, ft-cd
-   Parts-Per: ppm, ppb, ppt, ppq
-   Voltage: V, mV, kV
-   Current: A, mA, kA
-   Power: W, mW, kW, MW, GW
-   Apparent Power: VA, mVA, kVA, MVA, GVA
-   Reactive Power: VAR, mVAR, kVAR, MVAR, GVAR
-   Energy: Wh, mWh, kWh, MWh, GWh, J, kJ
-   Reactive Energy: VARh, mVARh, kVARh, MVARh, GVARh
-   Angle: deg, rad, grad, arcmin, arcsec
-   Charge: c, mC, μC, nC, pC
-   Force: N, kN, lbf
-   Acceleration: g (g-force), m/s2

!!! note

    Conversion between unit systems are generally not possible. Only between the two digital units it is possible.

    | Value | From | To  | Result | Note                                       |
    | ----- | ---- | --- | ------ | ------------------------------------------ |
    | 2     | KB   | KB  | 2      |                                            |
    | 2     | KiB  | KB  | 2.048  | binary -> decimal                          |
    | 2.048 | KB   | KiB | 2      | decimal -> binary                          |
    | 2KB   | KB   | KB  | 2      |                                            |
    | 2KiB  | KB   | KB  | 2.048  | binary -> decimal                          |
    | 2KB   | KiB  | KB  | 2.048  | read value always binary if from is binary |

    The last line shows the special handling of binary `from` setting. As often binary values are written unspecific it will be read always as binary.

See the `format` option on how to use units in formatting into strings.

### round

This allows to round the value to a given precision (number of fraction digits) and a rounding method ('arithmetic', 'floor', 'ceil'). The default is to use arithmetic but you need always a precision which is the number of digits behind the floating point.

```js
const schema = new NumberSchema({
    round: { precision: 1 }
});
// 5.34 => 5.3
```

The above use the arithmetic method, but you may also use another method:

```js
const schema = new NumberSchema({
    round: { method: 'ceil', precision: 0 }
});
// 5.3 => 6
```

### integer

Only integers are allowed. Together with `sanitize` it will also round float values to become integers.

```js
const schema = new NumberSchema({
    integer: true
});
// 5 => OK
// 5.3 => fail
```

This can also be set to a specific integer size using: byte (8-bit), short (16-bit), long (32-bit), safe (53-bit), quad (64-bit). The safe type is the maximum integer numbers which can be used without loosing precision in JavaScript.

```js
const schema = new NumberSchema({
    integer: { type: 'byte', unsigned: true }
});
// 5 => OK
// -1 => fail
// 600 => fail
```

The maximum range of each are:

| Type           | Bit | Min                  | Max                  |
| -------------- | --- | -------------------- | -------------------- |
| byte           | 8   | -128                 | 127                  |
| byte unsigned  | 8   | 0                    | 255                  |
| short          | 16  | -32768               | 32767                |
| short unsigned | 16  | 0                    | 65535                |
| long           | 32  | -2147483648          | 2147483647           |
| long unsigned  | 32  | 0                    | 4294967295           |
| safe           | 53  | -4503599627370496    | 4503599627370495     |
| safe unsigned  | 53  | 0                    | 9007199254740991     |
| quad           | 64  | −9223372036854775808 | 9223372036854775807  |
| quad unsigned  | 64  | 0                    | 18446744073709551615 |

> Keep in mind that JavaScript not really knows integers and will store all numbers as float so a quad integer will be unsafe because it loose precision.

### min

Set a minimum value, the number should be equal or greater than that.

```js
const schema = new NumberSchema({
    min: 5
});
// 5 => OK
// 4 => fail
```

### max

Set a maximum value, the number should be equal or less than that.

```js
const schema = new NumberSchema({
    max: 100
});
// 100 => OK
// 100.2 => fail
```

### greater

Set an excluding minimum value, the number should be greater than that.

```js
const schema = new NumberSchema({
    greater: 5
});
// 5.3 => OK
// 5 => fail
```

### less

Set an excluding maximum value, the number should be less than that.

```js
const schema = new NumberSchema({
    less: 100
});
// 99 => OK
// 100 => fail
```

### multipleOf

The value has to be a multiple of the value given here.

```js
const schema = new NumberSchema({
    multipleOf: 8
});
// 256 => OK
// 250 => fail
```

### allow

This gives a list of values or ranges which can be set. All values which are not matched will fail here.

```js
const schema = new NumberSchema({ allow: [5, 9] });
// 5 => OK
// 6 => fail
const schema = new NumberSchema({ allow: [[1, 3], [6, 9]] });
// 3 => OK
// 4 => fail
```

Values and ranges may also be mixed.
You may also use [references](reference.md) to get the allowed values.

```js
const schema = new NumberSchema({
    allow: new Reference({ 'data:context', 'ranges'})
});
```

And it can also be mixed together with concrete values:

```js
const schema = new AnySchema({
    allow: [
        7,
        new Reference({ 'data:context', 'ranges'})
    ]
});
```

### disallow

The `disallow` is like a blacklist defining all values or ranges that will fail.

```js
const schema = new NumberSchema({ disallow: [5, 8] });
// 5 => fail
// 6 => OK
const schema = new NumberSchema({ disallow: [[5, 8]] });
// 6 => OK
// 9 => fail
```

If `allow` and also `disallow` is set, the `disallow` has priority and values which are in both are disallowed. Reference are also possible like shown in [allow](#allow).

### ranges

A list of named ranges can be given which are used in the `allow` and `disallow` lists.

```js
const schema = new NumberSchema({
    disallow: ['system'],
    ranges: {
        system: [0, 1023],
        registered: [1024, 49151],
        dynamic: [49152, 65535]
    }
});
// 80 => fail
// 8080 => OK
```

This is mostly used in subschema classes.

### format

By setting one of the following format strings you will get the value back as a formatted string:

| Number     | Format       | String        |
| ---------- | ------------ | ------------- |
| 10000      | '0,0.0000'   | 10,000.0000   |
| 10000.23   | '0,0'        | 10,000        |
| 10000.23   | '+0,0'       | +10,000       |
| -10000     | '0,0.0'      | -10,000.0     |
| 10000.1234 | '0.000'      | 10000.123     |
| 100.1234   | '00000'      | 00100         |
| 1000.1234  | '000000,0'   | 001,000       |
| 10         | '000.00'     | 010.00        |
| 10000.1234 | '0[.]00000'  | 10000.12340   |
| -10000     | '(0,0.0000)' | (10,000.0000) |
| -0.23      | '.00'        | -.23          |
| -0.23      | '(.00)'      | (.23)         |
| 0.23       | '0.00000'    | 0.23000       |
| 0.23       | '0.0[0000]'  | 0.23          |
| 1230974    | '0.0a'       | 1.2m          |
| 1460       | '0 a'        | 1 k           |
| -104000    | '0a'         | -104k         |
| 1          | '0o'         | 1st           |
| 100        | '0o'         | 100th         |

You can also add the unit if set earlier by adding `$unit` to the format string.

```js
const schema = new NumberSchema({
    unit: { from: 'mm', to: 'cm' },
    format: '0.0 $unit'
});
// 194 => '19.4 cm'
```

And last but not least use `$best` to let the system change the unit to the best selection:

```js
const schema = new NumberSchema({
    unit: { from: 'mm', to: 'mm' },
    format: '0.0 $best'
});
// 1900 => '19.0 m'
```

{!docs/assets/abbreviations.txt!}
