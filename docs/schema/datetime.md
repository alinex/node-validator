title: DateTime

# DateTimeSchema

This type is used to hold a date or time value. The internal time will be stored as UTC time. The possible range is from year 0000 to 9999.

The following options are possible:

-   Meta Data: [title](README.md#title), [detail](README.md#detail)
-   Sanitizing: [default](README.md#default), [parse](datetime.md#parse), [defaultTimezone](datetime.md#defaulttimezone), [reference](datetime.md#reference)
-   Validating: [min](datetime.md#min), [max](datetime.md#max), [greater](datetime.md#greater), [less](datetime.md#less), [allow](any.md#allow), [disallow](any.md#disallow)
-   Sanitizing: [format](datetime.md#format), [timezone](datetime.md#timezone), [locale](datetime.md#locale)
-   Revert: [raw](README.md#raw)

## Specific Options

### parse

If this flag is set to `true` or a locale is given it will try to auto recognize some common formats. Additionally a list of possible formats can be given to allow specific other formats.

```js
const schema = new DateTimeSchema({
    parse: true
});
// '2013-02-08T12:00:00.000Z' => 2013-02-08T12:00:00.000Z
```

The following locales are supported: en, en_GB, de, pt, es, fr, ja:

```js
const schema = new DateTimeSchema({
    parse: 'en_GB'
});
// '02/08/2013' => 2013-02-08T12:00:00.000Z
```

The parser will detect a lot of different formats and variations. As seen above the simplest is the default string format in JavaScript:

-   2013-02-08T12:00:00.000Z

Within the text (can be combined with any date pattern):

-   It has to be after 2013-02-08T12:00:00.000Z
-   Give me a call at 2013-02-08T12:00:00.000Z but not later!

[ISO 8601 (RFC3339)](https://www.ietf.org/rfc/rfc3339.txt):

| Format                     | Example                  | Result                   |
| -------------------------- | ------------------------ | ------------------------ |
| `YYYY-MM-DD`               | 2013-02-08               | 2013-02-08T12:00:00.000Z |
| `YYYY-MM-DDTHH:mm`         | 2013-02-08T11:30         | 2013-02-08T11:30:00.000Z |
| `YYYY-MM-DDTHH:mm:ss`      | 2013-02-08T11:30:05      | 2013-02-08T11:30:05.000Z |
| `YYYY-MM-DDTHH:mm:ss.sss`  | 2013-02-08T11:30:05.222  | 2013-02-08T11:30:05.222Z |
| `YYYY-MM-DDTHH:mm:ss.sssZ` | 2013-02-08T11:30:05.000Z | 2013-02-08T11:30:05.000Z |
| `YYYY-MM-DDTHH:mm±hh`      | 2013-02-08T11:30-01      | 2013-02-08T12:30:00.000Z |
| `YYYY-MM-DDTHH:mm±hh:mm`   | 2013-02-08T11:30+01:30   | 2013-02-08T10:00:00.000Z |

[IETF (RFC 2922)](https://tools.ietf.org/html/rfc2822#page-14):

| Format                    | Example                 | Result                   |
| ------------------------- | ----------------------- | ------------------------ |
| `YYYY-MM-DD`              | 2013-02-08              | 2013-02-08T12:00:00.000Z |
| `YYYY-MM-DD HH:mm`        | 2013-02-08 11:30        | 2013-02-08 11:30:00.000Z |
| `YYYY-MM-DD HH:mm:ss`     | 2013-02-08 11:30:05     | 2013-02-08 11:30:05.000Z |
| `YYYY-MM-DD HH:mm:ss.sss` | 2013-02-08 11:30:05.222 | 2013-02-08 11:30:05.222Z |
| `YYYY-MM-DD HH:mm GMT`    | 2013-02-08 11:30 GMT    | 2013-02-08 11:30:05.000Z |
| `YYYY-MM-DD HH:mm CEST`   | 2013-02-08 11:30 CEST   | 2013-02-08 09:30:05.000Z |
| `YYYY-MM-DD HH:mm ±hh`    | 2013-02-08 11:30 -01    | 2013-02-08 12:30:00.000Z |
| `YYYY-MM-DD HH:mm ±hh:mm` | 2013-02-08 11:30 +01:30 | 2013-02-08 10:00:00.000Z |

Slash dates, here the use syntax with month first is used. (The british format with day first is only detected if the day is larger 12.):

| Format                      | Example    | Result                   |
| --------------------------- | ---------- | ------------------------ |
| `MM/YYYY`                   | 02/2016    | 2016-02-01T12:00:00.000Z |
| `M/YYYY`                    | 2/2016     | 2016-02-01T12:00:00.000Z |
| `MM/DD/YYYY`                | 02/08/2016 | 2016-02-08T12:00:00.000Z |
| `DD/MM/YYYY` (locale en_GB) | 08/02/2016 | 2016-02-08T12:00:00.000Z |
| `M/D/YYYY`                  | 2/8/2016   | 2016-02-08T12:00:00.000Z |
| `M/D/YYYY` (locale en_GB)   | 2/8/2016   | 2016-02-08T12:00:00.000Z |
| `MM/DD/YY`                  | 02/08/16   | 2016-02-08T12:00:00.000Z |
| `M/D/YY`                    | 2/8/16     | 2016-02-08T12:00:00.000Z |
| `YYYY/MM/DD`                | 2016/02/08 | 2016-02-08T12:00:00.000Z |
| `YYYY/M/D`                  | 2016/2/8   | 2016-02-08T12:00:00.000Z |

With month and day names:

| Example                         | Result                      |
| ------------------------------- | --------------------------- |
| 2012/Aug/10                     | 2012-08-10T12:00:00.000Z    |
| Juli 2017                       | 2017-07-01T12:00:00.000Z    |
| 2012, August                    | 2012-08-01T12:00:00.000Z    |
| August 10, 2012                 | 2012-08-10T12:00:00.000Z    |
| August-10, 2012                 | 2012-08-10T12:00:00.000Z    |
| May eighth, 2010                | 2010-05-08T12:00:00.000Z    |
| Sat, 21 Feb 2015 11:50:48 -0500 | 2015-02-21T16:50:48.000Z    |
| August 10, 8 AD                 | 0008-08-10T12:00:00.000Z    |
| August 10, 345 BC               | -000345-08-10T12:00:00.000Z |

Parse english dates:

-   09/25/2017 10:31:50.522 PM -> 2017-09-25T22:31:50.522Z

Parsing german dates:

-   Freitag 30.12.16 -> 2016-12-30T12:00:00.000Z

If no timezone is given the local timezone is used.

### defaultTimezone

As described above times without a timezone are treated like GMT but you may set another default timezone. Therefore use the abbreviations: A, ACDT, ACST, ADT, AEDT, AEST, AFT, AKDT, AKST, ALMT, AMST, AMT, ANAST, ANAT, AQTT, ART, AST, AWDT, AWST, AZOST, AZOT, AZST, AZT, B, BNT, BOT, BRST, BRT, BST, BTT, C, CAST, CAT, CCT, CDT, CEST, CET, CHADT, CHAST, CKT, CLST, CLT, COT, CST, CVT, CXT, ChST, D, DAVT, E, EASST, EAST, EAT, ECT, EDT, EEST, EET, EGST, EGT, EST, ET, F, FJST, FJT, FKST, FKT, FNT, G, GALT, GAMT, GET, GFT, GILT, GMT, GST, GYT, H, HAA, HAC, HADT, HAE, HAP, HAR, HAST, HAT, HAY, HKT, HLV, HNA, HNC, HNE, HNP, HNR, HNT, HNY, HOVT, I, ICT, IDT, IOT, IRDT, IRKST, IRKT, IRST, IST, JST, K, KGT, KRAST, KRAT, KST, KUYT, L, LHDT, LHST, LINT, M, MAGST, MAGT, MART, MAWT, MDT, MESZ, MEZ, MHT, MMT, MSD, MSK, MST, MUT, MVT, MYT, N, NCT, NDT, NFT, NOVST, NOVT, NPT, NST, NUT, NZDT, NZST, O, OMSST, OMST, P, PDT, PET, PETST, PETT, PGT, PHOT, PHT, PKT, PMDT, PMST, PONT, PST, PT, PWT, PYST, PYT, Q, R, RET, S, SAMT, SAST, SBT, SCT, SGT, SRT, SST, T, TAHT, TFT, TJT, TKT, TLT, TMT, TVT, U, ULAT, UTC, UYST, UYT, UZT, V, VET, VLAST, VLAT, VUT, W, WAST, WAT, WEST, WESZ, WET, WEZ, WFT, WGST, WGT, WIB, WIT, WITA, WST, WT, X, Y, YAKST, YAKT, YAPT, YEKST, YEKT, Z or local.

```js
const schema = new DateTimeSchema({
    parse: true,
    defaultTimezone: 'MESZ'
});
```

### reference

Define a reference date to calculate relative dates from. If not defined the current date and time is used.

```js
const schema = new DateTimeSchema({
    parse: true,
    reference: new Date('2018-12-24T12:00:00.000Z')
});
// 'yesterday' => 2018-12-23T12:00:00.000Z
```

If times are considered here they are always calculated with the local timezone if no `defaultTimezone` is given.

Supported are:

| Example           | Reference                | Result                   |
| ----------------- | ------------------------ | ------------------------ |
| today             | 2018-12-24T16:00:00.000Z | 2018-12-24T12:00:00.000Z |
| today 5PM         | 2018-12-24T16:00:00.000Z | 2018-12-24T17:00:00.000Z |
| tomorrow          | 2018-12-24T16:00:00.000Z | 2018-12-25T12:00:00.000Z |
| yesterday         | 2018-12-24T16:00:00.000Z | 2018-12-23T12:00:00.000Z |
| last day          | 2018-12-24T16:00:00.000Z | 2018-12-23T12:00:00.000Z |
| last 2 days       | 2018-12-24T16:00:00.000Z | 2018-12-23T12:00:00.000Z |
| past week         | 2018-12-24T16:00:00.000Z | 2018-12-23T12:00:00.000Z |
| next four weeks   | 2018-12-24T16:00:00.000Z | 2018-12-23T12:00:00.000Z |
| last day          | 2018-12-24T16:00:00.000Z | 2018-12-23T12:00:00.000Z |
| this Friday       | 2018-12-24T16:00:00.000Z | 2018-12-28T12:00:00.000Z |
| last Friday       | 2018-12-24T16:00:00.000Z | 2018-12-21T12:00:00.000Z |
| next Friday       | 2018-12-24T16:00:00.000Z | 2019-01-04T12:00:00.000Z |
| 8pm               | 2018-12-24T16:00:00.000Z | 2018-12-24T20:00:00.000Z |
| 11 at night       | 2018-12-24T16:00:00.000Z | 2018-12-24T23:00:00.000Z |
| now               | 2018-12-24T16:00:00.000Z | 2018-12-24T13:00:00.000Z |
| this morning      | 2018-12-24T16:00:00.000Z | 2018-12-24T06:00:00.000Z |
| this afternoon    | 2018-12-24T16:00:00.000Z | 2018-12-24T15:00:00.000Z |
| this evening      | 2018-12-24T16:00:00.000Z | 2018-12-24T20:00:00.000Z |
| tonight           | 2018-12-24T16:00:00.000Z | 2018-12-24T22:00:00.000Z |
| tonight 8pm       | 2018-12-24T16:00:00.000Z | 2018-12-24T20:00:00.000Z |
| tonight at 8      | 2018-12-24T16:00:00.000Z | 2018-12-24T20:00:00.000Z |
| last night        | 2018-12-24T16:00:00.000Z | 2018-12-23T00:00:00.000Z |
| August            | 2018-12-24T16:00:00.000Z | 2018-08-01T12:00:00.000Z |
| August 10         | 2018-12-24T16:00:00.000Z | 2018-08-10T12:00:00.000Z |
| Aug 10            | 2018-12-24T16:00:00.000Z | 2018-08-10T12:00:00.000Z |
| May twenty-fourth | 2018-12-24T16:00:00.000Z | 2018-05-24T12:00:00.000Z |

This are only some examples, a lot more combinations of the above are possible.

Also other languages are supported like german: `jetzt`, `heute`, `morgen`, `gestern`, `letzte Nacht` ...

### min

Set a minimum value, the date should be equal or greater than that.

```js
const schema = new DateTimeSchema({
    min: new Date('2018-12-24T12:00:00.000Z')
});
// '2018-12-24T12:00:00.000Z' => OK
// '2018-12-22T12:00:00.000Z' => fail
```

### max

Set a maximum value, the date should be equal or less than that.

```js
const schema = new DateTimeSchema({
    max: new Date('2018-12-24T12:00:00.000Z')
});
// '2018-12-20T12:00:00.000Z' => OK
// '2018-12-24T12:00:01.000Z' => fail
```

### greater

Set an excluding minimum value, the date should be greater than that.

```js
const schema = new DateTimeSchema({
    greater: new Date('2018-12-24T12:00:00.000Z')
});
// '2018-12-24T15:00:00.000Z' => OK
// '2018-12-24T12:00:00.000Z' => fail
```

### less

Set an excluding maximum value, the date should be less than that.

```js
const schema = new DateTimeSchema({
    less: new Date('2018-12-24T12:00:00.000Z')
});
// '2018-12-24T11:00:00.000Z' => OK
// '2018-12-24T20:00:00.000Z' => fail
```

### format

The format allows to get the date, time or both in a defined string format.

```js
const schema = new DateTimeSchema({
    format: 'YYYY-MM-DD HH:mm:ss'
});
```

| Part                              | Token              | Output                                  |
| --------------------------------- | ------------------ | --------------------------------------- |
| Month                             | M                  | 1 2 ... 11 12                           |
|                                   | Mo                 | 1st 2nd ... 11th 12th                   |
|                                   | MM                 | 01 02 ... 11 12                         |
|                                   | MMM                | Jan Feb ... Nov Dec                     |
|                                   | MMMM               | January February ... November December  |
| Quarter                           | Q                  | 1 2 3 4                                 |
|                                   | Qo                 | 1st 2nd 3rd 4th                         |
| Day of Month                      | D                  | 1 2 ... 30 31                           |
|                                   | Do                 | 1st 2nd ... 30th 31st                   |
|                                   | DD                 | 01 02 ... 30 31                         |
| Day of Year                       | DDD                | 1 2 ... 364 365                         |
|                                   | DDDo               | 1st 2nd ... 364th 365th                 |
|                                   | DDDD               | 001 002 ... 364 365                     |
| Day of Week                       | d                  | 0 1 ... 5 6                             |
|                                   | do                 | 0th 1st ... 5th 6th                     |
|                                   | dd                 | Su Mo ... Fr Sa                         |
|                                   | ddd                | Sun Mon ... Fri Sat                     |
|                                   | dddd               | Sunday Monday ... Friday Saturday       |
| Day of Week (Locale)              | e                  | 0 1 ... 5 6                             |
| Day of Week (ISO)                 | E                  | 1 2 ... 6 7                             |
| Week of Year                      | w                  | 1 2 ... 52 53                           |
|                                   | wo                 | 1st 2nd ... 52nd 53rd                   |
|                                   | ww                 | 01 02 ... 52 53                         |
| Week of Year (ISO)                | W                  | 1 2 ... 52 53                           |
|                                   | Wo                 | 1st 2nd ... 52nd 53rd                   |
|                                   | WW                 | 01 02 ... 52 53                         |
| Year                              | YY                 | 70 71 ... 29 30                         |
|                                   | YYYY               | 1970 1971 ... 2029 2030                 |
|                                   | Y                  | 1970 1971 ... 9999 +10000 +10001        |
| Week Year                         | gg                 | 70 71 ... 29 30                         |
|                                   | gggg               | 1970 1971 ... 2029 2030                 |
| Week Year (ISO)                   | GG                 | 70 71 ... 29 30                         |
|                                   | GGGG               | 1970 1971 ... 2029 2030                 |
| AM/PM                             | A                  | AM PM                                   |
|                                   | a                  | am pm                                   |
| Hour                              | H                  | 0 1 ... 22 23                           |
|                                   | HH                 | 00 01 ... 22 23                         |
|                                   | h                  | 1 2 ... 11 12                           |
|                                   | hh                 | 01 02 ... 11 12                         |
|                                   | k                  | 1 2 ... 23 24                           |
|                                   | kk                 | 01 02 ... 23 24                         |
| Minute                            | m                  | 0 1 ... 58 59                           |
|                                   | mm                 | 00 01 ... 58 59                         |
| Second                            | s                  | 0 1 ... 58 59                           |
|                                   | ss                 | 00 01 ... 58 59                         |
| Fractional Second                 | S                  | 0 1 ... 8 9                             |
|                                   | SS                 | 00 01 ... 98 99                         |
|                                   | SSS                | 000 001 ... 998 999                     |
|                                   | SSSS ... SSSSSSSSS | 000[0..] 001[0..] ... 998[0..] 999[0..] |
| Time Zone                         | z or zz            | EST CST ... MST PST                     |
|                                   | Z                  | -07:00 -06:00 ... +06:00 +07:00         |
|                                   | ZZ                 | -0700 -0600 ... +0600 +0700             |
| Unix Timestamp                    | X                  | 1360013296                              |
| Unix Millisecond Timestamp        | x                  | 1360013296123                           |
| Time                              | LT                 | 8:30 PM                                 |
| Time with seconds                 | LTS                | 8:30:25 PM                              |
| Month numeral, day of month, year | L                  | 09/04/1986                              |
|                                   | l                  | 9/4/1986                                |
| Month name, day of month, year    | LL                 | September 4, 1986                       |
|                                   | ll                 | Sep 4, 1986                             |
| ...with time                      | LLL                | September 4, 1986 8:30 PM               |
|                                   | lll                | Sep 4, 1986 8:30 PM                     |
| ...with day of week               | LLLL               | Thursday, September 4, 1986 8:30 PM     |
|                                   | llll               | Thu, Sep 4, 1986 8:30 PM                |
| Predefined format                 | ISO8601            | YYYY-MM-DDTHH:mm:ssZ                    |
|                                   | ISO8601-date       | YYYY-MM-DD                              |
|                                   | ISO8601            | YYYY-MM-DDTHH:mm:ssZ                    |
|                                   | RFC1123            | ddd, DD MMM YYYY HH:mm:ss z             |
|                                   | RFC2822            | ddd, DD MMM YYYY HH:mm:ss ZZ            |
|                                   | RFC822             | ddd, DD MMM YY HH:mm:ss ZZ              |
|                                   | RFC1036            | ddd, D MMM YY HH:mm:ss ZZ               |
| Other formats                     | seconds            | 1545663600                              |
|                                   | milliseconds       | 1545663600000                           |
|                                   | relative           | 6 months ago                            |
|                                   | range              | 120 (seconds from now)                  |

### timezone

The timezone used to format the date to a string.

```js
const schema = new DateTimeSchema({
    timezone: 'Europe/Berlin'
});
```

### locale

If textual formats are used the names can be localized if a locale is given. Nearly all two character language codes are supported.

```js
const schema = new DateTimeSchema({
    format: 'LLL',
    locale: 'de'
});
```

## Examples

Some useful more complex examples are shown below.

### Time range

This may be used for timeouts or the like to get seconds from natural description.

```js
const schema = new DateTimeSchema({
    parse: true,
    format: range
});
// '2 hours 10 seconds' => 7210
```

To change the unit it has to be combined with the NumberSchema using LogicSchema.

```js
const schema = new LogicSchema({
    // combined using AND
    check: [
        new DateTimeSchema({
            parse: true,
            format: 'range'
        }),
        new NumberSchema({
            unit: { from: 's', to: 'min' },
            round: { precision: 0 }
        })
    ]
});
// '2 hours 10 seconds' => 120
```

{!docs/assets/abbreviations.txt!}
