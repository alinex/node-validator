title: File

# FileSchema

An URL representing an internet address.

The following options are possible:

-   Meta Data: [title](README.md#title), [detail](README.md#detail)
-   Sanitizing: [default](README.md#default), [baseDir](file.md#basedir)
-   Validating: [allow](string.md#allow), [disallow](string.md#disallow), [type](file.md#type), [exists](file.md#exists), [readable](file.md#readable), [writable](file.md#writable), [executable](file.md#executable), [minSize](file.md#minsize), [maxSize](file.md#maxsize), [minCTime](file.md#minctime), [maxCTime](file.md#maxctime), [minMTime](file.md#minmtime), [maxMTime](file.md#maxmtime), [minATime](file.md#minatime), [maxATime](file.md#maxatime)
-   Revert: [raw](README.md#raw)

!!! info

    **Home Directory**
    
    The home directory shortcut `~` like used in UNIX systems is supported here. If the filename starts with it, it will be treated as the local home directory of the current user.

    Use the `baseDir` option to resolve it. The file test operations will support it, too but keep the value as it is.

    If it is not resolved you can always do so yourself using `value.replace(/^~/, require('os').homedir())`

## Specific Options

### baseDir

If not set or if this directory is relative the base will be the current working directory.
Which mostly is the directory the application is started from.

```js
const schema = new FileSchema({ baseDir: '/data' });
// 'test' => '/data/test'
```

### exists

Check if the file really exists. Using multiple access checks is the same as using only the highest order (write > read > exists).
It is also possible that a location really exists but is not visible to the current process, so it is assumed as non-existent.

```js
const schema = new FileSchema({ exists: true });
```

### readable

Check if the file is accessible for reading. Using multiple access checks is the same as using only the highest order (write > read > exists).

```js
const schema = new FileSchema({ readable: true });
```

### writable

Check if the file is accessible for writing. Using multiple access checks is the same as using only the highest order (write > read > exists).

```js
const schema = new FileSchema({ writable: true });
```

### executable

Check if the file can be executed by the current process.

```js
const schema = new FileSchema({ executable: true });
```

### type

Check the concrete file type which may be one of the following:

-   `block` - block device
-   `character` - character device
-   `dir` - directory
-   `fifo` - FIFO pipe
-   `file` - normal files
-   `socket` - unix socket
-   `link` - softlink

```js
const schema = new FileSchema({ type: 'dir' });
```

### minSize

Also the size (in bytes) of the file can be checked:

```js
const schema = new FileSchema({ minSize: 100 });
// more than 100B
```

### maxSize

Also the size (in bytes) of the file can be checked:

```js
const schema = new FileSchema({ maxSize: 1024 * 1024 });
// not more than 1MB
```

### minCTime

And at last the inode times may be validated. This can be done with min and max constraints given as concrete dates or as seconds ago. The file date has to be newer than minXXX or older tan maxXXX.

This will check that the file creation time is at or after the given time.

```js
const schema = new FileSchema({ minCTime: 60 * 60 * 24 * 7 }); // in seconds ago
const schema = new FileSchema({ minCTime: new Date('2018-01-01') });
```

### maxCTime

This will check that the file creation time is at or before the given time.

```js
const schema = new FileSchema({ maxCTime: 60 * 60 * 24 * 7 }); // in seconds ago
const schema = new FileSchema({ maxCTime: new Date('2018-01-01') });
```

### minMTime

This will check that the last file modification time is at or before the given time.

```js
const schema = new FileSchema({ minMTime: 60 * 60 * 24 * 7 }); // in seconds ago
const schema = new FileSchema({ minMTime: new Date('2018-01-01') });
```

### maxMTime

This will check that the last file modification time is at or after the given time.

```js
const schema = new FileSchema({ maxMTime: 60 * 60 * 24 * 7 }); // in seconds ago
const schema = new FileSchema({ maxMTime: new Date('2018-01-01') });
```

### minATime

This will check that the last file access time is at or before the given time.

```js
const schema = new FileSchema({ minATime: 60 * 60 * 24 * 7 }); // in seconds ago
const schema = new FileSchema({ minATime: new Date('2018-01-01') });
```

### maxATime

This will check that the last file access time is at or after the given time.

```js
const schema = new FileSchema({ maxATime: 60 * 60 * 24 * 7 }); // in seconds ago
const schema = new FileSchema({ maxATime: new Date('2018-01-01') });
```

{!docs/assets/abbreviations.txt!}
