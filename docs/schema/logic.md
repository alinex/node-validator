title: Logic

# LogicSchema

This is not a new data type but a logical wrapper around other types.

It allows you to combine different schema settings using different logical operators. Giving
you the possibility to define alternatives or put different schema definitions in queue together like String sanitization with number conversion.

All these can be achieved by combining the following options:

-   Meta Data: [title](README.md#title), [detail](README.md#detail)
-   Sanitizing: [default](README.md#default)
-   Validating: [if](logic.md#if)
-   Subchecks: [check](logic.md#check), [else](logic.md#else)
-   Revert: [raw](README.md#raw)

At least a `check` or `if` + `else` definition is needed.

## Specific Options

Here all the options are working together.

### if

Here you may give an initial check which will decide which type of check to run:

-   if this succeeds the normal `check` definition is used
-   and if it failed the `else` definition is used

Not both checks have to be there.

```js
const schema = new LogicSchema({
    if: new NumberSchema(),
    check: new NumberSchema({ min: 100 }),
    else: new StringSchema({ allow: ['googillion'] })
});
// if a number is given, it has to be over 100
// else the text 'googillion' is also possible
```

Some other scenarios are using only check-block:

```js
const schema = new LogicSchema({
    if: schema1,
    check: schema2
});
// this will fail if one of the abiove fails and is the same as:
const schema = new LogicSchema({
    check: [schema1({ raw: true }), schema2]
});
```

Or only the else-block:

```js
const schema = new LogicSchema({
    if: schema1,
    else: schema2
});
// this will fail if schema1 succeeds, so you can make a negation easily using:
const schema = new LogicSchema({
    if: schema1,
    else: new AnySchema() // always succeed
});
```

### operator

Two separate operators are possible, which will control the processing for multiple schema checks:

-   `AND` - This is the **default** and will validate each given schema definition after the other. It only succeeds if all of them succeeds. Each of them work on the same value which maybe further changed by the next one.
-   `OR` - Here all schema definitions are checked in parallel on the raw value. It fails only if all checks failed and the result value used is from the first successful (in order) check.

### check

One or multiple schema checks. If an if-condition is given this is only evaluated after success.
See also the operator setting above which will control how to work with a list of schema checks.

```js
const schema = new LogicSchema({
    check: schema1
});
// without an if statement this is the same as only using schema1 check directly
```

```js
const schema = new LogicSchema({
    check: [schema1, schema2]
});
// here both need to succeed
```

!!! Attention

    If multiple checks are used with `and` logic, it will work like a waterfall seriously. This means that the second check will get the result from the first. You may prevent this using the `raw: true` option to don't forward the transformed data element to the next validation.

    This is often a problem in combination with multiple array checks using filters.

### else

This is the same as `check` but only evaluated after the if-condition failed.

```js
const schema = new LogicSchema({
    if: schema1,
    else: [schema2, schema3]
});
```

{!docs/assets/abbreviations.txt!}
