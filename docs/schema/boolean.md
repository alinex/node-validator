title: Boolean

# BooleanSchema

This type is a possibility to store flag like true/false values.

The following options are possible:

-   Meta Data: [title](README.md#title), [detail](README.md#detail)
-   Sanitizing: [default](README.md#default), [truthy](boolean.md#truthy), [falsy](boolean.md#falsy), [insensitive](boolean.md#insensitive), [tolerant](boolean.md#tolerant)
-   Validating: [value](boolean.md#value)
-   Sanitizing: [format](boolean.md#format)
-   Revert: [raw](README.md#raw)

## Specific Options

### truthy

With these multiple values can be set which are interpreted as true. A real boolean value is always used as it is.

```js
const schema = new BooleanSchema({ truthy: ['y', 'j'] });
// 'y' => true
```

### falsy

With these multiple values can be set which are interpreted as false. A real boolean value is always used as it is.

```js
const schema = new BooleanSchema({ falsy: ['n'] });
// 'n' => false
```

### insensitive

This makes only sense together with truthy and/or falsy and will match strings case insensitive.

```js
const schema = new BooleanSchema({
    truthy: ['y', 'j'],
    falsy: ['n'],
    insensitive: true
});
// 'Y' => true
// 'N' => false
```

### tolerant

This is equal to set the following values:

    truthy: 1, '1', 'true', 'on', 'y', 'yes', '+', 'x'
    falsy: 0, '0', 'false', 'off', 'n', 'no', '-'
    insensitive: true

But it will also take into account the translated values and works insensitive.

```js
const schema = new BooleanSchema({ tolerant: true });
// 'Y' => true
// 'N' => false
```

### value

If this is set a concrete value has to be given.

```js
const schema = new BooleanSchema({ value: true });
```

This is the same as using [AnySchema](any.md) with allow:

```js
const schema = new AnySchema({ allow: [true] });
```

But it makes sense to prefer BooleanSchema especially if other options like `truthy`, `falsy`, `tolerant` and `insensitive` are used.

### format

To specify the value returned for true and false call this method with the values used for both.

```js
const schema = new BooleanSchema({ format: ['X', '-'] });
// true => 'X'
// false => '-'
```

{!docs/assets/abbreviations.txt!}
