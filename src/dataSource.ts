import AnySchema from './type/any';
import ArraySchema from './type/array';
import BooleanSchema from './type/boolean';
import NumberSchema from './type/number';
import ObjectSchema from './type/object';
import RegExpSchema from './type/regexp';
import StringSchema from './type/string';

export default new ObjectSchema({
  item: {
    source: new StringSchema(),
    options: new ObjectSchema({
      item: {
        proxy: new StringSchema(),
        httpMethod: new StringSchema(),
        httpHeader: new ArraySchema({ item: { '*': new StringSchema()} }),
        httpData: new StringSchema(),
        ignoreError: new BooleanSchema(),
        privateKey: new StringSchema(),
        passphrase: new StringSchema(),
        tail: new NumberSchema(),
        compression: new StringSchema({
          allow: ['gzip', 'brotli', 'bzip2', 'lzma', 'tar', 'tgz', 'tbz2', 'tlz', 'zip']
        }),
        compressionLevel: new NumberSchema(),
        format: new StringSchema({
          allow: ['bson', 'coffee', 'cson', 'csv', 'ini', 'js', 'json',
          'msgpack', 'properties', 'toml', 'xml', 'yaml']
        }),
        pattern: new RegExpSchema(),
        records: new BooleanSchema(),
        module: new BooleanSchema(),
        rootName: new StringSchema()
      },
      denyUndefined: true
    }),
    data: new AnySchema(),
    base: new StringSchema(),
    depend: new ArraySchema({ item: { '*': new StringSchema() } }),
    alternate: new ArraySchema({ item: { '*': new StringSchema() } }),
    object: new StringSchema({ allow: ['combine', 'replace', 'missing', 'alternate'] }),
    array: new StringSchema({
      allow: ['concat', 'replace', 'overwrite', 'combine']
    }),
    pathObject: new ObjectSchema({
      item: { '*': new StringSchema({ allow: ['combine', 'replace', 'missing', 'alternate'] }) }
    }),
    pathArray: new ObjectSchema({
      item: {
        '*': new StringSchema({
          allow: ['concat', 'replace', 'overwrite', 'combine']
        })
      }
    }),
    clone: new BooleanSchema(),
    allowFlags: new BooleanSchema(),
    map: new BooleanSchema(),
  },
  denyUndefined: true
})
