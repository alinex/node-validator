const i18next = require('i18next')
import { readdirSync, lstatSync } from 'fs'
import Backend from 'i18next-fs-backend'
import { join } from 'path'
import CLILanguageDetector from 'i18next-cli-language-detector'
import moment from 'moment-timezone'

const i18n = i18next.createInstance()

i18n
  .use(Backend)
  .use(CLILanguageDetector)
  .init({
    // debug: true,
    // updateFiles: true,
    fallbackLng: 'en',
    initImmediate: false,
    preload: readdirSync(join(__dirname, '../data/locales')).filter((fileName: string) => {
      const joinedPath = join(join(__dirname, '../data/locales'), fileName)
      const isDirectory = lstatSync(joinedPath).isDirectory()
      return isDirectory
    }),
    ns: 'core',
    defaultNS: 'core',
    backend: {
      loadPath: join(__dirname, '../data/locales/{{lng}}/{{ns}}.yml')
    },
    interpolation: {
      escapeValue: false,
      format: function (value: any, format: string, lng: string, options: any) {
        const f = format.split(/\s+/)
        if (f[0] === 'percent') return (value * 100) + '%'
        else if (f[0] === 'uppercase') return value.toString().toUpperCase()
        else if (f[0] === 'lowercase') return value.toString().toLowerCase()
        else if (value instanceof Date) {
          const d = moment(value)
          d.locale(lng)
          return d.format(format)
        }
        return value
      }
    }
  })

// export i18n
// export default i18n
export function t (keys: string | string[], options?: any) {
  return i18n.t(keys, options)
}
export function T (lang: string, ns?: string) {
  return i18n.getFixedT(lang, ns)
}
export function language () {
  return i18n.language
}
export function loadNamespace (ns: string) {
  return i18n.loadNamespaces(ns)
}

// console.log(t('test'))
// const x = T('en')
// console.log(x('test'))
//
// loadNamespace('b')
// console.log(x('b:test'))
