import { inspect } from 'util';

import DataStore from '@alinex/datastore';
import Workbench from './workbench';

const CACHE_TIME = 60000;

export class Reference {
    public source: string;
    public filter: string;

    constructor(source: string, filter = '') {
        this.source = source;
        this.filter = filter;
    }

    [inspect.custom](depth: number, options: any): string {
        const inner = this.source + (this.filter ? ' -> ' + this.filter : '');
        return `${options.stylize(this.constructor.name, 'class')} {${inner}}`;
    }

    public async any(work: Workbench): Promise<any> {
        if (!work.ref[this.source]) work.ref[this.source] = new DataStore({ source: this.source });
        const ref = work.ref[this.source];
        if (ref.source) await ref.reload(CACHE_TIME);
        return ref.get(this.filter);
    }

    public async array(work: Workbench): Promise<any[]> {
        const data = await this.any(work);
        return Array.isArray(data) ? data : [data];
    }

    public async stringArray(work: Workbench): Promise<string[]> {
        const data = await this.any(work);
        let res: string[];
        if (Array.isArray(data)) {
            res = data.map(e => e.toString());
        } else res = [data.toString()];
        return res;
    }
}

export default Reference;
