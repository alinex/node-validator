import { readFileSync } from 'fs';
import * as yargs from 'yargs';
import core from '@alinex/core';
import { DataStore } from '@alinex/datastore';
import { formats } from '@alinex/datastore/lib/format';
import { compressions } from '@alinex/datastore/lib/compression';

import Validator from '.';

// Support quiet mode through switch
let quiet = false;
for (const a of ['--get-yargs-completions', 'bashrc', '-q', '--quiet']) {
    if (process.argv.includes(a)) quiet = true;
}

if (!quiet) console.log(core.logo('Alinex Validator'));

process.on('uncaughtException', err => core.exit(err, 1));
process.on('unhandledRejection', (err: any) => core.exit(err, 1));

yargs
    .env('VALIDATOR')
    .usage('Usage: $0 [options]')
    // examples
    .example('$0 -i /etc/my.json -s my-schema.js', 'check data structure')
    .example('$0 -i /etc/my.json -s my-schema.js -o /etc/my.yaml', 'transform data structure')
    .example('$0 definition -i my.json -s my-schema.js', 'use input structure defined in file')
    // options
    .option('input', {
        alias: 'i',
        describe: 'input URI to read from',
        type: 'string',
        group: 'Input Format Options:'
    })
    .option('input-format', {
        describe: 'format used to parse input',
        choices: formats,
        group: 'Input Format Options:'
    })
    .option('input-compression', {
        describe: 'compression method to be used',
        choices: compressions,
        group: 'Input Format Options:'
    })
    .option('records', {
        describe: 'flag to read the CSV always as with records',
        type: 'boolean',
        group: 'Input Format Options:'
    })
    .option('filter', {
        alias: 'f',
        describe: 'filter rule to modify data structure on read',
        type: 'string',
        group: 'Input Format Options:'
    })
    .option('schema', {
        alias: 's',
        describe: 'schema definition file',
        type: 'string',
        group: 'Input Format Options:',
        demandOption: true
    })
    .option('output', {
        alias: 'o',
        describe: 'output URI to write to',
        type: 'string',
        group: 'Output Format Options:'
    })
    .option('ouput-format', {
        describe: 'format used to transform for output',
        choices: formats,
        group: 'Output Format Options:'
    })
    .option('output-compression', {
        describe: 'compression method to be used',
        choices: compressions,
        group: 'Output Format Options:'
    })
    .option('compression-level', {
        describe: 'compression level 0 (no compression) to 9 (best compression, default)',
        type: 'number',
        group: 'Output Format Options:'
    })
    .option('module', {
        describe: 'use module format in storing JavaScript, to load it using require or import',
        type: 'boolean',
        group: 'Output Format Options:'
    })
    .option('rootName', {
        describe: 'root element name in formatting as XML',
        type: 'string',
        group: 'Output Format Options:'
    })
    .option('sftp-privatekey', {
        describe: 'private key file',
        type: 'string',
        group: 'Protocol Options:'
    })
    .option('sftp-passphrase', {
        describe: 'passphrase for private key, if needed',
        type: 'string',
        group: 'Protocol Options:'
    })
    .option('quiet', {
        alias: 'q',
        describe: 'only output result',
        type: 'boolean'
    })
    .command('*', 'work with single input')
    .command('definition', 'use input as definition (allow multiple sources)')
    .wrap(yargs.terminalWidth())
    .help()
    .completion('bashrc-script', false)
    .strict()
    .epilog('Copyright Alexander Schilling 2014-2021');

main(yargs.argv);

async function main(args: any) {
    if (args['sftp-privatekey']) args['sftp-privatekey'] = readFileSync(args['sftp-privatekey']);
    // load data
    const ds = new DataStore();
    if (args.input) {
        await ds.load({ source: args.input });
    } else {
        if (!args['input-format']) throw 'No --input-format for STDIN defined.';
        await ds.load({
            source: 'file:/dev/stdin',
            options: {
                privateKey: args['sftp-privatekey'],
                passphrase: args['sftp-passphrase'],
                format: args['input-format'],
                compression: args['input-compression'],
                records: args.records
            }
        });
    }
    // definition calls
    if (args._.length && args._[0] === 'definition') {
        if (!Array.isArray(ds.data)) throw new Error('The loaded definition of input is no array!');
        await ds.load(...ds.data);
    }
    // validate
    const val = await Validator.datastore(await import(args.schema), undefined, ds);
    // filter
    const dt = new DataStore();
    dt.data = val.get(args.filter);
    // output
    if (args.output) {
        try {
            await dt.save({
                source: args.output,
                options: {
                    privateKey: args['sftp-privatekey'],
                    passphrase: args['sftp-passphrase'],
                    format: args['output-format'],
                    compression: args['output-compression'],
                    module: args.module,
                    rootName: args.rootName
                }
            });
            if (!args.quiet) console.log(`Transformed ${ds.source} to ${dt.source}.`);
        } catch (err) {
            core.exit(err, 2);
        }
    } else {
        const format = args['output-format'] || 'js';
        if (!args.quiet) {
            console.log(`Data of ${ds.source} formatted as ${format}:`);
            console.log();
        }
        console.log((await dt.format('file:/dev/stdout', { format: format })).toString());
    }
}
