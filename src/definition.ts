import * as Builder from './schema';

// Naming convention:
// type_xxx - basic types
// ref_xxx - reference types
// fields_xxx - fields for schema type
// def_xxx - schema type definition

// basic types

const type_boolean = new Builder.BooleanSchema({ tolerant: true });
const type_any = new Builder.AnySchema();
const type_string = new Builder.StringSchema();
const type_string_array = new Builder.ArraySchema({ item: { '*': type_string }, min: 1 });
const type_regexp = new Builder.RegExpSchema();
const type_datetime = new Builder.DateTimeSchema({ parse: true });
const type_number = new Builder.NumberSchema();
const type_integer_pos = new Builder.NumberSchema({ integer: { unsigned: true }, sanitize: true });
const type_integer_pos_short = new Builder.NumberSchema({
  integer: { type: 'short', unsigned: true }
});
const type_bytes = new Builder.NumberSchema({
  unit: { from: 'B', to: 'B' },
  integer: { unsigned: true },
  sanitize: true
});
const type_age = new Builder.LogicSchema({
  operator: 'or',
  check: [type_datetime, type_integer_pos]
});
const type_regexp_string = new Builder.LogicSchema({
  operator: 'or',
  check: [type_regexp, type_string]
});

// is any schema definition

const def_all = new Builder.LogicSchema(); // will be set later because of recursion

// references

const ref = new Builder.ObjectSchema({
  item: {
    SCHEMA_TYPE: new Builder.StringSchema({ lowerCase: true, allow: ['reference'] }),
    source: new Builder.StringSchema({ allow: [/:\/\//, /^data:(inline|context)$/] }),
    filter: type_string
  },
  mandatory: ['SCHEMA_TYPE', 'source']
});
const ref_any = new Builder.LogicSchema({
  operator: 'or',
  check: [ref, type_any]
});
const ref_any_array = new Builder.LogicSchema({
  operator: 'or',
  check: [ref, new Builder.ArraySchema({ item: { '*': ref_any }, min: 1 })]
});
const ref_array = new Builder.LogicSchema({
  operator: 'or',
  check: [ref, new Builder.ArraySchema()]
});
const ref_boolean = new Builder.LogicSchema({
  operator: 'or',
  check: [ref, type_boolean]
});
const ref_datetime = new Builder.LogicSchema({
  operator: 'or',
  check: [ref, type_datetime]
});
const ref_number = new Builder.LogicSchema({
  operator: 'or',
  check: [ref, new Builder.NumberSchema()]
});
const ref_number_array = new Builder.LogicSchema({
  operator: 'or',
  check: [ref, new Builder.ArraySchema({ item: { '*': ref_number }, min: 1 })]
});
const ref_regexp = new Builder.LogicSchema({
  operator: 'or',
  check: [ref, type_regexp]
});
const ref_string = new Builder.LogicSchema({
  operator: 'or',
  check: [ref, type_string]
});
const ref_string_array = new Builder.LogicSchema({
  operator: 'or',
  check: [ref, new Builder.ArraySchema({ item: { '*': ref_string }, min: 1 })]
});
const ref_string_re = new Builder.LogicSchema({
  operator: 'or',
  check: [ref, type_string, type_regexp]
});
const ref_string_re_array = new Builder.LogicSchema({
  operator: 'or',
  check: [ref, new Builder.ArraySchema({ item: { '*': ref_string_re }, min: 1 })]
});

// base schema

const fields_base: any = {
  title: new Builder.StringSchema({ trim: true, min: 3 }),
  detail: new Builder.StringSchema({ trim: true, min: 3 }),
  raw: type_boolean
};

// schema definitions

const fields_any = {
  ...fields_base,
  SCHEMA_TYPE: new Builder.StringSchema({ lowerCase: true, allow: ['any'] }),
  default: ref_any,
  allow: ref_any_array,
  disallow: ref_any_array
};
const def_any = new Builder.ObjectSchema({
  title: 'AnySchema',
  item: fields_any,
  mandatory: ['SCHEMA_TYPE']
});

const fields_array = {
  ...fields_base,
  SCHEMA_TYPE: new Builder.StringSchema({ lowerCase: true, allow: ['array'] }),
  default: ref_array,
  split: new Builder.LogicSchema({
    operator: 'or',
    check: [new Builder.StringSchema({ min: 1 }), type_regexp]
  }),
  makeArray: type_boolean,
  removeDuplicate: type_boolean,
  filter: new Builder.LogicSchema({
    operator: 'or',
    check: [type_boolean, def_all]
  }),
  shuffle: type_boolean,
  sort: new Builder.LogicSchema({
    operator: 'or',
    check: [type_boolean, new Builder.StringSchema({ allow: ['alpha', 'num'] })]
  }),
  reverse: type_boolean,
  item: new Builder.ObjectSchema({
    item: { index: def_all }
  }),
  unique: type_boolean,
  min: type_integer_pos,
  max: type_integer_pos,
  allow: def_all,
  disallow: def_all
};
const def_array = new Builder.ObjectSchema({
  title: 'ArraySchema',
  item: fields_array,
  mandatory: ['SCHEMA_TYPE']
});

const fields_boolean = {
  ...fields_base,
  SCHEMA_TYPE: new Builder.StringSchema({ lowerCase: true, allow: ['boolean'] }),
  default: ref_boolean,
  truthy: new Builder.ArraySchema({
    item: { '*': type_any }
  }),
  falsy: new Builder.ArraySchema({
    item: { '*': type_any }
  }),
  insensitive: type_boolean,
  tolerant: type_boolean,
  format: new Builder.ArraySchema({
    item: { '0': type_any, '1': type_any },
    min: 2,
    max: 2
  }),
  allow: type_boolean,
  disallow: type_boolean
};
const def_boolean = new Builder.ObjectSchema({
  title: 'BooleanSchema',
  item: fields_boolean,
  mandatory: ['SCHEMA_TYPE']
});

const list_timezones = [
  'local',
  'A',
  'ACDT',
  'ACST',
  'ADT',
  'AEDT',
  'AEST',
  'AFT',
  'AKDT',
  'AKST',
  'ALMT',
  'AMST',
  'AMT',
  'ANAST',
  'ANAT',
  'AQTT',
  'ART',
  'AST',
  'AWDT',
  'AWST',
  'AZOST',
  'AZOT',
  'AZST',
  'AZT',
  'B',
  'BNT',
  'BOT',
  'BRST',
  'BRT',
  'BST',
  'BTT',
  'C',
  'CAST',
  'CAT',
  'CCT',
  'CDT',
  'CEST',
  'CET',
  'CHADT',
  'CHAST',
  'CKT',
  'CLST',
  'CLT',
  'COT',
  'CST',
  'CVT',
  'CXT',
  'ChST',
  'D',
  'DAVT',
  'E',
  'EASST',
  'EAST',
  'EAT',
  'ECT',
  'EDT',
  'EEST',
  'EET',
  'EGST',
  'EGT',
  'EST',
  'ET',
  'F',
  'FJST',
  'FJT',
  'FKST',
  'FKT',
  'FNT',
  'G',
  'GALT',
  'GAMT',
  'GET',
  'GFT',
  'GILT',
  'GMT',
  'GST',
  'GYT',
  'H',
  'HAA',
  'HAC',
  'HADT',
  'HAE',
  'HAP',
  'HAR',
  'HAST',
  'HAT',
  'HAY',
  'HKT',
  'HLV',
  'HNA',
  'HNC',
  'HNE',
  'HNP',
  'HNR',
  'HNT',
  'HNY',
  'HOVT',
  'I',
  'ICT',
  'IDT',
  'IOT',
  'IRDT',
  'IRKST',
  'IRKT',
  'IRST',
  'IST',
  'JST',
  'K',
  'KGT',
  'KRAST',
  'KRAT',
  'KST',
  'KUYT',
  'L',
  'LHDT',
  'LHST',
  'LINT',
  'M',
  'MAGST',
  'MAGT',
  'MART',
  'MAWT',
  'MDT',
  'MESZ',
  'MEZ',
  'MHT',
  'MMT',
  'MSD',
  'MSK',
  'MST',
  'MUT',
  'MVT',
  'MYT',
  'N',
  'NCT',
  'NDT',
  'NFT',
  'NOVST',
  'NOVT',
  'NPT',
  'NST',
  'NUT',
  'NZDT',
  'NZST',
  'O',
  'OMSST',
  'OMST',
  'P',
  'PDT',
  'PET',
  'PETST',
  'PETT',
  'PGT',
  'PHOT',
  'PHT',
  'PKT',
  'PMDT',
  'PMST',
  'PONT',
  'PST',
  'PT',
  'PWT',
  'PYST',
  'PYT',
  'Q',
  'R',
  'RET',
  'S',
  'SAMT',
  'SAST',
  'SBT',
  'SCT',
  'SGT',
  'SRT',
  'SST',
  'T',
  'TAHT',
  'TFT',
  'TJT',
  'TKT',
  'TLT',
  'TMT',
  'TVT',
  'U',
  'ULAT',
  'UTC',
  'UYST',
  'UYT',
  'UZT',
  'V',
  'VET',
  'VLAST',
  'VLAT',
  'VUT',
  'W',
  'WAST',
  'WAT',
  'WEST',
  'WESZ',
  'WET',
  'WEZ',
  'WFT',
  'WGST',
  'WGT',
  'WIB',
  'WIT',
  'WITA',
  'WST',
  'WT',
  'X',
  'Y',
  'YAKST',
  'YAKT',
  'YAPT',
  'YEKST',
  'YEKT',
  'Z'
];
const fields_datetime = {
  ...fields_base,
  SCHEMA_TYPE: new Builder.StringSchema({ lowerCase: true, allow: ['datetime'] }),
  default: ref_datetime,
  parse: new Builder.LogicSchema({
    operator: 'or',
    check: [type_boolean, new Builder.StringSchema({ allow: ['en', 'en_GB', 'de', 'pt', 'es', 'fr', 'ja'] })]
  }),
  defaultTimezone: new Builder.StringSchema({ allow: list_timezones }),
  reference: type_datetime,
  min: type_datetime,
  max: type_datetime,
  greater: type_datetime,
  less: type_datetime,
  allow: ref_any_array,
  disallow: ref_any_array,
  format: type_string,
  timezone: new Builder.StringSchema({ allow: list_timezones }),
  locale: new Builder.StringSchema({ min: 2, allow: [/^[a-z]{2}(-[a-z]{2})?$/] })
};
const def_datetime = new Builder.ObjectSchema({
  title: 'DateTimeSchema',
  item: fields_datetime,
  mandatory: ['SCHEMA_TYPE']
});

const fields_domain = {
  ...fields_base,
  SCHEMA_TYPE: new Builder.StringSchema({ lowerCase: true, allow: ['domain'] }),
  default: ref_string,
  sanitize: type_boolean,
  min: new Builder.NumberSchema({ min: 1, max: 127 }),
  max: new Builder.NumberSchema({ min: 1, max: 127 }),
  allow: ref_string_array,
  disallow: ref_string_array,
  allowCountry: ref_string_re_array,
  disallowCountry: ref_string_re_array,
  registered: new Builder.LogicSchema({
    operator: 'or',
    check: [
      type_boolean,
      new Builder.ArraySchema({
        item: {
          '*': new Builder.StringSchema({
            allow: ['A', 'AAAA', 'ANY', 'CNAME', 'MX', 'NAPTR', 'NS', 'PTR', 'SOA', 'SRV', 'TXT']
          })
        }
      })
    ]
  }),
  punycode: type_boolean,
  resolve: type_boolean
};
const def_domain = new Builder.ObjectSchema({
  title: 'DomainSchema',
  item: fields_domain,
  mandatory: ['SCHEMA_TYPE']
});

const fields_email = {
  ...fields_base,
  SCHEMA_TYPE: new Builder.StringSchema({ lowerCase: true, allow: ['email'] }),
  default: ref_string,
  makeString: type_boolean,
  normalize: type_boolean,
  registered: type_boolean,
  allow: ref_string_re_array,
  disallow: ref_string_re_array,
  blacklist: type_boolean,
  graylist: type_boolean,
  connect: type_boolean,
  format: new Builder.StringSchema({ allow: ['address', 'displayName', 'object'] })
};
const def_email = new Builder.ObjectSchema({
  title: 'EmailSchema',
  item: fields_email,
  mandatory: ['SCHEMA_TYPE']
});

const fields_file = {
  ...fields_base,
  SCHEMA_TYPE: new Builder.StringSchema({ lowerCase: true, allow: ['file'] }),
  default: ref_string,
  baseDir: type_string,
  type: new Builder.StringSchema({
    allow: ['block', 'character', 'dir', 'fifo', 'file', 'socket', 'link']
  }),
  minSize: type_bytes,
  maxSize: type_bytes,
  minCTime: type_age,
  maxCTime: type_age,
  minMTime: type_age,
  maxMTime: type_age,
  minATime: type_age,
  maxATime: type_age,
  allow: ref_string_re_array,
  disallow: ref_string_re_array,
  exists: type_boolean,
  readable: type_boolean,
  writable: type_boolean,
  executable: type_boolean
};
const def_file = new Builder.ObjectSchema({
  title: 'FileSchema',
  item: fields_file,
  mandatory: ['SCHEMA_TYPE']
});

const fields_ip = {
  ...fields_base,
  SCHEMA_TYPE: new Builder.StringSchema({ lowerCase: true, allow: ['ip'] }),
  default: ref_string,
  lookup: type_boolean,
  map: type_boolean,
  version: new Builder.NumberSchema({ allow: [4, 6] }),
  allow: ref_string_array,
  disallow: ref_string_array,
  allowCountry: ref_string_array,
  disallowCountry: ref_string_array,
  format: new Builder.StringSchema({ allow: ['short', 'long', 'array', 'bytes'] })
};
const def_ip = new Builder.ObjectSchema({
  title: 'IPSchema',
  item: fields_ip,
  mandatory: ['SCHEMA_TYPE']
});

const fields_logic = {
  ...fields_base,
  SCHEMA_TYPE: new Builder.StringSchema({ lowerCase: true, allow: ['logic'] }),
  if: def_all,
  operator: new Builder.StringSchema({ allow: ['and', 'or'] }),
  check: new Builder.LogicSchema({
    operator: 'or',
    check: [def_all, new Builder.ArraySchema({ item: { '*': def_all }, min: 1 })]
  }),
  else: new Builder.LogicSchema({
    operator: 'or',
    check: [def_all, new Builder.ArraySchema({ item: { '*': def_all }, min: 1 })]
  })
};
const def_logic = new Builder.ObjectSchema({ item: fields_logic, mandatory: ['SCHEMA_TYPE'] });

const fields_number = {
  ...fields_base,
  SCHEMA_TYPE: new Builder.StringSchema({ lowerCase: true, allow: ['number'] }),
  default: ref_number,
  sanitize: type_boolean,
  unit: new Builder.ObjectSchema({
    item: { from: type_string, to: type_string },
    mandatory: ['from', 'to']
  }),
  round: new Builder.ObjectSchema({
    item: {
      method: new Builder.StringSchema({ allow: ['floor', 'ceil', 'arithmetic'] }),
      precision: type_integer_pos
    }
  }),
  integer: new Builder.LogicSchema({
    operator: 'or',
    check: [
      type_boolean,
      new Builder.ObjectSchema({
        item: {
          type: new Builder.StringSchema({
            allow: ['byte', 'short', 'long', 'safe', 'quad']
          }),
          unsigned: type_boolean
        }
      })
    ]
  }),
  min: type_number,
  max: type_number,
  greater: type_number,
  less: type_number,
  multipleOf: type_integer_pos,
  allow: ref_number_array,
  disallow: ref_number_array,
  ranges: new Builder.ObjectSchema({
    item: {
      '*': new Builder.ArraySchema({
        min: 2,
        max: 2,
        item: { '*': type_number }
      })
    }
  }),
  format: type_string
};
const def_number = new Builder.ObjectSchema({
  title: 'NumberSchema',
  item: fields_number,
  mandatory: ['SCHEMA_TYPE']
});

const fields_object = {
  ...fields_base,
  SCHEMA_TYPE: new Builder.StringSchema({ lowerCase: true, allow: ['object'] }),
  default: ref_any,
  flatten: new Builder.LogicSchema({ operator: 'or', check: [type_boolean, type_string] }),
  rearange: new Builder.ArraySchema({
    item: {
      '*': new Builder.ObjectSchema({
        item: { key: type_string, copyTo: type_string, moveTo: type_string, overwrite: type_boolean },
        mandatory: ['key'],
        combination: [{ xor: ['copyTo', 'moveTo'] }]
      })
    },
    min: 1
  }),
  unflatten: new Builder.LogicSchema({ operator: 'or', check: [type_boolean, type_string] }),
  item: new Builder.ObjectSchema({
    item: { '*': def_all }
  }),
  removeUndefined: type_boolean,
  denyUndefined: new Builder.LogicSchema({
    operator: 'or',
    check: [type_boolean, new Builder.ArraySchema({ item: { '*': type_regexp_string }, min: 1 })]
  }),
  forbidden: new Builder.ArraySchema({ item: { '*': type_regexp_string }, min: 1 }),
  combination: new Builder.ArraySchema({
    item: {
      '*': new Builder.ObjectSchema({
        item: {
          and: type_string_array,
          nand: type_string_array,
          or: type_string_array,
          xor: type_string_array,
          key: type_string,
          with: type_string_array,
          without: type_string_array
        },
        combination: [
          { xor: ['and', 'nand', 'or', 'xor', 'key'] },
          { key: 'with', with: ['key'] },
          { key: 'with', without: ['without'] },
          { key: 'without', with: ['key'] },
          { key: 'without', without: ['with'] }
        ]
      })
    },
    min: 1
  }),
  min: type_integer_pos,
  max: type_integer_pos
};
const def_object = new Builder.ObjectSchema({
  title: 'ObjectSchema',
  item: fields_object,
  mandatory: ['SCHEMA_TYPE']
});

const fields_port = {
  ...fields_base,
  SCHEMA_TYPE: new Builder.StringSchema({ lowerCase: true, allow: ['port'] }),
  default: new Builder.LogicSchema({
    operator: 'or',
    check: [ref, type_integer_pos_short]
  }),
  min: type_integer_pos_short,
  max: type_integer_pos_short,
  greater: type_integer_pos_short,
  less: type_integer_pos_short,
  allow: new Builder.LogicSchema({
    operator: 'or',
    check: [
      ref,
      new Builder.ArraySchema({
        item: {
          '*': new Builder.LogicSchema({
            operator: 'or',
            check: [
              ref,
              type_integer_pos_short,
              new Builder.ArraySchema({
                item: { '*': type_integer_pos_short },
                min: 2,
                max: 2
              }),
              new Builder.StringSchema({
                allow: ['system', 'registered', 'dynamic']
              })
            ]
          })
        },
        min: 1
      })
    ]
  }),
  disllow: new Builder.LogicSchema({
    operator: 'or',
    check: [
      ref,
      new Builder.ArraySchema({
        item: {
          '*': new Builder.LogicSchema({
            operator: 'or',
            check: [
              ref,
              type_integer_pos_short,
              new Builder.ArraySchema({
                item: { '*': type_integer_pos_short },
                min: 2,
                max: 2
              }),
              new Builder.StringSchema({
                allow: ['system', 'registered', 'dynamic']
              })
            ]
          })
        },
        min: 1
      })
    ]
  }),
  makeString: type_boolean
};
const def_port = new Builder.ObjectSchema({
  title: 'PortSchema',
  item: fields_port,
  mandatory: ['SCHEMA_TYPE']
});

const fields_regexp = {
  ...fields_base,
  SCHEMA_TYPE: new Builder.StringSchema({ lowerCase: true, allow: ['regexp'] }),
  default: ref_regexp,
  min: type_integer_pos,
  max: type_integer_pos
};
const def_regexp = new Builder.ObjectSchema({
  title: 'RegExpSchema',
  item: fields_regexp,
  mandatory: ['SCHEMA_TYPE']
});

const fields_string = {
  ...fields_base,
  SCHEMA_TYPE: new Builder.StringSchema({ lowerCase: true, allow: ['string'] }),
  default: ref_string,
  makeString: type_boolean,
  trim: new Builder.LogicSchema({
    operator: 'or',
    check: [type_boolean, new Builder.StringSchema({ allow: ['left', 'right', 'both'] })]
  }),
  replace: new Builder.ArraySchema({
    item: {
      '*': new Builder.ObjectSchema({
        item: {
          match: new Builder.LogicSchema({
            operator: 'or',
            check: [type_regexp, type_string]
          }),
          replace: type_string,
          hint: new Builder.StringSchema({ min: 1 })
        },
        mandatory: ['match', 'replace']
      })
    },
    makeArray: true
  }),
  alphaNum: type_boolean,
  hex: type_boolean,
  noHTML: new Builder.LogicSchema({
    operator: 'or',
    check: [type_boolean, new Builder.ArraySchema({ item: { '*': type_string } })]
  }),
  stripDisallowed: type_boolean,
  upperCase: new Builder.LogicSchema({
    operator: 'or',
    check: [type_boolean, new Builder.StringSchema({ allow: ['first'], lowerCase: true })]
  }),
  lowerCase: new Builder.LogicSchema({
    operator: 'or',
    check: [type_boolean, new Builder.StringSchema({ allow: ['first'], lowerCase: true })]
  }),
  min: type_integer_pos,
  max: type_integer_pos,
  pad: new Builder.ObjectSchema({
    item: {
      side: new Builder.StringSchema({
        allow: ['right', 'left', 'both'],
        lowerCase: true
      }),
      characters: new Builder.StringSchema({ min: 1 })
    },
    mandatory: ['side', 'characters']
  }),
  truncate: new Builder.LogicSchema({
    operator: 'or',
    check: [type_boolean, new Builder.StringSchema({ min: 1 })]
  }),
  allow: ref_string_re_array,
  disallow: ref_string_re_array
};
const def_string = new Builder.ObjectSchema({
  title: 'StringSchema',
  item: fields_string,
  mandatory: ['SCHEMA_TYPE']
});

const fields_url = {
  ...fields_base,
  SCHEMA_TYPE: new Builder.StringSchema({ lowerCase: true, allow: ['url'] }),
  default: ref_string,
  resolve: ref_string,
  allow: ref_string_re_array,
  disallow: ref_string_re_array,
  allowIP: ref_string_array,
  disallowIP: ref_string_array,
  allowCountry: ref_string_array,
  disallowCountry: ref_string_array,
  exists: type_boolean,
  format: new Builder.StringSchema({ allow: ['href', 'object'] })
};
const def_url = new Builder.ObjectSchema({
  title: 'UrlSchema',
  item: fields_url,
  mandatory: ['SCHEMA_TYPE']
});

const def_timeout = new Builder.ObjectSchema({
  title: 'Timeout (NumberSchema)',
  item: {
    SCHEMA_TYPE: new Builder.StringSchema({ lowerCase: true, allow: ['timeout'] }),
    default: new Builder.StringSchema({ min: 1 }),
    unit: new Builder.StringSchema({
      allow: ['s', 'min', 'h', 'd', 'week', 'month', 'year']
    })
  },
  mandatory: ['SCHEMA_TYPE']
});

const def_parent = new Builder.ObjectSchema({
  title: 'Indirect Object',
  item: { '*': def_all },
  min: 1
});

def_all.set({
  title: 'AnySchema',
  if: new Builder.ObjectSchema({ mandatory: ['SCHEMA_TYPE'] }),
  operator: 'or',
  check: [
    def_any,
    def_array,
    def_boolean,
    def_datetime,
    def_domain,
    def_email,
    def_file,
    def_ip,
    def_logic,
    def_number,
    def_object,
    def_port,
    def_regexp,
    def_string,
    def_url,
    // meta
    def_timeout
  ],
  else: def_parent
});

export default def_all;
