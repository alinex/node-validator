// The workbench is a working position to validate

import { inspect } from 'util';
import DataStore from '@alinex/datastore';
//import Schema from './schema'
const cloneDeep = require('clone-deep');

export default class Workbench {
    ds: DataStore; // data source
    pos: string; // position in data store
    // schema: Schema; // complete schema definition
    value: any; // current value
    ref: { [key: string]: DataStore }; // reference data
    //    value: any // current value (will change while validating)
    orig: any; // original value for reporting
    //  source: string // source path for reporting
    //  options: Object // open for enhancement
    temp: any = {}; // storage for additional data between the rules
    parent: any // parent data structure (used for references)
    key: any // key in parent used for this element
    //  root: Data // root data structure (used for references)
    //  status: Promise<any> // wait till value is checked (for references)
    //  done: Function // method used to fullfill status promise

    constructor(ds: DataStore, ref: { [key: string]: DataStore } = {}, pos = '', value: any = undefined, key: any = undefined) {
        this.ds = ds;
        this.ref = ref;
        this.pos = pos;
        this.parent = value;
        this.key = key;
        this.orig = this.value = value !== undefined ? value[key] : ds.get(pos);
    }

    [inspect.custom](depth: number, options: any): string {
        const newOptions = Object.assign({}, options, {
            depth: options.depth === null ? null : options.depth - 1
        });
        const out = { source: this.ds.source, pos: this.pos, value: this.value };
        const inner = inspect(out, newOptions).replace(/\n/g, `\n${' '.repeat(5)}`);
        return `${options.stylize(this.constructor.name, 'class')} ${inner}`;
    }

    sub(key: string | number): Workbench {
        return new Workbench(this.ds, this.ref, appendPos(this.pos, key), this.value, key);
    }

    clone(key?: string | number): Workbench {
        return new Workbench(cloneDeep(this.ds), this.ref, appendPos(this.pos, key));
    }

    done() {
      if (this.parent) this.parent[this.key] = this.value
      else this.ds.set(this.pos, this.value)
    }
}

function appendPos(pos: string | number, sub?: number | string): string {
    return (
        pos +
        (sub || sub === 0
            ? typeof sub === 'string' && sub.includes('.')
                ? `[${sub}]`
                : (pos ? '.' : '') + sub
            : '')
    );
}
