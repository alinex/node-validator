import { Schema, LogicSchema, DateTimeSchema, NumberSchema } from './schema';
import { T } from './i18n';

export function timeout(opt?: {
    default?: string;
    unit?: 's' | 'min' | 'h' | 'd' | 'week' | 'month' | 'year';
}): Schema {
    if (!opt) opt = {};
    const dateOpt: any = {
        parse: true,
        format: 'range'
    };
    if (opt.default) dateOpt.default = opt.default;
    const numberSchema = new NumberSchema({
        unit: { from: 's', to: opt.unit || 's' },
        min: 0,
        round: { precision: 0 }
    });
    return new LogicSchema({
        SCHEMA_META: { SCHEMA_TYPE: 'timeout', ...opt },
        detail: (lang) => T(lang)('meta.timeout'),
        // combined using AND
        if: new DateTimeSchema(dateOpt),
        check: [new DateTimeSchema(dateOpt), numberSchema],
        else: numberSchema
    });
}
