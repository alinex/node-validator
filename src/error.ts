import { inspect } from 'util';

import Schema from './schema';
import BaseSchema from './type/base';
import Workbench from './workbench';
import { T } from './i18n';

class ValidationError extends Error {
  schema: BaseSchema | Schema;
  work: Workbench;

  constructor(schema: BaseSchema | Schema, work: Workbench, msg: string) {
    super(msg);
    this.schema = schema;
    this.work = work;
  }

  [inspect.custom](): string {
    //        return `Error at ${this.work.pos.replace(/\[/g, "'").replace(/\](.)/g, "'.$1")} (${
    const source = !this.work.ds.source ? ''
      : typeof this.work.ds.source === 'string' ? ` (${this.work.ds.source})`
        : ` (${this.work.ds.source.map(e => e.source).join(', ')})`;
    return `Error at ${this.work.pos || '/'}${source}: ${this.message.trim()}\nFound value: ${inspect(this.work.orig)}`;
  }

  toString(lang: string = 'en') {
    const t = T(lang)
    const source = !this.work.ds.source ? ''
      : typeof this.work.ds.source === 'string' ? ` (${this.work.ds.source})`
        : ` (${this.work.ds.source.map(e => e.source).join(', ')})`;
    return t('error.at', { pos: `${this.work.pos || '/'}${source}`, message: this.message.trim() });
  }

  describe(lang: string = 'en') {
    const t = T(lang)
    return t('describe.schema', { schema: this.schema.describe(1, undefined, lang) });
  }

}

export default ValidationError;
