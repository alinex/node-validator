import { inspect } from 'util';
import Debug from 'debug';
import { clone } from '@alinex/data';

import Workbench from '../workbench';
import Reference from '../reference';
import { T, language } from '../i18n';
// import ValidationError from '../error';

const DESCRIBE_DEPTH = 5;

type fnT = (lang: string) => string;
export interface Options {
  title?: string | fnT;
  detail?: string | fnT;
  default?: any | Reference;
  raw?: boolean;
  SCHEMA_META?: any;
}

export class BaseSchema {
  private debug: debug.Debugger;
  protected opt: Options;
  // rules
  protected describer: any[];
  protected validator: any[];

  constructor(opt: Options) {
    this.debug = Debug(`validator:${this.constructor.name.replace(/Schema/, '').toLowerCase()}`);
    if (Object.keys(opt).length) this.check(opt);
    this.opt = opt;
    // rules
    this.describer = [
      this.defaultDescriber, // 0: default
      this.rawDescriber // 1: raw
    ];
    this.validator = [
      this.defaultValidator, // 0: default
      this.rawValidator // 1: raw
    ];
  }

  public set (opt: Options) {
    if (Object.keys(opt).length) this.check(opt);
    this.opt = opt;
  }

  get id (): string {
    return this.constructor.name;
  }

  public getTitle (lang: string = language()): string {
    if (!this.opt.title) return this.constructor.name
    if (typeof this.opt.title === 'string') return this.opt.title
    return this.opt.title(lang)
  }
  public getDetail (lang: string = language()): string | undefined {
    if (!this.opt.detail) return undefined
    if (typeof this.opt.detail === 'string') return this.opt.detail
    return this.opt.detail(lang)
  }

  [inspect.custom] (depth: number, options: any): string {
    const newOptions = Object.assign({}, options, {
      depth: options.depth === null ? null : options.depth - 1
    });
    const inner = inspect(this.opt, newOptions).replace(/\n/g, `\n${' '.repeat(5)}`);
    return `${options.stylize(this.constructor.name, 'class')} ${inner}`;
  }

  protected check (opt: Options) {
    // option validity
    if (opt.title && !['string', 'function'].includes(typeof opt.title))
      throw new Error(`Option title needs a string value or i18n function, but ${inspect(opt.title)} was given.`);
    if (opt.detail && !['string', 'function'].includes(typeof opt.detail))
      throw new Error(`Option detail needs a string value or i18n function, but ${inspect(opt.detail)} was given.`);
    if (opt.raw && typeof opt.raw !== 'boolean')
      throw new Error(`Option raw needs a boolean value, but ${inspect(opt.raw)} was given.`);
  }

  /**
   * Export schema as tree data structure.
   */
  export (): any {
    if (this.opt.SCHEMA_META) return this.opt.SCHEMA_META;
    const res: any = clone(this.opt);
    res.SCHEMA_TYPE = this.constructor.name.replace(/Schema$/, '');
    if (res.default !== undefined) res.default = this.exportReference(res.default);
    // simplification (reversed in schema.importTree())
    ['allow', 'disallow'].forEach(k => {
      if (res[k] && res[k].length === 1) res[k] = res[k][0];
    });
    return res;
  }
  protected exportReference (res: any): any {
    if (res instanceof Reference)
      return {
        SCHEMA_TYPE: 'Reference',
        source: res.source,
        filter: res.filter
      };
    return res;
  }

  describe (depth = DESCRIBE_DEPTH, path = '', lang: string = language()): string {
    let msg = '';
    if (!path.length) {
      msg = this.getTitle(lang);
      // create message using the different rules
      if (--depth < 0) return msg + '...'; // stop if depth already reached
      msg += '\n';
      if (this.opt.detail) msg += this.getDetail(lang) + '\n';
    }
    return (
      msg +
      this.describer
        .map(rule => (!path.length || rule.length === 2 ? rule.call(this, depth, path, lang) : ''))
        .filter(e => e)
        .join(' ')
        .replace(/\n (\w)/g, '\n$1')
        .trim()
    ).replace(/\n/g, '\n    ');
  }

  validate (work: Workbench, lang: string = language()): Promise<void> {
    if (this.debug.enabled) {
      this.debug(this);
      this.debug(work);
    }
    // validate
    let p = Promise.resolve();
    this.validator.forEach(rule => {
      p = p.then(() => rule.call(this, work, lang));
    });
    // collect result
    return p
      .then(() => {
        work.done();
        this.debug(`=> ${inspect(work.value)}`);
      })
      .catch(err => {
        if (this.debug.enabled) {
          if (err) this.debug(`=> ${inspect(err)}`);
          else {
            work.done();
            this.debug(`=> ${inspect(work.value)}`);
          }
        }
        return err ? Promise.reject(err) : Promise.resolve();
      });
  }

  private defaultDescriber (depth: number, path: string, lang: string) {
    const t = T(lang)
    const opt = <Options>this.opt;
    if (opt.default) return t('describe.default', { default: inspect(opt.default) });
  }
  private async defaultValidator (work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    if (typeof opt.default !== 'undefined') {
      if (
        work.value === '' ||
        work.value === null ||
        (Array.isArray(work.value) && !work.value.length) ||
        (Object.keys(work.value).length === 0 && work.value.constructor === Object)
      ) {
        if (opt.default instanceof Reference) work.value = await opt.default.any(work);
        else work.value = opt.default;
      }
    }
  }

  private rawDescriber (depth: number, path: string, lang: string) {
    const t = T(lang)
    const opt = <Options>this.opt;
    if (opt.raw) return t('describe.raw');
  }
  private async rawValidator (work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    if (opt.raw) work.value = work.orig;
  }
}

export default BaseSchema;
