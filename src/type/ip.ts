import { inspect, promisify } from 'util';
import * as ipaddr from 'ipaddr.js';
import * as dns from 'dns';
import DataStore from '@alinex/datastore';
// geoip-lite on demand

import { AnySchema, Options as AnyOptions } from './any';
import Workbench from '../workbench';
import Reference from '../reference';
import ValidationError from '../error';
import StringSchema from './string';
import { T, loadNamespace } from '../i18n';

loadNamespace('ip')

const lookup = promisify(dns.lookup);

const specialRanges: { [key: string]: string[] } = {
  unspecified: [
    '0.0.0.0/8',
    '0::/128' // RFC4291, here and after
  ],
  broadcast: ['255.255.255.255/32'],
  multicast: [
    '224.0.0.0/4', // RFC3171
    'ff00::/8'
  ],
  linklocal: [
    '169.254.0.0/16', // RFC3927
    'fe80::/10'
  ],
  loopback: ['127.0.0.0/8', '::1/128'], // RFC5735
  private: [
    '10.0.0.0/8', // RFC1918
    '172.16.0.0/12', // RFC1918
    '192.168.0.0/16' // RFC1918
  ],
  // Reserved and testing-only ranges; RFCs 5735, 5737, 2544, 1700
  reserved: [
    '192.0.0.0/24',
    '192.88.99.0/24',
    '198.51.100.0/24',
    '203.0.113.0/24',
    '240.0.0.0/4',
    '2001:db8::/32' // RFC4291
  ],
  uniquelocal: ['fc00::/7'],
  ipv4mapped: ['::ffff:0:0/96'],
  rfc6145: ['::ffff:0:0:0/96'], // RFC6145
  rfc6052: ['64:ff9b::/96'], // RFC6052
  '6to4': ['2002::/16'], // RFC3056
  teredo: ['2001::/32'], // RFC6052, RFC6146
  special: [] // fill up with all special ranges
};
for (const key of Object.keys(specialRanges)) {
  if (key !== 'special') specialRanges.special.concat(specialRanges[key]);
}

export interface Options extends AnyOptions {
  default?: string | Reference;
  lookup?: boolean;
  map?: boolean;
  version?: 4 | 6;
  allow?: (string | Reference)[] | Reference;
  disallow?: (string | Reference)[] | Reference;
  allowCountry?: (string | Reference)[] | Reference;
  disallowCountry?: (string | Reference)[] | Reference;
  format?: 'short' | 'long' | 'array' | 'bytes';
}

export class IPSchema extends AnySchema {
  private countrySchema: StringSchema;
  constructor(opt: Options = {}) {
    // extended options
    super(opt);
    // rules
    this.describer = [
      this.typeDescriber, // 0
      this.describer[1], // 1: default
      this.lookupDescriber, // 2: lookup
      this.versionDescriber, // 3: version, map
      this.allowDescriber, // 4: allow, disallow
      this.countryDescriber, // 5: allowCountry, disallowCountry
      this.formatDescriber, // 6: format
      this.describer[3] // 7: raw
    ];
    this.validator = [
      this.validator[0], // 0: default
      this.lookupValidator, // 1: lookup
      this.typeValidator, // 2
      this.versionValidator, // 3: version, map
      this.allowValidator, // 4: allow, disallow
      this.countryValidator, // 5: allowCountry, disallowCountry
      this.formatValidator, // 6: format
      this.validator[2] // 7: raw
    ];
    // set sub schema
    this.countrySchema = new StringSchema({
      allow: opt.allowCountry,
      disallow: opt.disallowCountry
    });
  }

  public set (opt: Options) {
    super.set(opt);
  }

  protected check (opt: Options) {
    super.check(opt);
    // option validity
    if (opt.lookup && typeof opt.lookup !== 'boolean')
      throw new Error(
        `Option lookup needs a boolean value, but ${inspect(opt.lookup)} was given.`
      );
    if (opt.version && opt.version !== 4 && opt.version !== 6)
      throw new Error(
        `Option version needs the number 4 or 6 as value, but ${inspect(
          opt.version
        )} was given.`
      );
    if (opt.map && typeof opt.map !== 'boolean')
      throw new Error(`Option map needs a boolean value, but ${inspect(opt.map)} was given.`);
    if (opt.format && !['short', 'long', 'array', 'bytes'].includes(opt.format))
      throw new Error(
        `Option format needs one of short, long, array or bytes, but ${inspect(
          opt.format
        )} was given.`
      );
    if (opt.allow && Array.isArray(opt.allow))
      opt.allow.forEach(e => {
        if (typeof e !== 'string' && !(e instanceof Reference))
          throw new Error(
            `Option allow needs string, RegExp or Reference values, but ${inspect(
              e
            )} was given.`
          );
      });
    if (opt.disallow && Array.isArray(opt.disallow))
      opt.disallow.forEach(e => {
        if (typeof e !== 'string' && !(e instanceof Reference))
          throw new Error(
            `Option disallow needs string, RegExp or Reference values, but ${inspect(
              e
            )} was given.`
          );
      });
    if (opt.allowCountry && Array.isArray(opt.allowCountry))
      opt.allowCountry.forEach(e => {
        if (typeof e !== 'string' && !(e instanceof Reference))
          throw new Error(
            `Option allowCountry needs string or Reference values, but ${inspect(
              e
            )} was given.`
          );
      });
    if (opt.disallowCountry && Array.isArray(opt.disallowCountry))
      opt.disallowCountry.forEach(e => {
        if (typeof e !== 'string' && !(e instanceof Reference))
          throw new Error(
            `Option disallowCountry needs string or Reference values, but ${inspect(
              e
            )} was given.`
          );
      });
  }

  protected lookupDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'ip')
    const opt = <Options>this.opt;
    if (opt.lookup)
      return t('describe.lookup')
  }
  protected async lookupValidator (work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    // check value
    if (typeof work.value === 'string' && opt.lookup)
      try {
        ipaddr.parse(work.value);
      } catch (e) {
        return lookup(work.value, { family: opt.version }).then(resolved => {
          work.value = resolved.address;
        });
      }
  }

  protected typeDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'ip')
    return t('describe.type');
  }
  protected async typeValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'ip')
    try {
      work.value =
        Array.isArray(work.value) && (work.value.length === 4 || work.value.length === 16)
          ? ipaddr.fromByteArray(work.value)
          : ipaddr.parse(work.value);
    } catch (err) {
      return Promise.reject(
        new ValidationError(
          this,
          work,
          t('error.type')
        )
      );
    }
  }

  protected versionDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'ip')
    const opt = <Options>this.opt;
    const msg: string[] = [];
    if (opt.version) {
      msg.push(t('describe.version', { version: opt.version }));
      if (opt.map)
        msg.push(
          t('describe.map', { from: opt.version === 4 ? 6 : 4, to: opt.version })
        );
    }
    if (msg.length) return msg.join(' ');
  }
  protected async versionValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'ip')
    const opt = <Options>this.opt;
    // check value
    if (opt.version) {
      if (opt.map) {
        if (
          opt.version === 4 &&
          work.value instanceof ipaddr.IPv6 &&
          work.value.isIPv4MappedAddress()
        )
          work.value = work.value.toIPv4Address();
        if (opt.version === 6 && work.value instanceof ipaddr.IPv4)
          work.value = work.value.toIPv4MappedAddress();
      }
      if (
        (opt.version === 4 && work.value instanceof ipaddr.IPv6) ||
        (opt.version === 6 && work.value instanceof ipaddr.IPv4)
      )
        return Promise.reject(
          new ValidationError(
            this,
            work,
            t('error.version', { version: opt.version })
          )
        );
    }
  }

  // overwritten because of special RegExp handling
  public allowDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'ip')
    const opt = <Options>this.opt;
    let values: string[] = [];
    if (opt.allow instanceof Reference) {
      values.push(
        (values.length ? t('core:phrase.or') + ' ' : '') +
        t('core:phrase.within', { source: opt.allow.source, filter: opt.allow.filter || "''" })
      );
    } else if (opt.allow && opt.allow.length)
      opt.allow.forEach(e => {
        values.push(
          (values.length ? t('core:phrase.or') + ' ' : '') +
          (typeof e === 'string'
            ? e.match(/^[0-9:.].*$/)
              ? t('phrase.is', { value: e })
              : t('phrase.range', { value: e })
            : t('core:phrase.within', { source: e.source, filter: e.filter || "''" }))
        );
      });
    if (opt.disallow instanceof Reference) {
      values.push(
        (values.length ? t('core:phrase.or') + ' ' : '') +
        t('core:phrase.within', { source: opt.disallow.source, filter: opt.disallow.filter })
      );
    } else if (opt.disallow && opt.disallow.length)
      opt.disallow.forEach(e => {
        values.push(
          (values.length ? t('core:phrase.and') + ' ' : '') +
          (typeof e === 'string'
            ? e.match(/^[0-9:.].*$/)
              ? t('phrase.notIs', { value: e })
              : t('phrase.notRange', { value: e })
            : t('core:phrase.notWithin', { source: e.source, filter: e.filter || "''" }))
        );
      });
    if (values.length)
      return (
        t('core:describe.allow') +
        '\n- ' +
        values.slice(0, 10).join('\n- ') +
        (values.length > 10 ? '\n- ' + t('core:phrase.more', { count: values.length - 10 }) : '') +
        '\n'
      );
  }
  // overwritten because of special RegExp handling
  protected async allowValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'ip')
    const opt = <Options>this.opt;
    if (!opt.allow && !opt.disallow) return; // nothing to check
    // get list
    let allow: string[] = [];
    let disallow: string[] = [];
    if (opt.allow instanceof Reference) {
      let data = await opt.allow.stringArray(work);
      if (data) allow = data.slice(0);
    } else if (opt.allow && opt.allow.length) {
      for (let index = 0; index < opt.allow.length; index++) {
        let e = opt.allow[index];
        if (e instanceof Reference) {
          let data = await e.stringArray(work);
          if (data) allow = allow.concat(data);
        } else if (specialRanges[e]) {
          allow = allow.concat(specialRanges[e].map(r => `${r}# (${e})`));
        } else allow.push(e);
      }
    }
    if (opt.disallow instanceof Reference) {
      let data = await opt.disallow.stringArray(work);
      if (data) disallow = data.slice(0);
    } else if (opt.disallow && opt.disallow.length) {
      for (let index = 0; index < opt.disallow.length; index++) {
        let e = opt.disallow[index];
        if (e instanceof Reference) {
          let data = await e.stringArray(work);
          if (data) disallow = disallow.concat(data);
        } else if (specialRanges[e])
          disallow = disallow.concat(specialRanges[e].map(r => `${r}# (${e})`));
        else disallow.push(e);
      }
    }
    // match ip to ranges
    let denyBits = 0;
    let denyTop = '';
    if (disallow.length) {
      for (const e of disallow) {
        let [n, name] = e.split('#', 2); // used to degrade indirectly added ranges
        name = name ? name : '';
        let range: [ipaddr.IPv4 | ipaddr.IPv6, number];
        if (n.match(/\//)) range = ipaddr.parseCIDR(n);
        else {
          const ip = ipaddr.parse(n);
          range = ip.kind() === 'ipv4' ? [ip, 32] : [ip, 128];
        }
        if (work.value.match(range)) {
          let bits = n.length < e.length ? 1 : range[1];
          if (Number.isNaN(bits)) bits = 0;
          if (bits > denyBits) {
            denyBits = bits;
            denyTop = range.toString().replace(/,/, '/') + name;
          }
        }
      }
    }
    let allowBits = 0;
    if (allow.length) {
      allowBits = -1;
      for (const e of allow) {
        const n = e.replace(/#.*$/, ''); // used to degrade indirectly added ranges
        let range: [ipaddr.IPv4 | ipaddr.IPv6, number];
        if (n.match(/\//)) range = ipaddr.parseCIDR(n);
        else {
          const ip = ipaddr.parse(n);
          range = ip.kind() === 'ipv4' ? [ip, 32] : [ip, 128];
        }
        if (work.value.match(range)) {
          let bits = n.length < e.length ? 1 : range[1];
          if (Number.isNaN(bits)) bits = 0;
          if (bits > allowBits) allowBits = bits;
        }
      }
    }
    // console.log(denyBits, allowBits);
    if (allowBits === -1)
      throw new ValidationError(
        this,
        work,
        t('error.allow', { list: inspect(opt.allow).replace(/[\[\]]/g, '').trim() })
      );
    else if (denyBits > allowBits) {
      throw new ValidationError(
        this,
        work,
        t('error.disallow', { range: denyTop })
      );
    }
  }

  public countryDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'ip')
    const opt = <Options>this.opt;
    const msg: string[] = [];
    if (opt.allowCountry)
      msg.push(
        t('describe.allowCountry', { list: inspect(opt.allowCountry).replace(/[\[\]]/g, '').trim() })
      );
    if (opt.disallowCountry)
      msg.push(
        t('describe.disallowCountry', { list: inspect(opt.disallowCountry).replace(/[\[\]]/g, '').trim() })
      );
    if (msg.length) return msg.join(' ');
  }
  protected async countryValidator (work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    // check value
    if (opt.allowCountry || opt.disallowCountry) {
      return import('geoip-lite').then(async geoip => {
        const meta = geoip.lookup(work.value.toString());
        if (meta)
          return this.countrySchema.validate(
            new Workbench(await DataStore.data(meta && meta.country))
          );
      });
    }
  }

  protected formatDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'ip')
    const opt = <Options>this.opt;
    return t('describe.format', { format: opt.format || 'short' });
  }
  protected async formatValidator (work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    // check value
    work.value =
      opt.format === 'bytes'
        ? (<ipaddr.IPv6>work.value).toByteArray()
        : opt.format === 'array'
          ? work.value.octets || work.value.parts
          : opt.format === 'long'
            ? (<ipaddr.IPv6>work.value).toNormalizedString()
            : work.value.toString();
  }
}

export default IPSchema;
