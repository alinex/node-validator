import { inspect } from 'util';
import { empty } from '@alinex/data';
import * as async from '@alinex/async';

import * as base from './base';
import Workbench from '../workbench';
import Schema from '../schema';
import Reference from '../reference';
import ValidationError from '../error';
import AnySchema from './any';
import { T, loadNamespace } from '../i18n';

loadNamespace('array')

let ID_COUNT = 0;
let obsoleteItems: boolean = false;

export interface Options extends base.Options {
  default?: any[] | Reference;
  split?: string | RegExp;
  makeArray?: boolean;
  removeDuplicate?: boolean;
  filter?: boolean | Schema;
  shuffle?: boolean;
  sort?: boolean | 'alpha' | 'num';
  reverse?: boolean;
  // deprecated:
  items?: { index?: number; schema: Schema }[];
  item?: { [key: string]: Schema };
  unique?: boolean;
  min?: number;
  max?: number;
  allow?: Schema[];
  disallow?: Schema[];
}

export class ArraySchema extends base.BaseSchema {
  _refID: string;
  _refNum = 0;
  constructor(opt: Options = {}) {
    super(opt);
    this._refID = `Logic_${++ID_COUNT}`;
    // rules
    this.describer = [
      this.typeDescriber, // 0:
      this.describer[0], // 1: default
      this.splitDescriber, // 2: split
      this.makeArrayDescriber, // 3: makeArray
      this.removeDuplicateDescriber, // 4: removeDuplicate
      this.filterDescriber, // 5: filter
      this.orderDescriber, // 6: shuffle, sort, reverse
      // deprecated:
      this.itemsDescriber, // 7: items
      this.itemDescriber, // 7: item
      this.uniqueDescriber, // 8: unique
      this.sizeDescriber, // 9: min, max
      this.allowDescriber, // 10: allow, disallow
      this.describer[1] // 11: raw
    ];
    this.validator = [
      this.validator[0], // 0: default
      this.splitValidator, // 1: split
      this.makeArrayValidator, // 2: makeArray
      this.typeValidator, // 3:
      this.removeDuplicateValidator, // 4: removeDuplicate
      this.filterValidator, // 5: filter
      this.orderValidator, // 6: shuffle, sort, reverse
      // deprecated:
      this.itemsValidator, // 7: items
      this.itemValidator, // 7: item
      this.uniqueValidator, // 8: unique
      this.sizeValidator, // 9: min, max
      this.allowValidator, // 10: allow, disallow
      this.validator[1] // 11: raw
    ];
  }

  public set (opt: Options) {
    super.set(opt);
  }

  protected check (opt: Options) {
    super.check(opt);
    // option validity
    if (opt.default && !(Array.isArray(opt.default) || opt.default instanceof Reference))
      throw new Error(`Option default needs an array or Reference value, but ${inspect(opt.default)} was given.`);
    if (opt.split && typeof opt.split !== 'string' && !(opt.split instanceof RegExp))
      throw new Error(`Option split needs a string or RegExp value, but ${inspect(opt.split)} was given.`);
    if (opt.makeArray && typeof opt.makeArray !== 'boolean')
      throw new Error(`Option makeArray needs a boolean value, but ${inspect(opt.makeArray)} was given.`);
    if (opt.removeDuplicate && typeof opt.removeDuplicate !== 'boolean')
      throw new Error(
        `Option removeDuplicate needs a boolean value, but ${inspect(opt.removeDuplicate)} was given.`
      );
    if (opt.filter && typeof opt.filter !== 'boolean' && !(opt.filter instanceof base.BaseSchema))
      throw new Error(`Option filter needs a boolean or Schema value, but ${inspect(opt.filter)} was given.`);
    if (opt.shuffle && typeof opt.shuffle !== 'boolean')
      throw new Error(`Option shuffle needs a boolean value, but ${inspect(opt.shuffle)} was given.`);
    if (opt.sort && typeof opt.sort !== 'boolean' && !['alpha', 'num'].includes(opt.sort))
      throw new Error(
        `Option sort needs a boolean or 'alpha', 'num' as value, but ${inspect(opt.sort)} was given.`
      );
    if (opt.reverse && typeof opt.reverse !== 'boolean')
      throw new Error(`Option reverse needs a boolean value, but ${inspect(opt.reverse)} was given.`);
    if (opt.shuffle && (opt.sort || opt.reverse))
      throw new Error('Sort and reverse option are not possible if shuffle is set.');
    if (opt.reverse && !opt.sort) throw new Error('Option reverse without sort is not possible.');
    // deprecated:
    if (opt.items) {
      if (!obsoleteItems) {
        console.warn('deprecated use of array.items, use array.item instead');
        obsoleteItems = true;
      }
      if (!Array.isArray(opt.items) || !opt.items.length)
        throw new Error(`Option items needs an object list, but ${inspect(opt.items)} was given.`);
      opt.items.forEach(e => {
        if (typeof e !== 'object')
          throw new Error(`Option items needs object elements, but ${inspect(e)} was given.`);
        if (typeof e.index !== 'number' && e.index !== undefined)
          throw new Error(
            `Option items needs elements with or without numeric index, but ${inspect(e)} was given.`
          );
        if (!(e.schema instanceof base.BaseSchema))
          throw new Error(`Option items needs elements with schema set, but ${inspect(e)} was given.`);
      });
    }
    if (opt.item) {
      for (let k in opt.item) {
        if (Object.prototype.hasOwnProperty.call(opt.item, k)) {
          if (!(opt.item[k] instanceof base.BaseSchema))
            throw new Error(
              `Option item needs elements with schema set, but ${inspect(
                opt.item[k]
              )} was given under key ${k}.`
            );
          if (!k.match(/^(\*|\d+|\d+\-(\d+)?|(\d+)?\-\d+)$/))
            throw new Error(
              `Option item needs '*', number, or a number range like 2-5 but ${inspect(k)} was given.`
            );
        }
      }
    }
    if (opt.unique && typeof opt.unique !== 'boolean')
      throw new Error(`Option unique needs a boolean value, but ${inspect(opt.unique)} was given.`);
    if (opt.min !== undefined && typeof opt.min !== 'number')
      throw new Error(`Option min needs a numeric value, but ${inspect(opt.min)} was given.`);
    if (opt.max !== undefined && typeof opt.max !== 'number')
      throw new Error(`Option max needs a numeric value, but ${inspect(opt.max)} was given.`);
    if (opt.min !== undefined && opt.max !== undefined && opt.min > opt.max)
      throw new Error(`Option min could not be greater than max, but ${opt.min} > ${opt.max} was given.`);
    if (opt.allow && Array.isArray(opt.allow))
      opt.allow.forEach(e => {
        if (!(e instanceof base.BaseSchema))
          throw new Error(`Option allow needs schema definitions, but ${inspect(e)} was given.`);
      });
    if (opt.disallow && Array.isArray(opt.disallow))
      opt.disallow.forEach(e => {
        if (!(e instanceof base.BaseSchema))
          throw new Error(`Option disallow needs schema definitions, but ${inspect(e)} was given.`);
      });
  }

  /**
   * Export schema as tree data structure.
   */
  export (parent: any = {}): any {
    const def: any = super.export();
    if (def.SCHEMA_META) return def.SCHEMA_META;
    // check for circular references
    if (parent[this._refID]) {
      parent[this._refID]._refNum++;
      return {
        SCHEMA_TYPE: this.constructor.name.replace(/Schema$/, ''),
        SCHEMA_REF: this._refID
      };
    }
    const map: any = { ...parent };
    map[this._refID] = this;
    // simple object
    // deprecated:
    if (def.items && Object.keys(this.opt).length === 1) {
      const res: any = {};
      def.items.forEach((e: any) => {
        res[typeof e.index === 'number' ? e.index : '*'] = e.schema.export(map);
      });
      return res;
    }
    if (def.item && Object.keys(this.opt).length === 1) {
      const res: any = {};
      for (let k in def.item) {
        if (Object.prototype.hasOwnProperty.call(def.item, k)) {
          res[k] = def.item[k].export(map);
        }
      }
      return res;
    }
    // exact export
    // deprecated:
    if (def.items)
      def.items.forEach((item: any) => {
        if (item.schema) item.schema = item.schema.export(map);
      });
    if (def.item)
      for (let k in def.item) {
        if (Object.prototype.hasOwnProperty.call(def.item, k)) {
          def.item[k] = def.item[k].export(map);
        }
      }
    if (this._refNum) def.SCHEMA_ID = this._refID;
    return def;
  }

  private splitDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'array')
    const opt = <Options>this.opt;
    if (opt.split)
      return t('describe.split', { value: inspect(opt.split) });
  }
  private async splitValidator (work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    // check value
    if (opt.split && typeof work.value === 'string') work.value = work.value.split(opt.split);
  }

  private makeArrayDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'array')
    const opt = <Options>this.opt;
    if (opt.makeArray) {
      return t('describe.makeArray');
    }
  }
  private async makeArrayValidator (work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    // check value
    if (opt.makeArray && !Array.isArray(work.value)) work.value = [work.value];
  }

  private typeDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'array')
    return t('describe.type');
  }
  private async typeValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'array')
    if (!Array.isArray(work.value))
      return Promise.reject(new ValidationError(this, work, t('error.type')));
  }

  private removeDuplicateDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'array')
    const opt = <Options>this.opt;
    if (opt.removeDuplicate)
      return t('describe.removeDuplicate');
  }
  private async removeDuplicateValidator (work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    // check value
    if (opt.removeDuplicate) {
      work.value = (<any[]>work.value).filter((e, pos) => work.value.indexOf(e) == pos);
    }
  }

  private filterDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'array')
    const opt = <Options>this.opt;
    if (opt.filter) {
      if (typeof opt.filter === 'boolean') return t('describe.filterEmpty');
      return t('describe.filter') + '\n-   ' + opt.filter.describe(depth, path, lang) + '\n'
    }
  }
  private async filterValidator (work: Workbench, lang: string): Promise<void> {
    const opt = <Options>this.opt;
    // check value
    if (opt.filter) {
      if (opt.filter instanceof base.BaseSchema) {
        let schema = opt.filter;
        work.value = await async.filter(
          work.value,
          async (e, index): Promise<boolean> => {
            return (
              schema
                // .validate(work.clone(index))
                .validate(work.sub(<number>index), lang)
                .then(() => true)
                .catch(() => false)
            );
          }
        );
      } else work.value = (<any[]>work.value).filter(e => !empty(e));
    }
  }

  private orderDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'array')
    const opt = <Options>this.opt;
    if (opt.shuffle) {
      return t('describe.shuffle');
    } else if (opt.sort) {
      let sort: string = opt.sort === true ? 'alpha' : opt.sort;
      if (opt.reverse) sort += '-reverse';
      return t('describe.order', { sort });
    }
  }
  private async orderValidator (work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    // check value
    if (opt.shuffle) {
      let a: any[] = work.value;
      for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
      }
    } else if (opt.sort) {
      let sorter =
        opt.sort === 'num'
          ? (a: any, b: any) => Number(a) - Number(b)
          : (a: any, b: any) => {
            var nameA = a.toString().toUpperCase();
            var nameB = b.toString().toUpperCase();
            if (nameA < nameB) return -1;
            if (nameA > nameB) return 1;
            return 0;
          };
      (<any[]>work.value).sort(sorter);
      if (opt.reverse) (<any[]>work.value).reverse();
    }
  }

  // deprecated:
  private itemsDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'array')
    const opt = <Options>this.opt;
    if (path.length) {
      const m = path.match(/^(\d+)(\.(.*))?$/);
      if (m && opt.items) {
        const idx = parseInt(m[1]);
        const sub = m[3] || '';
        let other = '';
        let key = '';
        opt.items.forEach(def => {
          if (def.index) {
            if (def.index === idx) key = def.schema.describe(depth, sub, lang);
          } else other = def.schema.describe(depth, sub, lang);
        });
        return key || other;
      }
      return '';
    }
    if (opt.items) {
      let other = '';
      let index = [];
      opt.items.forEach(def => {
        if (def.index) {
          index.push(`-   ${def.index}: ${def.schema.describe(depth, path, lang)}`);
        } else other = '-   ' + 'default' + ': ' + def.schema.describe(depth, path, lang);
      });
      index.unshift(other);
      return t('describe.item') + '\n' + index.join('\n') + '\n';
    }
  }
  private async itemsValidator (work: Workbench, lang: string): Promise<void> {
    const opt = <Options>this.opt;
    // check value
    if (opt.items) {
      // get schema
      let other: Schema = new AnySchema();
      let index: { [key: number]: Schema } = {};
      opt.items.forEach(def => {
        if (def.index) index[def.index] = def.schema;
        else other = def.schema;
      });
      // validate
      return async.each(work.value, async (e, idx) => {
        const schema = index[<number>idx] || other;
        return schema.validate(work.sub(<number>idx), lang);
      });
    }
  }

  private itemDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'array')
    const opt = <Options>this.opt;
    if (path.length) {
      const m = path.match(/^(\d+)(\.(.*))?$/);
      if (m && opt.item) {
        const idx = parseInt(m[1]);
        const sub = m[3] || '';
        let other = '';
        let key = '';
        let range = '';
        for (let k in opt.item) {
          if (Object.prototype.hasOwnProperty.call(opt.item, k)) {
            if (k === '*') other = opt.item[k].describe(depth, sub, lang);
            else if (k === idx.toString()) key = opt.item[k].describe(depth, sub, lang);
            else {
              const m = k.match(/^(\d+)?\-(\d+)?$/);
              if (m) {
                if (m[1] && m[2]) {
                  if (parseInt(m[1]) <= idx && parseInt(m[2]) >= idx)
                    range = opt.item[k].describe(depth, sub, lang);
                } else if (m[1]) {
                  if (parseInt(m[1]) <= idx) range = opt.item[k].describe(depth, sub, lang);
                }
              }
            }
          }
        }
        return key || range || other;
      }
      return '';
    }
    if (opt.item) {
      let index: string[] = [];
      for (let k in opt.item) {
        if (Object.prototype.hasOwnProperty.call(opt.item, k)) {
          index.push(`-   ${k}: ${opt.item[k].describe(depth, path, lang)}`);
        }
      }
      return t('describe.item') + '\n' + index.join('\n') + '\n';
    }
  }
  private async itemValidator (work: Workbench, lang: string): Promise<void> {
    const opt = <Options>this.opt;
    // check value
    if (opt.item) {
      // get schema
      let other: Schema = new AnySchema();
      let index: { [key: number]: Schema } = {};
      for (let k in opt.item) {
        if (Object.prototype.hasOwnProperty.call(opt.item, k)) {
          if (k === '*') other = opt.item[k];
          else if (k.match(/^\d+$/)) index[parseInt(k)] = opt.item[k];
          else {
            const m = k.match(/^(\d+)?\-(\d+)?$/);
            if (m) {
              if (m[1] && m[2]) {
                for (let i = parseInt(m[1]); i <= parseInt(m[2]); i++) index[i] = opt.item[k];
              } else if (m[1]) {
                for (let i = parseInt(m[1]); i <= 999; i++) if (!index[i]) index[i] = opt.item[k];
              } else {
                index[-parseInt(m[2])] = opt.item[k];
              }
            }
          }
        }
      }
      // validate
      return async.each(work.value, async (e, idx) => {
        const schema = index[<number>idx] || index[<number>idx - work.value.length] || other;
        return schema.validate(work.sub(<number>idx), lang);
      });
    }
  }

  private uniqueDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'array')
    const opt = <Options>this.opt;
    if (opt.unique) {
      return t('describe.unique');
    }
  }
  private async uniqueValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'array')
    const opt = <Options>this.opt;
    // check value
    if (opt.unique) {
      const c = new Set();
      for (const e of work.value) {
        if (c.has(e))
          throw new ValidationError(
            this,
            work,
            t('error.unique', { value: inspect(e) })
          );
        c.add(e);
      }
    }
  }

  private sizeDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'array')
    const opt = <Options>this.opt;
    let msg = '';
    if (opt.min !== undefined && opt.min === opt.max) msg += t('describe.exact', { count: opt.max });
    else if (opt.min !== undefined && opt.max !== undefined)
      msg += t('describe.between', { min: opt.min, max: opt.max });
    else if (opt.min !== undefined) msg += t('describe.min', { count: opt.min });
    else if (opt.max !== undefined) msg += t('describe.max', { count: opt.max });
    if (msg) return msg.trim();
  }
  private async sizeValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'array')
    const opt = <Options>this.opt;
    let num = (<any[]>work.value).length;
    // check length
    if (opt.min !== undefined && num < opt.min) {
      throw new ValidationError(
        this,
        work,
        t('error.min', { count: num, min: opt.min })
      );
    }
    if (opt.max !== undefined && num > opt.max) {
      throw new ValidationError(
        this,
        work,
        t('error.max', { count: num, max: opt.max })
      );
    }
  }

  protected allowDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'array')
    const opt = <Options>this.opt;
    if (opt.allow && opt.allow.length)
      return (
        t('core:describe.allow') +
        '\n-   ' +
        opt.allow
          .slice(0, 10)
          .map(e => e.describe(depth, path, lang))
          .join('\n-   ') +
        (opt.allow.length > 10 ? '\n-   ' + t('core:phrase.more', { count: opt.allow.length - 10 }) : '') +
        '\n'
      );
    if (opt.disallow && opt.disallow.length)
      return (
        t('core:describe.disallow') +
        '\n-   ' +
        opt.disallow
          .slice(0, 10)
          .map(e => e.describe(depth, path, lang))
          .join('\n-   ') +
        (opt.disallow.length > 10 ? '\n-   ' + t('core:phrase.more', { core: opt.disallow.length - 10 }) : '') +
        '\n'
      );
  }
  protected async allowValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'array')
    const opt = <Options>this.opt;
    // validate
    if (opt.allow && opt.allow.length) {
      await async.each(work.value, async (e, idx) => {
        const found = await async.filter(<Schema[]>opt.allow, e =>
          e
            .validate(work.sub(<number>idx).clone(), lang)
            .then(_ => true)
            .catch(_ => false)
        );
        if (!found.length)
          throw new ValidationError(
            this,
            work,
            t('error.allow', { index: idx })
          );
      });
    }
    if (opt.disallow && opt.disallow.length) {
      await async.each(work.value, async (e, idx) => {
        const found = await async.filter(<Schema[]>opt.disallow, e =>
          e
            .validate(work.sub(<number>idx).clone(), lang)
            .then(_ => true)
            .catch(_ => false)
        );
        if (found.length)
          throw new ValidationError(
            this,
            work,
            t('error.disallow', {
              index: idx,
              schema: '\n-   ' + found.map(e => e.describe(5, '', lang).replace(/\n/g, '\n    ')).join('\n-   ')
            })
          );
      });
    }
  }
}

export default ArraySchema;
