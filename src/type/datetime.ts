import { inspect } from 'util';
import moment from 'moment-timezone';
// chrono-node on demand

import * as any from './any';
import Workbench from '../workbench';
import Reference from '../reference';
import ValidationError from '../error';
import { T, loadNamespace } from '../i18n';

loadNamespace('datetime')

export interface Options extends any.Options {
  default?: Date | Reference;
  parse?: boolean | 'en' | 'en_GB' | 'de' | 'pt' | 'es' | 'fr' | 'ja';
  defaultTimezone?: string;
  reference?: Date;
  min?: Date;
  max?: Date;
  greater?: Date;
  less?: Date;
  allow?: (Date | Reference)[] | Reference;
  disallow?: (Date | Reference)[] | Reference;
  format?: string;
  timezone?: string;
  locale?: string;
}

const locales = ['en', 'en_GB', 'de', 'pt', 'es', 'fr', 'ja'];

const timezones: { [key: string]: number } = {
  local: new Date().getTimezoneOffset(),
  A: 60,
  ACDT: 630,
  ACST: 570,
  ADT: -180,
  AEDT: 660,
  AEST: 600,
  AFT: 270,
  AKDT: -480,
  AKST: -540,
  ALMT: 360,
  AMST: -180,
  AMT: -240,
  ANAST: 720,
  ANAT: 720,
  AQTT: 300,
  ART: -180,
  AST: -240,
  AWDT: 540,
  AWST: 480,
  AZOST: 0,
  AZOT: -60,
  AZST: 300,
  AZT: 240,
  B: 120,
  BNT: 480,
  BOT: -240,
  BRST: -120,
  BRT: -180,
  BST: 60,
  BTT: 360,
  C: 180,
  CAST: 480,
  CAT: 120,
  CCT: 390,
  CDT: -300,
  CEST: 120,
  CET: 60,
  CHADT: 825,
  CHAST: 765,
  CKT: -600,
  CLST: -180,
  CLT: -240,
  COT: -300,
  CST: -360,
  CVT: -60,
  CXT: 420,
  ChST: 600,
  D: 240,
  DAVT: 420,
  E: 300,
  EASST: -300,
  EAST: -360,
  EAT: 180,
  ECT: -300,
  EDT: -240,
  EEST: 180,
  EET: 120,
  EGST: 0,
  EGT: -60,
  EST: -300,
  ET: -300,
  F: 360,
  FJST: 780,
  FJT: 720,
  FKST: -180,
  FKT: -240,
  FNT: -120,
  G: 420,
  GALT: -360,
  GAMT: -540,
  GET: 240,
  GFT: -180,
  GILT: 720,
  GMT: 0,
  GST: 240,
  GYT: -240,
  H: 480,
  HAA: -180,
  HAC: -300,
  HADT: -540,
  HAE: -240,
  HAP: -420,
  HAR: -360,
  HAST: -600,
  HAT: -90,
  HAY: -480,
  HKT: 480,
  HLV: -210,
  HNA: -240,
  HNC: -360,
  HNE: -300,
  HNP: -480,
  HNR: -420,
  HNT: -150,
  HNY: -540,
  HOVT: 420,
  I: 540,
  ICT: 420,
  IDT: 180,
  IOT: 360,
  IRDT: 270,
  IRKST: 540,
  IRKT: 540,
  IRST: 210,
  IST: 60,
  JST: 540,
  K: 600,
  KGT: 360,
  KRAST: 480,
  KRAT: 480,
  KST: 540,
  KUYT: 240,
  L: 660,
  LHDT: 660,
  LHST: 630,
  LINT: 840,
  M: 720,
  MAGST: 720,
  MAGT: 720,
  MART: -510,
  MAWT: 300,
  MDT: -360,
  MESZ: 120,
  MEZ: 60,
  MHT: 720,
  MMT: 390,
  MSD: 240,
  MSK: 240,
  MST: -420,
  MUT: 240,
  MVT: 300,
  MYT: 480,
  N: -60,
  NCT: 660,
  NDT: -90,
  NFT: 690,
  NOVST: 420,
  NOVT: 360,
  NPT: 345,
  NST: -150,
  NUT: -660,
  NZDT: 780,
  NZST: 720,
  O: -120,
  OMSST: 420,
  OMST: 420,
  P: -180,
  PDT: -420,
  PET: -300,
  PETST: 720,
  PETT: 720,
  PGT: 600,
  PHOT: 780,
  PHT: 480,
  PKT: 300,
  PMDT: -120,
  PMST: -180,
  PONT: 660,
  PST: -480,
  PT: -480,
  PWT: 540,
  PYST: -180,
  PYT: -240,
  Q: -240,
  R: -300,
  RET: 240,
  S: -360,
  SAMT: 240,
  SAST: 120,
  SBT: 660,
  SCT: 240,
  SGT: 480,
  SRT: -180,
  SST: -660,
  T: -420,
  TAHT: -600,
  TFT: 300,
  TJT: 300,
  TKT: 780,
  TLT: 540,
  TMT: 300,
  TVT: 720,
  U: -480,
  ULAT: 480,
  UTC: 0,
  UYST: -120,
  UYT: -180,
  UZT: 300,
  V: -540,
  VET: -210,
  VLAST: 660,
  VLAT: 660,
  VUT: 660,
  W: -600,
  WAST: 120,
  WAT: 60,
  WEST: 60,
  WESZ: 60,
  WET: 0,
  WEZ: 0,
  WFT: 720,
  WGST: -120,
  WGT: -180,
  WIB: 420,
  WIT: 540,
  WITA: 480,
  WST: 780,
  WT: 0,
  X: -660,
  Y: -720,
  YAKST: 600,
  YAKT: 600,
  YAPT: 600,
  YEKST: 360,
  YEKT: 360,
  Z: 0
};

const alias: { [key: string]: string } = {
  ISO8601: 'YYYY-MM-DDTHH:mm:ssZ',
  RFC1123: 'ddd, DD MMM YYYY HH:mm:ss z',
  RFC2822: 'ddd, DD MMM YYYY HH:mm:ss ZZ',
  RFC822: 'ddd, DD MMM YY HH:mm:ss ZZ',
  RFC1036: 'ddd, D MMM YY HH:mm:ss ZZ',
  //    RFC850:  'dddd, D-MMM-ZZ HH:mm:ss Europe/Paris',
  //    COOKIE:  'Friday, 13-Feb-09 14:53:27 Europe/Paris',
  'ISO8601-date': 'YYYY-MM-DD'
};

export class DateTimeSchema extends any.AnySchema {
  constructor(opt: Options = {}) {
    // option validity
    super(opt);
    // rules
    this.describer = [
      this.typeDescriber, // 0:
      this.describer[1], // 1: default
      this.parseDescriber, // 2: parse, defaultTimezone, reference
      this.rangeDescriber, // 3: min, max, greater, less
      this.describer[2], // 4: allow, disallow
      this.formatDescriber, // 5: timezone, locale, format
      this.describer[3] // 6: raw
    ];
    this.validator = [
      this.validator[0], // 0: default
      this.parseValidator, // 1: parse, defaultTimezone, reference
      this.typeValidator, // 2:
      this.rangeValidator, // 3: min, max, greater, less
      this.validator[1], // 4: allow, disallow
      this.formatValidator, // 5: timezone, locale, format
      this.validator[2] // 6: raw
    ];
  }

  public set (opt: Options) {
    super.set(opt);
  }

  protected check (opt: Options) {
    super.check(opt);
    if (opt.parse && opt.parse !== true && !locales.includes(opt.parse))
      throw new Error(
        `Option parse needs boolean value or a locale of ${locales.join(
          ', '
        )}, but ${inspect(opt.parse)} was given.`
      );
    if (opt.reference && !(opt.reference instanceof Date))
      throw new Error(
        `Option reference needs to be a date value, but ${inspect(
          opt.reference
        )} was given.`
      );
    if (opt.defaultTimezone) {
      if (typeof opt.defaultTimezone !== 'string')
        throw new Error(
          `Option defaultTimezone needs a string value, but ${inspect(
            opt.defaultTimezone
          )} was given.`
        );
      if (timezones[opt.defaultTimezone] === undefined)
        throw new Error(
          `Option defaultTimezone has to be one of ${Object.keys(timezones).join(
            ', '
          )}, but ${inspect(opt.defaultTimezone)} was given.`
        );
    }
    if (opt.min !== undefined && !(typeof opt.min === 'object' && opt.min instanceof Date))
      throw new Error(`Option min needs a Date value, but ${inspect(opt.min)} was given.`);
    if (opt.max !== undefined && !(typeof opt.max === 'object' && opt.max instanceof Date))
      throw new Error(`Option max needs a Date value, but ${inspect(opt.max)} was given.`);
    if (
      opt.greater !== undefined &&
      !(typeof opt.greater === 'object' && opt.greater instanceof Date)
    )
      throw new Error(
        `Option greater needs a Date value, but ${inspect(opt.greater)} was given.`
      );
    if (opt.less !== undefined && !(typeof opt.less === 'object' && opt.less instanceof Date))
      throw new Error(`Option less needs a Date value, but ${inspect(opt.less)} was given.`);
    if (opt.min !== undefined && opt.max !== undefined && opt.min > opt.max)
      throw new Error(
        `Option min could not be greater than max, but ${opt.min} > ${opt.max} was given.`
      );
    if (opt.min !== undefined && opt.less !== undefined && opt.min >= opt.less)
      throw new Error(
        `Option min could not be less or equal less value, but ${opt.min} >= ${opt.less} was given.`
      );
    if (opt.max !== undefined && opt.greater !== undefined && opt.max <= opt.greater)
      throw new Error(
        `Option max could not be greater than max, but ${opt.max} <= ${opt.greater} was given.`
      );
    if (opt.less !== undefined && opt.greater !== undefined && opt.less <= opt.greater)
      throw new Error(
        `Option less could not be less than greater, but ${opt.min} <= ${opt.max} was given.`
      );
    if (opt.allow && Array.isArray(opt.allow))
      opt.allow.forEach(e => {
        if (typeof e !== 'object' || !(e instanceof Date))
          throw new Error(
            `Option allow needs Date or Reference values, but ${inspect(e)} was given.`
          );
      });
    if (opt.disallow && Array.isArray(opt.disallow))
      opt.disallow.forEach(e => {
        if (typeof e !== 'object' || !(e instanceof Date))
          throw new Error(
            `Option disallow needs Date or Reference values, but ${inspect(
              e
            )} was given.`
          );
      });
    if (opt.format !== undefined && typeof opt.format !== 'string')
      throw new Error(`Option format needs string value, but ${opt.format} was given.`);
    if (opt.timezone !== undefined && typeof opt.timezone !== 'string')
      throw new Error(`Option timezone needs string value, but ${opt.timezone} was given.`);
    if (opt.timezone !== undefined && opt.format === undefined)
      throw new Error(`Option timezone without format will not work.`);
    if (opt.timezone !== undefined && typeof opt.timezone !== 'string')
      throw new Error(`Option locale needs string value, but ${opt.locale} was given.`);
    if (opt.locale !== undefined && opt.format === undefined)
      throw new Error(`Option locale without format will not work.`);
  }

  private parseDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'datetime')
    const opt = <Options>this.opt;
    let msg: string[] = [];
    if (opt.parse) {
      msg.push(t('describe.parse'));
      if (opt.reference)
        msg.push(
          t('describe.parseReference', { date: opt.reference })
        );
      if (typeof opt.parse === 'string')
        msg.push(t('describe.parseString', { format: opt.parse }));
      if (opt.defaultTimezone)
        msg.push(t('describe.parseTimezone', { timezone: opt.defaultTimezone }));
      return msg.join(' ');
    }
  }
  private async parseValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'datetime')
    const opt = <Options>this.opt;
    if (opt.parse && !(work.value instanceof Date)) {
      // @ts-ignore
      let parsed = await import('chrono-node').then(chrono =>
        typeof opt.parse === 'string'
          ? chrono[opt.parse]?.parse(work.value, opt.reference)
          : chrono.parse(work.value, opt.reference)
      );
      if (parsed[0] && parsed[0].start) {
        parsed = parsed[0].start;
        if (parsed.knownValues.timezoneOffset === undefined)
          parsed.imply(
            'timezoneOffset',
            opt.defaultTimezone ? timezones[opt.defaultTimezone] : moment().utcOffset()
          );
        work.value = parsed.date();
        // new Date(work.value.getTime() - work.value.getTimezoneOffset()*60000)
      } else
        throw new ValidationError(
          this,
          work,
          t('error.parse')
        );
    }
  }

  protected typeDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'datetime')
    return t('describe.type');
  }
  protected async typeValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'datetime')
    // parse date
    if (!(work.value instanceof Date))
      throw new ValidationError(this, work, t('error.type'));
  }

  private rangeDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'datetime')
    const opt = <Options>this.opt;
    let msg: string[] = [];
    let min: Date | undefined = undefined;
    let max: Date | undefined = undefined;
    let greater: Date | undefined = undefined;
    let less: Date | undefined = undefined;
    if (min === undefined || (opt.min !== undefined && opt.min < min)) min = opt.min;
    if (max === undefined || (opt.max !== undefined && opt.max > max)) max = opt.max;
    if (opt.greater !== undefined) {
      if (min === undefined) greater = opt.greater;
      else if (min < opt.greater) {
        min = undefined;
        greater = opt.greater;
      }
    }
    if (opt.less !== undefined) {
      if (max === undefined) less = opt.less;
      else if (max > opt.less) {
        max = undefined;
        less = opt.less;
      }
    }
    // messages
    if (min !== undefined && max !== undefined)
      msg.push(t('describe.betweenEqual', { min, max }));
    else {
      if (min !== undefined) msg.push(t('describe.min', { min }));
      if (max !== undefined) msg.push(t('describe.max', { max }));
    }
    if (less !== undefined && greater !== undefined)
      msg.push(t('describe.between', { greater, less }));
    else {
      if (greater !== undefined)
        msg.push(t('describe.greater', { greater }));
      if (less !== undefined) msg.push(t('describe.less', { less }));
    }
    return msg.join(' ');
  }
  private async rangeValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'datetime')
    const opt = <Options>this.opt;
    let min: Date | undefined = undefined;
    let max: Date | undefined = undefined;
    let greater: Date | undefined = undefined;
    let less: Date | undefined = undefined;
    if (min === undefined || (opt.min !== undefined && opt.min < min)) min = opt.min;
    if (max === undefined || (opt.max !== undefined && opt.max > max)) max = opt.max;
    if (opt.greater !== undefined) {
      if (min === undefined) greater = opt.greater;
      else if (min < opt.greater) {
        min = undefined;
        greater = opt.greater;
      }
    }
    if (opt.less !== undefined) {
      if (max === undefined) less = opt.less;
      else if (max > opt.less) {
        max = undefined;
        less = opt.less;
      }
    }
    // check value
    if (min !== undefined && work.value < min)
      throw new ValidationError(
        this,
        work,
        t('error.min', { min, value: work.value })
      );
    if (max !== undefined && work.value > max)
      throw new ValidationError(
        this,
        work,
        t('error.max', { max, value: work.value })
      );
    if (greater !== undefined && work.value <= greater)
      throw new ValidationError(
        this,
        work,
        t('error.greater', { greater, value: work.value })
      );
    if (less !== undefined && work.value >= less)
      throw new ValidationError(
        this,
        work,
        t('error.less', { less, value: work.value })
      );
  }

  private formatDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'datetime')
    const opt = <Options>this.opt;
    const msg: string[] = [];
    if (opt.timezone) msg.push(t('describe.timezone', { timezone: opt.timezone }));
    if (opt.format) {
      if (opt.locale)
        msg.push(
          t('describe.locale', { locale: opt.locale, format: opt.format })
        );
      else msg.push(t('describe.format', { format: opt.format }));
    }
    return msg.join(' ');
  }
  private async formatValidator (work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    let date = moment(work.value);
    if (opt.timezone) date = date.tz(opt.timezone);
    if (opt.locale) date = date.locale(opt.locale);
    if (opt.format) {
      if (opt.format === 'milliseconds') work.value = date.valueOf();
      else if (opt.format === 'seconds') work.value = date.unix();
      else if (opt.format === 'relative') work.value = date.from(opt.reference || new Date());
      else if (opt.format === 'range') work.value = date.unix() - moment().unix();
      else work.value = date.format(alias[opt.format] || opt.format);
    }
  }
}

export default DateTimeSchema;
