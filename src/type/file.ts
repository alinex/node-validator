import { inspect } from 'util';
import { resolve } from 'path';
import * as fs from 'fs';
import DataStore from '@alinex/datastore';

import { AnySchema, Options as AnyOptions } from './any';
import Workbench from '../workbench';
import Reference from '../reference';
import ValidationError from '../error';
import StringSchema from './string';
import { T, loadNamespace } from '../i18n';

loadNamespace('file')

export interface Options extends AnyOptions {
  default?: string | Reference;
  baseDir?: string;
  type?: 'block' | 'character' | 'dir' | 'fifo' | 'file' | 'socket' | 'link';
  minSize?: number;
  maxSize?: number;
  minCTime?: Date | number;
  maxCTime?: Date | number;
  minMTime?: Date | number;
  maxMTime?: Date | number;
  minATime?: Date | number;
  maxATime?: Date | number;
  allow?: (string | RegExp | Reference)[] | Reference;
  disallow?: (string | RegExp | Reference)[] | Reference;
  exists?: boolean;
  readable?: boolean;
  writable?: boolean;
  executable?: boolean;
}

export class FileSchema extends AnySchema {
  private stringSchema: StringSchema;
  constructor(opt: Options = {}) {
    // extended options
    super(opt);
    // rules
    this.describer = [
      this.typeDescriber, // 0
      this.describer[1], // 1: default
      this.baseDirDescriber, // 2: baseDir
      this.stringDescriber, // 3: allow, disallow
      this.existsDescriber, // 4: exists
      this.fileTypeDescriber, // 5: type
      this.sizeDescriber, // 6: minSize, maxSize
      this.timeDescriber, // 7: minCTime, maxCTime, minMTime, maxMTime, minATime, maxATime
      this.describer[3] // 8: raw
    ];
    this.validator = [
      this.validator[0], // 0: default
      this.typeValidator, // 1
      this.baseDirValidator, // 2: baseDir
      this.stringValidator, // 3: allow, disallow
      this.existsValidator, // 4: exists
      this.fileTypeValidator, // 5: type
      this.sizeValidator, // 6: minSize, maxSize
      this.timeValidator, // 7: minCTime, maxCTime, minMTime, maxMTime, minATime, maxATime
      this.validator[2] // 8: raw
    ];
    // set sub schema
    this.stringSchema = new StringSchema({
      allow: opt.allow,
      disallow: opt.disallow
    });
  }

  public set(opt: Options) {
    super.set(opt);
  }

  protected check(opt: Options) {
    super.check(opt);
    // option validity
    if (opt.baseDir && typeof opt.baseDir !== 'string')
      throw new Error(
        `Option baseDir needs a string value, but ${inspect(opt.baseDir)} was given.`
      );
    if (opt.exists && typeof opt.exists !== 'boolean')
      throw new Error(
        `Option exists needs a boolean value, but ${inspect(opt.exists)} was given.`
      );
    if (opt.readable && typeof opt.readable !== 'boolean')
      throw new Error(
        `Option readable needs a boolean value, but ${inspect(opt.readable)} was given.`
      );
    if (opt.writable && typeof opt.writable !== 'boolean')
      throw new Error(
        `Option writable needs a boolean value, but ${inspect(opt.writable)} was given.`
      );
    if (opt.executable && typeof opt.executable !== 'boolean')
      throw new Error(
        `Option executable needs a boolean value, but ${inspect(opt.executable)} was given.`
      );
    if (
      opt.type &&
      (typeof opt.type !== 'string' ||
        !['block', 'character', 'dir', 'fifo', 'file', 'socket', 'link'].includes(opt.type))
    )
      throw new Error(
        `Option type needs one of block, character, dir, fifo, file, socket, link as value, but ${inspect(
          opt.type
        )} was given.`
      );
    if (opt.minSize && typeof opt.minSize !== 'number')
      throw new Error(
        `Option minSize needs a numeric value, but ${inspect(opt.minSize)} was given.`
      );
    if (opt.maxSize && typeof opt.maxSize !== 'number')
      throw new Error(
        `Option maxSize needs a numeric value, but ${inspect(opt.maxSize)} was given.`
      );
    if (
      opt.minCTime &&
      typeof opt.minCTime !== 'number' &&
      !(<any>opt.minCTime instanceof Date)
    )
      throw new Error(
        `Option minCTime needs a numeric or Date value, but ${inspect(
          opt.minCTime
        )} was given.`
      );
    if (
      opt.maxCTime &&
      typeof opt.maxCTime !== 'number' &&
      !(<any>opt.maxCTime instanceof Date)
    )
      throw new Error(
        `Option maxCTime needs a numeric or Date value, but ${inspect(
          opt.maxCTime
        )} was given.`
      );
    if (
      opt.minMTime &&
      typeof opt.minMTime !== 'number' &&
      !(<any>opt.minMTime instanceof Date)
    )
      throw new Error(
        `Option minMTime needs a numeric or Date value, but ${inspect(
          opt.minMTime
        )} was given.`
      );
    if (
      opt.maxMTime &&
      typeof opt.maxMTime !== 'number' &&
      !(<any>opt.maxMTime instanceof Date)
    )
      throw new Error(
        `Option maxMTime needs a numeric or Date value, but ${inspect(
          opt.maxMTime
        )} was given.`
      );
    if (
      opt.minATime &&
      typeof opt.minATime !== 'number' &&
      !(<any>opt.minATime instanceof Date)
    )
      throw new Error(
        `Option minATime needs a numeric or Date value, but ${inspect(
          opt.minATime
        )} was given.`
      );
    if (
      opt.maxATime &&
      typeof opt.maxATime !== 'number' &&
      !(<any>opt.maxATime instanceof Date)
    )
      throw new Error(
        `Option maxATime needs a numeric or Date value, but ${inspect(
          opt.maxATime
        )} was given.`
      );
  }

  protected typeDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'file')
    return t('describe.type');
  }
  protected async typeValidator(work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'file')
    if (typeof work.value !== 'string') {
      return Promise.reject(
        new ValidationError(this, work, t('error.type'))
      );
    }
  }

  protected baseDirDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'file')
    const opt = <Options>this.opt;
    if (opt.baseDir) return t('describe.baseDir', { base: opt.baseDir });
  }
  protected async baseDirValidator(work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    if (opt.baseDir) work.value = resolve(opt.baseDir.replace(/^~/, require('os').homedir()), work.value.replace(/^~/, require('os').homedir()));
  }

  public stringDescriber(depth: number, path: string, lang: string) {
    const msg: string[] = [];
    msg.join(this.stringSchema.allowDescriber(depth, path, lang));
    return msg.filter(e => e).join(' ');
  }
  protected async stringValidator(work: Workbench, lang: string): Promise<void> {
    const opt = <Options>this.opt;
    // check value
    if (opt.allow || opt.disallow)
      return this.stringSchema.validate(new Workbench(new DataStore(work.value.href)), lang);
  }

  protected existsDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'file')
    const opt = <Options>this.opt;
    if (opt.executable) return t('describe.executable');
    if (opt.writable) return t('describe.writable');
    if (opt.readable) return t('describe.readable');
    if (opt.exists) return t('describe.exists');
  }
  protected async existsValidator(work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'file')
    const opt = <Options>this.opt;
    const { access } = fs.promises
    const tests: Promise<void>[] = []
    const file = work.value.replace(/^~/, require('os').homedir())
    if (opt.exists)
      tests.push(access(file, fs.constants.F_OK).catch(e =>
        Promise.reject(
          new ValidationError(this, work, t('error.exists', { file }))
        )
      ))
    if (opt.executable)
      tests.push(access(file, fs.constants.X_OK).catch(e =>
        Promise.reject(
          new ValidationError(this, work, t('error.executable', { file }))
        )
      ))
    if (opt.writable)
      tests.push(access(file, fs.constants.W_OK).catch(e =>
        Promise.reject(
          new ValidationError(this, work, t('error.writable', { file }))
        )
      ))
    if (opt.readable)
      tests.push(access(file, fs.constants.R_OK).catch(e =>
        Promise.reject(
          new ValidationError(this, work, t('error.readable', { file }))
        )
      ))
    return Promise.all(tests).then(_ => Promise.resolve())
  }

  protected fileTypeDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'file')
    const opt = <Options>this.opt;
    if (opt.type) return t('describe.fileType', { context: opt.type });
  }
  protected async fileTypeValidator(work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'file')
    const opt = <Options>this.opt;
    if (opt.type) {
      const file = work.value.replace(/^~/, require('os').homedir())
      if (!work.temp.stats) work.temp.stats = await fs.promises.lstat(file);
      const stats = work.temp.stats;
      if (opt.type === 'block' && !stats.isBlockDevice())
        return Promise.reject(
          new ValidationError(
            this,
            work,
            t('error.fileType', { context: 'block', value: file })
          )
        );
      if (opt.type === 'character' && !stats.isCharacterDevice())
        return Promise.reject(
          new ValidationError(
            this,
            work,
            t('error.fileType', { context: 'character', value: file })
          )
        );
      if (opt.type === 'dir' && !stats.isDirectory())
        return Promise.reject(
          new ValidationError(
            this,
            work,
            t('error.fileType', { context: 'dir', value: file })
          )
        );
      if (opt.type === 'fifo' && !stats.isFIFO())
        return Promise.reject(
          new ValidationError(
            this,
            work,
            t('error.fileType', { context: 'fifo', value: file })
          )
        );
      if (opt.type === 'file' && !stats.isFile())
        return Promise.reject(
          new ValidationError(
            this,
            work,
            t('error.fileType', { context: 'file', value: file })
          )
        );
      if (opt.type === 'socket' && !stats.isSocket())
        return Promise.reject(
          new ValidationError(
            this,
            work,
            t('error.fileType', { context: 'socket', value: file })
          )
        );
      if (opt.type === 'link' && !stats.isSymbolicLink())
        return Promise.reject(
          new ValidationError(
            this,
            work,
            t('error.fileType', { context: 'link', value: file })
          )
        );
    }
  }

  protected sizeDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'file')
    const opt = <Options>this.opt;
    let msg: string[] = [];
    if (opt.minSize) msg.push(t('describe.min', { min: opt.minSize }));
    if (opt.maxSize) msg.push(t('describe.max', { max: opt.maxSize }));
    return msg.join(' ');
  }
  protected async sizeValidator(work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'file')
    const opt = <Options>this.opt;
    const file = work.value.replace(/^~/, require('os').homedir())
    if (opt.minSize) {
      if (!work.temp.stats) work.temp.stats = await fs.promises.lstat(file);
      const stats: fs.Stats = work.temp.stats;
      if (stats.size < opt.minSize)
        return Promise.reject(
          new ValidationError(
            this,
            work,
            t('error.min', { size: stats.size, min: opt.minSize })
          )
        );
    }
    if (opt.maxSize) {
      if (!work.temp.stats) work.temp.stats = await fs.promises.lstat(file);
      const stats: fs.Stats = work.temp.stats;
      if (stats.size > opt.maxSize)
        return Promise.reject(
          new ValidationError(
            this,
            work,
            t('error.max', { size: stats.size, max: opt.maxSize })
          )
        );
    }
  }

  protected timeDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'file')
    const opt = <Options>this.opt;
    let msg: string[] = [];
    if (opt.minCTime)
      msg.push(
        t('describe.minCTime', { context: typeof opt.minCTime === 'number' ? 'number' : 'date', min: opt.minCTime })
      );
    if (opt.maxCTime)
      msg.push(
        t('describe.maxCTime', { context: typeof opt.maxCTime === 'number' ? 'number' : 'date', max: opt.maxCTime })
      );
    if (opt.minMTime)
      msg.push(
        t('describe.minMTime', { context: typeof opt.minMTime === 'number' ? 'number' : 'date', min: opt.minMTime })
      );
    if (opt.maxMTime)
      msg.push(
        t('describe.maxMTime', { context: typeof opt.maxMTime === 'number' ? 'number' : 'date', max: opt.maxMTime })
      );
    if (opt.minATime)
      msg.push(
        t('describe.minATime', { context: typeof opt.minATime === 'number' ? 'number' : 'date', min: opt.minATime })
      );
    if (opt.maxATime)
      msg.push(
        t('describe.maxATime', { context: typeof opt.maxATime === 'number' ? 'number' : 'date', max: opt.maxATime })
      );
    return msg.join(' ');
  }
  protected async timeValidator(work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'file')
    const opt = <Options>this.opt;
    const file = work.value.replace(/^~/, require('os').homedir())
    if (opt.minCTime) {
      if (!work.temp.stats) work.temp.stats = await fs.promises.lstat(file);
      const stats: fs.Stats = work.temp.stats;
      const check =
        typeof opt.minCTime === 'number'
          ? new Date(new Date().getTime() - opt.minCTime * 1000)
          : opt.minCTime;
      if (stats.ctime < check)
        return Promise.reject(
          new ValidationError(
            this,
            work,
            t('error.minCTime', { time: stats.ctime })
          )
        );
    }
    if (opt.maxCTime) {
      if (!work.temp.stats) work.temp.stats = await fs.promises.lstat(file);
      const stats: fs.Stats = work.temp.stats;
      const check =
        typeof opt.maxCTime === 'number'
          ? new Date(new Date().getTime() - opt.maxCTime * 1000)
          : opt.maxCTime;
      if (stats.ctime > check)
        return Promise.reject(
          new ValidationError(
            this,
            work,
            t('error.maxCTime', { time: stats.ctime })
          )
        );
    }

    if (opt.minMTime) {
      if (!work.temp.stats) work.temp.stats = await fs.promises.lstat(file);
      const stats: fs.Stats = work.temp.stats;
      const check =
        typeof opt.minMTime === 'number'
          ? new Date(new Date().getTime() - opt.minMTime * 1000)
          : opt.minMTime;
      if (stats.mtime < check)
        return Promise.reject(
          new ValidationError(
            this,
            work,
            t('error.minMTime', { time: stats.ctime })
          )
        );
    }
    if (opt.maxMTime) {
      if (!work.temp.stats) work.temp.stats = await fs.promises.lstat(file);
      const stats: fs.Stats = work.temp.stats;
      const check =
        typeof opt.maxMTime === 'number'
          ? new Date(new Date().getTime() - opt.maxMTime * 1000)
          : opt.maxMTime;
      if (stats.mtime > check)
        return Promise.reject(
          new ValidationError(
            this,
            work,
            t('error.maxMTime', { time: stats.ctime })
          )
        );
    }

    if (opt.minATime) {
      if (!work.temp.stats) work.temp.stats = await fs.promises.lstat(file);
      const stats: fs.Stats = work.temp.stats;
      const check =
        typeof opt.minATime === 'number'
          ? new Date(new Date().getTime() - opt.minATime * 1000)
          : opt.minATime;
      if (stats.atime < check)
        return Promise.reject(
          new ValidationError(
            this,
            work,
            t('error.minATime', { time: stats.ctime })
          )
        );
    }
    if (opt.maxATime) {
      if (!work.temp.stats) work.temp.stats = await fs.promises.lstat(file);
      const stats: fs.Stats = work.temp.stats;
      const check =
        typeof opt.maxATime === 'number'
          ? new Date(new Date().getTime() - opt.maxATime * 1000)
          : opt.maxATime;
      if (stats.atime > check)
        return Promise.reject(
          new ValidationError(
            this,
            work,
            t('error.maxATime', { time: stats.ctime })
          )
        );
    }
  }
}

export default FileSchema;
