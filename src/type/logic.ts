import { inspect } from 'util';
import async from '@alinex/async';
import Debug from 'debug';

import * as base from './base';
import Workbench from '../workbench';
import Schema from '../schema';
import ValidationError from '../error';
import { T, loadNamespace } from '../i18n';

loadNamespace('logic')

const debug = Debug('validator:logic');

export interface Options extends base.Options {
  if?: Schema;
  operator?: 'and' | 'or'; // default is and
  check?: Schema | Schema[];
  else?: Schema | Schema[];
}

let ID_COUNT = 0;

export class LogicSchema extends base.BaseSchema {
  _refID: string;
  _refNum = 0;
  constructor(opt: Options = {}) {
    super(opt);
    this._refID = `Logic_${++ID_COUNT}`;
    // rules
    this.describer = [
      this.describer[0], // 0: default
      this.conditionDescriber, // 1: if, operator, check, else
      this.describer[1] // 2: raw
    ];
    this.validator = [
      this.validator[0], // 0: default
      this.conditionValidator, // 1: if, operator, check, else
      this.validator[1] // 2: raw
    ];
  }

  public set (opt: Options) {
    super.set(opt);
  }

  protected check (opt: Options) {
    super.check(opt);
    if (opt.if && !(opt.if instanceof base.BaseSchema))
      throw new Error(
        `Option if needs a schema definition, but ${inspect(opt.if)} was given.`
      );
    if (opt.operator && !['and', 'or'].includes(opt.operator))
      throw new Error(
        `Option operator should be one of 'and', 'or', but ${inspect(
          opt.operator
        )} was given.`
      );
    if (
      opt.check &&
      (!(opt.check instanceof base.BaseSchema) &&
        (!Array.isArray(opt.check) ||
          opt.check.filter(e => !(e instanceof base.BaseSchema)).length))
    )
      throw new Error(
        `Option check needs an array with schema definitions, but ${inspect(
          opt.check
        )} was given.`
      );
    if (
      opt.else &&
      (!(opt.else instanceof base.BaseSchema) &&
        (!Array.isArray(opt.else) ||
          opt.else.filter(e => !(e instanceof base.BaseSchema)).length))
    )
      throw new Error(
        `Option else needs an array with schema definitions, but ${inspect(
          opt.else
        )} was given.`
      );
    if (!opt.check && !opt.if)
      throw new Error(`As long as no if-block is defined a check-definition is needed.`);
    if (opt.else && !opt.if)
      throw new Error(`An else-block makes only sense combined with an if-condition.`);
  }

  /**
   * Export schema as tree data structure.
   */
  export (parent: any = {}): any {
    const def: any = super.export();
    if (def.SCHEMA_META) return def.SCHEMA_META;
    // check for circular references
    if (parent[this._refID]) {
      parent[this._refID]._refNum++;
      return {
        SCHEMA_TYPE: this.constructor.name.replace(/Schema$/, ''),
        SCHEMA_REF: this._refID
      };
    }
    const map: any = { ...parent };
    map[this._refID] = this;
    // export
    if (def.if) def.if = def.if.export(map);
    if (def.check) {
      if (Array.isArray(def.check))
        def.check = def.check.map((schema: any) => schema.export(map));
      else def.check = def.check.export(map);
    }
    if (def.else) {
      if (Array.isArray(def.else))
        def.else = def.else.map((schema: any) => schema.export(map));
      else def.else = def.else.export(map);
    }
    if (this._refNum) def.SCHEMA_ID = this._refID;
    return def;
  }

  protected conditionDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'logic')
    const opt = <Options>this.opt;
    const msg: string[] = [];
    if (opt.if) msg.push(t('describe.if', { condition: opt.if.describe(depth, path, lang) }));
    const op = t('describe.operator', { operator: t(`core:phrase.${opt.operator || 'and'}`).toUpperCase() })
    if (opt.check) {
      const sub = Array.isArray(opt.check)
        ? op +
        '\n-   ' +
        opt.check.map(e => e.describe(depth, path, lang).replace(/\n/g, '\n    ')).join('\n-   ')
        : t('describe.check', { condition: opt.check.describe(depth, path, lang) });
      const s = `${opt.if ? t('phrase.then') + ' ' : ''}${sub}`;
      msg.push(s.substr(0, 1).toUpperCase() + s.substr(1));
    }
    if (opt.if && opt.else) {
      const sub = Array.isArray(opt.else)
        ? op +
        '\n- ' +
        opt.else.map(e => e.describe(depth, path, lang).replace(/\n/g, '\n  ')).join('\n- ')
        : t('describe.check', { condition: opt.else.describe(depth, path, lang) });
      const s = `${t('phrase.else')} ${sub}`;
      msg.push(s.substr(0, 1).toUpperCase() + s.substr(1));
    }
    return msg.join('\n');
  }
  protected async conditionValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'logic')
    const opt = <Options>this.opt;
    let run = 'check';
    let ifFailure = new Error();
    if (opt.if) {
      debug('run if-condition');
      await opt.if.validate(work.clone(), lang).catch(e => {
        run = 'else';
        ifFailure = e;
      });
    }
    const op = opt.operator || 'and';
    // run checks
    if (run === 'check' && opt.check) {
      debug('run check-block');
      if (Array.isArray(opt.check)) {
        if (op === 'or') {
          // OR
          const res = await async.map(opt.check, (schema: Schema) => {
            const pwork = work.clone();
            return schema
              .validate(pwork, lang)
              .then(() => Promise.resolve(pwork))
              .catch(e => Promise.resolve(e))
          });
          const success = res.filter((e: any) => !(e instanceof Error));
          if (success.length) work.value = success[0].value;
          else
            throw new ValidationError(
              this,
              work,
              t('error.none') +
              '\n-   ' +
              res.map((e: any) => e.toString().replace(/\n/g, '\n    ')).join('\n-   ')
            );
        } // AND
        else await async.each(opt.check, (schema: Schema) => schema.validate(work, lang), 1);
      } else await opt.check.validate(work, lang);
    } else if (run === 'else' && opt.else) {
      debug('run else-block');
      if (Array.isArray(opt.else)) await async.each(opt.else, (schema: Schema) => schema.validate(work, lang), 1);
      else await opt.else.validate(work, lang);
    } else {
      if (run === 'else')
        throw new ValidationError(
          this,
          work,
          t('error.else', { message: ifFailure.message || ifFailure })
        );
      else
        throw new ValidationError(
          this,
          work,
          t('error.check')
        );
    }
  }
}

export default LogicSchema;
