import { inspect } from 'util';

import * as base from './base';
import Workbench from '../workbench';
import Reference from '../reference';
import ValidationError from '../error';
import { T, loadNamespace } from '../i18n';

loadNamespace('boolean')

export interface Options extends base.Options {
  default?: boolean | Reference;
  truthy?: any[];
  falsy?: any[];
  value?: boolean;
  insensitive?: boolean;
  tolerant?: boolean;
  format?: [any, any];
}

export class BooleanSchema extends base.BaseSchema {
  constructor(opt: Options = {}) {
    super(opt);
    // rules
    this.describer = [
      this.typeDescriber, // 0
      this.describer[0], // 1: default
      this.parseDescriber, // 2: truthy, falsy, insensitive, tolerant
      this.valueDescriber, // 3: raw
      this.formatDescriber, // 4: format
      this.describer[1] // 5: raw
    ];
    this.validator = [
      this.validator[0], // 0: default
      this.parseValidator, // 1: truthy, falsy, insensitive, tolerant
      this.valueValidator, // 2: format
      this.formatValidator, // 3: format
      this.validator[1] // 4: raw
    ];
  }

  public set (opt: Options) {
    super.set(opt);
  }

  protected check (opt: Options) {
    super.check(opt);
    if (opt.default && !(typeof opt.default === 'boolean' || opt.default instanceof Reference))
      throw new Error(
        `Option default needs a boolean or Reference value, but ${inspect(
          opt.default
        )} was given.`
      );
    if (opt.truthy && !(Array.isArray(opt.truthy) && opt.truthy.length > 0))
      throw new Error(
        `Option truthy needs an array list, but ${inspect(opt.truthy)} was given.`
      );
    if (opt.falsy && !(Array.isArray(opt.falsy) && opt.falsy.length > 0))
      throw new Error(
        `Option falsy needs an array list, but ${inspect(opt.falsy)} was given.`
      );
    if (opt.insensitive && typeof opt.insensitive !== 'boolean')
      throw new Error(
        `Option insensitive needs a boolean value, but ${inspect(
          opt.insensitive
        )} was given.`
      );
    if (opt.insensitive && !(opt.truthy || opt.falsy))
      throw new Error(`Option insensitive makes only sense together with truthy or falsy.`);
    if (opt.tolerant && typeof opt.tolerant !== 'boolean')
      throw new Error(
        `Option tolerant needs a boolean value, but ${inspect(opt.tolerant)} was given.`
      );
    if (typeof opt.value !== 'undefined' && typeof opt.value !== 'boolean')
      throw new Error(
        `Option value has to be a boolean, but ${inspect(opt.value)} was given.`
      );
    if (opt.format && !(Array.isArray(opt.format) && opt.format.length === 2))
      throw new Error(
        `Option format needs an array with a true and lfalse value, but ${inspect(
          opt.format
        )} was given.`
      );
  }

  protected typeDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'boolean')
    return t('describe.type');
  }

  private parseDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'boolean')
    const opt = <Options>this.opt;
    let truthy: any[] = opt.truthy || [true];
    let falsy: any[] = opt.falsy || [false];
    if (opt.tolerant) {
      truthy.push(
        1,
        '1',
        'true',
        'on',
        'y',
        'yes',
        '+',
        'x',
        t('value.y'),
        t('value.yes'),
        t('value.on'),
        t('value.true')
      );
      truthy = truthy.filter((e, pos) => truthy.indexOf(e) == pos);
      falsy.push(
        0,
        '0',
        'false',
        'off',
        'n',
        'no',
        '-',
        t('value.n'),
        t('value.no'),
        t('value.off'),
        t('value.false')
      );
      falsy = falsy.filter((e, pos) => falsy.indexOf(e) == pos);
    }
    let msg = t('describe.parse', { truthy: inspect(truthy), falsy: inspect(falsy) })
    if (opt.tolerant || opt.insensitive)
      msg += ' ' + t('describe.tolerant')
    return msg;
  }
  private async parseValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'boolean')
    const opt = <Options>this.opt;
    let truthy: any[] = opt.truthy || [];
    let falsy: any[] = opt.falsy || [];
    if (opt.tolerant) {
      truthy.push(
        1,
        '1',
        'true',
        'on',
        'y',
        'yes',
        '+',
        'x',
        t('value.y'),
        t('value.yes'),
        t('value.on'),
        t('value.true')
      );
      falsy.push(
        0,
        '0',
        'false',
        'off',
        'n',
        'no',
        '-',
        t('value.n'),
        t('value.no'),
        t('value.off'),
        t('value.false')
      );
    }
    if (opt.insensitive || opt.tolerant) {
      truthy = truthy.map(e => (typeof e === 'string' ? e.toLowerCase() : e));
      falsy = falsy.map(e => (typeof e === 'string' ? e.toLowerCase() : e));
    }
    // check value
    let test =
      (opt.insensitive || opt.tolerant) && typeof work.value === 'string'
        ? work.value.toLowerCase()
        : work.value;
    if (truthy.includes(test)) work.value = true;
    if (falsy.includes(test)) work.value = false;
    if (work.value !== true && work.value !== false) {
      throw new ValidationError(
        this,
        work,
        t('error.parse')
      );
    }
  }

  private valueDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'boolean')
    const opt = <Options>this.opt;
    if (opt.value)
      return t('describe.value', { value: opt.value })
  }
  private async valueValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'boolean')
    const opt = <Options>this.opt;
    if (typeof opt.value === 'boolean' && work.value !== opt.value) {
      throw new ValidationError(
        this,
        work,
        t('error.value', { value: opt.value })
      );
    }
  }

  private formatDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'boolean')
    const opt = <Options>this.opt;
    if (opt.format)
      return t('describe.format', { true: inspect(opt.format[0]), false: inspect(opt.format[1]) })
  }
  private async formatValidator (work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    if (opt.format) work.value = opt.format[work.value ? 0 : 1];
  }
}

export default BooleanSchema;
