import { inspect } from 'util';
import axios from 'axios';
import DataStore from '@alinex/datastore';
import { URL } from 'url';

import { AnySchema, Options as AnyOptions } from './any';
import Workbench from '../workbench';
import Reference from '../reference';
import ValidationError from '../error';
import IPSchema from './ip';
import StringSchema from './string';
import { T, loadNamespace } from '../i18n';

loadNamespace('url')

export interface Options extends AnyOptions {
  default?: string | Reference;
  resolve?: string;
  allow?: (string | RegExp | Reference)[] | Reference;
  disallow?: (string | RegExp | Reference)[] | Reference;
  allowIP?: (string | Reference)[] | Reference;
  disallowIP?: (string | Reference)[] | Reference;
  allowCountry?: (string | Reference)[] | Reference;
  disallowCountry?: (string | Reference)[] | Reference;
  exists?: boolean;
  format?: 'href' | 'object';
}

export class URLSchema extends AnySchema {
  private ipSchema: IPSchema;
  private stringSchema: StringSchema;
  constructor(opt: Options = {}) {
    // extended options
    super(opt);
    // rules
    this.describer = [
      this.typeDescriber, // 0
      this.describer[1], // 1: default
      this.resolveDescriber, // 2: resolve
      this.stringDescriber, // 3: allow, disallow
      this.ipDescriber, // 4: allowIP, disallowIP, allowCountry, disallowCountry
      this.existsDescriber, // 5: exists
      this.formatDescriber, // 6: format
      this.describer[3] // 7: raw
    ];
    this.validator = [
      this.validator[0], // 0: default
      this.resolveValidator, // 1: resolve
      this.typeValidator, // 2
      this.stringValidator, // 3: allow,disallow
      this.ipValidator, // 4: allowIP, disallowIP, allowCountry, disallowCountry
      this.existsValidator, // 5: exists
      this.formatValidator, // 6: format
      this.validator[2] // 7: raw
    ];
    // set sub schema
    this.stringSchema = new StringSchema({
      allow: opt.allow,
      disallow: opt.disallow
    });
    this.ipSchema = new IPSchema({
      lookup: true,
      allow: opt.allowIP,
      disallow: opt.disallowIP,
      allowCountry: opt.allowCountry,
      disallowCountry: opt.disallowCountry
    });
  }

  public set(opt: Options) {
    super.set(opt);
  }

  protected check(opt: Options) {
    super.check(opt);
    // option validity
    if (opt.resolve) {
      if (typeof opt.resolve !== 'string')
        throw new Error(
          `Option resolve needs a string value, but ${inspect(opt.resolve)} was given.`
        );
      try {
        new URL(opt.resolve);
      } catch (e) {
        throw new Error('The option resolve has no valid URL.');
      }
    }
    if (opt.allowIP && Array.isArray(opt.allowIP))
      opt.allowIP.forEach(e => {
        if (typeof e !== 'string' && !(e instanceof Reference))
          throw new Error(
            `Option allowIP needs string, RegExp or Reference values, but ${inspect(
              e
            )} was given.`
          );
      });
    if (opt.disallowIP && Array.isArray(opt.disallowIP))
      opt.disallowIP.forEach(e => {
        if (typeof e !== 'string' && !(e instanceof Reference))
          throw new Error(
            `Option disallowIP needs string, RegExp or Reference values, but ${inspect(
              e
            )} was given.`
          );
      });
    if (opt.allowCountry && Array.isArray(opt.allowCountry))
      opt.allowCountry.forEach(e => {
        if (typeof e !== 'string' && !(e instanceof Reference))
          throw new Error(
            `Option allowCountry needs string or Reference values, but ${inspect(
              e
            )} was given.`
          );
      });
    if (opt.disallowCountry && Array.isArray(opt.disallowCountry))
      opt.disallowCountry.forEach(e => {
        if (typeof e !== 'string' && !(e instanceof Reference))
          throw new Error(
            `Option disallowCountry needs string or Reference values, but ${inspect(
              e
            )} was given.`
          );
      });
    if (opt.exists && typeof opt.exists !== 'boolean')
      throw new Error(
        `Option exists needs a boolean value, but ${inspect(opt.exists)} was given.`
      );
    if (opt.format && !['href', 'object'].includes(opt.format))
      throw new Error(
        `Option format needs 'href', 'object' as value, but ${inspect(
          opt.format
        )} was given.`
      );
  }

  protected resolveDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'url')
    const opt = <Options>this.opt;
    if (opt.resolve) return t('describe.resolve', { base: opt.resolve });
  }
  protected async resolveValidator(work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    if (opt.resolve) work.value = new URL(work.value, opt.resolve);
  }

  protected typeDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'url')
    return t('describe.type');
  }
  protected async typeValidator(work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'url')
    // parse url into it's parts
    try {
      work.value = new URL(work.value);
    } catch (e) {
      throw new ValidationError(this, work, t('error.type'));
    }
  }

  public stringDescriber(depth: number, path: string, lang: string) {
    const msg: string[] = [];
    msg.join(this.stringSchema.allowDescriber(depth, path, lang));
    return msg.filter(e => e).join(' ');
  }
  protected async stringValidator(work: Workbench, lang: string): Promise<void> {
    const opt = <Options>this.opt;
    // check value
    if (opt.allow || opt.disallow)
      return this.stringSchema.validate(new Workbench(new DataStore(work.value.href)), lang);
  }

  public ipDescriber(depth: number, path: string, lang: string) {
    const msg: string[] = [];
    msg.join(this.ipSchema.allowDescriber(depth, path, lang));
    msg.join(this.ipSchema.countryDescriber(depth, path, lang));
    return msg.filter(e => e).join(' ');
  }
  protected async ipValidator(work: Workbench, lang: string): Promise<void> {
    const opt = <Options>this.opt;
    // check value
    if (opt.allowIP || opt.disallowIP || opt.allowCountry || opt.disallowCountry)
      return this.ipSchema.validate(work.clone(), lang);
  }

  protected existsDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'url')
    const opt = <Options>this.opt;
    if (opt.exists) return t('describe.exists');
  }
  protected async existsValidator(work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'url')
    const opt = <Options>this.opt;
    if (opt.exists)
      try {
        await axios.get(work.value.href);
      } catch (e) {
        throw new ValidationError(
          this,
          work,
          t(`error.exists`, { url: work.value.href, error: e })
        );
      }
  }

  private formatDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'url')
    const opt = <Options>this.opt;
    if (!opt.raw) {
      const format = opt.format || 'address';
      return t('describe.format', { format });
    }
  }
  private async formatValidator(work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    // check length
    const format = opt.format || 'href';
    if (format !== 'object' && !opt.raw) work.value = work.value[format];
  }
}

export default URLSchema;
