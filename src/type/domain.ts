import * as punycode from 'punycode';
import { inspect, promisify } from 'util';
import * as dns from 'dns';

import { AnySchema, Options as AnyOptions } from './any';
import Workbench from '../workbench';
import Reference from '../reference';
import ValidationError from '../error';
import IPSchema from './ip';
import { T, loadNamespace } from '../i18n';

loadNamespace('domain')

const lookup = promisify(dns.lookup);
// use multiple nameservers for stability
const resolver = new dns.Resolver();
resolver.setServers(
  resolver.getServers().concat(['8.8.8.8', '8.8.4.4', '77.243.183.22', '77.243.183.66'])
);
const resolve = promisify(resolver.resolve.bind(resolver));

const DNS_RECORDS = ['A', 'AAAA', 'ANY', 'CNAME', 'MX', 'NAPTR', 'NS', 'PTR', 'SOA', 'SRV', 'TXT'];

export interface Options extends AnyOptions {
  default?: string | Reference;
  sanitize?: boolean;
  min?: number;
  max?: number;
  allow?: (string | Reference)[] | Reference;
  disallow?: (string | Reference)[] | Reference;
  allowCountry?: (string | Reference)[] | Reference;
  disallowCountry?: (string | Reference)[] | Reference;
  registered?: boolean | string[];
  punycode?: boolean;
  resolve?: boolean;
}

export class DomainSchema extends AnySchema {
  private countrySchema: IPSchema;
  constructor(opt: Options = {}) {
    // extended options
    super(opt);
    // rules
    this.describer = [
      this.typeDescriber, // 0
      this.describer[1], // 1: default
      this.sanitizeDescriber, // 2: sanitize
      this.depthDescriber, // 3: min, max
      this.allowDescriber, // 4: allow, disallow
      this.countryDescriber, // 5: allowCountry, disallowCountry
      this.registeredDescriber, // 6: registered
      this.formatDescriber, // 7: punycode, resolve
      this.describer[3] // 8: raw
    ];
    this.validator = [
      this.validator[0], // 0: default
      this.sanitizeValidator, // 1: sanitize
      this.typeValidator, // 2
      this.depthValidator, // 3: min, max
      this.allowValidator, // 4: allow, disallow
      this.countryValidator, // 5: allowCountry, disallowCountry
      this.registeredValidator, // 6: registered
      this.formatValidator, // 7: punycode, resolve
      this.validator[2] // 8: raw
    ];
    // set sub schema
    this.countrySchema = new IPSchema({
      lookup: true,
      allowCountry: opt.allowCountry,
      disallowCountry: opt.disallowCountry,
      raw: true
    });
  }

  public set (opt: Options) {
    super.set(opt);
  }

  protected check (opt: Options) {
    super.check(opt);
    // option validity
    if (opt.sanitize && typeof opt.sanitize !== 'boolean')
      throw new Error(
        `Option sanitize needs a boolean value, but ${inspect(opt.sanitize)} was given.`
      );
    if (opt.min !== undefined && typeof opt.min !== 'number')
      throw new Error(`Option min needs a numeric value, but ${inspect(opt.min)} was given.`);
    if (opt.max !== undefined && typeof opt.max !== 'number')
      throw new Error(`Option max needs a numeric value, but ${inspect(opt.max)} was given.`);
    if (opt.min !== undefined && opt.max !== undefined && opt.min > opt.max)
      throw new Error(
        `Option min could not be greater than max, but ${opt.min} > ${opt.max} was given.`
      );
    if (opt.allow && Array.isArray(opt.allow))
      opt.allow.forEach(e => {
        if (typeof e !== 'string' && !(e instanceof Reference))
          throw new Error(
            `Option allow needs string or Reference values, but ${inspect(
              e
            )} was given.`
          );
      });
    if (opt.disallow && Array.isArray(opt.disallow))
      opt.disallow.forEach(e => {
        if (typeof e !== 'string' && !(e instanceof Reference))
          throw new Error(
            `Option disallow needs string or Reference values, but ${inspect(
              e
            )} was given.`
          );
      });
    if (opt.registered && typeof opt.registered !== 'boolean' && !Array.isArray(opt.registered))
      throw new Error(
        `Option registered needs a boolean or record type list as value, but ${inspect(
          opt.registered
        )} was given.`
      );
    if (Array.isArray(opt.registered))
      opt.registered.forEach(e => {
        if (!DNS_RECORDS.includes(e))
          throw new Error(
            `Option registered has ${inspect(
              opt.registered
            )} but only record types ${DNS_RECORDS.join(', ')} are possible.`
          );
      });
    if (opt.punycode && typeof opt.punycode !== 'boolean')
      throw new Error(
        `Option punycode needs a boolean value, but ${inspect(opt.punycode)} was given.`
      );
    if (opt.resolve && typeof opt.resolve !== 'boolean')
      throw new Error(
        `Option resolve needs a boolean value, but ${inspect(opt.resolve)} was given.`
      );
  }

  protected sanitizeDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'domain')
    const opt = <Options>this.opt;
    if (opt.sanitize) return t('describe.sanitize');
  }
  protected async sanitizeValidator (work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    // check value
    if (typeof work.value === 'string' && opt.sanitize)
      work.value = work.value.replace(/^.*(\/\/|@)|\/.*$/g, '');
  }

  protected typeDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'domain')
    return t('describe.type');
  }
  protected async typeValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'domain')
    work.value = punycode.toASCII(work.value);
    if (typeof work.value !== 'string') {
      return Promise.reject(
        new ValidationError(this, work, t('error.typeString'))
      );
    }
    if (work.value.length > 253) {
      return Promise.reject(
        new ValidationError(
          this,
          work,
          t('error.typeToLong', { value: work.value, length: work.value.length })
        )
      );
    }
    if (!work.value.match(/^[a-zA-Z0-9-.]+$/)) {
      return Promise.reject(
        new ValidationError(
          this,
          work,
          t('error.typeInvalid')
        )
      );
    }
    for (const p of work.value.split('.')) {
      if (p.length > 63)
        return Promise.reject(
          new ValidationError(
            this,
            work,
            t('error.typePartToLong', { value: p })
          )
        );
      if (!p.match(/^[a-zA-Z0-9]([a-zA-Z0-9-]*[a-zA-Z0-9])?$/))
        return Promise.reject(
          new ValidationError(
            this,
            work,
            t('error.typePartInvalid', { value: p })
          )
        );
    }
    if (!work.value.match(/(^|\.)[a-zA-Z]{2,63}$/)) {
      return Promise.reject(
        new ValidationError(
          this,
          work,
          t('error.typeTLD')
        )
      );
    }
  }

  private depthDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'domain')
    const opt = <Options>this.opt;
    if (opt.min && opt.min === opt.max)
      return t('describe.exact', { count: opt.max });
    else if (opt.min && opt.max)
      return t('describe.between', { min: opt.min, max: opt.max });
    else if (opt.min) return t('describe.min', { min: opt.min });
    else if (opt.max) return t('describe.max', { max: opt.max });
  }
  private async depthValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'domain')
    const opt = <Options>this.opt;
    let num = (work.value.match(/\./g) || []).length + 1;
    // check length
    if (opt.min && num < opt.min) {
      throw new ValidationError(
        this,
        work,
        t('error.min', { num, min: opt.min })
      );
    }
    if (opt.max && num > opt.max) {
      throw new ValidationError(
        this,
        work,
        t('error.max', { num, max: opt.max })
      );
    }
  }

  protected allowDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'domain')
    const opt = <Options>this.opt;
    let values: string[] = [];
    if (opt.allow instanceof Reference) {
      values.push(
        (values.length ? t('core:phrase.or') + ' ' : '') +
        t('core.phrase.within', { source: opt.allow.source, filter: opt.allow.filter || "''" })
      );
    } else if (opt.allow && opt.allow.length)
      opt.allow.forEach(e => {
        values.push(
          (values.length ? t('core:phrase.or') + ' ' : '') +
          (e instanceof Reference
            ? t('core.phrase.within', { source: e.source, filter: e.filter || "''" })
            : t('phrase.below', { value: inspect(e) }))
        );
      });
    if (opt.disallow instanceof Reference) {
      values.push(
        (values.length ? t('core:phrase.or') + ' ' : '') +
        t('core.phrase.within', { source: opt.disallow.source, filter: opt.disallow.filter })
      );
    } else if (opt.disallow && opt.disallow.length)
      opt.disallow.forEach(e => {
        values.push(
          (values.length ? t('core:phrase.and') + ' ' : '') +
          (e instanceof Reference
            ? t('core.phrase.notWithin', { source: e.source, filter: e.filter })
            : t('phrase.notBelow', { value: inspect(e) }))
        );
      });
    if (values.length)
      return (
        t('core:describe.allow') +
        '\n- ' +
        values.slice(0, 10).join('\n- ') +
        (values.length > 10 ? '\n- ' + t('core:phrase.more', { count: values.length - 10 }) : '') +
        '\n'
      );
  }
  protected async allowValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'domain')
    const opt = <Options>this.opt;
    // get list
    let allow: any[] = [];
    let disallow: any[] = [];
    if (opt.allow instanceof Reference) {
      let data = await opt.allow.stringArray(work);
      if (data) allow = data.slice(0);
    } else if (opt.allow && opt.allow.length) {
      for (let index = 0; index < opt.allow.length; index++) {
        let e = opt.allow[index];
        if (e instanceof Reference) {
          let data = await e.stringArray(work);
          if (data) allow = allow.concat(data);
        } else allow.push(e);
      }
    }
    if (opt.disallow instanceof Reference) {
      let data = await opt.disallow.stringArray(work);
      if (data) disallow = data.slice(0);
    } else if (opt.disallow && opt.disallow.length) {
      for (let index = 0; index < opt.disallow.length; index++) {
        let e = opt.disallow[index];
        if (e instanceof Reference) {
          let data = await e.stringArray(work);
          if (data) disallow = disallow.concat(data);
        } else disallow.push(e);
      }
    }
    // validate
    let uni = punycode.toUnicode(work.value);
    let tld = work.value.match(/[^.]*$/)[0];
    let tldOK = false;
    try {
      await lookup(tld);
      tldOK = true;
    } catch (e) { }
    let allowLevel = -1;
    if (allow && allow.length) {
      allowLevel = -10;
      allow.forEach(e => {
        if (
          work.value === e ||
          uni === e ||
          work.value.endsWith('.' + e) ||
          uni.endsWith('.' + e)
        ) {
          let num = e.match(/\ /) ? 0 : (e.match(/\./g) || []).length + 1;
          if (num > allowLevel) allowLevel = num;
        } else if (e.match(/tld|TLD/)) {
          let priv = e.match(/private/);
          if (((priv && !tldOK) || (!priv && tldOK)) && allowLevel < 0) allowLevel = 0;
        }
      });
    }
    let disallowLevel = -2;
    let disallowTop = '';
    if (disallow && disallow.length)
      disallow.forEach(e => {
        if (
          work.value === e ||
          uni === e ||
          work.value.endsWith('.' + e) ||
          uni.endsWith('.' + e)
        ) {
          let num = e.match(/\ /) ? 0 : (e.match(/\./g) || []).length + 1;
          if (num > disallowLevel) disallowLevel = num;
          disallowTop = e;
        } else if (e.match(/tld|TLD/)) {
          let priv = e.match(/private/);
          if (((priv && !tldOK) || (!priv && tldOK)) && allowLevel < 0) {
            allowLevel = 0;
            disallowTop = e;
          }
        }
      });
    // console.log(denyBits, allowBits);
    if (allowLevel === -10)
      throw new ValidationError(
        this,
        work,
        t('error.allow', { list: inspect(opt.allow).replace(/[\[\]]/g, '').trim() })
      );
    else if (disallowLevel >= allowLevel) {
      throw new ValidationError(
        this,
        work,
        t('error.disallow', { range: disallowTop })
      );
    }
  }

  protected countryDescriber (depth: number, path: string, lang: string) {
    return this.countrySchema.countryDescriber(depth, path, lang);
  }
  protected async countryValidator (work: Workbench, lang: string): Promise<void> {
    const opt = <Options>this.opt;
    // check value
    if (opt.allowCountry || opt.disallowCountry)
      return this.countrySchema.validate(work.clone(), lang);
  }

  private registeredDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'domain')
    const opt = <Options>this.opt;
    if (opt.registered === true)
      return t('describe.registered');
    else if (Array.isArray(opt.registered))
      return t('describe.registeredType', { type: opt.registered.join(', ') })
  }
  private async registeredValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'domain')
    const opt = <Options>this.opt;
    // check length
    if (opt.registered === true) {
      try {
        await resolve(work.value);
      } catch (e) {
        throw new ValidationError(
          this,
          work,
          t('error.registered')
        );
      }
    } else if (Array.isArray(opt.registered)) {
      let found = false;
      for (const e of opt.registered) {
        try {
          await resolve(work.value, e);
          found = true;
        } catch (e) { }
      }
      if (!found)
        throw new ValidationError(
          this,
          work,
          t('error.registeredType', { type: opt.registered.join(', ') })
        );
    }
  }

  private formatDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'domain')
    const opt = <Options>this.opt;
    if (opt.resolve)
      return t('describe.resolve');
    else if (opt.punycode)
      return t('describe.punycode');
  }
  private async formatValidator (work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    // check length
    if (opt.resolve)
      try {
        let res = await lookup(work.value);
        work.value = res.address;
      } catch (e) { }
    else if (!opt.punycode) work.value = punycode.toUnicode(work.value);
  }
}

export default DomainSchema;
