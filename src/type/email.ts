import { inspect, promisify } from 'util';
import * as email from 'email-addresses';
import { Resolver, MxRecord } from 'dns';
import * as net from 'net';
import { default as axios } from 'axios';
import async from '@alinex/async';
import { DataStore } from '@alinex/datastore';

import { Options as AnyOptions } from './any';
import { StringSchema } from './string';
import Workbench from '../workbench';
import Reference from '../reference';
import ValidationError from '../error';
import { T, loadNamespace } from '../i18n';

loadNamespace('email')

const PARALLEL_DNS_CALLS = 20;

const blacklists = new DataStore({ source: __dirname + '/../../data/blacklists.json' });
const graylists = new DataStore({ source: __dirname + '/../../data/graylists.json' });

// use multiple nameservers for stability
const resolver = new Resolver();
resolver.setServers(
  resolver.getServers().concat(['8.8.8.8', '8.8.4.4', '77.243.183.22', '77.243.183.66'])
);
const resolve = promisify(resolver.resolve.bind(resolver));
const resolveMx = promisify(resolver.resolveMx.bind(resolver));

export interface Options extends AnyOptions {
  default?: string | Reference;
  makeString?: boolean;
  normalize?: boolean;
  registered?: boolean;
  allow?: (string | RegExp | Reference)[] | Reference;
  disallow?: (string | RegExp | Reference)[] | Reference;
  blacklist?: boolean;
  graylist?: boolean;
  connect?: boolean;
  format?: 'address' | 'displayName' | 'object';
}

type parsedEmail = {
  name: string;
  address: string;
  local: string;
  domain: string;
};

const alias: { [key: string]: string } = {
  'googlemail.com': 'gmail.com'
};

const normFn: { [key: string]: Function } = {
  removeDots: (email: parsedEmail) => {
    email.local = email.local.replace(/\./g, '');
  },
  removePlusTags: (email: parsedEmail) => {
    email.local = email.local.replace(/\+.*$/, '');
  },
  removeMinusTags: (email: parsedEmail) => {
    email.local = email.local.replace(/-.*$/, '');
  },
  // not needed because this is always done
  //    lowercase: (email: parsedEmail) => {
  //        email.local = email.local.toLowerCase();
  //    },
  subdomainUser: (email: parsedEmail) => {
    // whatever@username.fastmail.com -> username@fastmail.com
    const parts = email.domain.split('.');
    if (parts.length === 3 && parts[0]) {
      email.local = <string>parts.shift();
      email.domain = parts.join('.');
    }
  }
};

async function detectProvider (email: parsedEmail): Promise<void> {
  let provider = await resolveMx(email.domain);
  // google
  if (provider.filter(e => e.exchange.match(/gmail-smtp.*\.google\.com$/)))
    normalizableProviders['gmail.com'].forEach(fn => fn(email));
  // fastmail
  if (provider.filter(e => e.exchange.match(/\.messagingengine\.com$/)))
    normalizableProviders['fastmail.com'].forEach(fn => fn(email));
}

const normalizableProviders: { [key: string]: Function[] } = {
  'facebook.com': [normFn.removeDots],
  'fastmail.com': [normFn.removePlusTags, normFn.subdomainUser],
  'fastmail.fm': [normFn.removePlusTags, normFn.subdomainUser],
  'gmail.com': [normFn.removeDots, normFn.removePlusTags],
  'google.com': [normFn.removeDots, normFn.removePlusTags],
  'hotmail.com': [normFn.removePlusTags],
  'live.com': [normFn.removeDots, normFn.removePlusTags],
  'outlook.com': [normFn.removePlusTags],
  'yahoo.com.ar': [normFn.removeMinusTags],
  'yahoo.com.au': [normFn.removeMinusTags],
  'yahoo.at': [normFn.removeMinusTags],
  'yahoo.be': [normFn.removeMinusTags],
  'yahoo.com.br': [normFn.removeMinusTags],
  'ca.yahoo.com': [normFn.removeMinusTags],
  'qc.yahoo.com': [normFn.removeMinusTags],
  'yahoo.com.co': [normFn.removeMinusTags],
  'yahoo.com.hr': [normFn.removeMinusTags],
  'yahoo.cz': [normFn.removeMinusTags],
  'yahoo.dk': [normFn.removeMinusTags],
  'yahoo.fi': [normFn.removeMinusTags],
  'yahoo.fr': [normFn.removeMinusTags],
  'yahoo.de': [normFn.removeMinusTags],
  'yahoo.gr': [normFn.removeMinusTags],
  'yahoo.com.hk': [normFn.removeMinusTags],
  'yahoo.hu': [normFn.removeMinusTags],
  'yahoo.co.in': [normFn.removeMinusTags],
  'yahoo.in': [normFn.removeMinusTags],
  'yahoo.co.id': [normFn.removeMinusTags],
  'yahoo.ie': [normFn.removeMinusTags],
  'yahoo.co.il': [normFn.removeMinusTags],
  'yahoo.it': [normFn.removeMinusTags],
  'yahoo.co.jp': [normFn.removeMinusTags],
  'yahoo.com.my': [normFn.removeMinusTags],
  'yahoo.com.mx': [normFn.removeMinusTags],
  'yahoo.ae': [normFn.removeMinusTags],
  'yahoo.nl': [normFn.removeMinusTags],
  'yahoo.co.nz': [normFn.removeMinusTags],
  'yahoo.no': [normFn.removeMinusTags],
  'yahoo.com.ph': [normFn.removeMinusTags],
  'yahoo.pl': [normFn.removeMinusTags],
  'yahoo.pt': [normFn.removeMinusTags],
  'yahoo.ro': [normFn.removeMinusTags],
  'yahoo.ru': [normFn.removeMinusTags],
  'yahoo.com.sg': [normFn.removeMinusTags],
  'yahoo.co.za': [normFn.removeMinusTags],
  'yahoo.es': [normFn.removeMinusTags],
  'yahoo.se': [normFn.removeMinusTags],
  'yahoo.ch': [normFn.removeMinusTags],
  'yahoo.com.tw': [normFn.removeMinusTags],
  'yahoo.co.th': [normFn.removeMinusTags],
  'yahoo.com.tr': [normFn.removeMinusTags],
  'yahoo.co.uk': [normFn.removeMinusTags],
  'yahoo.com': [normFn.removeMinusTags],
  'yahoo.com.vn': [normFn.removeMinusTags]
};

function checklist (
  name: string,
  access: { zone: string; url: string; desc?: string },
  record: MxRecord
): Promise<string | undefined> {
  const domain = record.exchange;
  const reverse = `${domain
    .split('.')
    .reverse()
    .join('.')}.${access.zone}`;
  //    console.log(`Check ${domain} in ${name} (${reverse})`);
  return resolve(reverse)
    .then(() => name)
    .catch(() => undefined);
}

let myIP: string = '';
async function getMyIP (): Promise<string> {
  if (myIP.length) return Promise.resolve(myIP);
  try {
    const res = await axios.get('http://ipinfo.io/ip');
    return res.data.trim();
  } catch {
    throw new Error(`Could not get own IP address (needed for further checks).`);
  }
}

async function connect (record: MxRecord): Promise<boolean> {
  const ip = await getMyIP();
  const domain = record.exchange;

  const timer = setTimeout(() => {
    //        debug('Attempt at connection exceeded timeout value');
    client.end();
    return false;
  }, 5000);
  const client = net.createConnection(25, domain, () => {
    //        debug('Send HELO command to mailserver...');
    client.write(`HELO ${ip}\r\n`);
    client.write('QUIT\r\n');
    client.end();
  });
  let res = '';
  client.on('data', data => {
    clearTimeout(timer);
    res += data.toString();
  });
  client.on('error', err => {
    clearTimeout(timer);
    //        debug(`Error from server ${domain}: ${err.message}`);
    return false;
  });
  client.on('end', () => {
    clearTimeout(timer);
    if (res.length) {
      //            debug(`Server ${domain} responded with:\n${res.trim()}`);
      return true;
    }
    //        debug(`No valid response from server ${domain}`);
    return false;
  });

  return true;
}

export class EmailSchema extends StringSchema {
  constructor(opt: Options = {}) {
    // extended options
    super(opt);
    // rules
    this.describer = [
      this.typeDescriber, // 0
      this.describer[1], // 1: default
      this.describer[2], // 2: makeString
      this.normalizeDescriber, // 3: normalize
      this.describer[8], // 4: allow, disallow
      this.registeredDescriber, // 5: registered
      this.blacklistDescriber, // 6: blacklist
      this.graylistDescriber, // 7: graylist
      this.connectDescriber, // 8: connect
      this.formatDescriber, // 9: format
      this.describer[9] // 10: raw
    ];
    this.validator = [
      this.validator[0], // 0: default
      this.validator[1], // 1: makeString
      this.typeValidator, // 2
      this.normalizeValidator, // 3: normalize
      this.validator[8], // 4: allow,disallow
      this.registeredValidator, // 5: registered
      this.blacklistValidator, // 6: blacklist
      this.graylistValidator, // 7: graylist
      this.connectValidator, // 8: connect
      this.formatValidator, // 9: format
      this.validator[9] // 10: raw
    ];
  }

  public set (opt: Options) {
    super.set(opt);
  }

  protected check (opt: Options) {
    super.check(opt);
    // option validity
    if (opt.normalize && typeof opt.normalize !== 'boolean')
      throw new Error(
        `Option normalize needs a boolean value, but ${inspect(opt.normalize)} was given.`
      );
    if (opt.registered && typeof opt.registered !== 'boolean')
      throw new Error(
        `Option registered needs a boolean value, but ${inspect(opt.registered)} was given.`
      );
    if (opt.blacklist && typeof opt.blacklist !== 'boolean')
      throw new Error(
        `Option blacklist needs a boolean value, but ${inspect(opt.blacklist)} was given.`
      );
    if (opt.graylist && typeof opt.graylist !== 'boolean')
      throw new Error(
        `Option graylist needs a boolean value, but ${inspect(opt.graylist)} was given.`
      );
    if (opt.connect && typeof opt.connect !== 'boolean')
      throw new Error(
        `Option connect needs a boolean value, but ${inspect(opt.connect)} was given.`
      );
    if (opt.format && !['address', 'displayName', 'object'].includes(opt.format))
      throw new Error(
        `Option format needs 'address', 'displayName', 'object' as value, but ${inspect(
          opt.format
        )} was given.`
      );
  }

  protected typeDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'email')
    return t('describe.type');
  }
  protected async typeValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'email')
    // extract name from email if present
    const parsed = email.parseOneAddress(work.value);
    if (!parsed) {
      if (typeof work.orig === 'string' && work.orig.length)
        return Promise.reject(
          new ValidationError(
            this,
            work,
            t('error.invalid', { value: work.orig })
          )
        );
      return Promise.reject(
        new ValidationError(this, work, t('error.type'))
      );
    }
    if (parsed.type === 'group')
      return Promise.reject(
        new ValidationError(
          this,
          work,
          t('error.noGroup')
        )
      );
    const mail = <email.ParsedMailbox>parsed;
    work.value = {
      name: mail.name,
      address: mail.address,
      local: mail.local,
      domain: mail.domain
    };
  }

  private normalizeDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'email')
    const opt = <Options>this.opt;
    if (opt.normalize)
      return t('describe.normalize')
  }
  private async normalizeValidator (work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    if (opt.normalize) {
      const email = work.value;
      const local = email.local;
      // always lowercase domain
      email.domain = email.domain.toLowerCase();
      // as no known big mailsystem is case-insensitive
      email.local = email.local.toLowerCase();
      // alias domains
      if (alias[email.domain]) email.domain = alias[email.domain];
      // provider rules
      if (normalizableProviders[email.domain])
        normalizableProviders[email.domain].forEach(fn => fn(email));
      else await detectProvider(email);
      // recalculate address
      email.address = email.local + '@' + email.domain;
      if (!email.name && local !== email.local) email.name = local;
    }
  }

  private registeredDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'email')
    const opt = <Options>this.opt;
    if (opt.registered)
      return t('describe.registered')
  }
  private async registeredValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'email')
    const opt = <Options>this.opt;
    if (opt.registered && !(await resolveMx(work.value.domain)).length)
      return Promise.reject(
        new ValidationError(
          this,
          work,
          t('error.registered', { value: work.value.domain })
        )
      );
  }

  private blacklistDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'email')
    const opt = <Options>this.opt;
    if (opt.blacklist) return t('describe.blacklist');
  }
  private async blacklistValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'email')
    const opt = <Options>this.opt;
    if (opt.blacklist) {
      const list: {
        [key: string]: { zone: string; url: string; desc?: string };
      } = await blacklists.load();
      const address = await resolveMx(work.value.domain);
      const checks: [string, MxRecord][] = [];
      Object.keys(list).forEach(bl => address.forEach(addr => checks.push([bl, addr])));
      const found = await async.filter(
        checks,
        (e: [string, MxRecord]) =>
          checklist(e[0], list[e[0]], e[1]).then(e => e !== undefined),
        PARALLEL_DNS_CALLS
      );
      if (found.length)
        return Promise.reject(
          new ValidationError(
            this,
            work,
            t('error.blacklist', { count: found.map(e => e[0]).length, list: found.map(e => e[0]).join(', ') })
          )
        );
    }
  }

  private graylistDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'email')
    const opt = <Options>this.opt;
    if (opt.graylist) return t('describe.graylist');
  }
  private async graylistValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'email')
    const opt = <Options>this.opt;
    if (opt.graylist) {
      const list: {
        [key: string]: { zone: string; url: string; desc?: string };
      } = await graylists.load();
      const address = await resolveMx(work.value.domain);
      const checks: [string, MxRecord][] = [];
      Object.keys(list).forEach(bl => address.forEach(addr => checks.push([bl, addr])));
      const found = await async.filter(
        checks,
        (e: [string, MxRecord]) =>
          checklist(e[0], list[e[0]], e[1]).then(e => e !== undefined),
        PARALLEL_DNS_CALLS
      );
      if (found.length)
        return Promise.reject(
          new ValidationError(
            this,
            work,
            t('error.graylist', { count: found.map(e => e[0]).length, list: found.map(e => e[0]).join(', ') })
          )
        );
    }
  }

  private connectDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'email')
    const opt = <Options>this.opt;
    if (opt.connect) return t('describe.connect');
  }
  private async connectValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'email')
    const opt = <Options>this.opt;
    if (opt.connect) {
      const address = await resolveMx(work.value.domain);
      const checks: PromiseLike<boolean>[] = [];
      address.forEach(addr => checks.push(async.retry(() => connect(addr))));
      let found: boolean[] = [];
      try {
        const res = await Promise.all(checks);
        found = res.filter(e => e);
      } catch { }
      if (!found.length)
        return Promise.reject(
          new ValidationError(
            this,
            work,
            t('error.connect')
          )
        );
    }
  }

  private formatDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'email')
    const opt = <Options>this.opt;
    if (!opt.raw) {
      const format = opt.format || 'address';
      return t('describe.format', { format: format });
    }
  }
  private async formatValidator (work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    // check length
    const format = opt.format || 'address';
    if (format !== 'object' && !opt.raw)
      work.value =
        format === 'displayName' && work.value.name
          ? `${work.value.name} <${work.value.address}>`
          : work.value.address;
  }
}

export default EmailSchema;
