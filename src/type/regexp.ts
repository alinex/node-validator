import { inspect } from 'util';

import { AnySchema, Options as AnyOptions } from './any';
import Workbench from '../workbench';
import Reference from '../reference';
import ValidationError from '../error';
import { T, loadNamespace } from '../i18n';

loadNamespace('regexp')

export interface Options extends AnyOptions {
  default?: RegExp | Reference;
  min?: number;
  max?: number;
}

export class RegExpSchema extends AnySchema {
  constructor(opt: Options = {}) {
    // extended options
    super(opt);
    // rules
    this.describer = [
      this.typeDescriber, // 0
      this.describer[1], // 1: default
      this.lengthDescriber, // 2: length
      this.describer[3] // 3: raw
    ];
    this.validator = [
      this.validator[0], // 0: default
      this.typeValidator, // 1
      this.lengthValidator, // 2: length
      this.validator[2] // 8: raw
    ];
  }

  public set (opt: Options) {
    super.set(opt);
  }

  protected check (opt: Options) {
    super.check(opt);
    // option validity
    if (opt.min !== undefined && typeof opt.min !== 'number')
      throw new Error(`Option min needs a numeric value, but ${inspect(opt.min)} was given.`);
    if (opt.max !== undefined && typeof opt.max !== 'number')
      throw new Error(`Option max needs a numeric value, but ${inspect(opt.max)} was given.`);
    if (opt.min !== undefined && opt.max !== undefined && opt.min > opt.max)
      throw new Error(
        `Option min could not be greater than max, but ${opt.min} > ${opt.max} was given.`
      );
  }

  protected typeDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'regexp')
    return t('describe.type');
  }
  protected async typeValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'regexp')
    if (typeof work.value === 'string') {
      //const m = work.value.match(/^\/(.*?)\/([gimy]*)$/);
      const m = work.value.match(
        /^\/((?![*+?])(?:[^\r\n\[/\\]|\\.|\[(?:[^\r\n\]\\]|\\.)*\])+)\/((?:g(?:im?|mi?)?|i(?:gm?|mg?)?|m(?:gi?|ig?)?)?)/
      );
      if (m) work.value = new RegExp(m[1], m[2]);
      else
        return Promise.reject(
          new ValidationError(
            this,
            work,
            t('error.parse')
          )
        );
    } else if (!(work.value instanceof RegExp))
      return Promise.reject(
        new ValidationError(
          this,
          work,
          t('error.type', { value: work.orig })
        )
      );
  }

  protected lengthDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'regexp')
    const opt = <Options>this.opt;
    if (opt.min !== undefined && opt.max !== undefined)
      return opt.min === opt.max
        ? t('describe.exact', { count: opt.min })
        : t('describe.between', { min: opt.min, max: opt.max });
    if (opt.min !== undefined)
      return t('describe.min', { min: opt.min });
    if (opt.max !== undefined)
      return t('describe.max', { max: opt.max });
  }
  protected async lengthValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'regexp')
    const opt = <Options>this.opt;
    // check value
    if (opt.min || opt.max) {
      const test = new RegExp(work.value.source + '|').exec('');
      const num = test ? test.length - 1 : 0;
      if (opt.min && num < opt.min)
        return Promise.reject(
          new ValidationError(
            this,
            work,
            t('error.min', { count: num, min: opt.min })
          )
        );
      if (opt.max && num > opt.max)
        return Promise.reject(
          new ValidationError(
            this,
            work,
            t('error.max', { count: num, max: opt.max })
          )
        );
    }
  }
}

export default RegExpSchema;
