import { inspect } from 'util';
import Numeral from 'numeral';
var convert = require('@bstoots/convert-units');

import * as any from './any';
import Workbench from '../workbench';
import Reference from '../reference';
import ValidationError from '../error';
import { T, loadNamespace } from '../i18n';

loadNamespace('number')

export interface Options extends any.Options {
  default?: number | Reference;
  sanitize?: boolean;
  unit?: { from: string; to: string };
  round?: { method?: 'floor' | 'ceil' | 'arithmetic'; precision: number };
  integer?: boolean | { type?: 'byte' | 'short' | 'long' | 'safe' | 'quad'; unsigned?: boolean };
  min?: number;
  max?: number;
  greater?: number;
  less?: number;
  multipleOf?: number;
  allow?: (number | [number, number] | string | Reference)[] | Reference;
  disallow?: (number | [number, number] | string | Reference)[] | Reference;
  ranges?: { [key: string]: [number, number] };
  format?: string;
}

const INTTYPE = {
  byte: 8,
  short: 16,
  long: 32,
  safe: 53,
  quad: 64
};

export class NumberSchema extends any.AnySchema {
  constructor(opt: Options = {}) {
    // option validity
    super(opt);
    // rules
    this.describer = [
      this.typeDescriber, // 0
      this.describer[1], // 1: default
      this.sanitizeDescriber, // 2: sanitize
      this.unitDescriber, // 3: unit
      this.roundDescriber, // 4: round
      this.rangeDescriber, // 5: integer, min, max, greater, less
      this.multipleDescriber, // 6: multipleOf
      this.allowDescriber, // 7: allow, disallow
      this.formatDescriber, // 8: format
      this.describer[3] // 9: raw
    ];
    this.validator = [
      this.validator[0], // 0: default
      this.unitValidator, // 1: unit
      this.sanitizeValidator, // 2: sanitize
      this.typeValidator, // 3
      this.roundValidator, // 4: round
      this.rangeValidator, // 5: integer, min, max, greater, less
      this.multipleValidator, // 6: multipleOf
      this.allowValidator, // 7: allow, disallow
      this.formatValidator, // 8: format
      this.validator[2] // 9: raw
    ];
  }

  public set(opt: Options) {
    super.set(opt);
  }

  protected check(opt: Options) {
    super.check(opt);
    if (opt.default && !(typeof opt.default === 'number' || opt.default instanceof Reference))
      throw new Error(
        `Option default needs a number or Reference value, but ${inspect(
          opt.default
        )} was given.`
      );
    if (opt.sanitize && typeof opt.sanitize !== 'boolean')
      throw new Error(
        `Option sanitize needs a boolean value, but ${inspect(opt.sanitize)} was given.`
      );
    if (opt.unit) {
      if (typeof opt.unit !== 'object')
        throw new Error(
          `Option unit needs an object value, but ${inspect(opt.unit)} was given.`
        );
      if (typeof opt.unit.from !== 'string')
        throw new Error(
          `Option unit needs a from type string, but ${inspect(opt.unit.from)} was given.`
        );
      else
        try {
          convert().from(opt.unit.from);
        } catch (e) {
          throw new Error(`Unit ${opt.unit.from} not recognized`);
        }
      if (typeof opt.unit.to !== 'string')
        throw new Error(
          `Option unit needs a to type string, but ${inspect(opt.unit.to)} was given.`
        );
      else
        try {
          convert().from(opt.unit.to);
        } catch (e) {
          throw new Error(`Unit ${opt.unit.to} not recognized`);
        }
      try {
        let m1 = opt.unit.from.match(/(i)?[bB]$/)
        let m2 = opt.unit.to.match(/(i)?[bB]$/)
        if (m1 && m2) {
          convert().from(opt.unit.from).to((m1[1] || '') + 'b');
          convert().from(opt.unit.to).to((m2[1] || '') + 'b');
        } else convert().from(opt.unit.from).to(opt.unit.to);
      } catch (e) {
        throw new Error(
          `Unit conversion from ${opt.unit.from} to ${opt.unit.to} impossible: ${e.message
          }`
        );
      }
    }
    if (opt.round) {
      if (typeof opt.round !== 'object')
        throw new Error(
          `Option round needs an object value, but ${inspect(opt.round)} was given.`
        );
      if (opt.round.method && !['floor', 'ceil', 'arithmetic'].includes(opt.round.method))
        throw new Error(
          `Unknown method ${inspect(
            opt.round.method
          )} in option round, only floor, ceil, arithmetic are allowed.`
        );
      if (typeof opt.round.precision !== 'number')
        throw new Error(
          `Option round needs an object with precision set, but only ${inspect(
            opt.round
          )} was given.`
        );
    }
    if (opt.integer) {
      if (
        typeof opt.integer !== 'boolean' &&
        (typeof opt.integer !== 'object' || Array.isArray(opt.integer))
      )
        throw new Error(
          `Option integer needs a boolean or object value, but ${inspect(
            opt.integer
          )} was given.`
        );
      if (typeof opt.integer === 'object' && typeof opt.integer.unsigned !== 'boolean')
        throw new Error(
          `Option integer needs a boolean as unsigned flag, but ${inspect(
            opt.integer.unsigned
          )} was given.`
        );
      if (
        typeof opt.integer === 'object' &&
        opt.integer.type &&
        !Object.keys(INTTYPE).includes(opt.integer.type)
      )
        throw new Error(
          `Option integer needs a boolean as unsigned flag, but ${inspect(
            opt.integer.unsigned
          )} was given.`
        );
      if (opt.integer && opt.round && opt.round.precision)
        throw new Error(
          `No round precision is possible with option integer, but ${inspect(
            opt.round
          )} was given.`
        );
    }
    if (opt.min !== undefined && typeof opt.min !== 'number')
      throw new Error(`Option min needs a numeric value, but ${inspect(opt.min)} was given.`);
    if (opt.max !== undefined && typeof opt.max !== 'number')
      throw new Error(`Option max needs a numeric value, but ${inspect(opt.max)} was given.`);
    if (opt.greater !== undefined && typeof opt.greater !== 'number')
      throw new Error(
        `Option greater needs a numeric value, but ${inspect(opt.greater)} was given.`
      );
    if (opt.less !== undefined && typeof opt.less !== 'number')
      throw new Error(
        `Option less needs a numeric value, but ${inspect(opt.less)} was given.`
      );
    if (opt.min !== undefined && opt.max !== undefined && opt.min > opt.max)
      throw new Error(
        `Option min could not be greater than max, but ${opt.min} > ${opt.max} was given.`
      );
    if (opt.min !== undefined && opt.less !== undefined && opt.min >= opt.less)
      throw new Error(
        `Option min could not be less or equal less value, but ${opt.min} >= ${opt.less
        } was given.`
      );
    if (opt.max !== undefined && opt.greater !== undefined && opt.max <= opt.greater)
      throw new Error(
        `Option max could not be greater than max, but ${opt.max} <= ${opt.greater
        } was given.`
      );
    if (opt.less !== undefined && opt.greater !== undefined && opt.less <= opt.greater)
      throw new Error(
        `Option less could not be less than greater, but ${opt.min} <= ${opt.max
        } was given.`
      );
    if (opt.multipleOf && !(typeof opt.multipleOf === 'number'))
      throw new Error(
        `Option multipleOf needs a numeric value, but ${inspect(opt.multipleOf)} was given.`
      );
    if (opt.allow && Array.isArray(opt.allow))
      opt.allow.forEach(e => {
        if (
          typeof e !== 'number' &&
          !(e instanceof Reference) &&
          !(
            Array.isArray(e) &&
            e.length === 2 &&
            typeof e[0] === 'number' &&
            typeof e[1] === 'number'
          ) &&
          !(typeof e === 'string' && opt.ranges && opt.ranges[e])
        )
          throw new Error(
            `Option allow needs number, number range or Reference values, but ${inspect(
              e
            )} was given.`
          );
      });
    if (opt.disallow && Array.isArray(opt.disallow))
      opt.disallow.forEach(e => {
        if (
          typeof e !== 'number' &&
          !(e instanceof Reference) &&
          !(
            Array.isArray(e) &&
            e.length === 2 &&
            typeof e[0] === 'number' &&
            typeof e[1] === 'number'
          ) &&
          !(typeof e === 'string' && opt.ranges && opt.ranges[e])
        )
          throw new Error(
            `Option disallow needs number, number range or Reference values, but ${inspect(
              e
            )} was given.`
          );
      });
    if (opt.ranges && !(typeof opt.ranges === 'object'))
      throw new Error(
        `Option ranges needs named numeric ranges, but ${inspect(opt.ranges)} was given.`
      );
    if (opt.ranges)
      Object.keys(opt.ranges).forEach(k => {
        // @ts-ignore
        let value = opt.ranges[k];
        if (
          value.length !== 2 ||
          typeof value[0] !== 'number' ||
          typeof value[1] !== 'number'
        )
          throw new Error(
            `Option ranges needs numeric ranges with start and end, but ${inspect(
              value
            )} was given.`
          );
      });
    if (opt.format && typeof opt.format !== 'string')
      throw new Error(
        `Option format needs a string value, but ${inspect(opt.format)} was given.`
      );
  }

  private sanitizeDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'number')
    const opt = <Options>this.opt;
    if (opt.sanitize) {
      return t('describe.sanitize');
    }
  }
  private async sanitizeValidator(work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    // check value
    if (opt.sanitize && typeof work.value !== 'number') {
      // work.value = work.value.replace(/^.*?([-+]?\d+\.?\d*).*?$/, '$1');
      const m = work.value.match(/^.*?([-+]?\d+\.?\d*)(\s*%)?.*?$/);
      if (m) work.value = m[2] ? m[1] / 100 : m[1];
    }
  }

  private unitDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'number')
    const opt = <Options>this.opt;
    if (opt.unit) {
      return t('describe.unit', { from: opt.unit.from, to: opt.unit.to });
    }
  }
  private async unitValidator(work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'number')
    const opt = <Options>this.opt;
    // check value
    if (opt.unit) {
      let value = opt.sanitize
        ? work.value.replace(/^.*?([-+]?\d+\.?\d*\s*\S*).*?$/, '$1')
        : work.value + '';
      let m1 = opt.unit.from.match(/(i)?[bB]$/)
      let m2 = opt.unit.to.match(/(i)?[bB]$/)
      let v = value.match(/(i)?[bB]$/)
      if (m1 && v && v[1]) m1 = v // switch 'from' to binary on binary value
      if (m1 && m1[1]) value = value.replace(/([^i])([bB])$/, '$1i$2') // replace units with binary units if 'from' is binary
      let quantity: any;
      try {
        const match = value.match(/(^[-+]?\d+\.?\d*)\s*(\S*)/);
        quantity = convert(match[1]).from(match[2].replace(/^k(i?[Bb])$/, 'K$1') || opt.unit.from);
      } catch (e) {
        throw new ValidationError(
          this,
          work,
          t('error.unit', { value: value, message: e.message })
        );
      }
      try {
        // convert between decimalBin, decimalDec
        if (m1 && m2 && m1[1] !== m2[1]) {
          let a = m1[1] ? 'ib' : 'b'
          let b = m2[1] ? 'ib' : 'b'
          const value = quantity.to(a);
          quantity = convert(value).from(b);
        }
        // normal conversion
        work.value = quantity.to(opt.unit.to);
      } catch (e) {
        throw new ValidationError(
          this,
          work,
          t('error.convert', { from: opt.unit.from, to: opt.unit.to, message: e.message })
        );
      }
    }
  }

  protected typeDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'number')
    return t('describe.type');
  }
  protected async typeValidator(work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'number')
    if (typeof work.value === 'string') {
      let num = Number(work.value);
      if (!isNaN(num)) work.value = num;
    }
    if (typeof work.value !== 'number') {
      return Promise.reject(
        new ValidationError(this, work, t('error.type'))
      );
    }
  }

  private roundDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'number')
    const opt = <Options>this.opt;
    if (opt.round) {
      let method = opt.round.method || 'arithmetic';
      let precision = opt.integer ? 0 : opt.round.precision;
      return t('describe.round', { method: inspect(method), precision });
    }
  }
  private async roundValidator(work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    // check value
    if (opt.round) {
      let method = opt.round.method || 'arithmetic';
      const exp = opt.integer ? 1 : 10 ** opt.round.precision;
      let value = work.value * exp;
      if (method === 'ceil') value = Math.ceil(value);
      else if (method === 'floor') value = Math.floor(value);
      else value = Math.round(value);
      work.value = value / exp;
    }
  }

  private rangeDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'number')
    const opt = <Options>this.opt;
    let msg: string[] = [];
    let min: number | undefined = undefined;
    let max: number | undefined = undefined;
    let greater: number | undefined = undefined;
    let less: number | undefined = undefined;
    if (typeof opt.integer === 'object') {
      const unsigned = opt.integer.unsigned ? 1 : 0;
      if (unsigned) min = 0;
      if (opt.integer.type) {
        max = 2 ** (INTTYPE[opt.integer.type] - 1 + unsigned) - 1;
        min = (unsigned - 1) * max - 1 + unsigned;
      }
    }
    if (min === undefined || (opt.min !== undefined && opt.min < min)) min = opt.min;
    if (max === undefined || (opt.max !== undefined && opt.max > max)) max = opt.max;
    if (opt.greater !== undefined) {
      if (min === undefined) greater = opt.greater;
      else if (min < opt.greater) {
        min = undefined;
        greater = opt.greater;
      }
    }
    if (opt.less !== undefined) {
      if (max === undefined) less = opt.less;
      else if (max > opt.less) {
        max = undefined;
        less = opt.less;
      }
    }
    // messages
    if (typeof opt.integer === 'object' && opt.integer.type)
      msg.push(
        t('describe.integerType', { type: (opt.integer.unsigned ? 'unsigned ' : '') + INTTYPE[opt.integer.type] + '-bit' })
      );
    else if (opt.integer && opt.sanitize && !opt.round)
      msg.push(t('describe.integerSanitize'));
    else if (opt.integer) msg.push(t('describe.integer'));
    if (min !== undefined && max !== undefined)
      msg.push(t('describe.betweenEqual', { min, max }));
    else {
      if (min !== undefined) msg.push(t('describe.min', { min }));
      if (max !== undefined) msg.push(t('describe.max', { max }));
    }
    if (less !== undefined && greater !== undefined)
      msg.push(t('describe.between', { greater, less }));
    else {
      if (greater !== undefined)
        msg.push(t('describe.greater', { greater }));
      if (less !== undefined) msg.push(t('describe.less', { less }));
    }
    return msg.join(' ');
  }
  private async rangeValidator(work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'number')
    const opt = <Options>this.opt;
    let min: number | undefined = undefined;
    let max: number | undefined = undefined;
    let greater: number | undefined = undefined;
    let less: number | undefined = undefined;
    if (typeof opt.integer === 'object') {
      const unsigned = opt.integer.unsigned ? 1 : 0;
      if (unsigned) min = 0;
      if (opt.integer.type) {
        max = 2 ** (INTTYPE[opt.integer.type] - 1 + unsigned) - 1;
        min = (unsigned - 1) * max - 1 + unsigned;
      }
    }
    if (min === undefined || (opt.min !== undefined && opt.min < min)) min = opt.min;
    if (max === undefined || (opt.max !== undefined && opt.max > max)) max = opt.max;
    if (opt.greater !== undefined) {
      if (min === undefined) greater = opt.greater;
      else if (min < opt.greater) {
        min = undefined;
        greater = opt.greater;
      }
    }
    if (opt.less !== undefined) {
      if (max === undefined) less = opt.less;
      else if (max > opt.less) {
        max = undefined;
        less = opt.less;
      }
    }
    // check value
    if (opt.integer && opt.sanitize) work.value = Math.round(work.value);
    if (opt.integer && !Number.isInteger(work.value))
      throw new ValidationError(
        this,
        work,
        t('error.integer', { value: work.value })
      );
    if (min !== undefined && work.value < min)
      throw new ValidationError(
        this,
        work,
        t('error.min', { min, value: work.value })
      );
    if (max !== undefined && work.value > max)
      throw new ValidationError(
        this,
        work,
        t('error.max', { max, value: work.value })
      );
    if (greater !== undefined && work.value <= greater)
      throw new ValidationError(
        this,
        work,
        t('error.greater', { greater, value: work.value })
      );
    if (less !== undefined && work.value >= less)
      throw new ValidationError(
        this,
        work,
        t('error.less', { less, value: work.value })
      );
  }

  private multipleDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'number')
    const opt = <Options>this.opt;
    if (opt.multipleOf) {
      return t('describe.multiple', { multipleOf: opt.multipleOf });
    }
  }
  private async multipleValidator(work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'number')
    const opt = <Options>this.opt;
    // check value
    if (opt.multipleOf && work.value % opt.multipleOf)
      throw new ValidationError(
        this,
        work,
        t('error.multiple', { multipleOf: opt.multipleOf, value: work.value })
      );
  }

  protected allowDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'number')
    const opt = <Options>this.opt;
    let values: string[] = [];
    if (opt.allow instanceof Reference) {
      values.push(
        (values.length ? t('core:prase.or') + ' ' : '') +
        t('core:phrase.within', { source: opt.allow.source, filter: opt.allow.filter || "''" })
      );
    } else if (opt.allow && opt.allow.length)
      opt.allow.forEach(e => {
        values.push(
          (values.length ? t('core:phrase.or') + ' ' : '') +
          (e instanceof Reference
            ? t('core:phrase.within', { source: e.source, filter: e.filter || "''" })
            : typeof e === 'string'
              ? t('phrase.namedRange', {
                min: (<{ [key: string]: [number, number] }>opt.ranges)[e][0],
                max: (<{ [key: string]: [number, number] }>opt.ranges)[e][1],
                name: e
              })
              : Array.isArray(e)
                ? t('phrase.range', { min: e[0], max: e[1] })
                : t('core:phrase.equal', { value: inspect(e) }))
        );
      });
    if (opt.disallow instanceof Reference) {
      values.push(
        (values.length ? t('core:phrase.or') + ' ' : '') +
        t('core:phrase.within', { source: opt.disallow.source, filter: opt.disallow.filter })
      );
    } else if (opt.disallow && opt.disallow.length)
      opt.disallow.forEach(e => {
        values.push(
          (values.length ? t('core:phrase.and') + ' ' : '') +
          (e instanceof Reference
            ? t('core:phrase.notWithin', { source: e.source, filter: e.filter })
            : typeof e === 'string'
              ? t('phrase.namedRange', {
                min: (<{ [key: string]: [number, number] }>opt.ranges)[e][0],
                max: (<{ [key: string]: [number, number] }>opt.ranges)[e][1],
                name: e
              })
              : Array.isArray(e)
                ? t('phrase.range', { min: e[0], max: e[1] })
                : t('core:phrase.notEqual', { value: inspect(e) }))
        );
      });
    if (values.length)
      return (
        t('core:describe.allow') +
        '\n- ' +
        values.slice(0, 10).join('\n- ') +
        (values.length > 10 ? '\n- ' + t('core:phrase.more', { count: values.length - 10 }) : '') +
        '\n'
      );
  }
  protected async allowValidator(work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'number')
    const opt = <Options>this.opt;
    // get list
    let allow: any[] = [];
    let disallow: any[] = [];
    if (opt.allow instanceof Reference) {
      let data = await opt.allow.stringArray(work);
      if (data) allow = data.slice(0);
    } else if (opt.allow && opt.allow.length) {
      for (let index = 0; index < opt.allow.length; index++) {
        let e = opt.allow[index];
        if (e instanceof Reference) {
          let data = await e.stringArray(work);
          if (data) allow = allow.concat(data);
        } else allow.push(e);
      }
    }
    if (opt.disallow instanceof Reference) {
      let data = await opt.disallow.stringArray(work);
      if (data) disallow = data.slice(0);
    } else if (opt.disallow && opt.disallow.length) {
      for (let index = 0; index < opt.disallow.length; index++) {
        let e = opt.disallow[index];
        if (e instanceof Reference) {
          let data = await e.stringArray(work);
          if (data) disallow = disallow.concat(data);
        } else disallow.push(e);
      }
    }
    // validate
    if (allow && allow.length) {
      let found = false;
      allow.forEach(e => {
        if (!found) {
          found =
            typeof e === 'string'
              ? work.value >=
              (<{ [key: string]: [number, number] }>opt.ranges)[e][0] &&
              work.value <= (<{ [key: string]: [number, number] }>opt.ranges)[e][1]
              : Array.isArray(e)
                ? work.value >= e[0] && work.value <= e[1]
                : e === work.value;
        }
      });
      if (!found)
        throw new ValidationError(
          this,
          work,
          t('core:error.allow', { list: allow.map(e => inspect(e)).join(', ') })
        );
    }
    if (disallow && disallow.length) {
      let found = false;
      disallow.forEach(e => {
        if (!found) {
          found =
            typeof e === 'string'
              ? work.value >=
              (<{ [key: string]: [number, number] }>opt.ranges)[e][0] &&
              work.value <= (<{ [key: string]: [number, number] }>opt.ranges)[e][1]
              : Array.isArray(e)
                ? work.value >= e[0] && work.value <= e[1]
                : e === work.value;
        }
      });
      if (found)
        throw new ValidationError(
          this,
          work,
          t('core:error.disallow', { list: disallow.map(e => inspect(e)).join(', ') })
        );
    }
  }

  protected formatDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'number')
    const opt = <Options>this.opt;
    if (opt.format) {
      return t('describe.format', { format: opt.format });
    }
  }
  protected async formatValidator(work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'number')
    const opt = <Options>this.opt;
    // check value
    if (opt.format) {
      const match = opt.format.match(/(^.*?)(\s*\$(?:unit|best))/);
      let unit = match ? match[2] : '';
      if (opt.unit && unit.includes('$best')) {
        const quantity = convert(work.value)
          .from(opt.unit.to)
          .toBest();
        work.value = quantity.val;
        unit = unit.replace('$best', quantity.unit);
      }
      if (opt.unit && unit.includes('$unit')) unit = unit.replace('$unit', opt.unit.to);
      const format = match ? match[1] : opt.format;
      try {
        work.value = Numeral(work.value).format(format) + unit;
      } catch (e) {
        throw new ValidationError(this, work, t('error.format', { message: e.message }));
      }
    }
  }
}

export default NumberSchema;
