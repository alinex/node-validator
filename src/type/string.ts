import { inspect } from 'util';
import striptags from 'striptags';

import * as any from './any';
import Workbench from '../workbench';
import Reference from '../reference';
import ValidationError from '../error';
import { T, loadNamespace } from '../i18n';

loadNamespace('string')

export interface Options extends any.Options {
  default?: string | Reference;
  makeString?: boolean;
  trim?: boolean | 'left' | 'right' | 'both';
  replace?: { match: string | RegExp; replace: string; hint?: string }[];
  alphaNum?: boolean;
  hex?: boolean;
  noHTML?: boolean | string[];
  stripDisallowed?: boolean;
  upperCase?: boolean | 'first';
  lowerCase?: boolean | 'first';
  min?: number;
  max?: number;
  pad?: { side: 'right' | 'left' | 'both'; characters: string };
  truncate?: boolean | string;
  allow?: (string | RegExp | Reference)[] | Reference;
  disallow?: (string | RegExp | Reference)[] | Reference;
}

export class StringSchema extends any.AnySchema {
  constructor(opt: Options = {}) {
    super(opt);
    // rules
    this.describer = [
      this.typeDescriber, // 0
      this.describer[1], // 1: default
      this.makeStringDescriber, // 2: makeString
      this.trimDescriber, // 3: trim
      this.replaceDescriber, // 4: replace
      this.characterDescriber, // 5: hex, alpha, noHTML, stripDisallowed
      this.caseDescriber, // 6: upperCase, lowerCase
      this.lengthDescriber, // 7: min, max, pad, truncate
      this.allowDescriber, // 8: allow, disallow
      this.describer[3] // 9: raw
    ];
    this.validator = [
      this.validator[0], // 0: default
      this.makeStringValidator, // 1: makeString
      this.typeValidator, // 2
      this.trimValidator, // 3: trim
      this.replaceValidator, // 4: replace
      this.characterValidator, // 5: hex, alpha, noHTML, stripDisallowed
      this.caseValidator, // 6: upperCase, lowerCase
      this.lengthValidator, // 7: min, max, pad, truncate
      this.allowValidator, // 8: allow, disallow
      this.validator[2] // 9: raw
    ];
  }

  public set (opt: Options) {
    super.set(opt);
  }

  protected check (opt: Options) {
    super.check(opt);
    // option validity
    if (opt.default && typeof opt.default !== 'string' && !(opt.default instanceof Reference))
      throw new Error(
        `Option default needs a string or Reference value, but ${inspect(
          opt.default
        )} was given.`
      );
    if (opt.makeString && typeof opt.makeString !== 'boolean')
      throw new Error(
        `Option makeString needs a boolean value, but ${inspect(opt.makeString)} was given.`
      );
    if (
      opt.trim &&
      opt.trim !== true &&
      opt.trim !== 'left' &&
      opt.trim !== 'right' &&
      opt.trim !== 'both'
    )
      throw new Error(
        `Option trim needs a boolean value, but ${inspect(opt.trim)} was given.`
      );
    if (opt.replace) {
      if (!Array.isArray(opt.replace))
        throw new Error(
          `Option replace needs an array, but ${inspect(opt.replace)} was given.`
        );
      opt.replace.forEach(e => {
        if (typeof e !== 'object')
          throw new Error(
            `Option replace needs object elements, but ${inspect(
              opt.replace
            )} was given.`
          );
        let m = e.match;
        if (
          typeof e.match !== 'string' &&
          !(e.match instanceof RegExp) &&
          !(m instanceof Reference)
        )
          throw new Error(
            `Option replace needs object with string or Reference as match, but ${inspect(
              e
            )} was given.`
          );
        if (typeof e.replace !== 'string')
          throw new Error(
            `Option replace needs objects with string replacements, but ${inspect(
              e
            )} was given.`
          );
      });
      if (opt.replace.length === 0) opt.replace = undefined;
    }
    if (opt.alphaNum && typeof opt.alphaNum !== 'boolean')
      throw new Error(
        `Option alphaNum needs a boolean value, but ${inspect(opt.alphaNum)} was given.`
      );
    if (opt.hex && typeof opt.hex !== 'boolean')
      throw new Error(`Option hex needs a boolean value, but ${inspect(opt.hex)} was given.`);
    if (opt.noHTML && typeof opt.noHTML !== 'boolean' && !Array.isArray(opt.noHTML))
      throw new Error(
        `Option noHTML needs a boolean or array value, but ${inspect(
          opt.noHTML
        )} was given.`
      );
    if (opt.stripDisallowed && typeof opt.stripDisallowed !== 'boolean')
      throw new Error(
        `Option stripDisallowed needs a boolean value, but ${inspect(
          opt.stripDisallowed
        )} was given.`
      );
    if (opt.upperCase && typeof opt.upperCase !== 'boolean' && opt.upperCase !== 'first')
      throw new Error(
        `Option upperCase needs a boolean value or 'first', but ${inspect(
          opt.upperCase
        )} was given.`
      );
    if (opt.lowerCase && typeof opt.lowerCase !== 'boolean' && opt.lowerCase !== 'first')
      throw new Error(
        `Option lowerCase needs a boolean value or 'first', but ${inspect(
          opt.lowerCase
        )} was given.`
      );
    if (opt.min !== undefined && typeof opt.min !== 'number')
      throw new Error(`Option min needs a numeric value, but ${inspect(opt.min)} was given.`);
    if (opt.max !== undefined && typeof opt.max !== 'number')
      throw new Error(`Option max needs a numeric value, but ${inspect(opt.max)} was given.`);
    if (opt.min !== undefined && opt.max !== undefined && opt.min > opt.max)
      throw new Error(
        `Option min could not be greater than max, but ${opt.min} > ${opt.max} was given.`
      );
    if (opt.pad) {
      if (typeof opt.pad !== 'object')
        throw new Error(
          `Option pad needs an object as value, but ${inspect(opt.pad)} was given.`
        );
      if (!['right', 'left', 'both'].includes(opt.pad.side))
        throw new Error(
          `Option pad needs an object with 'right', 'left', 'both' as side, but ${inspect(
            opt.pad
          )} was given.`
        );
      if (typeof opt.pad.characters !== 'string')
        throw new Error(
          `Option pad needs an object with string as characters, but ${inspect(
            opt.pad
          )} was given.`
        );
    }
    if (opt.truncate && opt.truncate !== true && typeof opt.truncate !== 'string')
      throw new Error(
        `Option truncate needs a boolean or string value, but ${inspect(
          opt.truncate
        )} was given.`
      );
    if (opt.allow && Array.isArray(opt.allow))
      opt.allow.forEach(e => {
        if (typeof e !== 'string' && !(e instanceof RegExp) && !(e instanceof Reference))
          throw new Error(
            `Option allow needs string, RegExp or Reference values, but ${inspect(
              e
            )} was given.`
          );
      });
    if (opt.disallow && Array.isArray(opt.disallow))
      opt.disallow.forEach(e => {
        if (typeof e !== 'string' && !(e instanceof RegExp) && !(e instanceof Reference))
          throw new Error(
            `Option disallow needs string, RegExp or Reference values, but ${inspect(
              e
            )} was given.`
          );
      });
  }

  protected typeDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'string')
    return t('describe.type');
  }
  protected async typeValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'string')
    if (typeof work.value !== 'string') {
      return Promise.reject(new ValidationError(this, work, t('error.type')));
    }
  }

  private makeStringDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'string')
    const opt = <Options>this.opt;
    if (opt.makeString) {
      return t('describe.makeString');
    }
  }
  private async makeStringValidator (work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    // check value
    if (opt.makeString && typeof work.value !== 'string') work.value = work.value.toString();
  }

  private trimDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'string')
    const opt = <Options>this.opt;
    if (opt.trim === 'both' || opt.trim === true)
      return t('describe.trimBoth');
    if (opt.trim === 'left') return t('describe.trimLeft');
    if (opt.trim === 'right') return t('describe.trimRight');
  }
  private async trimValidator (work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    if (opt.trim === 'both' || opt.trim === true) work.value = work.value.trim();
    if (opt.trim === 'left') work.value = work.value.replace(/^[\s\uFEFF\xA0]+/g, '');
    if (opt.trim === 'right') work.value = work.value.replace(/[\s\uFEFF\xA0]+$/g, '');
  }

  private replaceDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'string')
    const opt = <Options>this.opt;
    if (opt.replace && opt.replace.length) {
      return (
        t('describe.replace') +
        '\n' +
        opt.replace
          .map(
            e => ` - ${inspect(e.match)} => ${e.replace}${e.hint ? ` (${e.hint})` : ''}`
          )
          .join('\n')
      );
    }
  }
  private async replaceValidator (work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    if (opt.replace && opt.replace.length) {
      opt.replace.forEach(e => (work.value = work.value.replace(e.match, e.replace)));
    }
  }

  private characterDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'string')
    const opt = <Options>this.opt;
    let msg = '';
    if (opt.hex) msg += t('describe.hex') + ' ';
    else if (opt.alphaNum) msg += t('describe.alphaNum') + ' ';
    else if (opt.noHTML) {
      if (Array.isArray(opt.noHTML) && opt.noHTML.length)
        msg +=
          t('describe.noHtmlList', { count: opt.noHTML.length, list: opt.noHTML.join(', ') }) + ' ';
      else msg += t('describe.noHtml') + ' ';
    }
    if (opt.stripDisallowed) {
      msg += t('describe.stripDisallowed') + ' ';
    }
    if (msg) return msg.trim();
  }
  private async characterValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'string')
    const opt = <Options>this.opt;
    if (opt.stripDisallowed) {
      if (opt.hex) work.value = work.value.replace(/[^a-fA-F0-9]/g, '');
      else if (opt.alphaNum) work.value = work.value.replace(/\W/g, '');
      else if (opt.noHTML) {
        work.value =
          Array.isArray(opt.noHTML) && opt.noHTML.length
            ? striptags(work.value, opt.noHTML)
            : striptags(work.value);
      }
    } else {
      if (opt.hex && work.value.match(/[^a-fA-F0-9]/)) {
        throw new ValidationError(
          this,
          work,
          t('error.hex')
        );
      } else if (opt.alphaNum && work.value.match(/\W/)) {
        throw new ValidationError(
          this,
          work,
          t('error.alphaNum')
        );
      } else if (opt.noHTML) {
        let test =
          Array.isArray(opt.noHTML) && opt.noHTML.length
            ? striptags(work.value, opt.noHTML)
            : striptags(work.value);
        if (test !== work.value)
          throw new ValidationError(
            this,
            work,
            t('error.noHtml')
          );
      }
    }
  }

  private caseDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'string')
    const opt = <Options>this.opt;
    let msg = '';
    if (opt.lowerCase === true) msg += t('describe.lowerCase') + ' ';
    else if (opt.upperCase === true) msg += t('describe.upperCase') + ' ';
    if (opt.lowerCase === 'first') msg += t('describe.lowerCaseFirst');
    else if (opt.upperCase === 'first')
      msg += t('describe.upperCaseFirst');
    if (msg) return msg.trim();
  }
  private async caseValidator (work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    if (opt.lowerCase === true) work.value = work.value.toLowerCase();
    else if (opt.upperCase === true) work.value = work.value.toUpperCase();
    if (opt.lowerCase === 'first') {
      work.value = work.value.substr(0, 1).toLowerCase() + work.value.substr(1);
    } else if (opt.upperCase === 'first') {
      work.value = work.value.substr(0, 1).toUpperCase() + work.value.substr(1);
    }
  }

  private lengthDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'string')
    const opt = <Options>this.opt;
    let msg = '';
    if (opt.min && opt.pad)
      msg +=
        t('describe.pad', { context: opt.pad.side, characters: opt.pad.characters, min: opt.min }) + ' ';
    if (opt.max && (opt.truncate || typeof opt.truncate === 'string'))
      msg += t('describe.truncate', { max: opt.max }) + ' ';
    if (
      opt.min &&
      opt.min === opt.max &&
      (!opt.pad || (!opt.truncate && typeof opt.truncate !== 'string'))
    )
      msg += t('describe.length', { length: opt.max });
    else if (
      opt.min &&
      opt.max &&
      (!opt.pad || (!opt.truncate && typeof opt.truncate !== 'string'))
    )
      msg += t('describe.between', { min: opt.min, max: opt.max })
    else if (opt.min && !opt.pad) msg += t('describe.min', { min: opt.min });
    else if (opt.max && (!opt.truncate && typeof opt.truncate !== 'string'))
      msg += t('describe.max', { max: opt.max });
    if (msg) return msg.trim();
  }
  private async lengthValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'string')
    const opt = <Options>this.opt;
    let num = work.value.length;
    // pad
    if (opt.pad && opt.min && num < opt.min) {
      const add = opt.min - num;
      let pad = opt.pad.characters;
      switch (opt.pad.side) {
        case 'right':
          if (pad.length < add) pad = pad.substr(0, 1).repeat(add - pad.length) + pad;
          work.value += pad.substr(0, add);
          break;
        case 'left':
          if (pad.length < add) pad += pad.slice(-1).repeat(add - pad.length);
          work.value = pad.substr(0, add) + work.value;
          break;
        default:
          let a = Math.ceil(add / 2);
          pad =
            opt.pad.characters.length > 1
              ? opt.pad.characters.slice(-Math.ceil(opt.pad.characters.length / 2))
              : opt.pad.characters;
          if (pad.length < a) pad += pad.slice(-1).repeat(a - pad.length);
          work.value += pad.slice(-a);
          pad =
            opt.pad.characters.length > 1
              ? opt.pad.characters.slice(0, Math.ceil(opt.pad.characters.length / 2))
              : opt.pad.characters;
          a = Math.floor(add / 2);
          if (pad.length < a) pad = `${pad.slice(0, 1).repeat(a - pad.length)}${pad}`;
          work.value = `${pad.slice(0, a)}${work.value}`;
      }
      num = work.value.length;
    }
    // truncate
    if (opt.truncate && opt.max && num > opt.max) {
      let a = typeof opt.truncate === 'string' ? opt.truncate.length : 0;
      work.value = work.value.substr(0, opt.max - a).replace(/\s+$/g, '');
      if (a) work.value += opt.truncate;
      num = work.value.length;
    }
    // check length
    if (opt.min && num < opt.min) {
      throw new ValidationError(
        this,
        work,
        t('error.min', { count: num, min: opt.min })
      );
    }
    if (opt.max && num > opt.max) {
      throw new ValidationError(
        this,
        work,
        t('error.max', { count: num, max: opt.max })
      );
    }
  }

  // overwritten because of special RegExp handling
  public allowDescriber (depth: number, path: string, lang: string) {
    const t = T(lang, 'string')
    const opt = <Options>this.opt;
    let values: string[] = [];
    if (opt.allow instanceof Reference) {
      values.push(
        (values.length ? t('core:phrase.or') + ' ' : '') +
        t('core:phrase.within', { source: opt.allow.source, filter: opt.allow.filter || "''" })
      );
    } else if (opt.allow && opt.allow.length)
      opt.allow.forEach(e => {
        values.push(
          (values.length ? t('core:phrase.or') + ' ' : '') +
          (typeof e === 'string'
            ? t('core:phrase.equal', { value: inspect(e) })
            : e instanceof RegExp
              ? t('core:phrase.match', { value: inspect(e) })
              : t('core:phrase.within', { source: e.source, filter: e.filter || "''" }))
        );
      });
    if (opt.disallow instanceof Reference) {
      values.push(
        (values.length ? t('core:phrase.or') + ' ' : '') +
        t('core:ohrase.within', { source: opt.disallow.source, filter: opt.disallow.filter })
      );
    } else if (opt.disallow && opt.disallow.length)
      opt.disallow.forEach(e => {
        values.push(
          (values.length ? t('core:phrase.and') + ' ' : '') +
          (typeof e === 'string'
            ? t('core:phrase.notEqual', { value: e })
            : e instanceof RegExp
              ? t('core:phrase.notMatch', { value: inspect(e) })
              : t('core:phrase.notWithin', { source: e.source, filter: e.filter }))
        );
      });
    if (values.length)
      return (
        t('core:describe.allow') +
        '\n-   ' +
        values.slice(0, 10).join('\n-   ') +
        (values.length > 10 ? '\n-   ' + t('core:phrase.more', { count: values.length - 10 }) : '')
      );
  }
  // overwritten because of special RegExp handling
  protected async allowValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'string')
    const opt = <Options>this.opt;
    // get list
    let allow: (string | RegExp)[] = [];
    let disallow: (string | RegExp)[] = [];
    if (opt.allow instanceof Reference) {
      let data = await opt.allow.stringArray(work);
      if (data) allow = data.slice(0);
    } else if (opt.allow && opt.allow.length) {
      for (let index = 0; index < opt.allow.length; index++) {
        let e = opt.allow[index];
        if (e instanceof Reference) {
          let data = await e.stringArray(work);
          if (data) allow = allow.concat(data);
        } else allow.push(e);
      }
    }
    if (opt.disallow instanceof Reference) {
      let data = await opt.disallow.stringArray(work);
      if (data) disallow = data.slice(0);
    } else if (opt.disallow && opt.disallow.length) {
      for (let index = 0; index < opt.disallow.length; index++) {
        let e = opt.disallow[index];
        if (e instanceof Reference) {
          let data = await e.stringArray(work);
          if (data) disallow = disallow.concat(data);
        } else disallow.push(e);
      }
    }
    // validate
    if (allow && allow.length) {
      let found = false;
      allow.forEach(e => {
        if (!found) {
          if (typeof e === 'string') found = e === work.value;
          else found = work.value.match(e);
        }
      });
      if (!found)
        throw new ValidationError(
          this,
          work,
          t('core:error.allow', { list: allow.map(e => `"${typeof e === 'string' ? e : inspect(e)}"`).join(', ') })
        );
    }
    if (disallow && disallow.length) {
      let found = false;
      disallow.forEach(e => {
        if (!found) {
          if (typeof e === 'string') found = e === work.value;
          else found = work.value.match(e);
        }
      });
      if (found)
        throw new ValidationError(
          this,
          work,
          t('core:error.disallow', { list: disallow.map(e => `"${typeof e === 'string' ? e : inspect(e)}"`).join(', ') })
        );
    }
  }
}

export default StringSchema;
