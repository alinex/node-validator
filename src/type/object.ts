import { inspect } from 'util';
import * as flat from 'flat';
import DataStore from '@alinex/datastore';
import async from '@alinex/async';

import * as base from './base';
import Workbench from '../workbench';
import Schema from '../schema';
import AnySchema from './any';
import Reference from '../reference';
import ValidationError from '../error';
import { T, loadNamespace } from '../i18n';

loadNamespace('object')

let obsoleteKeys: boolean = false;

export interface Options extends base.Options {
  default?: any | Reference;
  flatten?: boolean | string;
  rearrange?: (
    | { key: string; copyTo: string; overwrite?: boolean }
    | { key: string; moveTo: string; overwrite?: boolean }
  )[];
  unflatten?: boolean | string;
  // deprecated:
  keys?: { key?: string | RegExp; schema: Schema }[];
  item?: { [key: string]: Schema };
  removeUndefined?: boolean;
  denyUndefined?: boolean;
  mandatory?: boolean | (string | RegExp)[];
  forbidden?: (string | RegExp)[];
  combination?: (
    | { and: string[] }
    | { nand: string[] }
    | { or: string[] }
    | { xor: string[] }
    | { key: string; with: string[] }
    | { key: string; without: string[] }
  )[];
  min?: number;
  max?: number;
}

let ID_COUNT = 0;

export class ObjectSchema extends base.BaseSchema {
  _refID: string;
  _refNum = 0;
  constructor(opt: Options = {}) {
    super(opt);
    this._refID = `Object_${++ID_COUNT}`;
    // rules
    this.describer = [
      this.typeDescriber, // 0:
      this.describer[0], // 1: default
      this.flattenDescriber, // 2: flatten
      this.rearrangeDescriber, // 3: rearrange
      this.unflattenDescriber, // 4: unflatten
      // deprecated:
      this.keysDescriber, // 5: keys
      this.itemDescriber, // 5: item
      this.removeUndefinedDescriber, // 6: removeUndefined
      this.denyUndefinedDescriber, // 7: denyUndefined
      this.requireDescriber, // 8: mandatory, forbidden
      this.combinationDescriber, // 9: min, max
      this.sizeDescriber, // 10: min, max
      this.describer[1] // 11: raw
    ];
    this.validator = [
      this.validator[0], // 0: default
      this.typeValidator, // 1:
      this.flattenValidator, // 2: flatten
      this.rearrangeValidator, // 3: rearrange
      this.unflattenValidator, // 4: unflatten
      // deprecated:
      this.keysValidator, // 5: keys
      this.itemValidator, // 5: item
      this.removeUndefinedValidator, // 6: removeUndefined
      this.denyUndefinedValidator, // 7: denyUndefined
      this.requireValidator, // 8: mandatory, forbidden
      this.combinationValidator, // 9: min, max
      this.sizeValidator, // 10: min, max
      this.validator[1] // 11: raw
    ];
  }

  public set(opt: Options) {
    super.set(opt);
  }

  protected check(opt: Options) {
    super.check(opt);
    // option validity
    if (
      opt.default &&
      (!(typeof opt.default === 'object' || opt.default instanceof Reference) || Array.isArray(opt.default))
    )
      throw new Error(
        `Option default needs an object or Reference value, but ${inspect(opt.default)} was given.`
      );
    if (opt.flatten && typeof opt.flatten !== 'boolean' && typeof opt.flatten !== 'string')
      throw new Error(
        `Option flatten needs a boolean value or separator character, but ${inspect(opt.flatten)} was given.`
      );
    if (opt.rearrange) {
      if (!Array.isArray(opt.rearrange) || !opt.rearrange.length)
        throw new Error(`Option rearrange needs an object list, but ${inspect(opt.rearrange)} was given.`);
      opt.rearrange.forEach(e => {
        if (typeof e !== 'object')
          throw new Error(`Option rearrange needs object elements, but ${inspect(e)} was given.`);
        if (typeof e.key !== 'string')
          throw new Error(`Option rearrange needs rules with a base key, but only ${inspect(e)} was given.`);
        // @ts-ignore
        if (typeof e.copyTo !== 'string' && typeof e.moveTo !== 'string')
          throw new Error(
            `Option rearrange needs rules with copyTo or moveTo setting, but only ${inspect(e)} was given.`
          );
        // @ts-ignore
        if (typeof e.copyTo === 'string' && typeof e.moveTo === 'string')
          throw new Error(
            `Option rearrange needs to have copyTo or moveTo, not both like given in ${inspect(e)}.`
          );
      });
    }
    if (opt.unflatten && typeof opt.unflatten !== 'boolean' && typeof opt.unflatten !== 'string')
      throw new Error(
        `Option unflatten needs a boolean value or separator character, but ${inspect(
          opt.unflatten
        )} was given.`
      );
    // deprecated:
    if (opt.keys) {
      if (!obsoleteKeys) {
        console.warn('deprecated use of object.keys, use array.item instead');
        obsoleteKeys = true;
      }
      if (!Array.isArray(opt.keys) || !opt.keys.length)
        throw new Error(`Option keys needs an object list, but ${inspect(opt.keys)} was given.`);
      opt.keys.forEach(e => {
        if (typeof e !== 'object')
          throw new Error(`Option keys needs object elements, but ${inspect(e)} was given.`);
        if (typeof e.key !== 'string' && e.key !== undefined && !(e.key instanceof RegExp))
          throw new Error(
            `Option keys needs elements with or without string, RegExp key, but ${inspect(e)} was given.`
          );
        if (!(e.schema instanceof base.BaseSchema))
          throw new Error(`Option keys needs elements with schema set, but ${inspect(e)} was given.`);
      });
    }
    if (opt.item) {
      for (let k in opt.item) {
        if (Object.prototype.hasOwnProperty.call(opt.item, k)) {
          if (!(opt.item[k] instanceof base.BaseSchema))
            throw new Error(
              `Option item needs elements with schema set, but ${inspect(
                opt.item[k]
              )} was given under key ${k}.`
            );
        }
      }
      if (opt.item['*']) {
        // not possible because of wildcard which allows everything
        delete opt.denyUndefined
        delete opt.removeUndefined
      }
    }
    if (opt.removeUndefined && typeof opt.removeUndefined !== 'boolean')
      throw new Error(
        `Option removeUndefined needs a boolean value, but ${inspect(opt.removeUndefined)} was given.`
      );
    // deprecated:
    if (opt.removeUndefined && !opt.keys && !opt.item)
      // replace with: if (opt.removeUndefined && !opt.item)
      throw new Error(
        `Option removeUndefined needs keys definition for all which should be kept, but no key definition is set.`
      );
    // deprecated:
    if (opt.denyUndefined && !opt.keys && !opt.item)
      // replace with: if (opt.denyUndefined && !opt.item)
      throw new Error(`Option denyUndefined will skip if any element is not captured by a key definition.`);
    // deprecated:
    if (opt.mandatory === true && !opt.keys && !opt.item)
      // replace with: if (opt.mandatory === true && !opt.item)
      throw new Error(
        `Option mandatory = true needs at least one key set, but none are defined (the default is not counting here).`
      );
    if (opt.mandatory && opt.mandatory !== true) {
      if (!Array.isArray(opt.mandatory) || !opt.mandatory.length)
        throw new Error(
          `Option mandatory needs a list of keys or patterns which are allowed, but ${inspect(
            opt.mandatory
          )} was given.`
        );
      opt.mandatory.forEach(e => {
        if (typeof e !== 'string' && e !== undefined && !(e instanceof RegExp))
          throw new Error(
            `Option mandatory needs elements with string or pattern, but ${inspect(e)} was given.`
          );
      });
    }
    if (opt.forbidden) {
      if (!Array.isArray(opt.forbidden) || !opt.forbidden.length)
        throw new Error(
          `Option forbidden needs a list of keys or patterns which are allowed, but ${inspect(
            opt.forbidden
          )} was given.`
        );
      opt.forbidden.forEach(e => {
        if (typeof e !== 'string' && e !== undefined && !(e instanceof RegExp))
          throw new Error(
            `Option forbidden needs elements with string or pattern, but ${inspect(e)} was given.`
          );
      });
    }
    if (opt.combination) {
      if (!Array.isArray(opt.combination) || !opt.combination.length)
        throw new Error(`Option combination needs a rule list, but ${inspect(opt.combination)} was given.`);
      opt.combination.forEach(e => {
        if (typeof e !== 'object')
          throw new Error(`Option combination needs object elements, but ${inspect(e)} was given.`);
        // @ts-ignore
        if (!e.and) {
          // @ts-ignore
          if (Array.isArray(e.and) && e.and.length > 1)
            throw new Error(
              `Option combination with \'and\' needs at least two keys, but only ${inspect(e)} was given.`
            );
          // @ts-ignore
        } else if (!e.nand) {
          // @ts-ignore
          if (Array.isArray(e.nand) && e.nand.length > 1)
            throw new Error(
              `Option combination with \'nand\' needs at least two keys, but only ${inspect(
                e
              )} was given.`
            );
          // @ts-ignore
        } else if (!e.or) {
          // @ts-ignore
          if (Array.isArray(e.or) && e.or.length > 1)
            throw new Error(
              `Option combination with \'or\' needs at least two keys, but only ${inspect(e)} was given.`
            );
          // @ts-ignore
        } else if (!e.xor) {
          // @ts-ignore
          if (Array.isArray(e.xor) && e.xor.length > 1)
            throw new Error(
              `Option combination with \'xor\' needs at least two keys, but only ${inspect(e)} was given.`
            );
          // @ts-ignore
        } else if (!e.with) {
          // @ts-ignore
          if (Array.isArray(e.with) && e.with.length)
            throw new Error(
              `Option combination with \'with\' needs at least one peer, but only ${inspect(
                e
              )} was given.`
            );
          // @ts-ignore
          if (typeof e.key === 'string')
            throw new Error(
              `Option combination with \'with\' needs a \'key\', but only ${inspect(e)} was given.`
            );
          // @ts-ignore
        } else if (!e.without) {
          // @ts-ignore
          if (Array.isArray(e.without) && e.without.length)
            throw new Error(
              `Option combination with \'without\' needs at least one peer, but only ${inspect(
                e
              )} was given.`
            );
          // @ts-ignore
          if (typeof e.key === 'string')
            throw new Error(
              `Option combination with \'without\' needs a \'key\', but only ${inspect(e)} was given.`
            );
        } else
          throw new Error(
            `Option combination needs and, nand, or, xor, with or withoout setting, but only ${inspect(
              e
            )} was given.`
          );
      });
    }
    if (opt.min !== undefined && typeof opt.min !== 'number')
      throw new Error(`Option min needs a numeric value, but ${inspect(opt.min)} was given.`);
    if (opt.max !== undefined && typeof opt.max !== 'number')
      throw new Error(`Option max needs a numeric value, but ${inspect(opt.max)} was given.`);
    if (opt.min !== undefined && opt.max !== undefined && opt.min > opt.max)
      throw new Error(`Option min could not be greater than max, but ${opt.min} > ${opt.max} was given.`);
  }

  /**
   * Export schema as tree data structure.
   */
  export(parent: any = {}): any {
    const def: any = super.export();
    if (def.SCHEMA_META) return def.SCHEMA_META;
    // check for circular references
    if (parent[this._refID]) {
      parent[this._refID]._refNum++;
      return {
        SCHEMA_TYPE: this.constructor.name.replace(/Schema$/, ''),
        SCHEMA_REF: this._refID
      };
    }
    const map: any = { ...parent };
    map[this._refID] = this;
    // simple object
    // deprecated:
    if (def.mandatory === true && def.keys && Object.keys(this.opt).length === 2) {
      const res: any = {};
      def.keys.forEach((e: any) => {
        res[e.key || '*'] = e.schema.export(map);
      });
      return res;
    }
    if (def.mandatory === true && def.item && Object.keys(this.opt).length === 2) {
      const res: any = {};
      for (let k in def.item) {
        if (Object.prototype.hasOwnProperty.call(def.item, k)) {
          res[k] = def.item[k].export(map);
        }
      }
      return res;
    }
    // exact export
    // deprecated:
    if (def.keys)
      def.keys.forEach((key: any) => {
        key.schema = key.schema.export(map);
      });
    if (def.item)
      for (let k in def.item) {
        if (Object.prototype.hasOwnProperty.call(def.item, k)) {
          def.item[k] = def.item[k].export(map);
        }
      }
    if (this._refNum) def.SCHEMA_ID = this._refID;
    return def;
  }

  protected typeDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'object')
    return t('describe.type');
  }
  protected async typeValidator(work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'object')
    if (typeof work.value !== 'object' || Array.isArray(work.value)) {
      return Promise.reject(new ValidationError(this, work,
        t('error.type', { type: Array.isArray(work.value) ? 'array' : typeof work.value }))
      );
    }
  }

  private flattenDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'object')
    const opt = <Options>this.opt;
    if (opt.flatten) {
      let char = typeof opt.flatten === 'string' ? opt.flatten : '.';
      return t("describe.flatten", { char });
    }
  }
  private async flattenValidator(work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    // check value
    if (opt.flatten) {
      let char = typeof opt.flatten === 'string' ? opt.flatten : '.';
      work.value = flat.flatten(work.value, { delimiter: char });
    }
  }

  private rearrangeDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'object')
    const opt = <Options>this.opt;
    if (opt.rearrange) {
      let msg: string[] = [];
      opt.rearrange.forEach(rule => {
        const over = rule.overwrite ? ' ' + t('phrase.over') : '';
        // @ts-ignore
        let dest = rule.copyTo || rule.moveTo;
        // @ts-ignore
        if (rule.moveTo)
          // @ts-ignore
          msg.push(t('describe.moveTo', { key: rule.key, dest, over }));
        // @ts-ignore
        else
          msg.push(
            // @ts-ignore
            t('describe.copyTo', { key: rule.key, dest, over })
          );
      });
      return msg.join(' ');
    }
  }
  private async rearrangeValidator(work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    // check value
    if (opt.rearrange) {
      opt.rearrange.forEach(rule => {
        // @ts-ignore
        let dest = rule.copyTo || rule.moveTo;
        // @ts-ignore
        if (rule.copyTo) {
          if (rule.overwrite || !work.value[dest]) work.value[dest] = work.value[rule.key];
        } else {
          if (rule.overwrite || !work.value[dest]) {
            work.value[dest] = work.value[rule.key];
            delete work.value[rule.key];
          }
        }
      });
    }
  }

  private unflattenDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'object')
    const opt = <Options>this.opt;
    if (opt.unflatten) {
      let char = typeof opt.unflatten === 'string' ? opt.unflatten : '.';
      return t("describe.unflatten", { char });
    }
  }
  private async unflattenValidator(work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    // check value
    if (opt.unflatten) {
      let char = typeof opt.unflatten === 'string' ? opt.unflatten : '.';
      work.value = flat.unflatten(work.value, { delimiter: char });
    }
  }

  // deprecated:
  private keysDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'object')
    const opt = <Options>this.opt;
    if (path.length) {
      const m = path.match(/^([^.]+)(\.(.*))?$/);
      if (m && opt.keys) {
        const sub = m[3] || '';
        let other = '';
        let key = '';
        opt.keys.forEach(def => {
          if (def.key) {
            if (def.key === m[1]) key = def.schema.describe(depth, sub, lang);
          } else other = def.schema.describe(depth, sub, lang);
        });
        return key || other;
      }
      return '';
    }
    if (opt.keys) {
      let other = '';
      let key = [];
      opt.keys.forEach(def => {
        if (def.key) {
          key.push(`-   '${def.key}': ${def.schema.describe(depth, path, lang)}`);
        } else other = '-   ' + t('phrase.default') + ': ' + def.schema.describe(depth, path, lang);
      });
      if (other) key.unshift(other);
      return t('describe.keys') + '\n' + key.join('\n')
    }
  }
  private async keysValidator(work: Workbench, lang: string): Promise<void> {
    const opt = <Options>this.opt;
    // check value
    if (opt.keys) {
      // get schema
      let other: Schema = new AnySchema();
      let index: { [key: string]: Schema } = {};
      let match: [RegExp, Schema][] = [];
      opt.keys.forEach(def => {
        if (def.key) {
          if (def.key instanceof RegExp) match.push([def.key, def.schema]);
          else index[def.key] = def.schema;
        } else other = def.schema;
      });
      // validate
      return async.each(Object.keys(work.value), async (k: string) => {
        //return asyncForEach(Object.keys(work.value), async (k: string) => {
        let schema = index[k];
        if (!schema)
          match.forEach(e => {
            if (k.match(e[0])) schema = e[1];
          });
        if (!schema) schema = other;
        return schema.validate(work.sub(k), lang);
      });
    }
  }

  private itemDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'object')
    const opt = <Options>this.opt;
    if (path.length) {
      const m = path.match(/^([^.]+)(\.(.*))?$/);
      if (m && opt.item) {
        const sub = m[3] || '';
        let other = '';
        let key = '';
        for (let k in opt.item) {
          if (Object.prototype.hasOwnProperty.call(opt.item, k)) {
            if (k === '*') other = opt.item[k].describe(depth, sub, lang);
            else key = opt.item[k].describe(depth, sub, lang);
          }
        }
        return key || other;
      }
      return '';
    }
    if (opt.item && Object.keys(opt.item).length) {
      let index: string[] = [];
      for (let k in opt.item) {
        if (Object.prototype.hasOwnProperty.call(opt.item, k)) {
          index.push(`-   ${k}: ${opt.item[k].describe(depth, path, lang)}`);
        }
      }
      return t('describe.item') + '\n' + index.join('\n') + '\n';
    }
  }
  private async itemValidator(work: Workbench, lang: string): Promise<void> {
    const opt = <Options>this.opt;
    // check value
    if (opt.item) {
      // get schema
      let other: Schema = new AnySchema();
      let index: { [key: string]: Schema } = {};
      let match: [RegExp, Schema][] = [];
      for (let k in opt.item) {
        if (Object.prototype.hasOwnProperty.call(opt.item, k)) {
          const m = k.match(/^\/(.*)\/([i]*?)$/);
          if (k === '*') other = opt.item[k];
          else if (m) match.push([new RegExp(m[1], m[2]), opt.item[k]]);
          else index[k] = opt.item[k];
        }
      }
      // validate
      return async.each(Object.keys(work.value), async (k: string) => {
        //return asyncForEach(Object.keys(work.value), async (k: string) => {
        let schema = index[k];
        if (!schema)
          match.forEach(e => {
            if (k.match(e[0])) schema = e[1];
          });
        if (!schema) schema = other;
        return schema.validate(work.sub(k), lang);
      });
    }
  }

  private removeUndefinedDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'object')
    const opt = <Options>this.opt;
    if (opt.removeUndefined)
      return t('describe.removeUndefined');
  }
  private async removeUndefinedValidator(work: Workbench): Promise<void> {
    const opt = <Options>this.opt;
    // check value
    if (opt.removeUndefined) {
      let index: { [key: string]: 1 } = {};
      let match: RegExp[] = [];
      // deprecated:
      if (opt.keys)
        opt.keys.forEach(def => {
          if (def.key) {
            if (def.key instanceof RegExp) match.push(def.key);
            else index[def.key] = 1;
          }
        });
      if (opt.item)
        for (let k in opt.item) {
          if (Object.prototype.hasOwnProperty.call(opt.item, k)) {
            const m = k.match(/^\/(.*)\/([i]*?)$/);
            if (m) match.push(new RegExp(m[1], m[2]));
            else index[k] = 1;
          }
        }
      Object.keys(work.value).forEach(k => {
        if (index[k]) return;
        if (match.filter(e => k.match(e)).length) return;
        delete work.value[k];
      });
    }
  }

  private denyUndefinedDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'object')
    const opt = <Options>this.opt;
    if (opt.denyUndefined) return t('describe.denyUndefined');
  }
  private async denyUndefinedValidator(work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'object')
    const opt = <Options>this.opt;
    // check value
    if (opt.denyUndefined) {
      let index: { [key: string]: 1 } = {};
      let match: RegExp[] = [];
      // deprecated:
      if (opt.keys)
        opt.keys.forEach(def => {
          if (def.key) {
            if (def.key instanceof RegExp) match.push(def.key);
            else index[def.key] = 1;
          }
        });
      if (opt.item)
        for (let k in opt.item) {
          if (Object.prototype.hasOwnProperty.call(opt.item, k)) {
            const m = k.match(/^\/(.*)\/([i]*?)$/);
            if (m) match.push(new RegExp(m[1], m[2]));
            else index[k] = 1;
          }
        }
      Object.keys(work.value).forEach(k => {
        if (index[k]) return;
        if (match.filter(e => k.match(e)).length) return;
        throw new ValidationError(
          this,
          work,
          t('error.denyUndefined', { key: k })
        );
      });
    }
  }

  private requireDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'object')
    const opt = <Options>this.opt;
    let msg: string[] = [];
    if (opt.mandatory)
      if (opt.mandatory === true) msg.push(t('describe.mandatory'));
      else {
        msg.push(
          t('describe.mandatoryKeys', { count: opt.mandatory.length, list: opt.mandatory.map(e => inspect(e)).join(', ') })
        );
      }
    if (opt.forbidden)
      msg.push(
        t('describe.forbidden', { count: opt.forbidden.length, list: opt.forbidden.map(e => inspect(e)).join(', ') })
      );
    return msg.join(' ');
  }
  private async requireValidator(work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'object')
    const opt = <Options>this.opt;
    // check value
    if (opt.mandatory) {
      // deprectaed:
      const list = opt.mandatory === true
        ? opt.item
          ? Object.keys(opt.item).filter(e => e !== '*')
          : (opt.keys || []).map((e: any) => e.key).filter(e => e)
        : // replace with: const list = (opt.mandatory === true ? (opt.item ? Object.keys(opt.item).filter(e => e !== '*') : []) :
        opt.mandatory
      // will be called with the list items as k
      const fn = (k: any): Promise<boolean> => {
        // is mandatory item k in work.value
        if (
          !Object.keys(work.value).filter(e => {
            if (typeof k === 'string') {
              const m = k.match(/^\/(.*)\/([i]*?)$/);
              return (m && new RegExp(m[1], m[2]).exec(e)) || e === k;
            } else {
              return k instanceof RegExp ? k.exec(e) : e === k;
            }
          }).length
        ) {
          // key is missing, maybe a item is defined which will generate a value
          if (typeof k === 'string' && opt.item && opt.item[k]) {
            const ds = new DataStore()
            return opt.item[k].validate(new Workbench(ds), lang)
              .then(() => {
                work.value[k] = ds.data
                return Promise.resolve(true)
              })
              .catch(() => Promise.resolve(false))
          }
          return Promise.resolve(false)
        }
        return Promise.resolve(true)
      }
      // return async.each(list, fn, 1)
      const missing = await async.reject(list, fn)
      if (missing.length)
        throw new ValidationError(this, work, t('error.missing', { key: missing }));
    }
    if (opt.forbidden)
      opt.forbidden.forEach(k => {
        if (Object.keys(work.value).filter(e => (typeof k === 'string' ? e === k : e.match(k))).length)
          throw new ValidationError(this, work, t('error.forbidden', { key: k }));
      });
  }

  private combinationDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'object')
    const opt = <Options>this.opt;
    if (opt.combination) {
      let msg: string[] = [];
      opt.combination.forEach(rule => {
        // @ts-ignore
        if (rule.and)
          msg.push(
            // @ts-ignore
            t('describe.and', { list: rule.and })
          );
        // @ts-ignore
        if (rule.nand)
          msg.push(
            // @ts-ignore
            t('describe.nand', { list: rule.nand })
          );
        // @ts-ignore
        if (rule.or)
          msg.push(
            // @ts-ignore
            t('describe.or', { list: rule.or })
          );
        // @ts-ignore
        if (rule.xor)
          msg.push(
            // @ts-ignore
            t('describe.xor', { list: rule.xor })
          );
        // @ts-ignore
        if (rule.with)
          msg.push(
            // @ts-ignore
            t('describe.with', { key: rule.key, list: rule.with })
          );
        // @ts-ignore
        if (rule.without)
          msg.push(
            // @ts-ignore
            t('describe.without', { key: rule.key, list: rule.without })
          );
      });
      return msg.join(' ');
    }
  }
  private async combinationValidator(work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'object')
    const opt = <Options>this.opt;
    // check value
    if (opt.combination) {
      opt.combination.forEach(rule => {
        // @ts-ignore
        if (rule.and && rule.and.filter((e: any) => work.value[e] === undefined).length)
          throw new ValidationError(
            this,
            work,
            // @ts-ignore
            t('error.and', { list: rule.and })
          );
        // @ts-ignore
        if (rule.nand && !rule.nand.filter((e: any) => work.value[e] === undefined).length)
          throw new ValidationError(
            this,
            work,
            // @ts-ignore
            t('error.nand', { list: rule.nand })
          );
        // @ts-ignore
        if (rule.or && !rule.or.filter((e: any) => work.value[e] !== undefined).length)
          throw new ValidationError(
            this,
            work,
            // @ts-ignore
            t('error.or', { list: rule.or })
          );
        if (
          // @ts-ignore
          rule.xor &&
          // @ts-ignore
          rule.xor.filter((e: any) => work.value[e] !== undefined).length !== 1
        )
          throw new ValidationError(
            this,
            work,
            // @ts-ignore
            t('error.xor', { list: rule.xor })
          );
        if (
          // @ts-ignore
          rule.with &&
          // @ts-ignore
          work.value[rule.key] !== undefined &&
          // @ts-ignore
          rule.with.filter((e: any) => work.value[e] === undefined).length
        )
          throw new ValidationError(
            this,
            work,
            // @ts-ignore
            t('error.with', { key: rule.key, list: rule.with })
          );
        if (
          // @ts-ignore
          rule.without &&
          // @ts-ignore
          work.value[rule.key] !== undefined &&
          // @ts-ignore
          rule.without.filter((e: any) => work.value[e] !== undefined).length
        )
          throw new ValidationError(
            this,
            work,
            // @ts-ignore
            t('error.without', { key: rule.key, list: rule.without })
          );
      });
    }
  }

  private sizeDescriber(depth: number, path: string, lang: string) {
    const t = T(lang, 'object')
    const opt = <Options>this.opt;
    let msg = '';
    if (opt.min && opt.min === opt.max) msg += t('describe.length', { count: opt.max });
    else if (opt.min && opt.max)
      msg += t('describe.between', { min: opt.min, max: opt.max });
    else if (opt.min) msg += t('describe.min', { min: opt.min });
    else if (opt.max) msg += t('describe.max', { max: opt.max });
    if (msg) return msg.trim();
  }
  private async sizeValidator(work: Workbench, lang: string): Promise<void> {
    const t = T(lang, 'object')
    const opt = <Options>this.opt;
    let num = work.value ? Object.keys(work.value).length : 0;
    // check length
    if (opt.min && num < opt.min) {
      throw new ValidationError(
        this,
        work,
        t('error.min', { count: num, min: opt.min })
      );
    }
    if (opt.max && num > opt.max) {
      throw new ValidationError(
        this,
        work,
        t('error.max', { count: num, max: opt.max })
      );
    }
  }
}

//async function asyncForEach (array: any[], fn: Function) {
//  // for (let index = 0; index < array.length; index++) {
//  //   await fn(array[index], index, array);
//  // }
//  // real async:
//  const list: Promise<any>[] = []
//  for (let index = 0; index < array.length; index++) {
//    list.push(fn(array[index], index, array));
//  }
//  //await Promise.all(list)
//  await Promise.allSettled(list)
//    .then(res => {
//      const errors = res.filter(e => e.status === "rejected").map((e: any) => e.reason)
//      if (errors.length) return Promise.reject(errors[0]) // reject only first error
//      return Promise.resolve(res.map((e: any) => e.value))
//    })
//}

export default ObjectSchema;
