import { inspect } from 'util';

import * as base from './base';
import Workbench from '../workbench';
import Reference from '../reference';
import ValidationError from '../error';
import { T } from '../i18n';

export interface Options extends base.Options {
  allow?: (any | Reference)[] | Reference;
  disallow?: (any | Reference)[] | Reference;
}

export class AnySchema extends base.BaseSchema {
  constructor(opt: Options = {}) {
    // option validity
    super(opt);
    // rules
    this.describer = [
      this.typeDescriber, // 0
      this.describer[0], // 1: default
      this.allowDescriber, // 2: allow, disallow
      this.describer[1] // 3: raw
    ];
    this.validator = [
      this.validator[0], // 0: default
      this.allowValidator, // 1: allow, disallow
      this.validator[1] // 2: raw
    ];
  }

  public set (opt: Options) {
    super.set(opt);
  }

  protected check (opt: Options) {
    super.check(opt);
    if (opt.allow && !Array.isArray(opt.allow) && !(opt.allow instanceof Reference))
      throw new Error(
        `Option allow needs an array or Reference value, but ${inspect(
          opt.allow
        )} was given.`
      );
    if (opt.disallow && !Array.isArray(opt.disallow) && !(opt.disallow instanceof Reference))
      throw new Error(
        `Option disallow needs an array or Reference value, but ${inspect(
          opt.disallow
        )} was given.`
      );
  }

  protected typeDescriber (depth: number, path: string, lang: string) {
    const t = T(lang)
    return t('describe.any');
  }

  protected allowDescriber (depth: number, path: string, lang: string) {
    const t = T(lang)
    const opt = <Options>this.opt;
    let values: string[] = [];
    if (opt.allow instanceof Reference) {
      values.push(
        (values.length ? t('phrase.or') + ' ' : '') +
        t('phrase.within', { source: opt.allow.source, filter: opt.allow.filter || "''" })
      );
    } else if (opt.allow && opt.allow.length)
      opt.allow.forEach(e => {
        values.push(
          (values.length ? t('phrase.or') + ' ' : '') +
          (e instanceof Reference
            ? t('phrase.within', { source: e.source, filter: e.filter || "''" })
            : t('phrase.equal', { value: inspect(e) }))
        );
      });
    if (opt.disallow instanceof Reference) {
      values.push(
        (values.length ? t('phrase.or') + ' ' : '') +
        t('phrase.within', { source: opt.disallow.source, filter: opt.disallow.filter })
      );
    } else if (opt.disallow && opt.disallow.length)
      opt.disallow.forEach(e => {
        values.push(
          (values.length ? t('phrase.and') + ' ' : '') +
          (e instanceof Reference
            ? t('phrase.notWithin', { source: e.source, filter: e.filter })
            : t('phrase.notEqual', { value: inspect(e) }))
        );
      });
    if (values.length)
      return (
        t('describe.allow') +
        '\n- ' +
        values.slice(0, 10).join('\n- ') +
        (values.length > 10 ? '\n- ' + t('phrase.more', { count: values.length - 10 }) : '') +
        '\n'
      );
  }
  protected async allowValidator (work: Workbench, lang: string): Promise<void> {
    const t = T(lang)
    const opt = <Options>this.opt;
    // get list
    let allow: any[] = [];
    let disallow: any[] = [];
    if (opt.allow instanceof Reference) {
      let data = await opt.allow.stringArray(work);
      if (data) allow = data.slice(0);
    } else if (opt.allow && opt.allow.length) {
      for (let index = 0; index < opt.allow.length; index++) {
        let e = opt.allow[index];
        if (e instanceof Reference) {
          let data = await e.stringArray(work);
          if (data) allow = allow.concat(data);
        } else allow.push(e);
      }
    }
    if (opt.disallow instanceof Reference) {
      let data = await opt.disallow.stringArray(work);
      if (data) disallow = data.slice(0);
    } else if (opt.disallow && opt.disallow.length) {
      for (let index = 0; index < opt.disallow.length; index++) {
        let e = opt.disallow[index];
        if (e instanceof Reference) {
          let data = await e.stringArray(work);
          if (data) disallow = disallow.concat(data);
        } else disallow.push(e);
      }
    }
    // validate
    if (allow && allow.length) {
      let found = false;
      allow.forEach(e => {
        if (!found) found = e === work.value;
      });
      if (!found)
        throw new ValidationError(
          this,
          work,
          t('error.allow', { list: allow.map(e => inspect(e)).join(', ') })
        );
    }
    if (disallow && disallow.length) {
      let found = false;
      disallow.forEach(e => {
        if (!found) found = e === work.value;
      });
      if (found)
        throw new ValidationError(
          this,
          work,
          t('error.disallow', { list: disallow.map(e => inspect(e)).join(', ') })
        );
    }
  }
}

export default AnySchema;
