import { DataStore, DataSource, Options } from '@alinex/datastore';
import { Schema } from './schema';
import Workbench from './workbench';

// load schema for schema validation
//const schemaDefinition = new DataStore();
//schemaDefinition.load('lib/schemaDef');

export class Validator {
    private schema: Schema;
    private store: DataStore;
    private ref: { [key: string]: DataStore }; // reference data

    /**
     * Create new validator instance
     * @param schema Definition of schema
     * @param input one or multiple data sources to later be loaded
     */
    constructor(schema: Schema, ...input: DataSource[]) {
        this.schema = schema;
        this.store = new DataStore(...input);
        this.ref = { 'data:context': new DataStore() };
    }

    /**
     * Create a new data store and load it from single source.
     * @param schema Definition of schema
     * @param lang language to use for error messages
     * @param input one or multiple data sources
     */
    public static async load(schema: Schema, lang?: string, ...input: DataSource[]): Promise<Validator> {
        const val = new Validator(schema);
        return val.load(lang, ...input).then(() => Promise.resolve(val));
    }

    /**
     * Create a new data store and load it from single source.
     * @param schema Definition of schema
     * @param lang language to use for error messages
     * @param input an already defined DataStore
     */
    public static async validate(schema: Schema, lang: string | undefined, input: DataStore): Promise<Validator> {
        const val = new Validator(schema);
        return val.validate(lang, input).then(() => Promise.resolve(val));
    }

    /**
     * Create a new data store and load it from single source.
     * @param schema Definition of schema
     * @param lang language to use for error messages
     * @param source URL to load
     * @param options options to load source
     */
    public static async url(schema: Schema, lang: string | undefined, source: string, options?: Options): Promise<Validator> {
        const val = new Validator(schema);
        return val.load(lang, { source, options }).then(() => Promise.resolve(val));
    }

    /**
     * Create a new data store with preset data.
     * @param schema Definition of schema
     * @param lang language to use for error messages
     * @param source data structure to be set
     */
    public static async data(schema: Schema, lang: string | undefined, source: any): Promise<Validator> {
        const val = new Validator(schema);
        return val.load(lang, { data: source }).then(() => Promise.resolve(val));
    }

    /**
     * Create a new data store with preset data.
     * @param schema Definition of schema
     * @param lang language for error messages
     * @param source data structure to be set
     */
    public static async datastore(schema: Schema, lang: string | undefined, source: DataStore): Promise<Validator> {
        const val = new Validator(schema);
        val.store = source;
        return val.load(lang).then(() => Promise.resolve(val));
    }

    /**
     * Get the context data used in references.
     */
    get context(): any {
        return this.ref['data:context'].data;
    }

    /**
     * Set the context data used in references.
     * @param uri URL specifying where the data is/will be stored
     */
    set context(data: any) {
        this.ref['data:context'].data = data;
    }

    /**
     * Get description of the schema.
     * @param depth number of levels to include in description
     * @param path path to start the description
     * @param lang language to use for error messages
     * @return Human readable schema description.
     */
    public describe(depth = 3, path = '', lang?: string): string {
        return this.schema.describe(depth, path, lang);
    }

    /**
     * Load data into validator and check them.
     * @param lang language to use for error messages
     * @param input DataStore with already defined source
     * @return validated structure
     */
    public async validate(lang?: string, input?: DataStore): Promise<any> {
        if (input) this.store = input
        this.store.load() // load if not already done
        return this.schema.validate(new Workbench(this.store, this.ref), lang).then(() => this.store.data);
    }

    /**
     * Load data into validator and check them.
     * @param lang language to use for error messages
     * @param input URL or list to read from
     * @return validated structure
     */
    public async load(lang?: string, ...input: DataSource[]): Promise<any> {
        await this.store.load(...input);
        return this.schema.validate(new Workbench(this.store, this.ref), lang).then(() => this.store.data);
    }

    /**
     * Reload and check already existing content again.
     * @param time number of seconds to wait till reload
     * @param lang language to use for error messages
     * @return flag if reload was done (only if changed)
     */
    public async reload(time = 0, lang?: string): Promise<boolean> {
        const changed = await this.store.reload(time);
        if (!changed) return changed;
        return this.schema.validate(new Workbench(this.store, this.ref), lang).then(() => changed);
    }

    /**
     * Save data structure to persistent store.
     * @param output URL to be stored to
     * @return true after storing
     */
    public async save(output?: DataSource): Promise<boolean> {
        return this.store.save(output);
    }

    /**
     * Check the given path and return true if this element is defined else false.
     * @param command filter command to select elements
     */
    public has(command?: string): boolean {
        return this.store.get(command);
    }

    /**
     * Get the element at the defined path.
     * @param command filter command to select elements
     * @param fallback default value to be used if not found
     */
    public get(command?: string, fallback?: any): any {
        return this.store.get(command, fallback);
    }
}

export default Validator;
