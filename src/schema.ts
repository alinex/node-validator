import Debug from 'debug';
import { inspect } from 'util';
import { clone, store, empty } from '@alinex/data';

import BaseSchema from './type/base';
import AnySchema from './type/any';
import BooleanSchema from './type/boolean';
import StringSchema from './type/string';
import NumberSchema from './type/number';
import DateTimeSchema from './type/datetime';
import ArraySchema from './type/array';
import ObjectSchema from './type/object';
import PortSchema from './type/port';
import IPSchema from './type/ip';
import DomainSchema from './type/domain';
import EmailSchema from './type/email';
import URLSchema from './type/url';
import FileSchema from './type/file';
import RegExpSchema from './type/regexp';
import LogicSchema from './type/logic';
import * as Meta from './meta';
import Reference from './reference';
import Validator from '.';

export type Schema =
    | AnySchema
    | BooleanSchema
    | StringSchema
    | NumberSchema
    | DateTimeSchema
    | ArraySchema
    | ObjectSchema
    | PortSchema
    | IPSchema
    | DomainSchema
    | EmailSchema
    | URLSchema
    | FileSchema
    | RegExpSchema
    | LogicSchema;

export {
    AnySchema,
    BooleanSchema,
    StringSchema,
    NumberSchema,
    DateTimeSchema,
    ArraySchema,
    ObjectSchema,
    PortSchema,
    IPSchema,
    DomainSchema,
    EmailSchema,
    URLSchema,
    FileSchema,
    RegExpSchema,
    LogicSchema,
    // other elements
    Meta
};

const debugImport = Debug('validator:import');
//const debugExport = Debug('validator:export')

function createSchema(def: { [key: string]: any }, map: any): Schema {
    const type: string = def.SCHEMA_TYPE;
    delete def.SCHEMA_TYPE;
    delete def.SCHEMA_REF;
    // find anchor for circular references
    const id: string = def.SCHEMA_ID;
    if (id) {
        map[id] = [];
        delete def.SCHEMA_ID;
    }
    let refList: any = [];
    // replace data references
    Object.keys(def).forEach((key: string) => {
        if (def[key].SCHEMA_TYPE && def[key].SCHEMA_TYPE.toLowerCase() === 'reference')
            def[key] = new Reference(def[key].source, def[key].filter);
    });
    // create schema instance
    let schema: Schema;
    switch (type.toLowerCase()) {
        case 'any':
            schema = new AnySchema(def);
            break;
        case 'boolean':
            schema = new BooleanSchema(def);
            break;
        case 'string':
            schema = new StringSchema(def);
            break;
        case 'number':
            schema = new NumberSchema(def);
            break;
        case 'datetime':
            schema = new DateTimeSchema(def);
            break;
        case 'array':
            // deprecated:
            if (def.items)
                def.items.forEach((e: any) => {
                    const ref = e.schema.SCHEMA_REF;
                    e.schema = createSchema(e.schema, map);
                    if (ref) refList.push([ref, e, 'schema']);
                });
            if (def.item)
                for (let k in def.item) {
                    if (Object.prototype.hasOwnProperty.call(def.item, k)) {
                        const ref = def.item[k].SCHEMA_REF;
                        def.item[k] = createSchema(def.item[k], map);
                        if (ref) refList.push([ref, def.item[k], '']);
                    }
                }
            if (def.filter) {
                const ref = def.filter.SCHEMA_REF;
                def.filter = createSchema(def.filter, map);
                if (ref) refList.push([ref, def.filter, 'schema']);
            }
            schema = new ArraySchema(def);
            break;
        case 'object':
            // deprecated:
            if (def.keys)
                def.keys.forEach((e: any) => {
                    const ref = e.schema.SCHEMA_REF;
                    e.schema = createSchema(e.schema, map);
                    if (ref) refList.push([ref, e, 'schema']);
                });
            if (def.item)
                for (let k in def.item) {
                    if (Object.prototype.hasOwnProperty.call(def.item, k)) {
                        const ref = def.item[k].SCHEMA_REF;
                        def.item[k] = createSchema(def.item[k], map);
                        if (ref) refList.push([ref, def.item, k]);
                    }
                }
            schema = new ObjectSchema(def);
            break;
        case 'port':
            schema = new PortSchema(def);
            break;
        case 'ip':
            schema = new IPSchema(def);
            break;
        case 'domain':
            schema = new DomainSchema(def);
            break;
        case 'email':
            schema = new EmailSchema(def);
            break;
        case 'url':
            schema = new URLSchema(def);
            break;
        case 'file':
            schema = new FileSchema(def);
            break;
        case 'regexp':
            schema = new RegExpSchema(def);
            break;
        case 'logic':
            if (def.if) def.if = createSchema(def.if, map);
            if (def.check) {
                if (Array.isArray(def.check))
                    def.check = def.check.map((s, i) => {
                        const ref = s.SCHEMA_REF;
                        const schema = createSchema(s, map);
                        if (ref) refList.push([ref, def.check.map, i]);
                        return schema;
                    });
                else {
                    const ref = def.check.SCHEMA_REF;
                    def.check = createSchema(def.check, map);
                    if (ref) refList.push([ref, def, 'check']);
                }
            }
            if (def.else) {
                if (Array.isArray(def.else))
                    def.else = def.else.map((s, i) => {
                        const ref = s.SCHEMA_REF;
                        const schema = createSchema(s, map);
                        if (ref) refList.push([ref, def.else.map, i]);
                        return schema;
                    });
                else {
                    const ref = def.else.SCHEMA_REF;
                    def.else = createSchema(def.else, map);
                    if (ref) refList.push([ref, def, 'else']);
                }
            }
            schema = new LogicSchema(def);
            break;
        case 'timeout':
            schema = Meta.timeout(def);
            break;
        default:
            throw new Error(`Unknown schema or meta type ${type}`);
    }
    // collect references to be replaced and replace in parent call
    if (refList.length) refList.forEach((e: any[]) => map[e[0]].push([e[1], e[2]]));
    if (id) map[id].forEach((e: any[]) => (e[0][e[1]] = schema));
    // return schema
    return schema;
}

/**
 * Return the schema as data structure to be saved.
 * @param schema to export
 */
export function exportTree(schema: Schema): { [key: string]: any } {
    return schema.export();
}

function _importTree(tree: { [key: string]: any }, map: any = {}): Schema {
    debugImport('converting %O', inspect(tree, { depth: 1 }));
    if (tree instanceof BaseSchema) return <Schema>tree;
    if (typeof tree !== 'object') throw new Error(`Could not import ${tree}!`);
    // transform
    if (tree.SCHEMA_TYPE) return createSchema(clone(tree), map);
    if (Object.keys(tree).filter(e => !e.match(/^\d+$|^\[\]$/)).length) {
        const res: any = {};
        for (let k in tree) {
            if (Object.prototype.hasOwnProperty.call(tree, k)) res[k] = _importTree(tree[k], map);
        }
        return new ObjectSchema({
            item: res,
            mandatory: true
        });
    }
    const res: any = {};
    for (let k in tree) {
        if (Object.prototype.hasOwnProperty.call(tree, k)) res[k] = _importTree(tree[k], map);
    }
    return new ArraySchema({ item: res });
}

function revertSimplification(tree: any): void {
    if (!tree || typeof tree !== 'object') return;
    // revert simplifications from base.export()
    if (tree.SCHEMA_TYPE) {
        ['allow', 'disallow'].forEach(k => {
            if (tree[k] !== undefined && !Array.isArray(tree[k])) tree[k] = [tree[k]];
        });
    }
    Object.keys(tree).forEach(k => revertSimplification(tree[k]));
}

/**
 * Create a schema definition from a simple schema object tree.
 * The tree may contain direct schemas or type definitions on any position.
 * @param tree object structure containing schemas or type definitions
 */
export async function importTree(tree: { [key: string]: any }): Promise<Schema> {
    debugImport('reading %O', tree);
    if (tree instanceof BaseSchema) return <Schema>tree;
    if (typeof tree !== 'object') throw new Error(`Could not import ${tree}!`);
    // revert simplifications from base.export()
    tree = clone(tree);
    revertSimplification(tree);
    // check
    const val = await Validator.data(require('./definition').default, undefined, tree);
    debugImport('validated %O', val.get());
    // transform
    return Promise.resolve(_importTree(val.get()));
    //return Promise.resolve(_importTree(tree));
}

/**
 * Return the schema as data structure to be saved.
 * @param schema to export
 */
export function exportList(schema: Schema): { [key: string]: any } {
    return combine(schema.export());
}

function combine(tree: any): any {
    const res: any = {};
    Object.keys(tree).forEach(k => {
        if (tree[k].SCHEMA_TYPE) res[k] = tree[k];
        else {
            const sub = combine(tree[k]);
            Object.keys(sub).forEach(s => {
                res[`${k.replace(/\./g, '\\.')}.${s}`] = sub[s];
            });
        }
    });
    return res;
}

/**
 * Create a schema definition from a simple schema list.
 * This is like tree format but with a flat structure using `.` as separator within the keys.
 * @param list array containing schemas or type definitions
 */
export async function importList(list: { [key: string]: any }): Promise<Schema> {
    let map: { [key: string]: {} } = {};
    Object.keys(list).forEach(k => {
        // objectPath.set(map, k, list[k]);
        store(map, k, list[k]);
    });
    return await importTree(map);
}

/**
 * Export into JSON Schema
 */
export function exportJsonSchema(schema: Schema): any {
    let def = schema.export();
    // any
    const len = Object.keys(def).length;
    if (def.SCHEMA_TYPE === 'Any' && Object.keys(def).length === 1) def = {};
    else if (len === 1 && def.SCHEMA_TYPE) {
        const map: any = {
            datetime: 'date'
        };
        def = { type: map[def.SCHEMA_TYPE] || def.SCHEMA_TYPE.toLowerCase() };
    }
    def = { $schema: 'http://json-schema.org/schema#', $id: schema.id || 'x1', ...def };
    return def;
}

/**
 * Import schema
 * @param def JSON Schema definition
 */
export async function importJsonSchema(json: any): Promise<Schema> {
    // remove definition
    let def = clone(json);
    delete def.$schema;
    delete def.$id;
    // convert
    if (def === true || (typeof def === 'object' && !Array.isArray(def) && empty(def))) def = { SCHEMA_TYPE: 'Any' };
    // should always be false
    //else if (def === false) def = { SCHEMA_TYPE: 'Any', allow: undefined };
    else if (typeof def === 'object') {
        const len = Object.keys(def).length;
        if (def.type && len === 1) {
            const map: any = {
                date: 'datetime'
            };
            def = { SCHEMA_TYPE: map[def.type] || def.type };
        }
    }
    return importTree(def);
}

export default Schema;
