# Alinex Validator

It is the **most powerful** and complete **data validation** library based on a schema definition which can validate complex data structures in depth and also **sanitize and transform** them. To complete it you can also **load** such **data from different sources** like files or web services in nearly **every data format**. Using the included CLI process you may also check, preprocess and transform data structures before they are used in any program.

## Documentation

Find a complete manual under [alinex.gitlab.io/node-validator](https://alinex.gitlab.io/node-validator).

If you want to have an offline access to the documentation, feel free to download the [PDF Documentation](https://alinex.gitlab.io/node-validator/validator-book.pdf).

## License

(C) Copyright 2018 - 2021 Alexander Schilling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

> <https://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
