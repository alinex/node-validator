import { expect } from 'chai';

import { Validator } from '../../src/index';
import { StringSchema } from '../../src/schema';

describe('string', () => {
    const schema = new StringSchema({
        title: 'string'
    });
    const val = new Validator(schema);
    describe('simple title', () => {
        it('should validate string', async () => {
            const data = 'example';
            let res: any;
            try {
                res = await val.load(undefined, { data });
            } catch (err) {
                throw new Error(err);
            }
            expect(res).deep.equal('example');
            expect(val.get()).deep.equal('example');
        });
        it('should describe schema', async () => {
            expect(val.describe()).to.be.a('string');
        });
    });
});
