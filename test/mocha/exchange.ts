import { expect } from 'chai';
// import { inspect } from 'util';
import Debug from 'debug';

import * as Schema from '../../src/schema';

const debug = Debug('test');

async function importJson(def: any, check: any) {
    debug('import %o', def);
    const imported = await Schema.importJsonSchema(def);
    const tree = Schema.exportTree(imported);
    debug('into   %o', tree);
    debug('check  %o', check);
    expect(tree).to.deep.equal(check);
}
async function exportJson(def: any, check: any) {
    debug('export %o', def);
    const schema = await Schema.importTree(def);
    const exported = Schema.exportJsonSchema(schema);
    debug('into   %o', exported);
    debug('check  %o', check);
    expect(exported).to.deep.equal(check);
}

describe('exchange', () => {
    describe('any', () => {
        const json_1 = { $schema: 'http://json-schema.org/schema#', $id: 'AnySchema' };
        const json_2 = {};
        const json_3 = true;
        const schema = { SCHEMA_TYPE: 'Any' };
        it('should import json schema', async () => {
            await importJson(json_1, schema);
            await importJson(json_2, schema);
            await importJson(json_3, schema);
        });
        it('should export to json schema', async () => {
            await exportJson(schema, json_1);
        });
    });
    describe('type', () => {
        const json = { $schema: 'http://json-schema.org/schema#', $id: 'StringSchema', type: 'string' };
        const schema = { SCHEMA_TYPE: 'String' };
        it('should import json schema', async () => {
            await importJson(json, schema);
        });
        it('should export to json schema', async () => {
            await exportJson(schema, json);
        });
    });
});
