import { expect } from 'chai';
// import { inspect } from 'util';
import Debug from 'debug';

import * as Schema from '../../src/schema';
import Reference from '../../src/reference';

const debug = Debug('test');

async function importExport(base: any, check: any) {
    debug('import from   %o', base);
    const schema = await Schema.importTree(base);
    debug('got schema    %o', schema);
    const data = Schema.exportTree(schema);
    debug('exported as   %o', data);
    debug('check agaunst %o', check);
    expect(data).to.deep.equal(check);
}
async function importExportList(base: any, check: any) {
    debug('import from   %o', base);
    const schema = await Schema.importList(base);
    debug('got schema    %o', schema);
    const data = Schema.exportList(schema);
    debug('exported as   %o', data);
    debug('check against %o', check);
    expect(data).to.deep.equal(check);
}

describe('schema', () => {
    describe('export/import', () => {
        it('should work with BooleanSchema', async () => {
            const base = new Schema.BooleanSchema();
            const check = { SCHEMA_TYPE: 'Boolean' };
            await importExport(base, check);
            await importExport(check, check);
        });
        it('should work with StringSchema with options', async () => {
            const base = new Schema.StringSchema({ trim: true });
            const check = { SCHEMA_TYPE: 'String', trim: true };
            await importExport(base, check);
            await importExport(check, check);
        });
        it('should allow simplified syntax in allow/disallow one', async () => {
            const base = new Schema.StringSchema({ allow: ['a'] });
            const check = { SCHEMA_TYPE: 'String', allow: 'a' };
            await importExport(base, check);
            await importExport(check, check);
        });
        it('should work with ObjectSchema', async () => {
            const base = new Schema.ObjectSchema({
                item: { flag: new Schema.BooleanSchema(), '/a*/': new Schema.StringSchema() }
            });
            const check = {
                item: { flag: { SCHEMA_TYPE: 'Boolean' }, '/a*/': { SCHEMA_TYPE: 'String' } },
                SCHEMA_TYPE: 'Object'
            };
            await importExport(base, check);
            await importExport(check, check);
        });
        it('should work with ArraySchema', async () => {
            const base = new Schema.ArraySchema({
                item: { '*': new Schema.StringSchema() }
            });
            const check = { '*': { SCHEMA_TYPE: 'String' } };
            await importExport(base, check);
            await importExport(check, check);
        });
        it('should work with meta schema', async () => {
            const base = Schema.Meta.timeout({ default: '1200' });
            const check = { SCHEMA_TYPE: 'timeout', default: '1200' };
            await importExport(base, check);
            await importExport(check, check);
        });
        it('should work with key object schema', async () => {
            const base = new Schema.ObjectSchema({
                item: { message: new Schema.StringSchema() },
                mandatory: true
            });
            const check = { message: { SCHEMA_TYPE: 'String' } };
            await importExport(base, check);
            await importExport(check, check);
        });
        it('should work with deep object schema', async () => {
            const base = new Schema.ObjectSchema({
                item: {
                    status: new Schema.ObjectSchema({
                        item: { message: new Schema.StringSchema() },
                        mandatory: true
                    })
                },
                mandatory: true
            });
            const check = { status: { message: { SCHEMA_TYPE: 'String' } } };
            await importExport(base, check);
            await importExport(check, check);
        });
        it('should work with indexed array schema', async () => {
            const base = new Schema.ArraySchema({
                item: { '0': new Schema.StringSchema() }
            });
            const check = { 0: { SCHEMA_TYPE: 'String' } };
            await importExport(base, check);
            await importExport(check, check);
        });
        it('should work with logic', async () => {
            const base = new Schema.LogicSchema({
                if: new Schema.NumberSchema(),
                check: new Schema.NumberSchema(),
                else: new Schema.StringSchema()
            });
            const check = {
                if: { SCHEMA_TYPE: 'Number' },
                check: { SCHEMA_TYPE: 'Number' },
                else: { SCHEMA_TYPE: 'String' },
                SCHEMA_TYPE: 'Logic'
            };
            await importExport(base, check);
            await importExport(check, check);
        });
        it('should work with circular reference', async () => {
            const all = new Schema.LogicSchema(); // will be set later because of recursion
            const string = new Schema.StringSchema();
            const object = new Schema.ObjectSchema({ item: { '*': all } });
            all.set({
                operator: 'or',
                check: [string, object]
            });
            const check = {
                operator: 'or',
                check: [
                    { SCHEMA_TYPE: 'String' },
                    {
                        item: { '*': { SCHEMA_TYPE: 'Logic', SCHEMA_REF: 'Logic_50' } },
                        SCHEMA_TYPE: 'Object'
                    }
                ],
                SCHEMA_TYPE: 'Logic',
                SCHEMA_ID: 'Logic_50'
            };
            const check2 = {
                operator: 'or',
                check: [
                    { SCHEMA_TYPE: 'String' },
                    {
                        item: { '*': { SCHEMA_TYPE: 'Logic', SCHEMA_REF: 'Logic_52' } },
                        SCHEMA_TYPE: 'Object'
                    }
                ],
                SCHEMA_TYPE: 'Logic',
                SCHEMA_ID: 'Logic_52'
            };
            await importExport(all, check);
            await importExport(check, check2);
        });
        it('should work with references', async () => {
            const base = new Schema.AnySchema({
                default: new Reference('data:context', 'cities')
            });
            const check = {
                default: { SCHEMA_TYPE: 'Reference', source: 'data:context', filter: 'cities' },
                SCHEMA_TYPE: 'Any'
            };
            await importExport(base, check);
            await importExport(check, check);
        });
    });
    describe('list format', () => {
        it('should work with deep object schema', async () => {
            const check = { 'status.message': { SCHEMA_TYPE: 'String' } };
            await importExportList(check, check);
        });
        it('should work with multiple definitions', async () => {
            const check = {
                'status.message': { SCHEMA_TYPE: 'String' },
                'status.code': { SCHEMA_TYPE: 'Number' }
            };
            await importExportList(check, check);
        });
        it('should work with masked dot', async () => {
            const check = { 'file.log\\.txt.size': { SCHEMA_TYPE: 'Number' } };
            await importExportList(check, check);
        });
    });
});
