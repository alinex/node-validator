import { expect } from 'chai';
import DataStore from '@alinex/datastore';
import { validator, describer } from './type/helper';

import * as Meta from '../../src/meta';

describe('meta', () => {
    let ds = new DataStore();
    it('should create timeout schema', async () => {
        const schema = Meta.timeout({
            default: '1 minute',
            unit: 'min'
        });
        expect(describer(schema)).to.be.a('string');
        ds.data = '2 hours 10 seconds';
        await validator(schema, ds);
        expect(ds.data).deep.equal(120);
        ds.data = '';
        await validator(schema, ds);
        expect(ds.data).deep.equal(1);
        ds.data = 7210;
        await validator(schema, ds);
        expect(ds.data).deep.equal(120);
    });
});
