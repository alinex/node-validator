import DataStore from '@alinex/datastore';

import { importList } from '../../../src/schema'

import { validator, fail } from '../type/helper';

const mobmiSchema = {
  "entries.entries": {
    SCHEMA_TYPE: "Array",
    item: {
      '*': {
        SCHEMA_TYPE: "Logic",
        operator: 'or',
        check: [
          {
            SCHEMA_TYPE: "Object",
            item: {
              success: { SCHEMA_TYPE: "Boolean", value: true },
              error: { SCHEMA_TYPE: "Any", allow: null },
            }
          },
          {
            SCHEMA_TYPE: "Object",
            item: {
              description: { SCHEMA_TYPE: "String", allow: 'Lendings' },
              error: { SCHEMA_TYPE: "Any", allow: null },
            }
          }
        ]
      }
    }
  }
}

describe('usecase mobmi', () => {
  describe('multiple xml entries', () => {
    it('should work', async () => {
      let ds = new DataStore({ source: `file://${__dirname}/mobmi-ok.xml` });
      await ds.load()
      const schema = await importList(mobmiSchema)
      await validator(schema, ds);
    });
    it('should fail', async () => {
      let ds = new DataStore({ source: `file://${__dirname}/mobmi-fail.xml` });
      await ds.load()
      const schema = await importList(mobmiSchema)
      await fail(schema, ds);
    });
  });
});
