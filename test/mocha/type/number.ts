import { expect } from 'chai';
import DataStore from '@alinex/datastore';
import { validator, fail, describer } from './helper';

import NumberSchema from '../../../src/type/number';

describe('number', () => {
    let ds = new DataStore();
    describe('without definition', () => {
        const schema = new NumberSchema({
            title: 'number'
        });
        it('should validate number', async () => {
            ds.data = 5;
            await validator(schema, ds);
            expect(ds.data).deep.equal(5);
        });
        it('should validate string', async () => {
            ds.data = '5';
            await validator(schema, ds);
            expect(ds.data).deep.equal(5);
        });
        it('should fail on boolean', async () => {
            ds.data = true;
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with sanitize option', () => {
        it('should extract within text', async () => {
            const schema = new NumberSchema({
                sanitize: true
            });
            ds.data = 'after 5.34 seconds';
            await validator(schema, ds);
            expect(ds.data).deep.equal(5.34);
        });
        it('should read percent value', async () => {
            const schema = new NumberSchema({
                sanitize: true
            });
            ds.data = '5%';
            await validator(schema, ds);
            expect(ds.data).deep.equal(0.05);
        });
        it('should describe', () => {
            const schema = new NumberSchema({
                sanitize: true
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with unit option', () => {
        it('should convert time', async () => {
            const schema = new NumberSchema({
                unit: { from: 's', to: 'min' }
            });
            ds.data = 300;
            await validator(schema, ds);
            expect(ds.data).deep.equal(5);
        });
        it('should detect time', async () => {
            const schema = new NumberSchema({
                unit: { from: 's', to: 'min' }
            });
            ds.data = '1h';
            await validator(schema, ds);
            expect(ds.data).deep.equal(60);
        });
        it('should convert between binary -> decimal', async () => {
            const schema = new NumberSchema({
                unit: { from: 'KiB', to: 'KB' }
            });
            ds.data = 2;
            await validator(schema, ds);
            expect(ds.data).deep.equal(2.048);
        });
        it('should convert between decimal -> binary', async () => {
            const schema = new NumberSchema({
                unit: { from: 'KB', to: 'KiB' }
            });
            ds.data = 2.048;
            await validator(schema, ds);
            expect(ds.data).deep.equal(2);
        });
        it('should convert and read binary', async () => {
            const schema = new NumberSchema({
                unit: { from: 'KiB', to: 'KiB' }
            });
            ds.data = '2KB';
            await validator(schema, ds);
            expect(ds.data).deep.equal(2);
        });
        it('should convert value to decimal', async () => {
            const schema = new NumberSchema({
                unit: { from: 'KB', to: 'KB' }
            });
            ds.data = '2KiB';
            await validator(schema, ds);
            expect(ds.data).deep.equal(2.048);
        });
        it('should describe', () => {
            const schema = new NumberSchema({
                unit: { from: 's', to: 'min' }
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with round option', () => {
        it('should use arithmetic rounding', async () => {
            const schema = new NumberSchema({
                round: { precision: 1 }
            });
            ds.data = 5.34;
            await validator(schema, ds);
            expect(ds.data).deep.equal(5.3);
        });
        it('should use floor rounding', async () => {
            const schema = new NumberSchema({
                round: { method: 'floor', precision: 0 }
            });
            ds.data = 5.34;
            await validator(schema, ds);
            expect(ds.data).deep.equal(5);
        });
        it('should use ceil rounding', async () => {
            const schema = new NumberSchema({
                round: { method: 'ceil', precision: 0 }
            });
            ds.data = 5.34;
            await validator(schema, ds);
            expect(ds.data).deep.equal(6);
        });
        it('should describe', () => {
            const schema = new NumberSchema({
                round: { method: 'ceil', precision: 0 }
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with integer option', () => {
        it('should allow integer', async () => {
            const schema = new NumberSchema({
                integer: true
            });
            ds.data = 5;
            await validator(schema, ds);
            expect(ds.data).deep.equal(5);
        });
        it('should fail if not integer', async () => {
            const schema = new NumberSchema({
                integer: true
            });
            ds.data = 5.34;
            await fail(schema, ds);
        });
        it('should allow byte integer', async () => {
            const schema = new NumberSchema({
                integer: { type: 'byte', unsigned: true }
            });
            ds.data = 0;
            await validator(schema, ds);
            ds.data = 255;
            await validator(schema, ds);
            ds.data = 5;
            await validator(schema, ds);
            expect(ds.data).deep.equal(5);
        });
        it('should fail if not byte integer', async () => {
            const schema = new NumberSchema({
                integer: { type: 'byte', unsigned: true }
            });
            ds.data = -5;
            await fail(schema, ds);
        });
        it('should sanitize integer', async () => {
            const schema = new NumberSchema({
                integer: true,
                sanitize: true
            });
            ds.data = 5.38;
            await validator(schema, ds);
            expect(ds.data).deep.equal(5);
        });
        it('should describe', () => {
            const schema = new NumberSchema({
                integer: true
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe with type', () => {
            const schema = new NumberSchema({
                integer: { type: 'byte', unsigned: true }
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with range options', () => {
        it('should support min option', async () => {
            const schema = new NumberSchema({
                min: 4
            });
            ds.data = 4;
            await validator(schema, ds);
            ds.data = 5;
            await validator(schema, ds);
            expect(ds.data).deep.equal(5);
        });
        it('should fail on min option', async () => {
            const schema = new NumberSchema({
                min: 4
            });
            ds.data = 3.999;
            await fail(schema, ds);
        });
        it('should support max option', async () => {
            const schema = new NumberSchema({
                max: 6
            });
            ds.data = 6;
            await validator(schema, ds);
            ds.data = 5;
            await validator(schema, ds);
            expect(ds.data).deep.equal(5);
        });
        it('should fail on max option', async () => {
            const schema = new NumberSchema({
                max: 6
            });
            ds.data = 6.001;
            await fail(schema, ds);
        });
        it('should support greater option', async () => {
            const schema = new NumberSchema({
                greater: 4
            });
            ds.data = 4.001;
            await validator(schema, ds);
            ds.data = 5;
            await validator(schema, ds);
            expect(ds.data).deep.equal(5);
        });
        it('should fail on greater option', async () => {
            const schema = new NumberSchema({
                greater: 4
            });
            ds.data = 4;
            await fail(schema, ds);
        });
        it('should support less option', async () => {
            const schema = new NumberSchema({
                less: 6
            });
            ds.data = 5.999;
            await validator(schema, ds);
            ds.data = 5;
            await validator(schema, ds);
            expect(ds.data).deep.equal(5);
        });
        it('should fail on less option', async () => {
            const schema = new NumberSchema({
                less: 6
            });
            ds.data = 6;
            await fail(schema, ds);
        });
        it('should describe min option', () => {
            const schema = new NumberSchema({
                min: 5
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe max option', () => {
            const schema = new NumberSchema({
                max: 5
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe with min and max', () => {
            const schema = new NumberSchema({
                min: 5,
                max: 8
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe greater option', () => {
            const schema = new NumberSchema({
                greater: 5
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe less option', () => {
            const schema = new NumberSchema({
                less: 5
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe with greater and less', () => {
            const schema = new NumberSchema({
                greater: 5,
                less: 8
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with multipleOf options', () => {
        it('should succeed', async () => {
            const schema = new NumberSchema({
                multipleOf: 4
            });
            ds.data = 8;
            await validator(schema, ds);
            expect(ds.data).deep.equal(8);
        });
        it('should fail', async () => {
            const schema = new NumberSchema({
                multipleOf: 4
            });
            ds.data = 6;
            await fail(schema, ds);
        });
        it('should describe with greater and less', () => {
            const schema = new NumberSchema({
                multipleOf: 4
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with allow ranges option', () => {
        const schema = new NumberSchema({
            allow: [[4, 8]]
        });
        it('should succeed', async () => {
            ds.data = 8;
            await validator(schema, ds);
            expect(ds.data).deep.equal(8);
        });
        it('should fail', async () => {
            ds.data = 2;
            await fail(schema, ds);
        });
        it('should describe with greater and less', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with format option', () => {
        it('should succeed', async () => {
            const schema = new NumberSchema({
                format: '0,0'
            });
            ds.data = 10000;
            await validator(schema, ds);
            expect(ds.data).deep.equal('10,000');
        });
        it('should convert with unit', async () => {
            const schema = new NumberSchema({
                unit: { from: 'mm', to: 'cm' },
                format: '0.0 $unit'
            });
            ds.data = 195;
            await validator(schema, ds);
            expect(ds.data).deep.equal('19.5 cm');
        });
        it('should convert with best unit', async () => {
            const schema = new NumberSchema({
                unit: { from: 'cm', to: 'cm' },
                format: '0.0 $best'
            });
            ds.data = 1700;
            await validator(schema, ds);
            expect(ds.data).deep.equal('17.0 m');
        });
        it('should describe with greater and less', () => {
            const schema = new NumberSchema({
                format: '0,0'
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
});
