import { expect } from 'chai';
import DataStore from '@alinex/datastore';

import { validator, fail, describer } from './helper';
import RegExpSchema from '../../../src/type/regexp';

describe('regexp', () => {
    let ds = new DataStore();
    describe('without definition', () => {
        const schema = new RegExpSchema({
            title: 'regexp'
        });
        it('should validate RegExp', async () => {
            ds.data = /[æb]+/g;
            await validator(schema, ds);
            expect(ds.data).deep.equal(/[æb]+/g);
        });
        it('should validate string', async () => {
            ds.data = '/[æb]+/g';
            await validator(schema, ds);
            expect(ds.data).deep.equal(/[æb]+/g);
        });
        it('should fail other text', async () => {
            ds.data = '// help';
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('wit length options', () => {
        it('should fail on too much groups', async () => {
            const schema = new RegExpSchema({
                max: 1
            });
            ds.data = /(x+)(y+)?/;
            await fail(schema, ds);
        });
        it('should fail on too few groups', async () => {
            const schema = new RegExpSchema({
                min: 3
            });
            ds.data = /(x+)/;
            await fail(schema, ds);
        });
        it('should describe exact length', () => {
            const schema = new RegExpSchema({
                min: 5,
                max: 5
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe length range', () => {
            const schema = new RegExpSchema({
                min: 3,
                max: 5
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe min length', () => {
            const schema = new RegExpSchema({
                min: 5
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe max length', () => {
            const schema = new RegExpSchema({
                max: 5
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
});
