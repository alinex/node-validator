import { expect } from 'chai';
import DataStore from '@alinex/datastore';
import { validator, fail, describer } from './helper';

import ArraySchema from '../../../src/type/array';
import StringSchema from '../../../src/type/string';
import AnySchema from '../../../src/type/any';
// import Reference from '../../../src/reference';

describe('array', () => {
    let ds = new DataStore();
    describe('without options', () => {
        const schema = new ArraySchema({
            title: 'array'
        });
        it('should validate array', async () => {
            ds.data = ['example'];
            await validator(schema, ds);
            expect(ds.data).deep.equal(['example']);
        });
        it('should validate empty array', async () => {
            ds.data = [];
            await validator(schema, ds);
            expect(ds.data).deep.equal([]);
        });
        it('should fail for string', async () => {
            ds.data = 'example';
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with split option', () => {
        it('should split by string', async () => {
            const schema = new ArraySchema({
                split: ':'
            });
            ds.data = '1:2:3';
            await validator(schema, ds);
            expect(ds.data).deep.equal(['1', '2', '3']);
        });
        it('should split by RegExp', async () => {
            const schema = new ArraySchema({
                split: /\s*,\s*/
            });
            ds.data = '1, 2    ,   3';
            await validator(schema, ds);
            expect(ds.data).deep.equal(['1', '2', '3']);
        });
        it('should describe', () => {
            const schema = new ArraySchema({
                split: ':'
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with makeArray option', () => {
        it('should put string into array', async () => {
            const schema = new ArraySchema({
                makeArray: true
            });
            ds.data = '123';
            await validator(schema, ds);
            expect(ds.data).deep.equal(['123']);
        });
        it('should describe', () => {
            const schema = new ArraySchema({
                makeArray: true
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with removeDuplicate option', () => {
        it('should remove duplicate numbers', async () => {
            const schema = new ArraySchema({
                removeDuplicate: true
            });
            ds.data = [1, 2, 4, 1, 9, 1, 3, 4];
            await validator(schema, ds);
            expect(ds.data).deep.equal([1, 2, 4, 9, 3]);
        });
        it('should describe', () => {
            const schema = new ArraySchema({
                removeDuplicate: true
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with filter option', () => {
        it('should remove empty elements', async () => {
            const schema = new ArraySchema({
                filter: true
            });
            ds.data = [1, '', [], {}, 's', ['']];
            await validator(schema, ds);
            expect(ds.data).deep.equal([1, 's', ['']]);
        });
        it('should remove by schema', async () => {
            const schema = new ArraySchema({
                filter: new StringSchema({ trim: true, min: 3 })
            });
            ds.data = [1, 'abc', [], {}, '     s    d', ['']];
            await validator(schema, ds);
            expect(ds.data).deep.equal(['abc', '     s    d']);
        });
        it('should describe empty filter', () => {
            const schema = new ArraySchema({
                filter: true
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe schema filter', () => {
            const schema = new ArraySchema({
                filter: new StringSchema()
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with order option', () => {
        it('should shuffle', async () => {
            const schema = new ArraySchema({
                shuffle: true
            });
            ds.data = [1, 2, 3, 4, 5, 6, 7, 8, 9];
            await validator(schema, ds);
            expect(ds.data).not.equal([1, 2, 3, 4, 5, 6, 7, 8, 9]);
        });
        it('should sort alphabetical', async () => {
            const schema = new ArraySchema({
                sort: true
            });
            ds.data = [3, 2, 11];
            await validator(schema, ds);
            expect(ds.data).not.equal([11, 2, 3]);
        });
        it('should sort numerical', async () => {
            const schema = new ArraySchema({
                sort: 'num'
            });
            ds.data = [3, 2, 11];
            await validator(schema, ds);
            expect(ds.data).not.equal([2, 3, 11]);
        });
        it('should sort reverse numerical', async () => {
            const schema = new ArraySchema({
                sort: 'num',
                reverse: true
            });
            ds.data = [3, 2, 11];
            await validator(schema, ds);
            expect(ds.data).not.equal([11, 3, 2]);
        });
        it('should describe with shuffle', () => {
            const schema = new ArraySchema({
                shuffle: true
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe with sort', () => {
            const schema = new ArraySchema({
                sort: 'num',
                reverse: true
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    // deprecated:
    describe('with items option', () => {
        it('should specify items', async () => {
            const schema = new ArraySchema({
                item: { '*': new StringSchema({ trim: true }) }
            });
            ds.data = ['a', '', '  s  '];
            await validator(schema, ds);
            expect(ds.data).deep.equal(['a', '', 's']);
        });
        it('should fail for other elements', async () => {
            const schema = new ArraySchema({
                item: { '*': new StringSchema({ trim: true }) }
            });
            ds.data = ['a', 1024, '  s  '];
            await fail(schema, ds);
        });
        it('should work with element specific format', async () => {
            const schema = new ArraySchema({
                item: { '*': new StringSchema({ trim: true }), '1': new AnySchema() }
            });
            ds.data = ['a', 1024, '  s  '];
            await validator(schema, ds);
            expect(ds.data).deep.equal(['a', 1024, 's']);
        });
        it('should describe', () => {
            const schema = new ArraySchema({
                item: { '*': new StringSchema() }
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with item option', () => {
        it('should specify item', async () => {
            const schema = new ArraySchema({
                item: { '*': new StringSchema({ trim: true }) }
            });
            ds.data = ['a', '', '  s  '];
            await validator(schema, ds);
            expect(ds.data).deep.equal(['a', '', 's']);
        });
        it('should fail for other elements', async () => {
            const schema = new ArraySchema({
                item: { '*': new StringSchema({ trim: true }) }
            });
            ds.data = ['a', 1024, '  s  '];
            await fail(schema, ds);
        });
        it('should work with element specific format', async () => {
            const schema = new ArraySchema({
                item: { '*': new StringSchema({ trim: true }), '1': new AnySchema() }
            });
            ds.data = ['a', 1024, '  s  '];
            await validator(schema, ds);
            expect(ds.data).deep.equal(['a', 1024, 's']);
        });
        it('should work with element specific (negative index) format', async () => {
            const schema = new ArraySchema({
                item: { '*': new StringSchema({ trim: true }), '-2': new AnySchema() }
            });
            ds.data = ['a', 1024, '  s  '];
            await validator(schema, ds);
            expect(ds.data).deep.equal(['a', 1024, 's']);
        });
        it('should work with range specific format', async () => {
            const schema = new ArraySchema({
                item: { '*': new StringSchema({ trim: true }), '1-2': new AnySchema() }
            });
            ds.data = ['a', 1024, 100, '  s  '];
            await validator(schema, ds);
            expect(ds.data).deep.equal(['a', 1024, 100, 's']);
        });
        it('should describe', () => {
            const schema = new ArraySchema({
                item: { '*': new StringSchema() }
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with unique option', () => {
        it('should fail on duplicate', async () => {
            const schema = new ArraySchema({
                unique: true
            });
            ds.data = [1, 2, 3, 4, 3];
            await fail(schema, ds);
        });
        it('should describe', () => {
            const schema = new ArraySchema({
                unique: true
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with size options', () => {
        it('should work with min', async () => {
            const schema = new ArraySchema({
                min: 3
            });
            ds.data = [1, 2, 3, 4, 3];
            await validator(schema, ds);
            expect(ds.data).deep.equal([1, 2, 3, 4, 3]);
        });
        it('should fail with min', async () => {
            const schema = new ArraySchema({
                min: 3
            });
            ds.data = [1, 2];
            await fail(schema, ds);
        });
        it('should work with max', async () => {
            const schema = new ArraySchema({
                max: 3
            });
            ds.data = [1, 2];
            await validator(schema, ds);
            expect(ds.data).deep.equal([1, 2]);
        });
        it('should fail with max', async () => {
            const schema = new ArraySchema({
                max: 3
            });
            ds.data = [1, 2, 3, 4, 3];
            await fail(schema, ds);
        });
        it('should describe min', () => {
            const schema = new ArraySchema({
                min: 3
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe max', () => {
            const schema = new ArraySchema({
                max: 3
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe range', () => {
            const schema = new ArraySchema({
                min: 3,
                max: 6
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe exact length', () => {
            const schema = new ArraySchema({
                min: 3,
                max: 3
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with allow option', () => {
        it('should work with strings', async () => {
            const schema = new ArraySchema({
                allow: [new StringSchema()]
            });
            ds.data = ['1', '2', '3', '4'];
            await validator(schema, ds);
            expect(ds.data).deep.equal(['1', '2', '3', '4']);
        });
        it('should fail on number', async () => {
            const schema = new ArraySchema({
                allow: [new StringSchema()]
            });
            ds.data = ['1', '2', '3', '4', 3];
            await fail(schema, ds);
        });
        it('should describe', () => {
            const schema = new ArraySchema({
                allow: [new StringSchema()]
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with disallow option', () => {
        it('should work with strings', async () => {
            const schema = new ArraySchema({
                disallow: [new StringSchema()]
            });
            ds.data = [1, 2, 3, 4];
            await validator(schema, ds);
            expect(ds.data).deep.equal([1, 2, 3, 4]);
        });
        it('should fail on number', async () => {
            const schema = new ArraySchema({
                disallow: [new StringSchema()]
            });
            ds.data = [1, 2, 3, 4, '3'];
            await fail(schema, ds);
        });
        it('should describe', () => {
            const schema = new ArraySchema({
                disallow: [new StringSchema()]
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
});
