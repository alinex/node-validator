import { expect } from 'chai';
import DataStore from '@alinex/datastore';
import { validator, fail, describer } from './helper';

import LogicSchema from '../../../src/type/logic';
import { StringSchema, NumberSchema } from '../../../src/schema';

describe('logic', () => {
    let ds = new DataStore();
    describe('without definition', () => {
        const schema = new LogicSchema({
            title: 'logic',
            check: new StringSchema()
        });
        it('should validate string', async () => {
            ds.data = 'example';
            await validator(schema, ds);
            expect(ds.data).deep.equal('example');
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('should check one schema', () => {
        const schema = new LogicSchema({
            check: new StringSchema()
        });
        it('should validate string', async () => {
            ds.data = 'example';
            await validator(schema, ds);
            expect(ds.data).deep.equal('example');
        });
        it('should fail on number', async () => {
            ds.data = 6;
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('should check multiple with AND', () => {
        const schema = new LogicSchema({
            check: [
                new StringSchema({ replace: [{ match: 'one', replace: '1' }] }),
                new NumberSchema()
            ]
        });
        it('should validate string', async () => {
            ds.data = 'one';
            await validator(schema, ds);
            expect(ds.data).deep.equal(1);
        });
        it('should fail on unreplaced string', async () => {
            ds.data = 'one and two';
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('should check multiple with OR', () => {
        const schema = new LogicSchema({
            operator: 'or',
            check: [
                new NumberSchema(),
                new StringSchema({ replace: [{ match: 'one', replace: '1' }] })
            ]
        });
        it('should validate string', async () => {
            ds.data = 'one';
            await validator(schema, ds);
            expect(ds.data).deep.equal('1');
        });
        it('should validate number', async () => {
            ds.data = '1';
            await validator(schema, ds);
            expect(ds.data).deep.equal(1);
        });
        it('should fail on array', async () => {
            ds.data = { number: 5 };
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('should work with if + check', () => {
        const schema = new LogicSchema({
            if: new StringSchema({ allow: [/age/] }),
            check: new NumberSchema({ sanitize: true })
        });
        it('should validate string', async () => {
            ds.data = 'age: 26';
            await validator(schema, ds);
            expect(ds.data).deep.equal(26);
        });
        it('should fail on number', async () => {
            ds.data = '26';
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('should work with if + else', () => {
        const schema = new LogicSchema({
            if: new StringSchema({ allow: [/age/] }),
            else: new StringSchema({ replace: [{ match: 'child', replace: '6' }] })
        });
        it('should validate string', async () => {
            ds.data = 'child';
            await validator(schema, ds);
            expect(ds.data).deep.equal('6');
        });
        it('should fail on number', async () => {
            ds.data = 'age: 26';
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
});
