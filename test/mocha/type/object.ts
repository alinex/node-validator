import { expect } from 'chai';
import DataStore from '@alinex/datastore';
import { validator, fail, describer } from './helper';

import ObjectSchema from '../../../src/type/object';
import StringSchema from '../../../src/type/string';
import AnySchema from '../../../src/type/any';
import NumberSchema from '../../../src/type/number';
import BooleanSchema from '../../../src/type/boolean';

describe('object', () => {
  let ds = new DataStore();
  describe('without options', () => {
    const schema = new ObjectSchema({
      title: 'object'
    });
    it('should validate object', async () => {
      ds.data = { name: 'example' };
      await validator(schema, ds);
      expect(ds.data).deep.equal({ name: 'example' });
    });
    it('should validate empty object', async () => {
      ds.data = {};
      await validator(schema, ds);
      expect(ds.data).deep.equal({});
    });
    it('should fail for string', async () => {
      ds.data = 'example';
      await fail(schema, ds);
    });
    it('should describe', () => {
      expect(describer(schema)).to.be.a('string');
    });
  });
  describe('with default', () => {
    const schema = new ObjectSchema({
      title: 'object',
      default: { host: 'localhost' }
    });
    it('should validate object', async () => {
      ds.data = {};
      await validator(schema, ds);
      expect(ds.data).deep.equal({ host: 'localhost' });
    });
    it('should describe', () => {
      expect(describer(schema)).to.be.a('string');
    });
  });
  describe('with flatten option', () => {
    it('should use default separator', async () => {
      const schema = new ObjectSchema({
        flatten: true
      });
      ds.data = { address: { city: 'Stuttgart', street: 'Königsstraße' } };
      await validator(schema, ds);
      expect(ds.data).deep.equal({
        'address.city': 'Stuttgart',
        'address.street': 'Königsstraße'
      });
    });
    it('should use specified separator', async () => {
      const schema = new ObjectSchema({
        flatten: '::'
      });
      ds.data = { address: { city: 'Stuttgart', street: 'Königsstraße' } };
      await validator(schema, ds);
      expect(ds.data).deep.equal({
        'address::city': 'Stuttgart',
        'address::street': 'Königsstraße'
      });
    });
    it('should describe', () => {
      const schema = new ObjectSchema({
        flatten: true
      });
      expect(describer(schema)).to.be.a('string');
    });
  });
  describe('with rearrange option', () => {
    it('should use rename', async () => {
      const schema = new ObjectSchema({
        rearrange: [{ key: 'capital', moveTo: 'city' }]
      });
      ds.data = {
        capital: 'Berlin',
        street: 'Wilhelmstraße'
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({ city: 'Berlin', street: 'Wilhelmstraße' });
    });
    it('should use copy', async () => {
      const schema = new ObjectSchema({
        rearrange: [{ key: 'capital', copyTo: 'city' }]
      });
      ds.data = {
        capital: 'Berlin',
        street: 'Wilhelmstraße'
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({
        capital: 'Berlin',
        city: 'Berlin',
        street: 'Wilhelmstraße'
      });
    });
    it('should use rename with overwrite', async () => {
      const schema = new ObjectSchema({
        rearrange: [{ key: 'capital', moveTo: 'city', overwrite: true }]
      });
      ds.data = {
        capital: 'Berlin',
        city: 'Stuttgart',
        street: 'Wilhelmstraße'
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({ city: 'Berlin', street: 'Wilhelmstraße' });
    });
    it('should use rename without overwrite', async () => {
      const schema = new ObjectSchema({
        rearrange: [{ key: 'capital', moveTo: 'city' }]
      });
      ds.data = {
        capital: 'Berlin',
        city: 'Stuttgart',
        street: 'Wilhelmstraße'
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({
        capital: 'Berlin',
        city: 'Stuttgart',
        street: 'Wilhelmstraße'
      });
    });
    it('should describe', () => {
      const schema = new ObjectSchema({
        rearrange: [{ key: 'capital', moveTo: 'city', overwrite: true }]
      });
      expect(describer(schema)).to.be.a('string');
    });
  });
  describe('with unflatten option', () => {
    it('should use default separator', async () => {
      const schema = new ObjectSchema({
        unflatten: true
      });
      ds.data = {
        'address.city': 'Stuttgart',
        'address.street': 'Königsstraße'
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({ address: { city: 'Stuttgart', street: 'Königsstraße' } });
    });
    it('should use specified separator', async () => {
      const schema = new ObjectSchema({
        unflatten: '::'
      });
      ds.data = {
        'address::city': 'Stuttgart',
        'address::street': 'Königsstraße'
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({ address: { city: 'Stuttgart', street: 'Königsstraße' } });
    });
    it('should describe', () => {
      const schema = new ObjectSchema({
        unflatten: true
      });
      expect(describer(schema)).to.be.a('string');
    });
  });
  // deprecated:
  describe('with item option', () => {
    it('should use default schema', async () => {
      const schema = new ObjectSchema({
        item: { '*': new StringSchema() }
      });
      ds.data = {
        city: 'Stuttgart',
        street: 'Königsstraße'
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({ city: 'Stuttgart', street: 'Königsstraße' });
    });
    it('should use specific key schema', async () => {
      const schema = new ObjectSchema({
        item: { '*': new StringSchema(), number: new NumberSchema() }
      });
      ds.data = {
        city: 'Stuttgart',
        street: 'Königsstraße',
        number: 6
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({ city: 'Stuttgart', street: 'Königsstraße', number: 6 });
    });
    it('should use wildcard with denyUndefined', async () => {
      const schema = new ObjectSchema({
        item: { '*': new StringSchema(), number: new NumberSchema() },
        denyUndefined: true
      });
      ds.data = {
        city: 'Stuttgart',
        street: 'Königsstraße',
        number: 6
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({ city: 'Stuttgart', street: 'Königsstraße', number: 6 });
    });
    it('should use pattern key schema', async () => {
      const schema = new ObjectSchema({
        item: {
          '*': new StringSchema(),
          '/^flag-/': new BooleanSchema({ tolerant: true })
        }
      });
      ds.data = {
        city: 'Stuttgart',
        street: 'Königsstraße',
        'flag-favorite': 1
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({
        city: 'Stuttgart',
        street: 'Königsstraße',
        'flag-favorite': true
      });
    });
    it('should describe', () => {
      const schema = new ObjectSchema({
        item: {
          '*': new StringSchema(),
          '/^flag-/': new BooleanSchema({ tolerant: true })
        }
      });
      expect(describer(schema)).to.be.a('string');
    });
  });
  describe('with item option', () => {
    it('should use default schema', async () => {
      const schema = new ObjectSchema({
        item: { '*': new StringSchema() }
      });
      ds.data = {
        city: 'Stuttgart',
        street: 'Königsstraße'
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({ city: 'Stuttgart', street: 'Königsstraße' });
    });
    it('should use specific key schema', async () => {
      const schema = new ObjectSchema({
        item: { '*': new StringSchema(), number: new AnySchema() }
      });
      ds.data = {
        city: 'Stuttgart',
        street: 'Königsstraße',
        number: 6
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({ city: 'Stuttgart', street: 'Königsstraße', number: 6 });
    });
    it('should use pattern key schema', async () => {
      const schema = new ObjectSchema({
        item: {
          '*': new StringSchema(),
          number: new AnySchema(),
          '/^flag-/': new BooleanSchema({ tolerant: true })
        }
      });
      ds.data = {
        city: 'Stuttgart',
        street: 'Königsstraße',
        'flag-favorite': 1
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({
        city: 'Stuttgart',
        street: 'Königsstraße',
        'flag-favorite': true
      });
    });
    it('should describe', () => {
      const schema = new ObjectSchema({
        item: {
          '*': new StringSchema(),
          number: new AnySchema(),
          '/^flag-/': new BooleanSchema({ tolerant: true })
        }
      });
      expect(describer(schema)).to.be.a('string');
    });
  });
  describe('with removeUndefined option', () => {
    it('should use specific key schema', async () => {
      const schema = new ObjectSchema({
        item: { street: new StringSchema(), number: new AnySchema() },
        removeUndefined: true
      });
      ds.data = {
        city: 'Stuttgart',
        street: 'Königsstraße',
        number: 6
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({ street: 'Königsstraße', number: 6 });
    });
    it('should use pattern key schema', async () => {
      const schema = new ObjectSchema({
        item: { city: new StringSchema(), '/^flag-/': new BooleanSchema({ tolerant: true }) },
        removeUndefined: true
      });
      ds.data = {
        city: 'Stuttgart',
        street: 'Königsstraße',
        'flag-favorite': 1
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({
        city: 'Stuttgart',
        'flag-favorite': true
      });
    });
    it('should describe', () => {
      const schema = new ObjectSchema({
        item: { number: new AnySchema(), '/^flag-/': new BooleanSchema({ tolerant: true }) },
        removeUndefined: true
      });
      expect(describer(schema)).to.be.a('string');
    });
  });
  describe('with denyUndefined option', () => {
    it('should use specific key schema', async () => {
      const schema = new ObjectSchema({
        item: { street: new StringSchema(), number: new AnySchema() },
        denyUndefined: true
      });
      ds.data = {
        city: 'Stuttgart',
        street: 'Königsstraße',
        number: 6
      };
      await fail(schema, ds);
    });
    it('should use pattern key schema', async () => {
      const schema = new ObjectSchema({
        item: { city: new StringSchema(), '/^flag-/': new BooleanSchema({ tolerant: true }) },
        denyUndefined: true
      });
      ds.data = {
        city: 'Stuttgart',
        'flag-favorite': 1
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({
        city: 'Stuttgart',
        'flag-favorite': true
      });
    });
    it('should describe', () => {
      const schema = new ObjectSchema({
        item: { number: new AnySchema(), '/^flag-/': new BooleanSchema({ tolerant: true }) },
        denyUndefined: true
      });
      expect(describer(schema)).to.be.a('string');
    });
  });
  describe('with mandatory option', () => {
    it('should fail on missing keys', async () => {
      const schema = new ObjectSchema({
        mandatory: ['street', 'number']
      });
      ds.data = {
        city: 'Stuttgart',
        street: 'Königsstraße'
      };
      await fail(schema, ds);
    });
    it('should work with default in schema', async () => {
      const schema = new ObjectSchema({
        item: {
          country: new StringSchema({ default: 'germany' })
        },
        mandatory: ['city', 'country']
      });
      ds.data = {
        city: 'Stuttgart'
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({
        city: 'Stuttgart',
        country: 'germany'
      });
    });
    it('should work on pattern key', async () => {
      const schema = new ObjectSchema({
        mandatory: ['city', /^flag-/]
      });
      ds.data = {
        city: 'Stuttgart',
        'flag-favorite': 1
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({
        city: 'Stuttgart',
        'flag-favorite': 1
      });
    });
    it('should describe', () => {
      const schema = new ObjectSchema({
        mandatory: ['city', /^flag-/]
      });
      expect(describer(schema)).to.be.a('string');
    });
  });
  describe('with mandatory all option', () => {
    it('should fail on missing keys', async () => {
      const schema = new ObjectSchema({
        item: { city: new StringSchema(), country: new StringSchema() },
        mandatory: true
      });
      ds.data = {
        city: 'Stuttgart',
        street: 'Königsstraße'
      };
      await fail(schema, ds);
    });
    it('should succeed', async () => {
      const schema = new ObjectSchema({
        item: { city: new StringSchema(), street: new StringSchema() },
        mandatory: true
      });
      ds.data = {
        city: 'Stuttgart',
        street: 'Königsstraße'
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({
        city: 'Stuttgart',
        street: 'Königsstraße'
      });
    });
    it('should describe', () => {
      const schema = new ObjectSchema({
        item: { city: new StringSchema(), street: new StringSchema() },
        mandatory: true
      });
      expect(describer(schema)).to.be.a('string');
    });
  });
  describe('with forbidden option', () => {
    it('should fail on disallowed keys', async () => {
      const schema = new ObjectSchema({
        forbidden: ['street']
      });
      ds.data = {
        city: 'Stuttgart',
        street: 'Königsstraße'
      };
      await fail(schema, ds);
    });
    it('should work on pattern key', async () => {
      const schema = new ObjectSchema({
        forbidden: ['wheels', /^flag-/]
      });
      ds.data = {
        city: 'Stuttgart',
        street: 'Königsstraße'
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({
        city: 'Stuttgart',
        street: 'Königsstraße'
      });
    });
    it('should describe', () => {
      const schema = new ObjectSchema({
        forbidden: ['city', /^flag-/]
      });
      expect(describer(schema)).to.be.a('string');
    });
  });
  describe('with combination option', () => {
    it('should succeed with and', async () => {
      const schema = new ObjectSchema({
        combination: [{ and: ['city', 'street'] }]
      });
      ds.data = {
        city: 'Berlin',
        street: 'Wilhelmstraße'
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({ city: 'Berlin', street: 'Wilhelmstraße' });
    });
    it('should fail with and', async () => {
      const schema = new ObjectSchema({
        combination: [{ and: ['city', 'street', 'number'] }]
      });
      ds.data = {
        city: 'Berlin',
        street: 'Wilhelmstraße'
      };
      await fail(schema, ds);
    });
    it('should succeed with nand', async () => {
      const schema = new ObjectSchema({
        combination: [{ nand: ['city', 'capital'] }]
      });
      ds.data = {
        city: 'Berlin',
        street: 'Wilhelmstraße'
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({ city: 'Berlin', street: 'Wilhelmstraße' });
    });
    it('should fail with nand', async () => {
      const schema = new ObjectSchema({
        combination: [{ nand: ['city', 'capital'] }]
      });
      ds.data = {
        capital: 'Berlin',
        city: 'Stuttgart',
        street: 'Wilhelmstraße'
      };
      await fail(schema, ds);
    });
    it('should succeed with or', async () => {
      const schema = new ObjectSchema({
        combination: [{ or: ['city', 'capital'] }]
      });
      ds.data = {
        city: 'Berlin',
        street: 'Wilhelmstraße'
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({ city: 'Berlin', street: 'Wilhelmstraße' });
    });
    it('should fail with or', async () => {
      const schema = new ObjectSchema({
        combination: [{ or: ['city', 'capital'] }]
      });
      ds.data = {
        street: 'Wilhelmstraße'
      };
      await fail(schema, ds);
    });
    it('should succeed with xor', async () => {
      const schema = new ObjectSchema({
        combination: [{ xor: ['city', 'capital'] }]
      });
      ds.data = {
        city: 'Berlin',
        street: 'Wilhelmstraße'
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({ city: 'Berlin', street: 'Wilhelmstraße' });
    });
    it('should fail with xor', async () => {
      const schema = new ObjectSchema({
        combination: [{ xor: ['city', 'capital'] }]
      });
      ds.data = {
        capital: 'Berlin',
        city: 'Stuttgart',
        street: 'Wilhelmstraße'
      };
      await fail(schema, ds);
    });
    it('should succeed with with', async () => {
      const schema = new ObjectSchema({
        combination: [{ key: 'street', with: ['number'] }]
      });
      ds.data = {
        city: 'Berlin',
        street: 'Wilhelmstraße',
        number: 5
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({ city: 'Berlin', street: 'Wilhelmstraße', number: 5 });
    });
    it('should fail with with', async () => {
      const schema = new ObjectSchema({
        combination: [{ key: 'street', with: ['number'] }]
      });
      ds.data = {
        city: 'Berlin',
        street: 'Wilhelmstraße'
      };
      await fail(schema, ds);
    });
    it('should succeed with without', async () => {
      const schema = new ObjectSchema({
        combination: [{ key: 'street', without: ['postbox'] }]
      });
      ds.data = {
        city: 'Berlin',
        street: 'Wilhelmstraße',
        number: 5
      };
      await validator(schema, ds);
      expect(ds.data).deep.equal({ city: 'Berlin', street: 'Wilhelmstraße', number: 5 });
    });
    it('should fail with without', async () => {
      const schema = new ObjectSchema({
        combination: [{ key: 'street', without: ['postbox'] }]
      });
      ds.data = {
        city: 'Berlin',
        street: 'Wilhelmstraße',
        postbox: 56734
      };
      await fail(schema, ds);
    });
    it('should describe', () => {
      const schema = new ObjectSchema({
        combination: [
          { and: ['color', 'shape'] },
          { nand: ['street', 'postbox'] },
          { or: ['length', 'weight', 'volume'] },
          { xor: ['wheels', 'rotor'] },
          { key: 'type', with: ['category'] },
          { key: 'type', without: ['city'] }
        ]
      });
      expect(describer(schema)).to.be.a('string');
    });
  });
  describe('with size options', () => {
    it('should work with min', async () => {
      const schema = new ObjectSchema({
        min: 3
      });
      ds.data = { a: 1, b: 2, c: 3 };
      await validator(schema, ds);
      expect(ds.data).deep.equal({ a: 1, b: 2, c: 3 });
    });
    it('should fail with min', async () => {
      const schema = new ObjectSchema({
        min: 3
      });
      ds.data = { a: 1, b: 2 };
      await fail(schema, ds);
    });
    it('should work with max', async () => {
      const schema = new ObjectSchema({
        max: 3
      });
      ds.data = { a: 1, b: 2 };
      await validator(schema, ds);
      expect(ds.data).deep.equal({ a: 1, b: 2 });
    });
    it('should fail with max', async () => {
      const schema = new ObjectSchema({
        max: 3
      });
      ds.data = { a: 1, b: 2, c: 3, d: 4, e: 5 };
      await fail(schema, ds);
    });
    it('should describe min', () => {
      const schema = new ObjectSchema({
        min: 3
      });
      expect(describer(schema)).to.be.a('string');
    });
    it('should describe max', () => {
      const schema = new ObjectSchema({
        max: 3
      });
      expect(describer(schema)).to.be.a('string');
    });
    it('should describe range', () => {
      const schema = new ObjectSchema({
        min: 3,
        max: 6
      });
      expect(describer(schema)).to.be.a('string');
    });
    it('should describe exact length', () => {
      const schema = new ObjectSchema({
        min: 3,
        max: 3
      });
      expect(describer(schema)).to.be.a('string');
    });
  });
});
