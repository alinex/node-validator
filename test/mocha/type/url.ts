import { expect } from 'chai';
import DataStore from '@alinex/datastore';

import { validator, fail, describer } from './helper';
import URLSchema from '../../../src/type/url';

describe('URL', () => {
    let ds = new DataStore();
    describe('without definition', () => {
        const schema = new URLSchema({
            title: 'url'
        });
        it('should validate', async () => {
            ds.data = 'http://alinex.de';
            await validator(schema, ds);
            expect(ds.data).deep.equal('http://alinex.de/');
        });
        it('should fail without protocol', async () => {
            ds.data = 'alinex.de';
            await fail(schema, ds);
        });
        it('should fail with number', async () => {
            ds.data = 5;
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with resolve option', () => {
        const schema = new URLSchema({
            resolve: 'http://alinex.de'
        });
        it('should validate', async () => {
            ds.data = '/index.html';
            await validator(schema, ds);
            expect(ds.data).deep.equal('http://alinex.de/index.html');
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with exists option', function() {
        this.timeout(10000);
        const schema = new URLSchema({
            exists: true
        });
        it('should validate', async () => {
            ds.data = 'http://alinex.de';
            await validator(schema, ds);
            expect(ds.data).deep.equal('http://alinex.de/');
        });
        it('should fail on invalid URL', async () => {
            ds.data = 'http://alinex.de/not-existing-url';
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with format option', () => {
        const schema = new URLSchema({
            format: 'object'
        });
        it('should validate', async () => {
            ds.data = 'http://alinex.de';
            await validator(schema, ds);
            expect(ds.data.href).deep.equal('http://alinex.de/');
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
});
