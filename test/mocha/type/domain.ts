import { expect } from 'chai';
import DataStore from '@alinex/datastore';

import { validator, fail, describer } from './helper';
import DomainSchema from '../../../src/type/domain';

describe('domain', () => {
    let ds = new DataStore();
    describe('without definition', () => {
        const schema = new DomainSchema({
            title: 'domain'
        });
        it('should validate', async () => {
            ds.data = 'alinex.de';
            await validator(schema, ds);
            expect(ds.data).deep.equal('alinex.de');
        });
        it('should fail on IPv4', async () => {
            ds.data = '192.168.1.15';
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with sanitize option', () => {
        const schema = new DomainSchema({
            sanitize: true
        });
        it('should extract from url', async () => {
            ds.data = 'http://alinex.de/test';
            await validator(schema, ds);
            expect(ds.data).deep.equal('alinex.de');
        });
        it('should extract from email', async () => {
            ds.data = 'info@alinex.de';
            await validator(schema, ds);
            expect(ds.data).deep.equal('alinex.de');
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with min/max option', () => {
        it('should succeed with min level', async () => {
            const schema = new DomainSchema({
                min: 2
            });
            ds.data = 'alinex.de';
            await validator(schema, ds);
            expect(ds.data).deep.equal('alinex.de');
        });
        it('should fail with min level', async () => {
            const schema = new DomainSchema({
                min: 2
            });
            ds.data = 'com';
            await fail(schema, ds);
        });
        it('should succeed with max level', async () => {
            const schema = new DomainSchema({
                max: 2
            });
            ds.data = 'alinex.de';
            await validator(schema, ds);
            expect(ds.data).deep.equal('alinex.de');
        });
        it('should fail with max level', async () => {
            const schema = new DomainSchema({
                max: 2
            });
            ds.data = 'demo.alinex.de';
            await fail(schema, ds);
        });
        it('should describe min', () => {
            const schema = new DomainSchema({
                min: 2
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe max', () => {
            const schema = new DomainSchema({
                max: 2
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe exact', () => {
            const schema = new DomainSchema({
                min: 2,
                max: 2
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe range', () => {
            const schema = new DomainSchema({
                min: 2,
                max: 3
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with allow/disallow option', () => {
        it('should allow domain', async () => {
            const schema = new DomainSchema({
                allow: ['de']
            });
            ds.data = 'alinex.de';
            await validator(schema, ds);
            expect(ds.data).deep.equal('alinex.de');
        });
        it('should fail on allow domain', async () => {
            const schema = new DomainSchema({
                allow: ['de']
            });
            ds.data = 'alinex.com';
            await fail(schema, ds);
        });
        it('should disallow private tld', async () => {
            const schema = new DomainSchema({
                disallow: ['private TLD']
            });
            ds.data = 'alinex.local';
            await validator(schema, ds);
            expect(ds.data).deep.equal('alinex.local');
        });
        it('should describe', () => {
            const schema = new DomainSchema({
                allow: ['de'],
                disallow: ['private TLD']
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with allow/disallowCountry option', () => {
        it('should allow germany', async () => {
            const schema = new DomainSchema({
                allowCountry: ['DE']
            });
            ds.data = 'alinex.de';
            await validator(schema, ds);
            expect(ds.data).deep.equal('alinex.de');
        });
        it('should fail for not germany', async () => {
            const schema = new DomainSchema({
                allowCountry: ['DE']
            });
            ds.data = 'google.com';
            await fail(schema, ds);
        });
        it('should describe', () => {
            const schema = new DomainSchema({
                allowCountry: ['DE']
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with registered option', () => {
        it('should succeed any record', async () => {
            const schema = new DomainSchema({
                registered: true
            });
            ds.data = 'alinex.de';
            await validator(schema, ds);
            expect(ds.data).deep.equal('alinex.de');
        });
        it('should fail for no record', async () => {
            const schema = new DomainSchema({
                registered: true
            });
            ds.data = 'mein.goŕilla';
            await fail(schema, ds);
        });
        it('should succeed specific record', async () => {
            const schema = new DomainSchema({
                registered: ['CNAME']
            });
            ds.data = 'www-test.onleihe.de';
            await validator(schema, ds);
            expect(ds.data).deep.equal('www-test.onleihe.de');
        });
        it('should fail for specific record', async () => {
            const schema = new DomainSchema({
                registered: ['CNAME']
            });
            ds.data = 'alinex.de';
            await fail(schema, ds);
        });
        it('should describe', () => {
            const schema = new DomainSchema({
                registered: true
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with punycode option', () => {
        const schema = new DomainSchema({
            punycode: true
        });
        it('should work with normal domain', async () => {
            ds.data = 'alinex.de';
            await validator(schema, ds);
            expect(ds.data).deep.equal('alinex.de');
        });
        it('should work with international domain', async () => {
            ds.data = 'käse.de';
            await validator(schema, ds);
            expect(ds.data).deep.equal('xn--kse-qla.de');
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with resolve option', () => {
        const schema = new DomainSchema({
            resolve: true
        });
        it('should return ip', async () => {
            ds.data = 'alinex.de';
            await validator(schema, ds);
            expect(ds.data).deep.equal('185.26.156.226');
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
});
