import { expect } from 'chai';
import DataStore from '@alinex/datastore';
import { validator, fail, describer } from './helper';

import BooleanSchema from '../../../src/type/boolean';

describe('boolean', () => {
    let ds = new DataStore();
    describe('without definition', () => {
        const schema = new BooleanSchema({
            title: 'boolean'
        });
        it('should detect true', async () => {
            ds.data = true;
            await validator(schema, ds);
            expect(ds.data).deep.equal(true);
        });
        it('should detect false', async () => {
            ds.data = false;
            await validator(schema, ds);
            expect(ds.data).deep.equal(false);
        });
        it('should fail on others', async () => {
            ds.data = 1;
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with truthy option', () => {
        const schema = new BooleanSchema({
            truthy: ['on']
        });
        it('should detect true', async () => {
            ds.data = 'on';
            await validator(schema, ds);
            expect(ds.data).deep.equal(true);
        });
        it('should fail on others', async () => {
            ds.data = 'true';
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with falsy option', () => {
        const schema = new BooleanSchema({
            falsy: ['off']
        });
        it('should detect false', async () => {
            ds.data = 'off';
            await validator(schema, ds);
            expect(ds.data).deep.equal(false);
        });
        it('should fail on others', async () => {
            ds.data = 'false';
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with insensitive option', () => {
        const schema = new BooleanSchema({
            truthy: ['on'],
            insensitive: true
        });
        it('should detect true', async () => {
            ds.data = 'ON';
            await validator(schema, ds);
            expect(ds.data).deep.equal(true);
        });
        it('should fail on others', async () => {
            ds.data = 'true';
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with tolerant option', () => {
        const schema = new BooleanSchema({
            tolerant: true
        });
        it('should detect true', async () => {
            ds.data = 'ON';
            await validator(schema, ds);
            expect(ds.data).deep.equal(true);
        });
        it('should fail on others', async () => {
            ds.data = '9';
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
      describe('with value option', () => {
        const schema = new BooleanSchema({
          value: true
        });
        it('should check against value', async () => {
          ds.data = true;
          await validator(schema, ds);
        });
        it('should fail on other value', async () => {
          ds.data = false;
          await fail(schema, ds);
        });
        it('should describe', () => {
          expect(describer(schema)).to.be.a('string');
        });
      });
    });
    describe('with format option', () => {
        const schema = new BooleanSchema({
            format: ['X', '-']
        });
        it('should format true', async () => {
            ds.data = true;
            await validator(schema, ds);
            expect(ds.data).deep.equal('X');
        });
        it('should format false', async () => {
            ds.data = false;
            await validator(schema, ds);
            expect(ds.data).deep.equal('-');
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
});
