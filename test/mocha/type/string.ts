import { expect } from 'chai';
import DataStore from '@alinex/datastore';
import { validator, fail, describer } from './helper';

import StringSchema from '../../../src/type/string';
import Reference from '../../../src/reference';

describe('string', () => {
    let ds = new DataStore();
    describe('without definition', () => {
        const schema = new StringSchema({
            title: 'string'
        });
        it('should validate string', async () => {
            ds.data = 'example';
            await validator(schema, ds);
            expect(ds.data).deep.equal('example');
        });
        it('should allow empty string', async () => {
            ds.data = '';
            await validator(schema, ds);
            expect(ds.data).deep.equal('');
        });
        it('should fail on boolean', async () => {
            ds.data = true;
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });

    describe('with option default', () => {
        const schema = new StringSchema({
            default: 'car'
        });
        it('should use default', async () => {
            ds.data = '';
            await validator(schema, ds);
            expect(ds.data).deep.equal('car');
        });
        it('should use reference element', async () => {
            const schema = new StringSchema({
                default: new Reference('data:context')
            });
            ds.data = '';
            let context = 'banana';
            await validator(schema, ds, context);
            expect(ds.data).deep.equal('banana');
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe with reference', () => {
            const schema = new StringSchema({
                default: new Reference('data:context')
            });
            expect(describer(schema)).to.be.a('string');
        });
    });

    describe('with option makeString', () => {
        const schema = new StringSchema({
            title: 'string',
            makeString: true
        });
        it('should convert number', async () => {
            ds.data = 12;
            await validator(schema, ds);
            expect(ds.data).deep.equal('12');
        });
        it('should convert array', async () => {
            ds.data = [1, 2, 3];
            await validator(schema, ds);
            expect(ds.data).deep.equal('1,2,3');
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });

    describe('with option trim', () => {
        it('should trim whitespace', async () => {
            const schema = new StringSchema({
                trim: true
            });
            ds.data = '   example\n';
            await validator(schema, ds);
            expect(ds.data).deep.equal('example');
        });
        it('should ltrim whitespace', async () => {
            const schema = new StringSchema({
                trim: 'left'
            });
            ds.data = '   example\n';
            await validator(schema, ds);
            expect(ds.data).deep.equal('example\n');
        });
        it('should rtrim whitespace', async () => {
            const schema = new StringSchema({
                trim: 'right'
            });
            ds.data = '   example\n';
            await validator(schema, ds);
            expect(ds.data).deep.equal('   example');
        });
        it('should describe', () => {
            const schema = new StringSchema({
                trim: true
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe', () => {
            const schema = new StringSchema({
                trim: 'left'
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe', () => {
            const schema = new StringSchema({
                trim: 'right'
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with option replace', () => {
        it('should replace with string', async () => {
            const schema = new StringSchema({
                replace: [{ match: '1', replace: 'one', hint: 'to string' }]
            });
            ds.data = '1 2 1';
            await validator(schema, ds);
            expect(ds.data).deep.equal('one 2 1');
        });
        it('should replace with regexp', async () => {
            const schema = new StringSchema({
                replace: [{ match: /1/g, replace: 'one', hint: 'to string' }]
            });
            ds.data = '1 2 1';
            await validator(schema, ds);
            expect(ds.data).deep.equal('one 2 one');
        });
        it('should describe', () => {
            const schema = new StringSchema({
                replace: [{ match: '1', replace: 'one', hint: 'to string' }]
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with character options', () => {
        it('should allow only hexadecimal digits', async () => {
            const schema = new StringSchema({
                hex: true
            });
            ds.data = '25bdff';
            await validator(schema, ds);
            expect(ds.data).deep.equal('25bdff');
        });
        it('should fail on no hexadecimal digits', async () => {
            const schema = new StringSchema({
                hex: true
            });
            ds.data = '25b dff';
            await fail(schema, ds);
        });
        it('should allow only alpha numerical strings', async () => {
            const schema = new StringSchema({
                alphaNum: true
            });
            ds.data = '25bdff';
            await validator(schema, ds);
            expect(ds.data).deep.equal('25bdff');
        });
        it('should fail on no alpha numerical characters', async () => {
            const schema = new StringSchema({
                alphaNum: true
            });
            ds.data = '25b dff';
            await fail(schema, ds);
        });
        it('should not allow HTML tags', async () => {
            const schema = new StringSchema({
                noHTML: true
            });
            ds.data = 'I am bold.';
            await validator(schema, ds);
            expect(ds.data).deep.equal('I am bold.');
        });
        it('should fail on HTML tags', async () => {
            const schema = new StringSchema({
                noHTML: true
            });
            ds.data = 'I am <b>bold</b>.';
            await fail(schema, ds);
        });
        it('should only allow specific HTML tags', async () => {
            const schema = new StringSchema({
                noHTML: ['b']
            });
            ds.data = 'I am <b>bold</b>.';
            await validator(schema, ds);
            expect(ds.data).deep.equal('I am <b>bold</b>.');
        });
        it('should fail on disallowed HTML tags', async () => {
            const schema = new StringSchema({
                noHTML: ['b']
            });
            ds.data = '<p>I am <b>bold</b>.</p>';
            await fail(schema, ds);
        });
        it('should sanitize non hexadecimal digits', async () => {
            const schema = new StringSchema({
                hex: true,
                stripDisallowed: true
            });
            ds.data = '25b dff';
            await validator(schema, ds);
            expect(ds.data).deep.equal('25bdff');
        });
        it('should sanitize some html tags', async () => {
            const schema = new StringSchema({
                noHTML: ['b'],
                stripDisallowed: true
            });
            ds.data = '<p>I am <b>bold</b>.</p>';
            await validator(schema, ds);
            expect(ds.data).deep.equal('I am <b>bold</b>.');
        });
        it('should describe strip disallowed hex', () => {
            const schema = new StringSchema({
                hex: true,
                stripDisallowed: true
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe strip disallowed alpha numeric', () => {
            const schema = new StringSchema({
                alphaNum: true,
                stripDisallowed: true
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe no HTML', () => {
            const schema = new StringSchema({
                noHTML: true
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe only one HTML tag allowed', () => {
            const schema = new StringSchema({
                noHTML: ['b']
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe multiple HTML tags allowed', () => {
            const schema = new StringSchema({
                noHTML: ['b', 'i', 'br']
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with case options', () => {
        it('should use lower case on string', async () => {
            const schema = new StringSchema({
                lowerCase: true
            });
            ds.data = 'eXaMpLe';
            await validator(schema, ds);
            expect(ds.data).deep.equal('example');
        });
        it('should use upper case on string', async () => {
            const schema = new StringSchema({
                upperCase: true
            });
            ds.data = 'eXaMpLe';
            await validator(schema, ds);
            expect(ds.data).deep.equal('EXAMPLE');
        });
        it('should use mixed case on string', async () => {
            const schema = new StringSchema({
                lowerCase: true,
                upperCase: 'first'
            });
            ds.data = 'eXaMpLe';
            await validator(schema, ds);
            expect(ds.data).deep.equal('Example');
        });
        it('should describe', () => {
            const schema = new StringSchema({
                lowerCase: true,
                upperCase: 'first'
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with length options', () => {
        it('should left pad to min', async () => {
            const schema = new StringSchema({
                min: 9,
                pad: { side: 'left', characters: '. ' }
            });
            ds.data = '1234';
            await validator(schema, ds);
            expect(ds.data).deep.equal('.    1234');
        });
        it('should right pad to min', async () => {
            const schema = new StringSchema({
                min: 9,
                pad: { side: 'right', characters: '. ' }
            });
            ds.data = '1234';
            await validator(schema, ds);
            expect(ds.data).deep.equal('1234.... ');
        });
        it('should center pad to min', async () => {
            const schema = new StringSchema({
                min: 9,
                pad: { side: 'both', characters: '. ' }
            });
            ds.data = '1234';
            await validator(schema, ds);
            expect(ds.data).deep.equal('..1234   ');
        });
        it('should truncate', async () => {
            const schema = new StringSchema({
                max: 5,
                truncate: true
            });
            ds.data = '123456789';
            await validator(schema, ds);
            expect(ds.data).deep.equal('12345');
        });
        it('should truncate with ending', async () => {
            const schema = new StringSchema({
                max: 5,
                truncate: '...'
            });
            ds.data = '123456789';
            await validator(schema, ds);
            expect(ds.data).deep.equal('12...');
        });
        it('should fail on too long text', async () => {
            const schema = new StringSchema({
                max: 3
            });
            ds.data = '12345';
            await fail(schema, ds);
        });
        it('should fail on too short text', async () => {
            const schema = new StringSchema({
                min: 5
            });
            ds.data = '123';
            await fail(schema, ds);
        });
        it('should describe pad', () => {
            const schema = new StringSchema({
                min: 5,
                pad: { side: 'right', characters: ' ' }
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe truncate', () => {
            const schema = new StringSchema({
                max: 5,
                truncate: true
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe exact length', () => {
            const schema = new StringSchema({
                min: 5,
                max: 5
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe length range', () => {
            const schema = new StringSchema({
                min: 3,
                max: 5
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe min length', () => {
            const schema = new StringSchema({
                min: 5
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe max length', () => {
            const schema = new StringSchema({
                max: 5
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with option allow/disallow', () => {
        it('should succeed with allow string', async () => {
            const schema = new StringSchema({
                allow: ['banana', 'apple']
            });
            ds.data = 'banana';
            await validator(schema, ds);
            expect(ds.data).deep.equal('banana');
        });
        it('should fail with allow string', async () => {
            const schema = new StringSchema({
                allow: ['banana', 'apple']
            });
            ds.data = 'tomato';
            await fail(schema, ds);
        });
        it('should succeed with allow regexp', async () => {
            const schema = new StringSchema({
                allow: ['banana', 'apple', /berry$/]
            });
            ds.data = 'strawberry';
            await validator(schema, ds);
            expect(ds.data).deep.equal('strawberry');
        });
        it('should succeed with reference', async () => {
            const schema = new StringSchema({
                allow: new Reference('data:context')
            });
            ds.data = 'apple';
            let context = ['banana', 'apple'];
            await validator(schema, ds, context);
            expect(ds.data).deep.equal('apple');
        });
        it('should succeed with reference as element', async () => {
            const schema = new StringSchema({
                allow: ['tomato', new Reference('data:context')]
            });
            ds.data = 'apple';
            let context = ['banana', 'apple'];
            await validator(schema, ds, context);
            expect(ds.data).deep.equal('apple');
        });
        it('should succeed with disallow string', async () => {
            const schema = new StringSchema({
                disallow: ['banana', 'apple']
            });
            ds.data = 'tomato';
            await validator(schema, ds);
            expect(ds.data).deep.equal('tomato');
        });
        it('should fail with disallow string', async () => {
            const schema = new StringSchema({
                disallow: ['banana', 'apple']
            });
            ds.data = 'banana';
            await fail(schema, ds);
        });
        it('should fail with disallow regexp', async () => {
            const schema = new StringSchema({
                disallow: ['banana', 'apple', /berry$/]
            });
            ds.data = 'strawberry';
            await fail(schema, ds);
        });
        it('should fail with disallow of allowed', async () => {
            const schema = new StringSchema({
                allow: ['banana', 'apple', /berry$/],
                disallow: ['blueberry']
            });
            ds.data = 'blueberry';
            await fail(schema, ds);
        });
        it('should fail with reference', async () => {
            const schema = new StringSchema({
                disallow: new Reference('data:context')
            });
            ds.data = 'apple';
            let context = ['banana', 'apple'];
            await fail(schema, ds, context);
        });
        it('should fail with referenceas element', async () => {
            const schema = new StringSchema({
                disallow: ['tomato', new Reference('data:context')]
            });
            ds.data = 'apple';
            let context = ['banana', 'apple'];
            await fail(schema, ds, context);
        });
        it('should describe', () => {
            const schema = new StringSchema({
                allow: ['banana', 'apple', /berry$/, new Reference('data:context')],
                disallow: ['blueberry']
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe long list', () => {
            const schema = new StringSchema({
                allow: [
                    'a',
                    'b',
                    'c',
                    'd',
                    'e',
                    'f',
                    'g',
                    'h',
                    'i',
                    'j',
                    'k',
                    'l',
                    'm',
                    'n',
                    'o',
                    'p',
                    'q',
                    'r',
                    's',
                    't',
                    'u',
                    'v',
                    'w',
                    'x',
                    'y',
                    'z'
                ],
                disallow: ['blueberry']
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
});
