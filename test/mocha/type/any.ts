import { expect } from 'chai';
import DataStore from '@alinex/datastore';
import { validator, fail, describer } from './helper';

import AnySchema from '../../../src/type/any';
import Reference from '../../../src/reference';

describe('any', () => {
    let ds = new DataStore();
    describe('without definition', () => {
        const schema = new AnySchema({
            title: 'array'
        });
        it('should validate string', async () => {
            ds.data = 'example';
            await validator(schema, ds);
            expect(ds.data).deep.equal('example');
        });
        it('should validate array', async () => {
            ds.data = ['example'];
            await validator(schema, ds);
            expect(ds.data).deep.equal(['example']);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with default option', () => {
        it('should use integer', async () => {
            const schema = new AnySchema({
                title: 'array',
                default: 15
            });
            ds.data = null;
            await validator(schema, ds);
            expect(ds.data).deep.equal(15);
        });
        it('should describe', () => {
            const schema = new AnySchema({
                title: 'array',
                default: 15
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with raw option', () => {
        it('should describe', () => {
            const schema = new AnySchema({
                title: 'array',
                raw: true
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with option allow/disallow', () => {
        it('should succeed with allow string', async () => {
            const schema = new AnySchema({
                allow: ['banana', 'apple']
            });
            ds.data = 'banana';
            await validator(schema, ds);
            expect(ds.data).deep.equal('banana');
        });
        it('should fail with allow string', async () => {
            const schema = new AnySchema({
                allow: ['banana', 'apple']
            });
            ds.data = 'tomato';
            await fail(schema, ds);
        });
        it('should succeed with reference', async () => {
            const schema = new AnySchema({
                allow: new Reference('data:context')
            });
            ds.data = 'apple';
            let context = ['banana', 'apple'];
            await validator(schema, ds, context);
            expect(ds.data).deep.equal('apple');
        });
        it('should succeed with reference as element', async () => {
            const schema = new AnySchema({
                allow: ['tomato', new Reference('data:context')]
            });
            ds.data = 'apple';
            let context = ['banana', 'apple'];
            await validator(schema, ds, context);
            expect(ds.data).deep.equal('apple');
        });
        it('should succeed with disallow string', async () => {
            const schema = new AnySchema({
                disallow: ['banana', 'apple']
            });
            ds.data = 'tomato';
            await validator(schema, ds);
            expect(ds.data).deep.equal('tomato');
        });
        it('should fail with disallow string', async () => {
            const schema = new AnySchema({
                disallow: ['banana', 'apple']
            });
            ds.data = 'banana';
            await fail(schema, ds);
        });
        it('should fail with disallow of allowed', async () => {
            const schema = new AnySchema({
                allow: ['banana', 'apple', /berry$/],
                disallow: ['blueberry']
            });
            ds.data = 'blueberry';
            await fail(schema, ds);
        });
        it('should fail with reference', async () => {
            const schema = new AnySchema({
                disallow: new Reference('data:context')
            });
            ds.data = 'apple';
            let context = ['banana', 'apple'];
            await fail(schema, ds, context);
        });
        it('should fail with referenceas element', async () => {
            const schema = new AnySchema({
                disallow: ['tomato', new Reference('data:context')]
            });
            ds.data = 'apple';
            let context = ['banana', 'apple'];
            await fail(schema, ds, context);
        });
        it('should describe', () => {
            const schema = new AnySchema({
                allow: ['banana', 'apple', /berry$/, new Reference('data:context')],
                disallow: ['blueberry']
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe long list', () => {
            const schema = new AnySchema({
                allow: [
                    'a',
                    'b',
                    'c',
                    'd',
                    'e',
                    'f',
                    'g',
                    'h',
                    'i',
                    'j',
                    'k',
                    'l',
                    'm',
                    'n',
                    'o',
                    'p',
                    'q',
                    'r',
                    's',
                    't',
                    'u',
                    'v',
                    'w',
                    'x',
                    'y',
                    'z'
                ],
                disallow: ['blueberry']
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
});
