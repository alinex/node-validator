import Debug from 'debug';
import DataStore from '@alinex/datastore';
import Workbench from '../../../src/workbench';
import Schema from '../../../src/schema';

const debug = Debug('test');

export async function validator(schema: Schema, ds: DataStore, context?: any) {
    const ref = { 'data:context': new DataStore() };
    ref['data:context'].data = context;
    const work = new Workbench(ds, ref);
    try {
        debug(schema);
        debug(work);
        await schema.validate(work);
        debug('result %o', ds.data);
    } catch (err) {
        debug('error %o', err.message);
        throw err;
    }
}

export async function fail(schema: Schema, ds: DataStore, context?: any) {
    try {
        await validator(schema, ds, context);
    } catch (err) {
        return;
    }
    throw new Error('Validation should fail on this value.');
}

export function describer(schema: Schema): string {
    debug(schema);
    const msg = schema.describe();
    debug(msg);
    return msg;
}
