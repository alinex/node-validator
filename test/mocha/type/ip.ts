import { expect } from 'chai';
import DataStore from '@alinex/datastore';
import { validator, fail, describer } from './helper';

import IPSchema from '../../../src/type/ip';

describe('ip', () => {
    let ds = new DataStore();
    describe('without definition', () => {
        const schema = new IPSchema({
            title: 'ip'
        });
        it('should validate IPv4', async () => {
            ds.data = '192.168.1.15';
            await validator(schema, ds);
            expect(ds.data).deep.equal('192.168.1.15');
        });
        it('should validate IPv6', async () => {
            ds.data = 'FE80:0000:0000:0000:0202:B3FF:FE1E:8329';
            await validator(schema, ds);
            expect(ds.data).deep.equal('fe80::202:b3ff:fe1e:8329');
        });
        it('should validate IPv4', async () => {
            ds.data = ['192', '12', '1', '1'];
            await validator(schema, ds);
            expect(ds.data).deep.equal('192.12.1.1');
        });
        it('should fail on string', async () => {
            ds.data = 'no ip';
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with option lookup', () => {
        const schema = new IPSchema({
            lookup: true
        });
        it('should resolve', async () => {
            ds.data = 'localhost';
            await validator(schema, ds);
            expect(ds.data).deep.equal('127.0.0.1');
        });
        it('should fail on lookup', async () => {
            ds.data = 'no.valid.domain.name';
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with option version', () => {
        it('should allow IPv4', async () => {
            const schema = new IPSchema({
                version: 4
            });
            ds.data = '192.168.1.15';
            await validator(schema, ds);
            expect(ds.data).deep.equal('192.168.1.15');
        });
        it('should fail on IPv6', async () => {
            const schema = new IPSchema({
                version: 4
            });
            ds.data = 'FE80:0000:0000:0000:0202:B3FF:FE1E:8329';
            await fail(schema, ds);
        });
        it('should allow IPv6', async () => {
            const schema = new IPSchema({
                version: 6
            });
            ds.data = 'FE80:0000:0000:0000:0202:B3FF:FE1E:8329';
            await validator(schema, ds);
            expect(ds.data).deep.equal('fe80::202:b3ff:fe1e:8329');
        });
        it('should fail on IPv4', async () => {
            const schema = new IPSchema({
                version: 6
            });
            ds.data = '192.168.1.15';
            await fail(schema, ds);
        });
        it('should describe', () => {
            const schema = new IPSchema({
                version: 4
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with option map', () => {
        it('should map IPv4 to IPv6', async () => {
            const schema = new IPSchema({
                version: 6,
                map: true
            });
            ds.data = '127.0.0.1';
            await validator(schema, ds);
            expect(ds.data).deep.equal('::ffff:7f00:1');
        });
        it('should map IPv6 to IPv4', async () => {
            const schema = new IPSchema({
                version: 4,
                map: true
            });
            ds.data = '::ffff:7f00:1';
            await validator(schema, ds);
            expect(ds.data).deep.equal('127.0.0.1');
        });
        it('should fail if impossible', async () => {
            const schema = new IPSchema({
                version: 4,
                map: true
            });
            ds.data = 'fe80::202:b3ff:fe1e:8329';
            await fail(schema, ds);
        });
        it('should describe', () => {
            const schema = new IPSchema({
                version: 6,
                map: true
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with option allow/disallow', () => {
        it('should allow IP', async () => {
            const schema = new IPSchema({
                allow: ['127.0.0.1']
            });
            ds.data = '127.0.0.1';
            await validator(schema, ds);
            expect(ds.data).deep.equal('127.0.0.1');
            ds.data = '192.168.15.201';
            await fail(schema, ds);
        });
        it('should disallow IP', async () => {
            const schema = new IPSchema({
                disallow: ['127.0.0.1']
            });
            ds.data = '192.168.15.201';
            await validator(schema, ds);
            expect(ds.data).deep.equal('192.168.15.201');
            ds.data = '127.0.0.1';
            await fail(schema, ds);
        });
        it('should allow range', async () => {
            const schema = new IPSchema({
                allow: ['192.168.0.0/16']
            });
            ds.data = '192.168.15.201';
            await validator(schema, ds);
            expect(ds.data).deep.equal('192.168.15.201');
            ds.data = '192.169.15.201';
            await fail(schema, ds);
        });
        it('should allow special range', async () => {
            const schema = new IPSchema({
                allow: ['private']
            });
            ds.data = '192.168.15.201';
            await validator(schema, ds);
            expect(ds.data).deep.equal('192.168.15.201');
            ds.data = '218.92.16.2';
            await fail(schema, ds);
        });
        it('should disallow special range', async () => {
            const schema = new IPSchema({
                disallow: ['private']
            });
            ds.data = '218.92.16.2';
            await validator(schema, ds);
            expect(ds.data).deep.equal('218.92.16.2');
            ds.data = '192.168.15.201';
            await fail(schema, ds);
        });
        it('should work with deny range but allowed sub range', async () => {
            const schema = new IPSchema({
                disallow: ['private'],
                allow: ['192.168.0.0/16']
            });
            ds.data = '192.168.100.1';
            await validator(schema, ds);
            expect(ds.data).deep.equal('192.168.100.1');
        });
        it('should fail with allow range and denied sub range', async () => {
            const schema = new IPSchema({
                allow: ['private'],
                disallow: ['192.168.0.0/16']
            });
            ds.data = '192.168.100.1';
            await fail(schema, ds);
        });
        it('should describe', () => {
            const schema = new IPSchema({
                allow: ['private', '192.168.0.1'],
                disallow: ['192.168.0.0/16']
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with country options', () => {
        it('should allow germany', async () => {
            const schema = new IPSchema({
                allowCountry: ['DE']
            });
            ds.data = '92.211.90.122';
            await validator(schema, ds);
            expect(ds.data).deep.equal('92.211.90.122');
        });
        it('should allow private addresses at any time', async () => {
            const schema = new IPSchema({
                allowCountry: ['AT']
            });
            ds.data = '192.168.0.1';
            await validator(schema, ds);
            expect(ds.data).deep.equal('192.168.0.1');
        });
        it('should disallow germany', async () => {
            const schema = new IPSchema({
                disallowCountry: ['DE']
            });
            ds.data = '92.211.90.122';
            await fail(schema, ds);
        });
        it('should describe', () => {
            const schema = new IPSchema({
                allowCountry: ['DE'],
                disallowCountry: ['US']
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with option format', () => {
        it('should get IPv4 with short', async () => {
            const schema = new IPSchema({
                format: 'short'
            });
            ds.data = '127.0.0.1';
            await validator(schema, ds);
            expect(ds.data).deep.equal('127.0.0.1');
        });
        it('should get IPv4 with long', async () => {
            const schema = new IPSchema({
                format: 'long'
            });
            ds.data = '127.0.0.1';
            await validator(schema, ds);
            expect(ds.data).deep.equal('127.0.0.1');
        });
        it('should get IPv4 with array', async () => {
            const schema = new IPSchema({
                format: 'array'
            });
            ds.data = '127.0.0.1';
            await validator(schema, ds);
            expect(ds.data).deep.equal([127, 0, 0, 1]);
        });
        it('should get IPv4 with bytes', async () => {
            const schema = new IPSchema({
                format: 'bytes'
            });
            ds.data = '127.0.0.1';
            await validator(schema, ds);
            expect(ds.data).deep.equal([127, 0, 0, 1]);
        });
        it('should get IPv6 with short', async () => {
            const schema = new IPSchema({
                format: 'short'
            });
            ds.data = '::ffff:7f00:1';
            await validator(schema, ds);
            expect(ds.data).deep.equal('::ffff:7f00:1');
        });
        it('should get IPv6 with long', async () => {
            const schema = new IPSchema({
                format: 'long'
            });
            ds.data = '::ffff:7f00:1';
            await validator(schema, ds);
            expect(ds.data).deep.equal('0:0:0:0:0:ffff:7f00:1');
        });
        it('should get IPv6 with array', async () => {
            const schema = new IPSchema({
                format: 'array'
            });
            ds.data = '::ffff:7f00:1';
            await validator(schema, ds);
            expect(ds.data).deep.equal([0, 0, 0, 0, 0, 0xffff, 0x7f00, 1]);
        });
        it('should get IPv6 with bytes', async () => {
            const schema = new IPSchema({
                format: 'bytes'
            });
            ds.data = '::ffff:7f00:1';
            await validator(schema, ds);
            expect(ds.data).deep.equal([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0xff, 0xff, 0x7f, 0, 0, 1]);
        });
        it('should describe', () => {
            const schema = new IPSchema({
                format: 'short'
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
});
