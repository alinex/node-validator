import { expect } from 'chai';
import DataStore from '@alinex/datastore';

import Reference from '../../../src/reference';
import Workbench from '../../../src/workbench';

describe('reference', () => {
    const ds = new DataStore();
    const context = new DataStore();
    context.data = {
        cities: ['London', 'Berlin', 'Wien', 'Rom']
    };
    const work = new Workbench(ds, { 'data:context': context });
    describe('context', () => {
        it('should retrieve string array', async () => {
            const ref = new Reference('data:context', 'cities');
            const res = await ref.stringArray(work);
            expect(res).deep.equal(['London', 'Berlin', 'Wien', 'Rom']);
        });
    });
});
