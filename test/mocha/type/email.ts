import { expect } from 'chai';
import DataStore from '@alinex/datastore';

import { validator, fail, describer } from './helper';
import EmailSchema from '../../../src/type/email';

describe('email', () => {
    let ds = new DataStore();
    describe('without definition', () => {
        const schema = new EmailSchema({
            title: 'email'
        });
        it('should validate', async () => {
            ds.data = 'info@alinex.de';
            await validator(schema, ds);
            expect(ds.data).deep.equal('info@alinex.de');
        });
        it('should fail with number', async () => {
            ds.data = 5;
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with normalize option', () => {
        const schema = new EmailSchema({
            normalize: true
        });
        it('should validate normal', async () => {
            ds.data = 'info@alinex.de';
            await validator(schema, ds);
            expect(ds.data).deep.equal('info@alinex.de');
        });
        it('should validate gmail', async () => {
            ds.data = 'Alexander.REINER.Schilling+INFO@googlemail.com';
            await validator(schema, ds);
            expect(ds.data).deep.equal('alexanderreinerschilling@gmail.com');
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with registered option', () => {
        const schema = new EmailSchema({
            registered: true
        });
        it('should validate normal', async () => {
            ds.data = 'info@alinex.de';
            await validator(schema, ds);
            expect(ds.data).deep.equal('info@alinex.de');
        });
        it('should validate gmail', async () => {
            ds.data = 'alexander.reiner.schilling@googlemail.com';
            await validator(schema, ds);
            expect(ds.data).deep.equal('alexander.reiner.schilling@googlemail.com');
        });
        it('should fail on unregistered', async () => {
            ds.data = 'info@my-non-existent-domain.com';
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with blacklist option', function() {
        this.timeout(10000);
        const schema = new EmailSchema({
            blacklist: true
        });
        it('should validate normal', async () => {
            ds.data = 'info@alinex.de';
            await validator(schema, ds);
            expect(ds.data).deep.equal('info@alinex.de');
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with graylist option', function() {
        this.timeout(10000);
        const schema = new EmailSchema({
            graylist: true
        });
        it('should validate normal', async () => {
            ds.data = 'info@alinex.de';
            await validator(schema, ds);
            expect(ds.data).deep.equal('info@alinex.de');
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with connect option', function() {
        this.timeout(10000);
        const schema = new EmailSchema({
            connect: true
        });
        it('should validate normal', async () => {
            ds.data = 'info@alinex.de';
            await validator(schema, ds);
            expect(ds.data).deep.equal('info@alinex.de');
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
});
