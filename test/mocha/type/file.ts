import { expect } from 'chai';
import DataStore from '@alinex/datastore';

import { validator, fail, describer } from './helper';
import FileSchema from '../../../src/type/file';

describe('file', () => {
    let ds = new DataStore();
    describe('without definition', () => {
        const schema = new FileSchema({
            title: 'file'
        });
        it('should validate', async () => {
            ds.data = 'test/data/test.txt';
            await validator(schema, ds);
            expect(ds.data).deep.equal('test/data/test.txt');
        });
        it('should fail on invalid type', async () => {
            ds.data = 12;
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with baseDir', () => {
        const schema = new FileSchema({
            title: 'file',
            baseDir: 'test/data'
        });
        it('should resolve relative path', async () => {
            ds.data = 'test.txt';
            await validator(schema, ds);
            expect(ds.data).deep.equal('/home/alex/code/node-validator/test/data/test.txt');
        });
        it('should resolve homedir', async () => {
            ds.data = '~/.ssh/known_hosts';
            await validator(schema, ds);
            expect(ds.data).deep.equal(require('os').homedir() + '/.ssh/known_hosts');
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('without exists option', () => {
        const schema = new FileSchema({
            title: 'file',
            exists: true
        });
        it('should exist', async () => {
            ds.data = 'test/data/test.txt';
            await validator(schema, ds);
            expect(ds.data).deep.equal('test/data/test.txt');
        });
        it('should fail on not existing', async () => {
            ds.data = 'test/mocha.notfound';
            await fail(schema, ds);
        });
        it('should exist with homedir', async () => {
            ds.data = '~/.ssh/known_hosts';
            await validator(schema, ds);
            expect(ds.data).deep.equal('~/.ssh/known_hosts');
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with type option', () => {
        const schema = new FileSchema({
            title: 'file',
            type: 'file'
        });
        it('should exist', async () => {
            ds.data = 'test/data/test.txt';
            await validator(schema, ds);
            expect(ds.data).deep.equal('test/data/test.txt');
        });
        it('should fail on directory', async () => {
            ds.data = 'test/';
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with size option', () => {
        it('should work with minSize', async () => {
            const schema = new FileSchema({
                minSize: 10
            });
            ds.data = 'test/data/test.txt';
            await validator(schema, ds);
            expect(ds.data).deep.equal('test/data/test.txt');
        });
        it('should fail on minSize', async () => {
            const schema = new FileSchema({
                minSize: 100000
            });
            ds.data = 'test/data/test.txt';
            await fail(schema, ds);
        });
        it('should work with maxSize', async () => {
            const schema = new FileSchema({
                maxSize: 100000
            });
            ds.data = 'test/data/test.txt';
            await validator(schema, ds);
            expect(ds.data).deep.equal('test/data/test.txt');
        });
        it('should fail on maxSize', async () => {
            const schema = new FileSchema({
                maxSize: 10
            });
            ds.data = 'test/data/test.txt';
            await fail(schema, ds);
        });
        it('should describe', () => {
            const schema = new FileSchema({
                minSize: 10,
                maxSize: 1000
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with time option', () => {
        it('should work with minCTime', async () => {
            const schema = new FileSchema({
                minCTime: new Date('2018-01-01')
            });
            ds.data = 'test/data/test.txt';
            await validator(schema, ds);
            expect(ds.data).deep.equal('test/data/test.txt');
        });
        it('should fail on minCTime', async () => {
            const schema = new FileSchema({
                minCTime: 100
            });
            ds.data = 'test/data/test.txt';
            await fail(schema, ds);
        });
        it('should work with maxCTime', async () => {
            const schema = new FileSchema({
                maxCTime: 100
            });
            ds.data = 'test/data/test.txt';
            await validator(schema, ds);
            expect(ds.data).deep.equal('test/data/test.txt');
        });
        it('should fail on maxCTime', async () => {
            const schema = new FileSchema({
                maxCTime: new Date('2018-01-01')
            });
            ds.data = 'test/data/test.txt';
            await fail(schema, ds);
        });
        it('should work with minMTime', async () => {
            const schema = new FileSchema({
                minMTime: new Date('2018-01-01')
            });
            ds.data = 'test/data/test.txt';
            await validator(schema, ds);
            expect(ds.data).deep.equal('test/data/test.txt');
        });
        it('should fail on minMTime', async () => {
            const schema = new FileSchema({
                minMTime: 100
            });
            ds.data = 'test/data/test.txt';
            await fail(schema, ds);
        });
        it('should work with maxMTime', async () => {
            const schema = new FileSchema({
                maxMTime: 100
            });
            ds.data = 'test/data/test.txt';
            await validator(schema, ds);
            expect(ds.data).deep.equal('test/data/test.txt');
        });
        it('should fail on maxMTime', async () => {
            const schema = new FileSchema({
                maxMTime: new Date('2018-01-01')
            });
            ds.data = 'test/data/test.txt';
            await fail(schema, ds);
        });
        it('should work with minATime', async () => {
            const schema = new FileSchema({
                minATime: new Date('2018-01-01')
            });
            ds.data = 'test/data/test.txt';
            await validator(schema, ds);
            expect(ds.data).deep.equal('test/data/test.txt');
        });
        it('should fail on minATime', async () => {
            const schema = new FileSchema({
                minATime: -1
            });
            ds.data = 'test/data/test.txt';
            await fail(schema, ds);
        });
        it('should work with maxATime', async () => {
            const schema = new FileSchema({
                maxATime: 0
            });
            ds.data = 'test/data/test.txt';
            await validator(schema, ds);
            expect(ds.data).deep.equal('test/data/test.txt');
        });
        it('should fail on maxATime', async () => {
            const schema = new FileSchema({
                maxATime: new Date('2018-01-01')
            });
            ds.data = 'test/data/test.txt';
            await fail(schema, ds);
        });
        it('should describe with seconds', () => {
            const schema = new FileSchema({
                minCTime: 10,
                maxCTime: 1000,
                minMTime: 10,
                maxMTime: 1000,
                minATime: 10,
                maxATime: 1000
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe with date', () => {
            const schema = new FileSchema({
                minCTime: new Date('2018-01-01'),
                maxCTime: new Date('2018-01-01'),
                minMTime: new Date('2018-01-01'),
                maxMTime: new Date('2018-01-01'),
                minATime: new Date('2018-01-01'),
                maxATime: new Date('2018-01-01')
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
});
