import { expect } from 'chai';
import DataStore from '@alinex/datastore';
import { validator, fail, describer } from './helper';

import DateTimeSchema from '../../../src/type/datetime';

describe('datetime', () => {
    let ds = new DataStore();
    describe('without definition', () => {
        const schema = new DateTimeSchema({
            title: 'datetime'
        });
        it('should succeed with date', async () => {
            ds.data = new Date('2013-02-08T10:00:00.000Z');
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2013-02-08T10:00:00.000Z'));
        });
        it('should fail on text', async () => {
            ds.data = '2013-02-08T10:00:00.000Z';
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with parse option', () => {
        const schema = new DateTimeSchema({
            parse: true,
            defaultTimezone: 'Z'
        });
        it('should parse date string', async () => {
            ds.data = '2013-02-08T12:00:00.000Z';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2013-02-08T12:00:00.000Z'));
        });
        it('should parse within text', async () => {
            ds.data = 'It has to be after 2013-02-08T12:00:00.000Z';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2013-02-08T12:00:00.000Z'));
            ds.data = 'Give me a call at 2013-02-08T12:00:00.000Z but not later!';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2013-02-08T12:00:00.000Z'));
        });
        it('should parse far ahead/behind dates', async () => {
            ds.data = '9999-12-31T23:59:00';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('9999-12-31T23:59:00.000Z'));
            ds.data = '1900-01-01T00:00:00-01:00';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('1900-01-01T01:00:00.000Z'));
            ds.data = '0001-01-01T00:00:00-01:00';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('0001-01-01T01:06:32.000Z'));
        });

        it('should parse ISO 8601 date + time', async () => {
            ds.data = '2013-02-08';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2013-02-08T12:00:00.000Z'));
            ds.data = '2013-02-08T11:30';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2013-02-08T11:30:00.000Z'));
            ds.data = '2013-02-08T11:30:05';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2013-02-08T11:30:05.000Z'));
            ds.data = '2013-02-08T11:30:05.222';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2013-02-08T11:30:05.222Z'));
            ds.data = '2013-02-08T11:30Z';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2013-02-08T11:30:00.000Z'));
            ds.data = '2013-02-08T11:30+01';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2013-02-08T10:30:00.000Z'));
            ds.data = '2013-02-08T11:30+01:30';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2013-02-08T10:00:00.000Z'));
            ds.data = '2013-02-08T11:30+0130';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2013-02-08T10:00:00.000Z'));
        });
        it('should parse IETF', async () => {
            ds.data = '2013-02-08';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2013-02-08T12:00:00.000Z'));
            ds.data = '2013-02-08 12:00';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2013-02-08T12:00:00.000Z'));
            ds.data = '2013-02-08 12:00:20';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2013-02-08T12:00:20.000Z'));
            ds.data = '2013-02-08 12:00:20.222';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2013-02-08T12:00:20.222Z'));
            ds.data = '2013-02-08 12:00 GMT';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2013-02-08T12:00:00.000Z'));
            ds.data = '2013-02-08 12:00 CEST';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2013-02-08T10:00:00.000Z'));
            ds.data = '2013-02-08 12:00 +02:00';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2013-02-08T10:00:00.000Z'));
            ds.data = '2013-02-08 12:00 +0200';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2013-02-08T10:00:00.000Z'));
        });
        it('should parse slash dates', async () => {
            ds.data = '09/2017';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2017-09-01T12:00:00.000Z'));
            ds.data = '9/2017';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2017-09-01T12:00:00.000Z'));
            ds.data = '08/09/2017';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2017-08-09T12:00:00.000Z'));
            ds.data = '8/9/2017';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2017-08-09T12:00:00.000Z'));
            ds.data = '08/09/17';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2017-08-09T12:00:00.000Z'));
            ds.data = '8/9/17';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2017-08-09T12:00:00.000Z'));
            ds.data = '18/9/2017';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2017-09-18T12:00:00.000Z'));
            ds.data = '2012/08/10';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2012-08-10T12:00:00.000Z'));
            ds.data = '2012/8/10';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2012-08-10T12:00:00.000Z'));
        });
        it('should parse day and month names', async () => {
            ds.data = '2012/Aug/10';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2012-08-10T12:00:00.000Z'));
            ds.data = 'July 2017';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2017-07-01T12:00:00.000Z'));
            ds.data = 'August 10, 2012';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2012-08-10T12:00:00.000Z'));
            ds.data = 'August-10, 2012';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2012-08-10T12:00:00.000Z'));
            ds.data = 'August, 2012';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2012-08-01T12:00:00.000Z'));
            ds.data = 'May eighth, 2010';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2010-05-08T12:00:00.000Z'));
            ds.data = 'Sat, 21 Feb 2015 11:50:48 -0500';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2015-02-21T16:50:48.000Z'));
            ds.data = 'August 10, 345 BC';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('-000345-08-10T12:06:32.000Z'));
            ds.data = 'August 10, 8 AD';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('0008-08-10T12:06:32.000Z'));
        });
        it('should parse english datetime', async () => {
            ds.data = '09/25/2017 10:31:50.522 PM';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2017-09-25T22:31:50.522Z'));
        });
        it('should parse german datetime', async () => {
            ds.data = 'Freitag 30.12.16';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2016-12-30T12:00:00.000Z'));
        });
        it('should fail on text', async () => {
            ds.data = 'no date';
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with locale en_GB', () => {
        const schema = new DateTimeSchema({
            parse: 'en_GB',
            defaultTimezone: 'Z'
        });
        it('should parse slash dates', async () => {
            ds.data = '08/09/2017';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2017-09-08T12:00:00.000Z'));
            ds.data = '8/9/2017';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2017-09-08T12:00:00.000Z'));
        });
    });
    describe('with reference option', () => {
        const schema = new DateTimeSchema({
            parse: true,
            defaultTimezone: 'Z',
            reference: new Date('2018-12-24T16:00:00.000Z')
        });
        it('should get time', async () => {
            ds.data = '8pm';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-12-24T20:00:00.000Z'));
            ds.data = '11 at night';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-12-24T23:00:00.000Z'));
        });
        it('should get named dates', async () => {
            ds.data = 'today';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-12-24T12:00:00.000Z'));
            ds.data = 'tomorrow';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-12-25T12:00:00.000Z'));
            ds.data = 'yesterday';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-12-23T12:00:00.000Z'));
            ds.data = 'last day';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-12-23T12:00:00.000Z'));
            ds.data = 'last 2 days';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-12-22T12:00:00.000Z'));
            ds.data = 'past week';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-12-17T12:00:00.000Z'));
            ds.data = 'next four weeks';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2019-01-21T12:00:00.000Z'));
            ds.data = 'today 5PM';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-12-24T17:00:00.000Z'));
            ds.data = 'this friday';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-12-28T12:00:00.000Z'));
            ds.data = 'next friday';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2019-01-04T12:00:00.000Z'));
            ds.data = 'last friday';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-12-21T12:00:00.000Z'));
            ds.data = 'August';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-08-01T12:00:00.000Z'));
            ds.data = 'August 10';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-08-10T12:00:00.000Z'));
            ds.data = 'Aug 10';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-08-10T12:00:00.000Z'));
            ds.data = 'May twenty-fourth';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2019-05-24T12:00:00.000Z'));
        });
        it('should get named time', async () => {
            ds.data = 'now';
            await validator(schema, ds);
            let refdate = new Date('2018-12-24T16:00:00.000Z');
            expect(ds.data).deep.equal(
                new Date(refdate.getTime() - refdate.getTimezoneOffset() * 60000)
            );
        });
        it('should get named datetime', async () => {
            ds.data = 'this morning';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-12-24T06:00:00.000Z'));
            ds.data = 'this afternoon';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-12-24T15:00:00.000Z'));
            ds.data = 'this evening';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-12-24T20:00:00.000Z'));
            ds.data = 'last night';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-12-23T00:00:00.000Z'));
            ds.data = 'tonight';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-12-24T22:00:00.000Z'));
            ds.data = 'tonight 8pm';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-12-24T20:00:00.000Z'));
            ds.data = 'tonight at 8';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-12-24T20:00:00.000Z'));
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('parse with locale de', () => {
        const schema = new DateTimeSchema({
            parse: 'de',
            defaultTimezone: 'Z',
            reference: new Date('2018-12-24T16:00:00.000Z')
        });
        it('should get named time', async () => {
            ds.data = 'jetzt';
            await validator(schema, ds);
            let refdate = new Date('2018-12-24T16:00:00.000Z');
            expect(ds.data).deep.equal(
                new Date(refdate.getTime() - refdate.getTimezoneOffset() * 60000)
            );
            ds.data = 'heute';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-12-24T12:00:00.000Z'));
            ds.data = 'morgen';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-12-25T12:00:00.000Z'));
            ds.data = 'gestern';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-12-23T12:00:00.000Z'));
            ds.data = 'letzte Nacht';
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-12-23T00:00:00.000Z'));
        });
    });
    describe('with range options', () => {
        it('should support min option', async () => {
            const schema = new DateTimeSchema({
                min: new Date('2018-12-01T00:00:00.000Z')
            });
            ds.data = new Date('2018-12-01T00:00:00.000Z');
            await validator(schema, ds);
            ds.data = new Date('2018-12-24T00:00:00.000Z');
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-12-24T00:00:00.000Z'));
        });
        it('should fail on min option', async () => {
            const schema = new DateTimeSchema({
                min: new Date('2018-12-01T00:00:00.000Z')
            });
            ds.data = new Date('2018-01-01T00:00:00.000Z');
            await fail(schema, ds);
        });
        it('should support max option', async () => {
            const schema = new DateTimeSchema({
                max: new Date('2018-12-01T00:00:00.000Z')
            });
            ds.data = new Date('2018-12-01T00:00:00.000Z');
            await validator(schema, ds);
            ds.data = new Date('2018-01-01T00:00:00.000Z');
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-01-01T00:00:00.000Z'));
        });
        it('should fail on max option', async () => {
            const schema = new DateTimeSchema({
                max: new Date('2018-12-01T00:00:00.000Z')
            });
            ds.data = new Date('2018-12-24T00:00:00.000Z');
            await fail(schema, ds);
        });
        it('should support greater option', async () => {
            const schema = new DateTimeSchema({
                greater: new Date('2018-12-01T00:00:00.000Z')
            });
            ds.data = new Date('2018-12-01T00:00:00.001Z');
            await validator(schema, ds);
            ds.data = new Date('2018-12-24T00:00:00.000Z');
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-12-24T00:00:00.000Z'));
        });
        it('should fail on greater option', async () => {
            const schema = new DateTimeSchema({
                greater: new Date('2018-12-01T00:00:00.000Z')
            });
            ds.data = new Date('2018-12-01T00:00:00.000Z');
            await fail(schema, ds);
        });
        it('should support less option', async () => {
            const schema = new DateTimeSchema({
                less: new Date('2018-12-01T00:00:00.000Z')
            });
            ds.data = new Date('2018-11-30T00:00:00.000Z');
            await validator(schema, ds);
            expect(ds.data).deep.equal(new Date('2018-11-30T00:00:00.000Z'));
        });
        it('should fail on less option', async () => {
            const schema = new DateTimeSchema({
                less: new Date('2018-12-01T00:00:00.000Z')
            });
            ds.data = new Date('2019-01-01T00:00:00.000Z');
            await fail(schema, ds);
        });
        it('should describe min option', () => {
            const schema = new DateTimeSchema({
                min: new Date('2018-12-01T00:00:00.000Z')
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe max option', () => {
            const schema = new DateTimeSchema({
                max: new Date('2018-12-01T00:00:00.000Z')
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe with min and max', () => {
            const schema = new DateTimeSchema({
                min: new Date('2018-12-01T00:00:00.000Z'),
                max: new Date('2018-12-31T00:00:00.000Z')
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe greater option', () => {
            const schema = new DateTimeSchema({
                greater: new Date('2018-12-01T00:00:00.000Z')
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe less option', () => {
            const schema = new DateTimeSchema({
                less: new Date('2018-12-01T00:00:00.000Z')
            });
            expect(describer(schema)).to.be.a('string');
        });
        it('should describe with greater and less', () => {
            const schema = new DateTimeSchema({
                greater: new Date('2018-12-01T00:00:00.000Z'),
                less: new Date('2019-01-01T00:00:00.000Z')
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with format option', () => {
        it('should format ISO datetime', async () => {
            const schema = new DateTimeSchema({
                format: 'YYYY-MM-DD HH:mm:ss'
            });
            ds.data = new Date('2018-12-24T16:00:00.000');
            await validator(schema, ds);
            expect(ds.data).deep.equal('2018-12-24 16:00:00');
        });
        it('should format human datetime', async () => {
            const schema = new DateTimeSchema({
                format: 'LLL'
            });
            ds.data = new Date('2018-12-24T16:00:00.000');
            await validator(schema, ds);
            expect(ds.data).deep.equal('December 24, 2018 4:00 PM');
        });
        it('should format german datetime', async () => {
            const schema = new DateTimeSchema({
                format: 'LLL',
                locale: 'de'
            });
            ds.data = new Date('2018-12-24T16:00:00.000');
            await validator(schema, ds);
            expect(ds.data).deep.equal('24. Dezember 2018 16:00');
        });
        it('should get seconds', async () => {
            const schema = new DateTimeSchema({
                format: 'seconds'
            });
            ds.data = new Date('2018-12-24T16:00:00.000');
            await validator(schema, ds);
            expect(ds.data).to.be.greaterThan(1000000000);
        });
        it('should get milliseconds', async () => {
            const schema = new DateTimeSchema({
                format: 'milliseconds'
            });
            ds.data = new Date('2018-12-24T16:00:00.000');
            await validator(schema, ds);
            expect(ds.data).to.be.greaterThan(1000000000000);
        });
        it('should format relative', async () => {
            const schema = new DateTimeSchema({
                format: 'relative'
            });
            ds.data = new Date('2018-12-24T16:00:00.000');
            await validator(schema, ds);
            expect(ds.data).to.contain('ago');
        });
        it('should get range from now', async () => {
            const schema = new DateTimeSchema({
                parse: true,
                format: 'range'
            });
            ds.data = '2 minutes';
            await validator(schema, ds);
            expect(ds.data).to.be.equal(120);
        });
        it('should describe less option', () => {
            const schema = new DateTimeSchema({
                format: 'YYYY-MM-DD HH:mm:ss',
                timezone: 'Europe/Berlin',
                locale: 'de'
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
});
