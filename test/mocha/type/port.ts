import { expect } from 'chai';
import DataStore from '@alinex/datastore';
import { validator, fail, describer } from './helper';

import PortSchema from '../../../src/type/port';

describe('port', () => {
    let ds = new DataStore();
    describe('without definition', () => {
        const schema = new PortSchema({
            title: 'port'
        });
        it('should validate number', async () => {
            ds.data = 5;
            await validator(schema, ds);
            expect(ds.data).deep.equal(5);
        });
        it('should allow names', async () => {
            ds.data = 'ftp';
            await validator(schema, ds);
            expect(ds.data).deep.equal(21);
        });
        it('should fail on negative number', async () => {
            ds.data = -80;
            await fail(schema, ds);
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with allow ranges', () => {
        it('should allow', async () => {
            const schema = new PortSchema({
                allow: ['registered']
            });
            ds.data = 80;
            await fail(schema, ds);
            ds.data = 8080;
            await validator(schema, ds);
            expect(ds.data).deep.equal(8080);
        });
        it('should disallow', async () => {
            const schema = new PortSchema({
                disallow: ['system']
            });
            ds.data = 80;
            await fail(schema, ds);
            ds.data = 8080;
            await validator(schema, ds);
            expect(ds.data).deep.equal(8080);
        });
        it('should describe', () => {
            const schema = new PortSchema({
                allow: ['registered'],
                disallow: ['system']
            });
            expect(describer(schema)).to.be.a('string');
        });
    });
    describe('with makeString option', () => {
        const schema = new PortSchema({
            makeString: true
        });
        it('should format string', async () => {
            ds.data = 21;
            await validator(schema, ds);
            expect(ds.data).deep.equal('ftp');
        });
        it('should describe', () => {
            expect(describer(schema)).to.be.a('string');
        });
    });
});
