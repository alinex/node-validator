import { expect } from 'chai';
import DataStore from '@alinex/datastore';

import schema from '../../src/definition';
// import { inspect } from 'util';
import { validator } from './type/helper';

describe('definition', () => {
  it('should describe schema', async () => {
    expect(schema.describe()).to.be.a('string');
    //        console.log(inspect(schema), { depth: 2 });
    //        console.log(schema.describe(2));
  });
  it('any', async () => {
    let ds = new DataStore();
    ds.data = { SCHEMA_TYPE: "Any", allow: [true] }
    await validator(schema, ds);
  });
});
